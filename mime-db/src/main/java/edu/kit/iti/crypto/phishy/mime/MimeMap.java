package edu.kit.iti.crypto.phishy.mime;

import javax.json.*;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Resolve Mime Types
 * <p>
 * This is based on https://github.com/jshttp/mime-db
 * Use mime-db.json files from there
 */
public final class MimeMap {

    private final Logger logger;
    // Map ext -> mime
    private Map<String, String> mimeMap = null;

    private static final MimeMap instance = new MimeMap();

    private MimeMap() {
        this.logger = Logger.getLogger(this.getClass().getName());
        this.loadMimeMap();
    }

    public static MimeMap getInstance() {
        return instance;
    }

    private InputStream openMimeMap() {

        return this.getClass().getResourceAsStream("mime-db.json");

    }

    private void loadMimeMap() {

        JsonReader reader = Json.createReader(openMimeMap());
        JsonObject mimeArray = reader.readObject();

        this.mimeMap = new HashMap<>();

        for (String key : mimeArray.keySet()) {

            JsonObject mimeInfo = mimeArray.getJsonObject(key);
            if (mimeInfo.containsKey("extensions")) {
                JsonArray extensionArray = mimeInfo.getJsonArray("extensions");
                for (JsonValue extensionObject : extensionArray) {
                    if (extensionObject.getValueType() == JsonValue.ValueType.STRING) {
                        String ext = ((JsonString) extensionObject).getString();
                        this.mimeMap.put(ext, key);
                    }
                }
            }

        }

    }

    public String getMime(String extension) {

        if (extension == null) {
            throw new IllegalArgumentException("Extension must not be null");
        }

        return this.mimeMap.getOrDefault(extension, "application/octet-stream");

    }


}
