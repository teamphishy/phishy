package edu.kit.iti.crypto.phishy.mime;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MimeMapTest {

    @Test
    public void testTextFile() {
        assertEquals("text/plain", MimeMap.getInstance().getMime("txt"));

    }

    @Test
    public void testHtmlFile() {
        assertEquals("text/html", MimeMap.getInstance().getMime("html"));

    }

    @Test
    public void testMailFile() {
        assertEquals("message/rfc822", MimeMap.getInstance().getMime("eml"));

    }

    @Test(expected = IllegalArgumentException.class)
    public void testNull() {
        MimeMap.getInstance().getMime(null);

    }

}