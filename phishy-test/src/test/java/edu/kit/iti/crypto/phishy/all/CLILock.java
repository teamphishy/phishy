package edu.kit.iti.crypto.phishy.all;

public class CLILock {

    private static CLILock lock = null;
    public static boolean ready = true;

    private CLILock() {

    }

    public static CLILock getInstance() {
        if (lock == null) {
            lock = new CLILock();
        }

        return lock;
    }
}
