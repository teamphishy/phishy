package edu.kit.iti.crypto.phishy.all;

import ch.vorburger.exec.ManagedProcessException;
import ch.vorburger.mariadb4j.DB;
import ch.vorburger.mariadb4j.DBConfiguration;
import ch.vorburger.mariadb4j.DBConfigurationBuilder;
import com.icegreen.greenmail.user.GreenMailUser;
import com.icegreen.greenmail.util.DummySSLSocketFactory;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import edu.kit.iti.crypto.phishy.cli.CLIMain;
import edu.kit.iti.crypto.phishy.core.ModalLock;
import edu.kit.iti.crypto.phishy.core.ModalLock.Lease;
import edu.kit.iti.crypto.phishy.core.ModalLock.ResourceLockedException;
import edu.kit.iti.crypto.phishy.data.ManagerFactory;
import hthurow.tomcatjndi.TomcatJNDI;
import org.apache.commons.io.FileUtils;
import org.apache.directory.server.annotations.CreateLdapServer;
import org.apache.directory.server.annotations.CreateTransport;
import org.apache.directory.server.core.annotations.ApplyLdifFiles;
import org.apache.directory.server.core.annotations.CreateDS;
import org.apache.directory.server.core.annotations.CreatePartition;
import org.apache.directory.server.core.integ.AbstractLdapTestUnit;
import org.apache.directory.server.core.integ.FrameworkRunner;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockserver.client.MockServerClient;
import org.mockserver.integration.ClientAndServer;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.security.Permission;
import java.security.Security;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockserver.model.HttpClassCallback.callback;
import static org.mockserver.model.HttpRequest.request;

@RunWith(FrameworkRunner.class)
@CreateDS(name = "myDS",
        partitions = {
                @CreatePartition(name = "test", suffix = "dc=test,dc=org")
        })
@CreateLdapServer(transports = {@CreateTransport(protocol = "LDAP", address = "localhost", port = 10389)})
@ApplyLdifFiles({"recipients.ldif"})
public class EntireProjectTest extends AbstractLdapTestUnit {


    private static final int CAMPAIGN_COUNT = 10;

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    private final TomcatJNDI tomcatJNDI = new TomcatJNDI();
    private MockServerClient server;
    private ServerSetup setupSmtp;
    private GreenMail greenMailSmtp;
    private ServerSetup setupImap;
    private GreenMail greenMailImap;
    private DB db;
    private int configs;
    private CLILock lock = CLILock.getInstance();


    @Before
    public void setUp() throws Exception {
        System.setSecurityManager(new NoExitSecurityManager());
        ManagerFactory.reset();

        System.setOut(new PrintStream(outContent));

        setupAPIServer();
        setupMailServer();
    }

    private void setupAPIServer() throws URISyntaxException, IOException {
        server = ClientAndServer.startClientAndServer(8888);
        server.when(request()).respond(callback().withCallbackClass(HttpResponseCallback.class));
    }

    private void setupMailServer() {
        Security.setProperty("ssl.SocketFactory.provider", DummySSLSocketFactory.class.getName());
        setupSmtp = new ServerSetup(48621, "localhost", "smtps");
        greenMailSmtp = new GreenMail(setupSmtp);
        greenMailSmtp.setUser("user1@mail.com", "admin", "password");
        greenMailSmtp.start();

        setupImap = new ServerSetup(48661, "localhost", "imaps");
        greenMailImap = new GreenMail(setupImap);
        greenMailImap.setUser("user1@mail.com", "admin", "password");
        greenMailImap.start();
    }

    private void resetSqlDatabase() throws IOException {
        try (InputStream is = new FileInputStream("./src/test/resources/db.sqlite");
             OutputStream os = new FileOutputStream("./src/test/resources/dbtest.sqlite")) {

            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        }
    }

    @After
    public void tearDown() throws Exception {
        System.setSecurityManager(null);
        System.setOut(originalOut);
        server.stop();
        tomcatJNDI.tearDown();
        greenMailSmtp.stop();
        greenMailImap.stop();

        File privateKey = new File("../private.key");
        privateKey.delete();
        File publicKey = new File("../public.key.pub");
        publicKey.delete();

        if (configs == 1) {
            resetSqlDatabase();

        } else if (configs == 2) {
            FileUtils.deleteDirectory(new File("./src/test/resources/phishy_git_clone/phishy"));
            FileUtils.deleteDirectory(new File("./src/test/resources/phishy_git/group"));

            db.stop();

        }
    }


    @Test
    public void testProjectWithFirstConfigs()
            throws InterruptedException, IOException, MessagingException, URISyntaxException {
        configs = 1;

        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("contextFirst.xml").toURI()).toFile());
        tomcatJNDI.start();

        testCampaign(1100, "csv");
        testGroup();

        testMail(10000, 9000, 1000);
    }


    @Test
    public void testProjectWithSecondConfigs()
            throws InterruptedException, IOException, MessagingException, URISyntaxException, ManagedProcessException,
            SQLException, InvalidRemoteException, TransportException, GitAPIException {
        configs = 2;

        DBConfiguration config = DBConfigurationBuilder.newBuilder().setPort(3307).addArg("--user=root").build();
        db = DB.newEmbeddedDB(config);
        db.start();
        db.createDB("phishy");
        db.source("edu/kit/iti/crypto/phishy/all/mariadb.sql", "root", "", "phishy");

        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("contextSecond.xml").toURI()).toFile());
        tomcatJNDI.start();

        testCampaign(180, "ldap");
        command(0, null, "template", "group", "create", "group", "-h", "http://localhost:8888", "-v");
        testGroup();
        addGitFiles();

        testMail(1800, 1620, 180);

    }

    private void testCampaign(int mailSize, String addressbookType) throws InterruptedException {
        for (int i = 0; i < CAMPAIGN_COUNT; i++) {
            command(0, null, "campaign", "create", "testCampaign" + i, "-h", "http://localhost:8888");
        }
        command(-1, null, "campaign", "create", "testCampaign0", "-h", "http://localhost:8888");
        command(0, null, "campaign", "create", "testCampaign10", "-h", "http://localhost:8888");

        command(0, null, "campaign", "link", "testCampaign10", "phishMail", "group-phishMail", "-h", "http://localhost:8888");
        command(0, null, "campaign", "print", "testCampaign10", "-h", "http://localhost:8888");
        command(0, null, "campaign", "unlink", "testCampaign10", "phishMail", "-h", "http://localhost:8888");
        command(0, null, "campaign", "print", "testCampaign10", "-h", "http://localhost:8888");
        command(0, null, "campaign", "remove", "testCampaign10", "--yes", "-h", "http://localhost:8888");
        command(-1, null, "campaign", "remove", "testCampaign10", "--yes", "-h", "http://localhost:8888");


        for (int i = 0; i < CAMPAIGN_COUNT; i++) {
            command(0, null, "campaign", "update", "testCampaign" + i, "-a", addressbookType,
                    "--mailLifetime", "10",
                    "--recipientSetSize", "" + mailSize,
                    "--mailInterval", "1",
                    "--phishMail", "group-phishMail",
                    "--phishWebsite", "group-phishWebsite",
                    "--infoMail", "group-infoMail",
                    "--infoWebsite", "group-infoWebsite",
                    "-h", "http://localhost:8888");
        }

        command(0, null, "campaign", "resume", "testCampaign0", "-h", "http://localhost:8888");
        command(0, null, "campaign", "pause", "testCampaign0", "-h", "http://localhost:8888");
        for (int i = 0; i < CAMPAIGN_COUNT; i++) {
            command(0, null, "campaign", "resume", "testCampaign" + i, "-h", "http://localhost:8888");
        }

        command(0, null, "campaign", "list", "-h", "http://localhost:8888");
    }

    private void testGroup() throws InterruptedException {
        command(0, null, "template", "group", "create", "template1", "-h", "http://localhost:8888");
        command(0, null, "template", "group", "list", "template1", "-h", "http://localhost:8888");
        command(0, null, "template", "group", "remove", "template1", "--yes", "-h", "http://localhost:8888");
        command(-1, null, "template", "group", "remove", "template1", "--yes", "-h", "http://localhost:8888");
    }

    private void testMail(int expectedSend, int expectedEvaluate, int expectedPhished) throws InterruptedException, IOException, MessagingException {
        command(0, null, "template", "list", "-h", "http://localhost:8888");

        command(0, null, "addressbook", "list", "-h", "http://localhost:8888");

        command(0, null, "addressbook", "update", "-h", "http://localhost:8888");
        waitForThreads();

        command(0, null, "send", "--yes", "-h", "http://localhost:8888");
        waitForThreads();

        Message[] messages = greenMailSmtp.getReceivedMessages();
        assertEquals(expectedSend, messages.length);

        simulatePhishing(messages);

        command(0, null, "evaluate", "--yes", "-h", "http://localhost:8888");
        waitForThreads();

        messages = greenMailSmtp.getReceivedMessages();
        assertEquals(expectedSend + expectedEvaluate, messages.length);

        command(0, "There were " + expectedSend + " mails sent, "
                        + expectedPhished + " of them successfully phished their recipients and "
                        + expectedEvaluate + " mails were reported.",
                "statistics", "-h", "http://localhost:8888");
    }

    private void simulatePhishing(Message[] messages) throws IOException, MessagingException {
        GreenMailUser user = greenMailImap.setUser("user1@mail.com", "admin", "password");

        MockWebsiteServlet phishyWebsite = new MockWebsiteServlet();
        for (int i = 0; i < messages.length / 10; i++) {
            String content = messages[i].getContent().toString();
            String id = content.substring(content.indexOf("id>") + 3, content.indexOf("<id"));
            HttpSession session = new MockHttpSession();
            Map<String, String[]> parameters = new HashMap<String, String[]>();
            parameters.put("name", new String[]{"uid=test" + i + ",ou=Recipient,dc=test,dc=org"});
            parameters.put("password", new String[]{"testpw" + i});
            MockHttpServletRequest phishyReq = new MockHttpServletRequest("phishy/api/test/login/index.html", id,
                    session, parameters);
            MockHttpServletResponse phishyResp = new MockHttpServletResponse(new StringWriter());
            phishyWebsite.doGet(phishyReq, phishyResp);
            phishyReq = new MockHttpServletRequest("phishy/api/test/login", id, phishyReq.getSession(), parameters);
            phishyResp = new MockHttpServletResponse(new StringWriter());
            phishyWebsite.doPost(phishyReq, phishyResp);
            MockWebsiteServlet infoWebsite = new MockWebsiteServlet();
            String[] pathParts = phishyResp.getHeader("Location").replace("?", "").split("/");
            MockHttpServletRequest reqInfo = new MockHttpServletRequest("phishy/api/test/" + pathParts[1], pathParts[2],
                    session, parameters);
            infoWebsite.doGet(reqInfo, new MockHttpServletResponse(new StringWriter()));
        }

        for (int i = messages.length / 20; i < messages.length; i++) {
            user.deliver((MimeMessage) messages[i]);
        }

        for (int i = messages.length / 10 * 9; i < messages.length; i++) {
            user.deliver((MimeMessage) messages[i]);
        }
    }

    private void addGitFiles() throws IOException,
            GitAPIException {
        File clone = new File("./src/test/resources/phishy_git_clone/phishy");
        clone.mkdir();
        Git.cloneRepository().setDirectory(clone)
                .setURI(Paths.get("./src/test/resources/phishy_git/group").toAbsolutePath().toString()).call();
        FileUtils.copyDirectory(new File("./src/test/resources/phishy_git_clone/content"), clone);

        Git workingRepo = Git.open(clone);
        workingRepo.add().addFilepattern(".").call();
        workingRepo.commit().setMessage("add group files").call();
        workingRepo.push().call();
    }

    private void waitForThreads() {
        synchronized (lock) {
            try (Lease lease = ModalLock.getInstance().getLease()) {
            } catch (ResourceLockedException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            CLILock.ready = true;
            lock.notify();
        }
    }

    private void command(int exitCode, String expected, String... commandArgs) throws InterruptedException {

        try {
            CLILock.ready = false;
            CLIMain.main(commandArgs);
            synchronized (lock) {
                while (!CLILock.ready) {
                    lock.wait();
                }
            }
        } catch (ExitException e) {
            assertEquals(exitCode, e.status);
            String content = outContent.toString();
            outContent.reset();
            originalOut.print(content);
            if (expected != null) {
                assertTrue(content.contains(expected));
            }

        }
    }

    protected static class ExitException extends SecurityException {
        /**
         *
         */
        private static final long serialVersionUID = 1L;
        public final int status;

        public ExitException(int status) {
            super("Exit expected");
            this.status = status;
        }
    }

    private static class NoExitSecurityManager extends SecurityManager {
        @Override
        public void checkPermission(Permission perm) {
            // allow anything.
        }

        @Override
        public void checkPermission(Permission perm, Object context) {
            // allow anything.
        }

        @Override
        public void checkExit(int status) {
            super.checkExit(status);
            throw new ExitException(status);
        }
    }
}
