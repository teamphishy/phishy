package edu.kit.iti.crypto.phishy.all;

import edu.kit.iti.crypto.phishy.data.template.GitTemplateManager;

import javax.naming.Context;
import javax.naming.Name;
import javax.naming.RefAddr;
import javax.naming.Reference;
import javax.naming.spi.ObjectFactory;
import java.util.Enumeration;
import java.util.Hashtable;

public class MockGitTemplateManagerFactory implements ObjectFactory {

    @Override
    public Object getObjectInstance(Object obj, Name name, Context nameCtx, Hashtable<?, ?> environment)
            throws Exception {

        Reference reference = (Reference) obj;
        Enumeration<RefAddr> references = reference.getAll();

        String basepath = "";

        while (references.hasMoreElements()) {

            RefAddr address = references.nextElement();
            String type = address.getType();
            String content = (String) address.getContent();


            switch (type) {
                case "basepath":
                    basepath = content;
                    break;
                default:
                    break; // NOP
            }


        }

        if (basepath.isEmpty()) {
            throw new IllegalStateException("basepath must not be empty");
        }

        return new GitTemplateManager(basepath);
    }

}
