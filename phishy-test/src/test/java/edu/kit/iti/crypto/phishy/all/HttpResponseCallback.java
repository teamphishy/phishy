package edu.kit.iti.crypto.phishy.all;

import org.mockserver.mock.action.ExpectationResponseCallback;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;

import java.io.*;

import static org.mockserver.model.HttpResponse.response;


public class HttpResponseCallback implements ExpectationResponseCallback {

    @Override
    public HttpResponse handle(HttpRequest httpRequest) throws IOException {
        String[] levels = httpRequest.getPath().getValue().split("/");
        if (levels[1].equals("phishy")) {
            MockAPISerlvet servlet = new MockAPISerlvet();
            byte[] inputBytes = httpRequest.getBodyAsRawBytes();
            InputStream input = new ByteArrayInputStream(inputBytes);
            MockHttpServletRequest request = new MockHttpServletRequest(httpRequest.getMethod().toString(),
                    httpRequest.getPath().toString(), inputBytes.length, input, "application/json");

            StringWriter strWriter = new StringWriter();
            MockHttpServletResponse response = new MockHttpServletResponse(strWriter);
            PrintWriter writer = response.getWriter();
            servlet.doGet(request, response);
            writer.close();

            return response().withBody(strWriter.toString()).withStatusCode(response.getStatus());

        } else {
            return response().withStatusCode(404);
        }
    }
}
