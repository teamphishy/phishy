package edu.kit.iti.crypto.phishy.all;

import edu.kit.iti.crypto.phishy.data.addressbook.SQLAddressbookCache;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.Name;
import javax.naming.spi.ObjectFactory;
import javax.sql.DataSource;
import java.util.Hashtable;

public class MockCSVAddressbookCacheFactory implements ObjectFactory {

    @Override
    public Object getObjectInstance(Object obj, Name name, Context nameCtx, Hashtable<?, ?> environment)
            throws Exception {
        System.out.println("test");
        InitialContext ctx = new InitialContext();
        return new SQLAddressbookCache((DataSource)
                ctx.lookup("java:comp/env/jdbc/test"));
    }
}