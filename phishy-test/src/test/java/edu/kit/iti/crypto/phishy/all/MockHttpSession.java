package edu.kit.iti.crypto.phishy.all;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class MockHttpSession implements HttpSession {

    Map<String, Object> attributes = new HashMap<String, Object>();

    @Override
    public long getCreationTime() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public String getId() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public long getLastAccessedTime() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public ServletContext getServletContext() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public void setMaxInactiveInterval(int interval) {
        throw new UnsupportedOperationException("not supported by mock");

    }

    @Override
    public int getMaxInactiveInterval() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public HttpSessionContext getSessionContext() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public Object getAttribute(String name) {
        return attributes.get(name);
    }

    @Override
    public Object getValue(String name) {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public Enumeration<String> getAttributeNames() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public String[] getValueNames() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public void setAttribute(String name, Object value) {
        attributes.put(name, value);

    }

    @Override
    public void putValue(String name, Object value) {
        throw new UnsupportedOperationException("not supported by mock");

    }

    @Override
    public void removeAttribute(String name) {
        throw new UnsupportedOperationException("not supported by mock");

    }

    @Override
    public void removeValue(String name) {
        throw new UnsupportedOperationException("not supported by mock");

    }

    @Override
    public void invalidate() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public boolean isNew() {
        throw new UnsupportedOperationException("not supported by mock");
    }

}
