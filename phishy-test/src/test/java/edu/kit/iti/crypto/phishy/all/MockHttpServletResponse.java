package edu.kit.iti.crypto.phishy.all;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.*;

public class MockHttpServletResponse implements HttpServletResponse {

    private Map<String, List<String>> header = new HashMap<>();
    private int status = 200;
    private String mimeType = "text/plain";
    private Writer outputWriter;
    private String encoding = "UTF-8";
    private long contentLength = 0;

    public MockHttpServletResponse(Writer outputWriter) {
        this.outputWriter = outputWriter;
    }

    @Override
    public void addCookie(Cookie cookie) {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public boolean containsHeader(String name) {
        return header.containsKey(name);
    }

    @Override
    public String encodeURL(String url) {
        return url;
    }

    @Override
    public String encodeRedirectURL(String url) {
        return url;
    }

    @Override
    public String encodeUrl(String url) {
        return url;
    }

    @Override
    public String encodeRedirectUrl(String url) {
        return url;
    }

    @Override
    public void sendError(int sc, String msg) throws IOException {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public void sendError(int sc) throws IOException {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public void sendRedirect(String location) throws IOException {
        header.put("Location", List.of(location));
    }

    @Override
    public void setDateHeader(String name, long date) {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public void addDateHeader(String name, long date) {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public void setHeader(String name, String value) {
        header.put(name, List.of(value));
    }

    @Override
    public void addHeader(String name, String value) {
        List<String> itemList = new ArrayList<>(header.getOrDefault(name, List.of()));
        itemList.add(value);
        header.put(name, itemList);
    }

    @Override
    public void setIntHeader(String name, int value) {
        setHeader(name, String.valueOf(value));
    }

    @Override
    public void addIntHeader(String name, int value) {
        addHeader(name, String.valueOf(value));
    }

    @Override
    public void setStatus(int sc) {
        status = sc;
    }

    @Override
    public void setStatus(int sc, String sm) {
        setStatus(sc);
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public String getHeader(String name) {
        return header.get(name).get(0);
    }

    @Override
    public Collection<String> getHeaders(String name) {
        return header.get(name);
    }

    @Override
    public Collection<String> getHeaderNames() {
        return header.keySet();
    }

    @Override
    public String getCharacterEncoding() {
        return encoding;
    }

    @Override
    public String getContentType() {
        return mimeType;
    }

    @Override
    public ServletOutputStream getOutputStream() throws IOException {
        return new ServletOutputStream() {

            @Override
            public void write(int b) throws IOException {
            }

            @Override
            public void setWriteListener(WriteListener writeListener) {
            }

            @Override
            public boolean isReady() {
                return true;
            }
        };
    }

    @Override
    public PrintWriter getWriter() throws IOException {
        return new PrintWriter(outputWriter);
    }

    @Override
    public void setCharacterEncoding(String charset) {
        encoding = charset;
    }

    @Override
    public void setContentLength(int len) {
        contentLength = len;
    }

    @Override
    public void setContentLengthLong(long len) {
        contentLength = len;
    }

    @Override
    public void setContentType(String type) {
        mimeType = type;
    }

    @Override
    public void setBufferSize(int size) {
        // not implemented; fail silently
    }

    @Override
    public int getBufferSize() {
        return 0;
    }

    @Override
    public void flushBuffer() throws IOException {
        // fail silently
    }

    @Override
    public void resetBuffer() {
        throw new IllegalStateException();
    }

    @Override
    public boolean isCommitted() {
        return true;
    }

    @Override
    public void reset() {
        throw new IllegalStateException();
    }

    @Override
    public void setLocale(Locale loc) {
        // fail silently
    }

    @Override
    public Locale getLocale() {
        return new Locale("en");
    }

}
