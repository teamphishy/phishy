package edu.kit.iti.crypto.phishy.all;

import edu.kit.iti.crypto.phishy.server.WebsiteServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MockWebsiteServlet extends WebsiteServlet {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        super.doGet(req, resp);
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp) {
        super.doPost(req, resp);
    }
}
