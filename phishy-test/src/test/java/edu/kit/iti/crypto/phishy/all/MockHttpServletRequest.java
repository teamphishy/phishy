package edu.kit.iti.crypto.phishy.all;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.security.Principal;
import java.util.*;

public class MockHttpServletRequest implements HttpServletRequest {

    private final Map<String, List<String>> header;
    private final String method;
    private final String pathInfo;
    private final String uri;
    private final InputStream input;
    private final long contentLength;
    private final String mimeType;
    private final Map<String, String[]> parameter;
    private final String queryString;
    private HttpSession session;

    public MockHttpServletRequest(Map<String, List<String>> header, String method, String pathInfo,
                                  String uri, InputStream input, long contentLength, String mimeType,
                                  Map<String, String[]> parameter, String queryString, HttpSession session) {
        this.header = header;
        this.method = method;
        this.pathInfo = pathInfo;
        this.uri = uri;
        this.input = input;
        this.contentLength = contentLength;
        this.mimeType = mimeType;
        this.parameter = parameter;
        this.queryString = queryString;
        this.session = session;
    }

    public MockHttpServletRequest(String method, String path, long contentLength, InputStream input,
                                  String mimeType) {

        this(new HashMap<>(), method, path, "https://localhost:8888" + path, input, contentLength, mimeType,
                new HashMap<>(), null, null);

    }

    public MockHttpServletRequest(String path, String queryString, HttpSession session, Map<String, String[]> parameters) {
        this(new HashMap<>(), null, path, "https://localhost:8888" + path, null, 0, null,
                parameters, queryString, session);
    }

    @Override
    public String getAuthType() {
        return null; // not authenticated
    }

    @Override
    public Cookie[] getCookies() {
        // Not needed
        return new Cookie[0];
    }

    @Override
    public long getDateHeader(String name) {
        return -1; // not neccessary
    }

    @Override
    public String getHeader(String name) {
        return header.get(name).get(0);
    }

    @Override
    public Enumeration<String> getHeaders(String name) {
        return Collections.enumeration(header.getOrDefault(name, new ArrayList<>()));
    }

    @Override
    public Enumeration<String> getHeaderNames() {
        return Collections.enumeration(header.keySet());
    }

    @Override
    public int getIntHeader(String name) {

        String header = getHeader(name);
        if (header == null) {
            return -1;
        }

        return Integer.parseInt(header);

    }

    @Override
    public String getMethod() {
        return method;
    }

    @Override
    public String getPathInfo() {
        return this.pathInfo.substring(11);
    }

    @Override
    public String getPathTranslated() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public String getContextPath() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public String getQueryString() {
        return queryString;
    }

    @Override
    public String getRemoteUser() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public boolean isUserInRole(String role) {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public Principal getUserPrincipal() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public String getRequestedSessionId() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public String getRequestURI() {
        return this.uri;
    }

    @Override
    public StringBuffer getRequestURL() {
        return new StringBuffer(this.uri);
    }

    @Override
    public String getServletPath() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public HttpSession getSession(boolean create) {
        return session;
    }

    @Override
    public HttpSession getSession() {
        return session;
    }

    @Override
    public String changeSessionId() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public boolean isRequestedSessionIdValid() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public boolean isRequestedSessionIdFromCookie() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public boolean isRequestedSessionIdFromURL() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public boolean isRequestedSessionIdFromUrl() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public boolean authenticate(HttpServletResponse response) throws IOException, ServletException {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public void login(String username, String password) throws ServletException {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public void logout() throws ServletException {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public Collection<Part> getParts() throws IOException, ServletException {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public Part getPart(String name) throws IOException, ServletException {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public <T extends HttpUpgradeHandler> T upgrade(Class<T> handlerClass) throws IOException, ServletException {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public Object getAttribute(String name) {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public Enumeration<String> getAttributeNames() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public String getCharacterEncoding() {
        return "UTF-8";
    }

    @Override
    public void setCharacterEncoding(String env) throws UnsupportedEncodingException {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public int getContentLength() {
        return (int) contentLength;
    }

    @Override
    public long getContentLengthLong() {
        return contentLength;
    }

    @Override
    public String getContentType() {
        return mimeType;
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public String getParameter(String name) {
        return parameter.getOrDefault(name, new String[]{""})[0];
    }

    @Override
    public Enumeration<String> getParameterNames() {
        return Collections.enumeration(parameter.keySet());
    }

    @Override
    public String[] getParameterValues(String name) {

        if (parameter.containsKey(name)) {
            return parameter.get(name);
        } else {
            return new String[0];
        }

    }

    @Override
    public Map<String, String[]> getParameterMap() {
        return parameter;
    }

    @Override
    public String getProtocol() {
        return "HTTP/1.1";
    }

    @Override
    public String getScheme() {
        return "https";
    }

    @Override
    public String getServerName() {
        return "localhost";
    }

    @Override
    public int getServerPort() {
        return 8080;
    }

    @Override
    public BufferedReader getReader() throws IOException {
        return new BufferedReader(new InputStreamReader(input));
    }

    @Override
    public String getRemoteAddr() {
        return "127.0.0.1";
    }

    @Override
    public String getRemoteHost() {
        return getRemoteAddr();
    }

    @Override
    public void setAttribute(String name, Object o) {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public void removeAttribute(String name) {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public Locale getLocale() {
        return new Locale("en");
    }

    @Override
    public Enumeration<Locale> getLocales() {
        return Collections.enumeration(List.of(getLocale()));
    }

    @Override
    public boolean isSecure() {
        return true; //  we mock a https request
    }

    @Override
    public RequestDispatcher getRequestDispatcher(String path) {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public String getRealPath(String path) {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public int getRemotePort() {
        return 0;
    }

    @Override
    public String getLocalName() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public String getLocalAddr() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public int getLocalPort() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public ServletContext getServletContext() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public AsyncContext startAsync() throws IllegalStateException {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public AsyncContext startAsync(ServletRequest servletRequest, ServletResponse servletResponse)
            throws IllegalStateException {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public boolean isAsyncStarted() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public boolean isAsyncSupported() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public AsyncContext getAsyncContext() {
        throw new UnsupportedOperationException("not supported by mock");
    }

    @Override
    public DispatcherType getDispatcherType() {
        throw new UnsupportedOperationException("not supported by mock");
    }

}
