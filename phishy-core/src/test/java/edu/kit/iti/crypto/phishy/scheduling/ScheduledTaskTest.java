package edu.kit.iti.crypto.phishy.scheduling;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class ScheduledTaskTest {

    @Test
    public void getWeekdaysTest() {
        Collection<Weekday> weekdays = Arrays.asList(Weekday.MONDAY, Weekday.FRIDAY);
        ScheduledTask task = new ScheduledTask(() -> {}, weekdays, new Date(), new HashMap<>());
        Assert.assertEquals(weekdays, task.getWeekdays());
    }

    @Test
    public void getTimeTest() {
        Date date = new Date();
        ScheduledTask task = new ScheduledTask(() -> {}, Arrays.asList(Weekday.MONDAY, Weekday.FRIDAY), date , new HashMap<>());
        Assert.assertEquals(date, task.getTime());
    }
}
