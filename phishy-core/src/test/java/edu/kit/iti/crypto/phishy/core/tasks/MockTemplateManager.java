package edu.kit.iti.crypto.phishy.core.tasks;

import edu.kit.iti.crypto.phishy.core.mail.MockTemplate;
import edu.kit.iti.crypto.phishy.data.template.Template;
import edu.kit.iti.crypto.phishy.data.template.TemplateDescription;
import edu.kit.iti.crypto.phishy.data.template.TemplateGroup;
import edu.kit.iti.crypto.phishy.data.template.TemplateManager;
import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.eclipse.jgit.transport.resolver.RepositoryResolver;

import javax.servlet.http.HttpServletRequest;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;

public class MockTemplateManager implements TemplateManager {

    private boolean cacheInvalidated = false;

    @Override
    public TemplateGroup getGroup(String name) throws NotFoundException {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public boolean hasGroup(String name) {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public TemplateGroup createGroup(String name) throws CreationException {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public void deleteGroup(String name) throws DeletionException {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public Collection<TemplateGroup> listGroups() {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public Template getTemplate(TemplateDescription description) throws NotFoundException {
        Path groupPath;
        try {
            groupPath = Paths.get(this.getClass().getResource("").toURI());
        } catch (URISyntaxException e) {
            throw new UnsupportedOperationException("Operation not supported on Mock");
        }
        return new MockTemplate(groupPath, "template1", null);
    }

    @Override
    public boolean hasTemplate(TemplateDescription description) {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public RepositoryResolver<HttpServletRequest> getGitResolver() {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public void invalidateCache() {
        cacheInvalidated = true;
    }

    public boolean isCacheInvalidated() {

        return cacheInvalidated;

    }

}
