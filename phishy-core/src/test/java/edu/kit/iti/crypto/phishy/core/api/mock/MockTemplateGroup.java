package edu.kit.iti.crypto.phishy.core.api.mock;

import edu.kit.iti.crypto.phishy.data.template.Template;
import edu.kit.iti.crypto.phishy.data.template.TemplateDescription;
import edu.kit.iti.crypto.phishy.data.template.TemplateGroup;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import java.util.Collection;
import java.util.List;

public class MockTemplateGroup implements TemplateGroup {

    private String name;
    private List<Template> templates;

    public MockTemplateGroup(String name) {
        this.name = name;
        this.templates = List.of(new MockTemplate("a", this), new MockTemplate("b", this));
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Template getTemplate(TemplateDescription description) throws NotFoundException {
        for (Template t : templates) {
            if (description.getName().equals(t.getName())) {
                return t;
            }
        }

        throw new NotFoundException();
    }

    @Override
    public boolean hasTemplate(TemplateDescription description) {
        for (Template t : templates) {
            if (description.getName().equals(t.getName())) {
                return true;
            }
        }

        return false;
    }

    @Override
    public Collection<Template> listTemplates() {
        return templates;
    }
}
