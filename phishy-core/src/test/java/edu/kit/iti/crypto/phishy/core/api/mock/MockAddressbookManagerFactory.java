package edu.kit.iti.crypto.phishy.core.api.mock;

import javax.naming.Context;
import javax.naming.Name;
import javax.naming.spi.ObjectFactory;
import java.util.Hashtable;

public class MockAddressbookManagerFactory implements ObjectFactory {


    @Override
    public Object getObjectInstance(Object obj, Name name, Context nameCtx, Hashtable<?, ?> environment)
            throws Exception {
        return new MockAddressbookManager();
    }
}
