package edu.kit.iti.crypto.phishy.core.tasks;

import edu.kit.iti.crypto.phishy.data.addressbook.Addressbook;
import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookDescription;
import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookManager;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import java.util.ArrayList;
import java.util.Collection;

public class MockAddressbookManager implements AddressbookManager {

    Collection<Addressbook> addressbooks = new ArrayList<>();


    @Override
    public Collection<Addressbook> listAddressbooks() {
        return addressbooks;
    }

    @Override
    public boolean hasAddressbook(AddressbookDescription description) {
        for (Addressbook ab : addressbooks) {
            if (ab.getName().equals(description.getName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Addressbook getAddressbook(AddressbookDescription description) throws NotFoundException {
        for (Addressbook ab : addressbooks) {
            if (ab.getName().equals(description.getName())) {
                return ab;
            }
        }
        throw new NotFoundException();
    }

    public void addAddressbook(Addressbook addressbook) {
        this.addressbooks.add(addressbook);
    }

}
