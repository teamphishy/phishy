package edu.kit.iti.crypto.phishy.scheduling;

public class MockTask implements Runnable {

    public boolean isRun = false;

    @Override
    public void run() {
        isRun = true;
    }
}