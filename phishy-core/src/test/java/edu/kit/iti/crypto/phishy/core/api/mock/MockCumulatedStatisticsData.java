package edu.kit.iti.crypto.phishy.core.api.mock;

import edu.kit.iti.crypto.phishy.data.statistics.CumulatedStatisticsData;

public class MockCumulatedStatisticsData implements CumulatedStatisticsData {

    private int sentMails;
    private int reportedMails;
    private int phishedMails;

    public MockCumulatedStatisticsData(int sentMails, int reportedMails, int phishedMails) {
        this.sentMails = sentMails;
        this.reportedMails = reportedMails;
        this.phishedMails = phishedMails;
    }

    @Override
    public int getSentMails() {
        return sentMails;
    }

    @Override
    public int getReportedMails() {
        return reportedMails;
    }

    @Override
    public int getPhishedMails() {
        return phishedMails;
    }
}
