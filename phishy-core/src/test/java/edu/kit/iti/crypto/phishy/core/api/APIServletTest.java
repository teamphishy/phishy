package edu.kit.iti.crypto.phishy.core.api;

import edu.kit.iti.crypto.phishy.core.ModalLock;
import edu.kit.iti.crypto.phishy.data.ManagerFactory;
import hthurow.tomcatjndi.TomcatJNDI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.io.Writer;
import java.net.URISyntaxException;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class APIServletTest {

    TomcatJNDI tomcatJNDI = new TomcatJNDI();

    @Before
    public void setup() throws URISyntaxException {

        System.out.println(Paths.get(this.getClass().getResource("context.xml").toURI()).toFile());
        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("context.xml").toURI()).toFile());
        tomcatJNDI.start();
        ManagerFactory.reset();

    }

    @After
    public void teardown() {

        tomcatJNDI.tearDown();

    }

    @Test
    public void testDeleteLocking() throws Exception {

        try (var lock = ModalLock.getInstance().getLeaseOrFail()) {
            assertNotNull(lock);

            MockServletRequest request = MockServletRequest.newDeleteRequest("/campaign/A");
            MockServletResponse response = new MockServletResponse(Writer.nullWriter());

            APIServlet servlet = new APIServlet();
            servlet.service(request, response);

            assertEquals(HttpResponseType.LOCKED.getStatusCode(), response.getStatus());
        }

    }

    @Test
    public void testPutLocking() throws Exception {

        try (var lock = ModalLock.getInstance().getLeaseOrFail()) {
            assertNotNull(lock);

            MockServletRequest request =
                    MockServletRequest.newPutRequest("/campaign/A", 0, InputStream.nullInputStream(), "text/plain");
            MockServletResponse response = new MockServletResponse(Writer.nullWriter());

            APIServlet servlet = new APIServlet();
            servlet.service(request, response);

            assertEquals(HttpResponseType.LOCKED.getStatusCode(), response.getStatus());

            ModalLock.getInstance().returnLease(lock);
        }

    }

    @Test
    public void testPostLocking() throws Exception {

        try (var lock = ModalLock.getInstance().getLeaseOrFail()) {
            assertNotNull(lock);

            MockServletRequest request =
                    MockServletRequest.newPostRequest("/campaign/A", 0, InputStream.nullInputStream(), "text/plain");
            MockServletResponse response = new MockServletResponse(Writer.nullWriter());

            APIServlet servlet = new APIServlet();
            servlet.service(request, response);

            assertEquals(HttpResponseType.LOCKED.getStatusCode(), response.getStatus());

            ModalLock.getInstance().returnLease(lock);
        }

    }

    @Test
    public void testGetLocking() throws Exception {
        // should not lock on GET
        try (var lock = ModalLock.getInstance().getLeaseOrFail()) {
            assertNotNull(lock);

            MockServletRequest request = MockServletRequest.newGetRequest("/campaign/A");
            MockServletResponse response = new MockServletResponse(Writer.nullWriter());

            APIServlet servlet = new APIServlet();
            servlet.service(request, response);

            assertEquals(HttpResponseType.OK.getStatusCode(), response.getStatus());

            ModalLock.getInstance().returnLease(lock);
        }

    }

}