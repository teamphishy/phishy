package edu.kit.iti.crypto.phishy.core.tasks;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.interfaces.RSAKeyProvider;
import com.icegreen.greenmail.user.GreenMailUser;
import com.icegreen.greenmail.util.DummySSLSocketFactory;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.GreenMailUtil;
import com.icegreen.greenmail.util.ServerSetup;
import edu.kit.iti.crypto.phishy.core.JWTKeyProvider;
import edu.kit.iti.crypto.phishy.data.ManagerFactory;
import edu.kit.iti.crypto.phishy.data.addressbook.Addressbook;
import edu.kit.iti.crypto.phishy.data.campaign.Campaign;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignDescription;
import edu.kit.iti.crypto.phishy.data.campaign.TestDescription;
import hthurow.tomcatjndi.TomcatJNDI;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.security.Security;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EvaluateReportedMailsTaskTest {

    final RSAKeyProvider rsaKeyProvider = new JWTKeyProvider();
    TomcatJNDI tomcatJNDI = new TomcatJNDI();
    ServerSetup setupSmtp;
    GreenMail greenMailSmtp;
    ServerSetup setupImap;
    GreenMail greenMailImap;
    MockCampaignManager mockCampMan;

    static final int CAMPAIAGN_COUNT = 3;
    String[] campaignNames = new String[CAMPAIAGN_COUNT];
    int[] recipSizes = new int[CAMPAIAGN_COUNT];
    static final int AMOUNT_OF_MAILS = 300;

    ArrayList<String> testMails = new ArrayList<String>();
    Algorithm tokenAlgorithm;

    @Before
    public void setUp() throws Exception {
        tokenAlgorithm = Algorithm.RSA256(rsaKeyProvider);
        Security.setProperty("ssl.SocketFactory.provider", DummySSLSocketFactory.class.getName());
    }

    private void testSetup() throws MessagingException {
        for (int i = 0; i < CAMPAIAGN_COUNT; i++) {
            campaignNames[i] = "Campaign" + i;
        }
        setImapServer();
        setSmtpServer();
        generateTestReportMails();

        ManagerFactory.reset();
        setCampaignManager();
        setAddressbookManager();

    }

    private void setImapServer() {
        setupImap = new ServerSetup(48661, "localhost", ServerSetup.PROTOCOL_IMAPS);
        greenMailImap = new GreenMail(setupImap);
        greenMailImap.start();
    }

    private void setSmtpServer() {
        setupSmtp = new ServerSetup(48621, "localhost", ServerSetup.PROTOCOL_SMTPS);
        greenMailSmtp = new GreenMail(setupSmtp);
        greenMailSmtp.setUser("user1@mail.com", "admin", "password");
        greenMailSmtp.start();
    }

    private void setCampaignManager() {
        mockCampMan = (MockCampaignManager) ManagerFactory.getCampaignManager();

        mockCampMan.campaignNames = campaignNames;
        mockCampMan.recipSizes = recipSizes;
        mockCampMan.createCampaigns();
    }


    private void setAddressbookManager() {
        MockAddressbookManager manager = (MockAddressbookManager) ManagerFactory.getAddressbookManager();
        Addressbook testAddrBook = new MockAddressbook("testAddrBook");
        manager.addAddressbook(testAddrBook);
    }

    private void generateTestReportMails() throws MessagingException {
        GreenMailUser user = greenMailImap.setUser("user1@mail.com", "admin", "password");

        for (int i = 0; i < AMOUNT_OF_MAILS; i++) {
            int campaignNum = (int) (Math.random() * CAMPAIAGN_COUNT);
            String campaignID = campaignNames[campaignNum];
            user.deliver(generateEmail(i, campaignID));
            recipSizes[campaignNum]++;
            testMails.add("test" + i + "@mail.com");
        }
    }

    private MimeMessage generateEmail(int i, String campaignID) throws IllegalArgumentException, JWTCreationException, MessagingException {
        MimeMessage message = GreenMailUtil.createTextEmail("test" + i + "@mail.com", "user1@mail.com", "subject",
                "body", greenMailImap.getImaps().getServerSetup());

        TestDescription testDescription = new TestDescription(new CampaignDescription(campaignID), UUID.randomUUID().toString());

        Calendar expiry = Calendar.getInstance();
        expiry.add(Calendar.DAY_OF_MONTH, 10);
        String jwt = JWT.create()
                .withClaim("nonce", testDescription.toIdentifier())
                .withClaim("email", "test" + i + "@mail.com")
                .withClaim("name", i)
                .withClaim("exp", expiry.getTime())
                .sign(tokenAlgorithm);

        message.setHeader("X-Phishy-ID", jwt);
        return message;
    }

    @After
    public void tearDown() throws Exception {
        tomcatJNDI.tearDown();
        greenMailImap.stop();
        greenMailSmtp.stop();
    }

    @AfterClass
    public static void tearDownClass() {
        File privateKey = new File("../private.key");
        privateKey.delete();
        File publicKey = new File("../public.key.pub");
        publicKey.delete();
    }

    @Test
    public void testEvaluatingMails() throws InterruptedException, MessagingException, URISyntaxException {
        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("context.xml").toURI()).toFile());
        tomcatJNDI.start();
        testSetup();

        EvaluateReportedMailsTask evaulateMailsTask = new EvaluateReportedMailsTask();
        Thread evaluateThread = new Thread(evaulateMailsTask);
        evaluateThread.start();
        evaluateThread.join();


        Message[] messages = greenMailSmtp.getReceivedMessages();
        assertEquals(AMOUNT_OF_MAILS, messages.length);

        for (Message message : messages) {
            assertTrue(testMails.remove(message.getRecipients(RecipientType.TO)[0].toString()));

        }
        assertEquals(0, testMails.size());

        for (Campaign campaign : mockCampMan.listCampaigns()) {
            assertEquals(0, ((MockCampaign) campaign).numberOfOutstandingTests);
        }

    }

    @Test
    public void testFailReceiveSession() throws InterruptedException, MessagingException, URISyntaxException {
        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("contextFail.xml").toURI()).toFile());
        tomcatJNDI.start();
        testSetup();

        EvaluateReportedMailsTask evaulateMailsTask = new EvaluateReportedMailsTask();
        Thread evaluateThread = new Thread(evaulateMailsTask);
        evaluateThread.start();
        evaluateThread.join();


        Message[] messages = greenMailSmtp.getReceivedMessages();
        assertEquals(0, messages.length);
    }

    @Test
    public void testFailSendSession() throws InterruptedException, MessagingException, URISyntaxException {
        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("contextFail2.xml").toURI()).toFile());
        tomcatJNDI.start();
        testSetup();

        EvaluateReportedMailsTask evaulateMailsTask = new EvaluateReportedMailsTask();
        Thread evaluateThread = new Thread(evaulateMailsTask);
        evaluateThread.start();
        evaluateThread.join();


        Message[] messages = greenMailSmtp.getReceivedMessages();
        assertEquals(0, messages.length);
    }

}
