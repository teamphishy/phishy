package edu.kit.iti.crypto.phishy.core.mail;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.RSAKeyProvider;
import edu.kit.iti.crypto.phishy.core.JWTKeyProvider;
import edu.kit.iti.crypto.phishy.data.addressbook.Recipient;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignDescription;
import edu.kit.iti.crypto.phishy.data.campaign.TestDescription;
import edu.kit.iti.crypto.phishy.data.template.Template;
import edu.kit.iti.crypto.phishy.data.template.TemplateFile;
import hthurow.tomcatjndi.TomcatJNDI;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class MailGeneratorTest {

    final RSAKeyProvider rsaKeyProvider = new JWTKeyProvider();
    int recipientCount = 3;
    List<Recipient> recipients = new ArrayList<Recipient>();
    MailSpool mailSpool = new MailSpool(recipientCount);
    Collection<TemplateFile> templateFiles = new ArrayList<TemplateFile>();
    Template template;
    Template failTemplate;
    Algorithm tokenAlgorithm;
    TomcatJNDI tomcatJNDI = new TomcatJNDI();

    @Before
    public void setUp() throws URISyntaxException {
        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("context.xml").toURI()).toFile());
        tomcatJNDI.start();
        tokenAlgorithm = Algorithm.RSA256(rsaKeyProvider);

        Recipient recipient1 = new Recipient("", new Date(1000));
        recipient1.setTestId(new TestDescription(new CampaignDescription("campaign1"), "idToTest1"));
        recipient1.setMail("test1@mail.com");
        recipients.add(recipient1);
        Recipient recipient2 = new Recipient("", new Date(2000));
        recipient2.setTestId(new TestDescription(new CampaignDescription("campaign2"), "idToTest2"));
        recipient2.setMail("test2@mail.com");
        recipients.add(recipient2);
        Recipient recipient3 = new Recipient("", new Date(3000));
        recipient3.setTestId(new TestDescription(new CampaignDescription("campaign3"), "idToTest3"));
        recipient3.setMail("test3@mail.com");
        recipients.add(recipient3);

        Path groupPath = Paths.get(this.getClass().getResource("").toURI());
        template = new MockTemplate(groupPath, "template1", null);

        failTemplate = new MockTemplate(groupPath, "template2", null);
    }

    @After
    public void tearDown() {
        tomcatJNDI.tearDown();
    }

    @AfterClass
    public static void tearDownClass() {
        File privateKey = new File("../private.key");
        privateKey.delete();
        File publicKey = new File("../public.key.pub");
        publicKey.delete();
    }

    @Test
    public void testNormalMailGenerator() throws MessagingException, InterruptedException {
        MailGenerator mailGenerator = new MailGenerator(mailSpool, template, recipients, tokenAlgorithm, 10);

        Thread generator = new Thread(mailGenerator);
        generator.start();
        generator.join();

        JWTVerifier verifier = JWT.require(tokenAlgorithm).build();
        for (int i = 0; i < recipients.size(); i++) {
            int nextMailIndex = mailSpool.reserveNextMail();
            MimeMessage message = mailSpool.getMailToSend(nextMailIndex);

            DecodedJWT decodedToken = verifier.verify(message.getHeader("X-Phishy-ID")[0]);

            assertEquals(recipients.get(i).getMail(), message.getRecipients(RecipientType.TO)[0].toString());
            assertEquals(recipients.get(i).getTestId().toIdentifier(), decodedToken.getClaim("nonce").asString());
        }


    }

    @Test
    public void testFailTemplate() throws MessagingException, InterruptedException {
        MailGenerator mailGenerator = new MailGenerator(mailSpool, failTemplate, recipients, tokenAlgorithm, 10);

        Thread generator = new Thread(mailGenerator);
        generator.start();
        mailSpool.notifyLastMailAdded();
        generator.join();


        assertFalse(mailSpool.isNextMailReady(mailSpool.reserveNextMail()));
    }

    @Test
    public void testMailWithName() throws MessagingException, InterruptedException {

        Recipient recipient1 = recipients.get(0);
        recipient1.setName("TestName");


        MailGenerator mailGenerator = new MailGenerator(mailSpool, template, recipients, tokenAlgorithm, 10);

        Thread generator = new Thread(mailGenerator);
        generator.start();
        generator.join();

        MimeMessage message = mailSpool.getMailToSend(mailSpool.reserveNextMail());
        InternetAddress address = (InternetAddress) message.getRecipients(RecipientType.TO)[0];

        assertEquals(recipient1.getMail(), address.getAddress());
        assertEquals(recipient1.getName(), address.getPersonal());

        mailSpool.getMailToSend(mailSpool.reserveNextMail());
        mailSpool.getMailToSend(mailSpool.reserveNextMail());
        mailSpool.notifyLastMailAdded();
        assertTrue(mailSpool.isNextIndexPossible(mailSpool.reserveNextMail()));

    }

}
