package edu.kit.iti.crypto.phishy.scheduling;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.junit.Assert.*;

public class SchedulerTest {


    private MockTask mockTask;
    private Scheduler scheduler;

    @Before
    public void setup() {
        scheduler = Scheduler.getInstance();
        mockTask = new MockTask();
    }

    @Test
    public void getInstance() {
        assertSame(scheduler, Scheduler.getInstance());

    }

    @Test
    public void getThreadPool() {
        ExecutorService pool = Executors.newFixedThreadPool(Math.max(2, Runtime.getRuntime().availableProcessors()));
        assertEquals(pool.getClass(), scheduler.getThreadPool().getClass());
    }

    @Test
    public void getSingleThread() {
        ExecutorService singleThread = Executors.newSingleThreadExecutor();
        assertEquals(singleThread.getClass(), scheduler.getSingleThread().getClass());
    }

    @Test
    public void dispatchNow() throws InterruptedException {
        Future future = scheduler.dispatchNow(mockTask);
        while (!future.isDone()) {
            Thread.sleep(50);
        }
        assertTrue(mockTask.isRun);
    }

    @Test
    public void schedule() {

        MockScheduledTask scheduledTask = new MockScheduledTask(mockTask, null, null, null);
        scheduler.schedule(scheduledTask);
    }
}
