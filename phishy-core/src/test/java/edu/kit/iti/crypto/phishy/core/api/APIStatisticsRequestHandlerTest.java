package edu.kit.iti.crypto.phishy.core.api;

import edu.kit.iti.crypto.phishy.data.ManagerFactory;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignDescription;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignManager;
import edu.kit.iti.crypto.phishy.data.statistics.StatisticsData;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import hthurow.tomcatjndi.TomcatJNDI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.io.Reader;
import java.net.URISyntaxException;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

public class APIStatisticsRequestHandlerTest {

    TomcatJNDI tomcatJNDI = new TomcatJNDI();

    @Before
    public void setup() throws URISyntaxException {

        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("context.xml").toURI()).toFile());
        tomcatJNDI.start();
        ManagerFactory.reset();

    }

    @After
    public void teardown() {

        tomcatJNDI.tearDown();

    }

    private Reader sendRequest(MockServletRequest request) throws IOException, ServletException {

        PipedReader responseReader = new PipedReader();
        MockServletResponse response = new MockServletResponse(new PipedWriter(responseReader));

        APIServlet servlet = new APIServlet();
        servlet.service(request, response);

        response.getWriter().close();

        return responseReader;

    }

    @Test
    public void testGetCumulatedStatistics() throws IOException, ServletException {

        MockServletRequest request = MockServletRequest.newGetRequest("/stat");
        Reader responseReader = sendRequest(request);

        JsonReader jsonReader = Json.createReader(responseReader);
        JsonObject jsonObject = jsonReader.readObject();

        CampaignManager manager = ManagerFactory.getCampaignManager();
        assert manager != null;

        StatisticsData stats = manager.getCumulatedStatistics();

        assertEquals(stats.getSentMails(), jsonObject.getInt("numSentMails"));
        assertEquals(stats.getReportedMails(), jsonObject.getInt("numReportedMails"));
        assertEquals(stats.getPhishedMails(), jsonObject.getInt("numPhishedMails"));


    }

    @Test
    public void testGetCampaignStatistics() throws IOException, ServletException, NotFoundException {

        MockServletRequest request = MockServletRequest.newGetRequest("/stat/A");
        Reader responseReader = sendRequest(request);

        JsonReader jsonReader = Json.createReader(responseReader);
        JsonObject jsonObject = jsonReader.readObject();

        CampaignManager manager = ManagerFactory.getCampaignManager();
        assert manager != null;

        StatisticsData stats = manager.getCampaign(new CampaignDescription("A")).getStatistics();

        assertEquals(stats.getSentMails(), jsonObject.getInt("numSentMails"));
        assertEquals(stats.getReportedMails(), jsonObject.getInt("numReportedMails"));
        assertEquals(stats.getPhishedMails(), jsonObject.getInt("numPhishedMails"));


    }


}