package edu.kit.iti.crypto.phishy.core.api;

import edu.kit.iti.crypto.phishy.data.ManagerFactory;
import edu.kit.iti.crypto.phishy.data.campaign.*;
import edu.kit.iti.crypto.phishy.data.template.TemplateDescription;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import hthurow.tomcatjndi.TomcatJNDI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.json.*;
import javax.servlet.ServletException;
import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class APICampaignRequestHandlerTest {

    TomcatJNDI tomcatJNDI = new TomcatJNDI();

    @Before
    public void setup() throws URISyntaxException {

        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("context.xml").toURI()).toFile());
        tomcatJNDI.start();
        ManagerFactory.reset();

    }

    @After
    public void teardown() {

        tomcatJNDI.tearDown();

    }

    private Reader sendRequest(MockServletRequest request) throws IOException, ServletException {

        PipedReader responseReader = new PipedReader();
        MockServletResponse response = new MockServletResponse(new PipedWriter(responseReader));

        APIServlet servlet = new APIServlet();
        servlet.service(request, response);

        response.getWriter().close();

        return responseReader;

    }

    @Test
    public void testListCampaigns() throws ServletException, IOException, NotFoundException {

        MockServletRequest request = MockServletRequest.newGetRequest("/campaign");
        Reader responseReader = sendRequest(request);

        JsonReader jsonReader = Json.createReader(responseReader);
        JsonArray responseArray = jsonReader.readArray();

        CampaignManager manager = ManagerFactory.getCampaignManager();
        assert manager != null;

        for (JsonValue item : responseArray) {

            assertEquals(JsonValue.ValueType.OBJECT, item.getValueType());
            JsonObject jsonCampaign = (JsonObject) item;

            String campaignName = jsonCampaign.getString("id");
            Campaign campaign = manager.getCampaign(new CampaignDescription(campaignName));
            assertEquals(campaign.getStatus().toString(), jsonCampaign.getString("status"));
        }

    }

    @Test
    public void testGetCampaign() throws ServletException, IOException, NotFoundException {

        CampaignManager manager = ManagerFactory.getCampaignManager();
        assert manager != null;

        Campaign campaign = manager.getCampaign(new CampaignDescription("A"));

        MockServletRequest request = MockServletRequest.newGetRequest("/campaign/A");
        Reader responseReader = sendRequest(request);

        JsonReader jsonReader = Json.createReader(responseReader);
        JsonObject responseObject = jsonReader.readObject();
        assertNotNull(responseObject);

        assertEquals(campaign.getName(), responseObject.getString("id"));
        assertEquals(campaign.getStatus().toString(), responseObject.getString("status"));
        assertEquals(campaign.getRecipientSetSize(), responseObject.getInt("recipientSetSize"));
        assertEquals(campaign.getMailLifetime(), responseObject.getInt("mailLifetime"));
        assertEquals(campaign.getMailInterval(), responseObject.getInt("mailInterval"));

        JsonObject templateObject = responseObject.getJsonObject("templates");
        assertNotNull(templateObject);

        for (TemplateType templateType : TemplateType.values()) {

            String templateName = templateObject.getString(templateType.toString());
            assertNotNull(templateName);
            if (templateName.isEmpty()) {
                assertFalse(campaign.hasTemplate(templateType));
            } else {
                assertEquals(TemplateDescription.fromFQN(templateName), campaign.getTemplate(templateType));
            }

        }

    }

    @Test
    public void testPutCampaign() throws IOException, ServletException, NotFoundException {

        JsonObjectBuilder jsonObject = Json.createObjectBuilder();
        jsonObject.add("addressbook", "anotherone");
        jsonObject.add("mailLifetime", 365);
        jsonObject.add("recipientSetSize", 1);
        jsonObject.add("status", CampaignStatus.ACTIVE.toString());
        jsonObject.add("mailInterval", 365);

        JsonObjectBuilder jsonTemplates = Json.createObjectBuilder();
        jsonTemplates.add(TemplateType.INFO_MAIL.toString(), "another-infomail");
        jsonTemplates.add(TemplateType.PHISH_WEBSITE.toString(), "another-phishwebsite");

        jsonObject.add("templates", jsonTemplates);

        byte[] inputBytes = jsonObject.build().toString().getBytes();
        InputStream input = new ByteArrayInputStream(inputBytes);

        MockServletRequest request =
                MockServletRequest.newPutRequest("/campaign/A", inputBytes.length, input, "application/json");

        sendRequest(request);

        CampaignManager manager = ManagerFactory.getCampaignManager();
        assert manager != null;

        Campaign campaign = manager.getCampaign(new CampaignDescription("A"));

        assertEquals("anotherone", campaign.getAddressbook().getName());
        assertEquals(365, campaign.getMailLifetime());
        assertEquals(1, campaign.getRecipientSetSize());
        assertEquals(CampaignStatus.ACTIVE, campaign.getStatus());
        assertEquals(365, campaign.getMailInterval());
        assertTrue(campaign.hasTemplate(TemplateType.INFO_MAIL));
        assertTrue(campaign.hasTemplate(TemplateType.PHISH_WEBSITE));
        assertEquals("another", campaign.getTemplate(TemplateType.INFO_MAIL).getGroup());
        assertEquals("infomail", campaign.getTemplate(TemplateType.INFO_MAIL).getName());
        assertEquals("another", campaign.getTemplate(TemplateType.PHISH_WEBSITE).getGroup());
        assertEquals("phishwebsite", campaign.getTemplate(TemplateType.PHISH_WEBSITE).getName());


    }

    @Test
    public void testPostCampaign() throws IOException, ServletException, NotFoundException {

        CampaignManager manager = ManagerFactory.getCampaignManager();
        assert manager != null;

        assertFalse(manager.hasCampaign(new CampaignDescription("new")));

        JsonObjectBuilder jsonObject = Json.createObjectBuilder();
        jsonObject.add("addressbook", "anotherone");
        jsonObject.add("mailLifetime", 365);
        jsonObject.add("recipientSetSize", 1);
        jsonObject.add("status", CampaignStatus.ACTIVE.toString());
        jsonObject.add("mailInterval", 365);

        JsonObjectBuilder jsonTemplates = Json.createObjectBuilder();
        jsonTemplates.add(TemplateType.INFO_MAIL.toString(), "another-infomail");
        jsonTemplates.add(TemplateType.PHISH_WEBSITE.toString(), "another-phishwebsite");

        jsonObject.add("templates", jsonTemplates);

        byte[] inputBytes = jsonObject.build().toString().getBytes();
        InputStream input = new ByteArrayInputStream(inputBytes);

        MockServletRequest request =
                MockServletRequest.newPostRequest("/campaign/new", inputBytes.length, input, "application/json");

        sendRequest(request);


        assertTrue(manager.hasCampaign(new CampaignDescription("new")));
        Campaign campaign = manager.getCampaign(new CampaignDescription("new"));

        assertEquals("anotherone", campaign.getAddressbook().getName());
        assertEquals(365, campaign.getMailLifetime());
        assertEquals(1, campaign.getRecipientSetSize());
        assertEquals(CampaignStatus.ACTIVE, campaign.getStatus());
        assertEquals(365, campaign.getMailInterval());
        assertTrue(campaign.hasTemplate(TemplateType.INFO_MAIL));
        assertTrue(campaign.hasTemplate(TemplateType.PHISH_WEBSITE));
        assertEquals("another", campaign.getTemplate(TemplateType.INFO_MAIL).getGroup());
        assertEquals("infomail", campaign.getTemplate(TemplateType.INFO_MAIL).getName());
        assertEquals("another", campaign.getTemplate(TemplateType.PHISH_WEBSITE).getGroup());
        assertEquals("phishwebsite", campaign.getTemplate(TemplateType.PHISH_WEBSITE).getName());


    }

    @Test
    public void testDeleteCampaign() throws IOException, ServletException {

        CampaignManager manager = ManagerFactory.getCampaignManager();
        assert manager != null;

        assertTrue(manager.hasCampaign(new CampaignDescription("A")));
        assertTrue(manager.hasCampaign(new CampaignDescription("B")));

        MockServletRequest request = MockServletRequest.newDeleteRequest("/campaign/A");
        sendRequest(request);

        assertFalse(manager.hasCampaign(new CampaignDescription("A")));
        assertTrue(manager.hasCampaign(new CampaignDescription("B")));


    }


    private String stringFromReader(Reader reader) {

        return new BufferedReader(reader)
                .lines()
                .collect(Collectors.joining("\n"));

    }


}