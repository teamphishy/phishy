package edu.kit.iti.crypto.phishy.core.tasks;

import edu.kit.iti.crypto.phishy.data.campaign.OutstandingTest;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import java.util.Date;

public class MockOutstandingTest implements OutstandingTest {

    MockCampaign mockCampaign;

    public MockOutstandingTest(MockCampaign mockCampaign) {
        this.mockCampaign = mockCampaign;
    }

    @Override
    public void increaseLoginAttempt() throws NotFoundException {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public String getId() {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public Date getDate() throws NotFoundException {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public int getLoginAttempt() throws NotFoundException {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public void delete() {
        mockCampaign.decreaseTests();
    }


}
