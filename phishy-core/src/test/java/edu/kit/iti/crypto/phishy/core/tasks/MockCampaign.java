package edu.kit.iti.crypto.phishy.core.tasks;

import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookDescription;
import edu.kit.iti.crypto.phishy.data.addressbook.Recipient;
import edu.kit.iti.crypto.phishy.data.campaign.*;
import edu.kit.iti.crypto.phishy.data.statistics.CampaignStatisticsData;
import edu.kit.iti.crypto.phishy.data.template.TemplateDescription;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import java.util.Collection;
import java.util.List;

public class MockCampaign extends Campaign {

    String name;
    int recipSize;
    int numberOfOutstandingTests;

    @Override
    public void linkTemplate(TemplateType type, TemplateDescription description) {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public void unlinkTemplate(TemplateType type) throws DeletionException {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public TemplateDescription getTemplate(TemplateType type) throws NotFoundException {
        if (name.equals("FailCampaign2")) {
            throw new NotFoundException();
        }

        if (type == TemplateType.PHISH_MAIL) {
            return new TemplateDescription("", "template1");
        } else if (type == TemplateType.INFO_MAIL) {
            return new TemplateDescription("", "template1");
        } else {
            throw new UnsupportedOperationException("Operation not supported on Mock");
        }

    }

    @Override
    public boolean hasTemplate(TemplateType type) {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public CampaignStatisticsData getStatistics() {
        return new CampaignStatisticsData() {

            @Override
            public int getSentMails() {
                return 0;
            }

            @Override
            public int getReportedMails() {
                return 0;
            }

            @Override
            public int getPhishedMails() {
                return 0;
            }

            @Override
            public void increaseSentMails(int amount) {
            }

            @Override
            public void increaseReportedMail(int amount) {
            }

            @Override
            public void increasePhishedMail(int amount) {
            }

            @Override
            public String getCampaignName() {
                return null;
            }
        };
    }

    @Override
    public OutstandingTest getOutstandingTest(String id) throws NotFoundException {
        return new MockOutstandingTest(this);
    }

    @Override
    public boolean hasOutstandingTest(String id) {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public AddressbookDescription getAddressbook() throws NotFoundException {
        if (name.equals("FailCampaign")) {
            throw new NotFoundException();
        }
        return new AddressbookDescription("testAddrBook");
    }

    @Override
    public void recordTests(Collection<TestDescription> tests) {
        //test not needed here
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getRecipientSetSize() {
        return recipSize;
    }

    @Override
    public void setRecipientSetSize(int recipientSetSize) {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public CampaignStatus getStatus() {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public void setStatus(CampaignStatus status) {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public int getMailLifetime() {
        return 10;
    }

    @Override
    public void setMailLifetime(int mailLifetime) {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public void setAddressbook(AddressbookDescription description) {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public int getMailInterval() {
        return 0;
    }

    @Override
    public void setMailInterval(int mailInterval) {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    public void decreaseTests() {
        numberOfOutstandingTests--;
    }

    @Override
    public List<Recipient> deleteTests(List<Recipient> recipients) {
        numberOfOutstandingTests -= recipients.size();
        return recipients;
    }
}
