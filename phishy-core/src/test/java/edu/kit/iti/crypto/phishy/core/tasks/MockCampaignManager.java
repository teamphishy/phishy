package edu.kit.iti.crypto.phishy.core.tasks;

import edu.kit.iti.crypto.phishy.data.campaign.Campaign;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignDescription;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignManager;
import edu.kit.iti.crypto.phishy.data.statistics.CampaignStatisticsData;
import edu.kit.iti.crypto.phishy.data.statistics.CumulatedStatisticsData;
import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MockCampaignManager implements CampaignManager {

    String[] campaignNames;
    int[] recipSizes;
    String singleCampaign;
    int singleSize;
    List<Campaign> campaigns = new ArrayList<Campaign>();

    public MockCampaignManager(DataSource dataSource) {

    }

    @Override
    public Campaign getCampaign(CampaignDescription name) throws NotFoundException {
        if (name.getName().equals("SingleCampaign")) {
            MockCampaign camp = new MockCampaign();
            camp.name = "SingleCampaign";
            camp.recipSize = singleSize;
            return camp;
        }
        for (int i = 0; i < campaigns.size(); i++) {
            if (name.getName().equals(campaigns.get(i).getName())) {
                return campaigns.get(i);
            }
        }
        throw new NotFoundException();
    }

    @Override
    public boolean hasCampaign(CampaignDescription name) {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public Collection<Campaign> listCampaigns() {
        return campaigns;
    }

    @Override
    public Campaign createCampaign(String name) throws CreationException {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public Campaign createCampaign(String name, int recipientSetSize, int mailLifetime, int mailInterval) throws CreationException {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public void deleteCampaign(CampaignDescription name) throws DeletionException {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public CumulatedStatisticsData getCumulatedStatistics() {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    @Override
    public Collection<CampaignStatisticsData> getCampaignStatisticsData() {
        throw new UnsupportedOperationException("Operation not supported on Mock");
    }

    public void createCampaigns() {
        for (int i = 0; i < campaignNames.length; i++) {
            MockCampaign camp = new MockCampaign();
            camp.name = campaignNames[i];
            camp.recipSize = recipSizes[i];
            camp.numberOfOutstandingTests = recipSizes[i];
            campaigns.add(camp);
        }

        MockCampaign campFail = new MockCampaign();
        campFail.name = "FailCampaign";
        campFail.recipSize = 200;
        campFail.numberOfOutstandingTests = 0;
        campaigns.add(campFail);

        MockCampaign campFail2 = new MockCampaign();
        campFail2.name = "FailCampaign2";
        campFail2.recipSize = 300;
        campFail2.numberOfOutstandingTests = 0;
        campaigns.add(campFail2);
    }
}
