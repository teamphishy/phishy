package edu.kit.iti.crypto.phishy.core.api;

import edu.kit.iti.crypto.phishy.data.ManagerFactory;
import edu.kit.iti.crypto.phishy.data.template.TemplateDescription;
import edu.kit.iti.crypto.phishy.data.template.TemplateManager;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import hthurow.tomcatjndi.TomcatJNDI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.json.*;
import javax.servlet.ServletException;
import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Paths;

import static org.junit.Assert.*;

public class APITemplateRequestHandlerTest {

    TomcatJNDI tomcatJNDI = new TomcatJNDI();

    @Before
    public void setup() throws URISyntaxException {

        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("context.xml").toURI()).toFile());
        tomcatJNDI.start();
        ManagerFactory.reset();

    }

    @After
    public void teardown() {

        tomcatJNDI.tearDown();

    }

    private Reader sendRequest(MockServletRequest request) throws IOException, ServletException {

        PipedReader responseReader = new PipedReader();
        MockServletResponse response = new MockServletResponse(new PipedWriter(responseReader));

        APIServlet servlet = new APIServlet();
        servlet.service(request, response);

        response.getWriter().close();

        return responseReader;

    }

    @Test
    public void testListGroups() throws IOException, ServletException {

        MockServletRequest request = MockServletRequest.newGetRequest("/template");
        Reader responseReader = sendRequest(request);
        JsonReader jsonReader = Json.createReader(responseReader);
        JsonObject jsonObject = jsonReader.readObject();

        JsonArray jsonGroups = jsonObject.getJsonArray("groups");
        JsonArray jsonTemplates = jsonObject.getJsonArray("templates");

        TemplateManager manager = ManagerFactory.getTemplateManager();
        assert manager != null;

        assertEquals(manager.listGroups().size(), jsonGroups.size());

        for (JsonValue jsonGroup : jsonGroups) {
            assertEquals(JsonValue.ValueType.OBJECT, jsonGroup.getValueType());
            JsonObject groupObj = (JsonObject) jsonGroup;
            assertTrue(manager.hasGroup(groupObj.getString("id")));
        }

        for (JsonValue jsonTemplate : jsonTemplates) {
            assertEquals(JsonValue.ValueType.OBJECT, jsonTemplate.getValueType());
            JsonObject templateObj = (JsonObject) jsonTemplate;
            assertTrue(manager.hasTemplate(TemplateDescription.fromFQN(templateObj.getString("id"))));
        }

    }

    @Test
    public void testCreateGroup() throws IOException, ServletException {

        TemplateManager manager = ManagerFactory.getTemplateManager();
        assert manager != null;

        assertFalse(manager.hasGroup("new"));

        MockServletRequest request = MockServletRequest
                .newPostRequest("/template/new", 2, new ByteArrayInputStream("{}".getBytes()), "application/json");

        sendRequest(request);

        assertTrue(manager.hasGroup("new"));

    }

    @Test
    public void testDeleteGroup() throws IOException, ServletException {

        TemplateManager manager = ManagerFactory.getTemplateManager();
        assert manager != null;

        assertTrue(manager.hasGroup("A"));

        MockServletRequest request = MockServletRequest.newDeleteRequest("/template/A");
        sendRequest(request);

        assertFalse(manager.hasGroup("A"));

    }

    @Test
    public void testListTemplatesOfGroup() throws IOException, ServletException, NotFoundException {

        MockServletRequest request = MockServletRequest.newGetRequest("/template/A");

        Reader responseReader = sendRequest(request);
        JsonReader jsonReader = Json.createReader(responseReader);
        JsonArray jsonArray = jsonReader.readArray();

        TemplateManager manager = ManagerFactory.getTemplateManager();
        assert manager != null;

        for (JsonValue jsonTemplate : jsonArray) {
            assertEquals(JsonValue.ValueType.OBJECT, jsonTemplate.getValueType());
            JsonObject templateObj = (JsonObject) jsonTemplate;
            assertTrue(manager.hasTemplate(TemplateDescription.fromFQN(templateObj.getString("id"))));
        }


    }


}