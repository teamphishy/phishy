package edu.kit.iti.crypto.phishy.core.api.mock;

import edu.kit.iti.crypto.phishy.data.statistics.CampaignStatisticsData;

public class MockCampaignStatisticsData implements CampaignStatisticsData {

    private String name;
    private int sentMails;
    private int reportedMails;
    private int phishedMails;

    public MockCampaignStatisticsData(String name, int sentMails, int reportedMails, int phishedMails) {
        this.name = name;
        this.sentMails = sentMails;
        this.reportedMails = reportedMails;
        this.phishedMails = phishedMails;
    }

    @Override
    public String getCampaignName() {
        return name;
    }

    @Override
    public void increaseSentMails(int amount) {
        sentMails += amount;
    }

    @Override
    public void increaseReportedMail(int amount) {
        reportedMails += amount;
    }

    @Override
    public void increasePhishedMail(int amount) {
        phishedMails += amount;
    }

    @Override
    public int getSentMails() {
        return sentMails;
    }

    @Override
    public int getReportedMails() {
        return reportedMails;
    }

    @Override
    public int getPhishedMails() {
        return phishedMails;
    }
}
