package edu.kit.iti.crypto.phishy.api;


import edu.kit.iti.crypto.phishy.core.api.APIResponseJsonObject;
import edu.kit.iti.crypto.phishy.core.api.HttpResponseType;
import org.junit.Assert;
import org.junit.Test;

import javax.json.*;

public class APIResponseJsonObjectTest {
    @Test
    public void getBooleanTest() {
        JsonObjectBuilder builder = Json.createObjectBuilder();

        builder.add("a", true);
        builder.add("b", false);
        JsonObject baseObject = builder.build();
        APIResponseJsonObject object = new APIResponseJsonObject(HttpResponseType.BAD_REQUEST, baseObject);

        Assert.assertEquals(object.getBoolean("a"), baseObject.getBoolean("a"));
        Assert.assertEquals(object.getBoolean("b"), baseObject.getBoolean("b"));
        Assert.assertEquals(object.getBoolean("c", true), true);
    }

    @Test
    public void getStringTest() {
        JsonObjectBuilder builder = Json.createObjectBuilder();

        builder.add("a", "Message1");
        builder.add("b", "Message2");
        JsonObject baseObject = builder.build();
        APIResponseJsonObject object = new APIResponseJsonObject(HttpResponseType.BAD_REQUEST, baseObject);

        Assert.assertEquals(object.getString("a"), baseObject.getString("a"));
        Assert.assertEquals(object.getString("b"), baseObject.getString("b"));
        Assert.assertEquals(object.getString("c", "Nothing"), "Nothing");
    }

    @Test
    public void getIntTest() {
        JsonObjectBuilder builder = Json.createObjectBuilder();

        builder.add("a", 1);
        builder.add("b", 2);
        JsonObject baseObject = builder.build();
        APIResponseJsonObject object = new APIResponseJsonObject(HttpResponseType.BAD_REQUEST, baseObject);

        Assert.assertEquals(object.getInt("a"), baseObject.getInt("a"));
        Assert.assertEquals(object.getInt("b"), baseObject.getInt("b"));
        Assert.assertEquals(object.getInt("c", 42), 42);
    }

    @Test
    public void isNullTest() {
        JsonObjectBuilder builder = Json.createObjectBuilder();

        builder.add("a", "String");
        builder.add("b", JsonValue.NULL);
        JsonObject baseObject = builder.build();
        APIResponseJsonObject object = new APIResponseJsonObject(HttpResponseType.BAD_REQUEST, baseObject);

        Assert.assertEquals(object.isNull("a"), baseObject.isNull("a"));
        Assert.assertEquals(object.isNull("b"), baseObject.isNull("b"));
    }

    @Test
    public void sizeTest() {
        JsonObjectBuilder builder = Json.createObjectBuilder();

        builder.add("a", "String");
        builder.add("b", JsonValue.NULL);
        JsonObject baseObject = builder.build();
        APIResponseJsonObject object = new APIResponseJsonObject(HttpResponseType.BAD_REQUEST, baseObject);

        Assert.assertEquals(object.size(), baseObject.size());
    }

    @Test
    public void isEmptyTest() {
        JsonObjectBuilder builder = Json.createObjectBuilder();

        builder.add("a", "String");
        builder.add("b", JsonValue.NULL);
        JsonObject baseObject = builder.build();
        APIResponseJsonObject object = new APIResponseJsonObject(HttpResponseType.BAD_REQUEST, baseObject);

        Assert.assertFalse(object.isEmpty());
    }

    @Test
    public void isEmptyTrueTest() {
        JsonObjectBuilder builder = Json.createObjectBuilder();

        JsonObject baseObject = builder.build();
        APIResponseJsonObject object = new APIResponseJsonObject(HttpResponseType.BAD_REQUEST, baseObject);

        Assert.assertTrue(object.isEmpty());
    }
}
