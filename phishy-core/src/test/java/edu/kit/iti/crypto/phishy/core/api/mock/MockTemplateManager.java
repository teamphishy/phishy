package edu.kit.iti.crypto.phishy.core.api.mock;

import edu.kit.iti.crypto.phishy.data.template.Template;
import edu.kit.iti.crypto.phishy.data.template.TemplateDescription;
import edu.kit.iti.crypto.phishy.data.template.TemplateGroup;
import edu.kit.iti.crypto.phishy.data.template.TemplateManager;
import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.eclipse.jgit.transport.resolver.RepositoryResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MockTemplateManager implements TemplateManager {

    private List<TemplateGroup> groups = new ArrayList<>();

    public MockTemplateManager() {

        this.groups.add(new MockTemplateGroup("A"));
        this.groups.add(new MockTemplateGroup("B"));
    }

    @Override
    public TemplateGroup getGroup(String name) throws NotFoundException {
        for (TemplateGroup g : groups) {
            if (name.equals(g.getName())) {
                return g;
            }
        }

        throw new NotFoundException();
    }

    @Override
    public boolean hasGroup(String name) {

        for (TemplateGroup g : groups) {
            if (name.equals(g.getName())) {
                return true;
            }
        }

        return false;
    }

    @Override
    public TemplateGroup createGroup(String name) throws CreationException {
        TemplateGroup group = new MockTemplateGroup(name);
        this.groups.add(group);
        return group;
    }

    @Override
    public void deleteGroup(String name) throws DeletionException {
        try {
            this.groups.remove(this.getGroup(name));
        } catch (NotFoundException e) {
            throw new DeletionException();
        }
    }

    @Override
    public Collection<TemplateGroup> listGroups() {
        return groups;
    }

    @Override
    public Template getTemplate(TemplateDescription description) throws NotFoundException {

        TemplateGroup group = getGroup(description.getGroup());
        return group.getTemplate(description);

    }

    @Override
    public boolean hasTemplate(TemplateDescription description) {

        try {
            TemplateGroup group = getGroup(description.getGroup());
            return group.hasTemplate(description);
        } catch (NotFoundException ex) {
            return false;
        }

    }

    @Override
    public RepositoryResolver<HttpServletRequest> getGitResolver() {
        throw new UnsupportedOperationException("Not supported by mock");
    }

    @Override
    public void invalidateCache() {
        throw new UnsupportedOperationException("Not supported by mock");
    }
}
