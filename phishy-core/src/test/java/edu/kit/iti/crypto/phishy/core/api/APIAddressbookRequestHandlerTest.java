package edu.kit.iti.crypto.phishy.core.api;

import edu.kit.iti.crypto.phishy.data.ManagerFactory;
import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookDescription;
import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookManager;
import hthurow.tomcatjndi.TomcatJNDI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.json.*;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.net.URISyntaxException;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class APIAddressbookRequestHandlerTest {

    TomcatJNDI tomcatJNDI = new TomcatJNDI();

    @Before
    public void setup() throws URISyntaxException {

        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("context.xml").toURI()).toFile());
        tomcatJNDI.start();
        ManagerFactory.reset();

    }

    @After
    public void teardown() {

        tomcatJNDI.tearDown();

    }

    private AddressbookManager getAddressbookManager() {
        return ManagerFactory.getAddressbookManager();
    }

    @Test
    public void testListAddressbooks() throws ServletException, IOException {

        MockServletRequest request = MockServletRequest.newGetRequest("/addressbook");
        PipedReader responseReader = new PipedReader();
        MockServletResponse response = new MockServletResponse(new PipedWriter(responseReader));

        APIServlet servlet = new APIServlet();
        servlet.service(request, response);

        JsonReader jsonReader = Json.createReader(responseReader);
        JsonArray responseArray = jsonReader.readArray();

        AddressbookManager abManager = getAddressbookManager();
        assert abManager != null;

        for (JsonValue item : responseArray) {

            assertEquals(JsonValue.ValueType.STRING, item.getValueType());
            JsonString jsonString = (JsonString) item;
            assertTrue(abManager.hasAddressbook(new AddressbookDescription(jsonString.getString())));

        }


    }


}