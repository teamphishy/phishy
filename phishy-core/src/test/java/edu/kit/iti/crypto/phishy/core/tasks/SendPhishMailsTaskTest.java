package edu.kit.iti.crypto.phishy.core.tasks;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.RSAKeyProvider;
import com.icegreen.greenmail.util.DummySSLSocketFactory;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import edu.kit.iti.crypto.phishy.core.JWTKeyProvider;
import edu.kit.iti.crypto.phishy.data.ManagerFactory;
import edu.kit.iti.crypto.phishy.data.addressbook.Addressbook;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignDescription;
import hthurow.tomcatjndi.TomcatJNDI;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import javax.mail.Message;
import javax.mail.MessagingException;
import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.security.Security;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SendPhishMailsTaskTest {

    final RSAKeyProvider rsaKeyProvider = new JWTKeyProvider();
    TomcatJNDI tomcatJNDI = new TomcatJNDI();
    ServerSetup setup;
    GreenMail greenMail;

    static final int CAMPAIAGN_COUNT = 3;
    String[] campaignNames = new String[CAMPAIAGN_COUNT];
    int[] recipSizes = new int[CAMPAIAGN_COUNT];
    int amountOfMails = 0;

    String singleCampaign = "SingleCampaign";
    int singleSize = 100;
    Algorithm tokenAlgorithm;

    @Before
    public void setUp() throws Exception {
        tokenAlgorithm = Algorithm.RSA256(rsaKeyProvider);
        Security.setProperty("ssl.SocketFactory.provider", DummySSLSocketFactory.class.getName());


    }

    private void setServer() {
        setup = new ServerSetup(48621, "localhost", "smtps");
        greenMail = new GreenMail(setup);
        greenMail.setUser("user1@mail.com", "admin", "password");
        greenMail.start();
    }

    private void setCampaignManager() {
        MockCampaignManager mockCampMan = (MockCampaignManager) ManagerFactory.getCampaignManager();

        for (int i = 0; i < CAMPAIAGN_COUNT; i++) {
            campaignNames[i] = "Campaign" + i;
            int recipientSize = (int) (Math.random() * 300);
            recipSizes[i] = recipientSize;
            amountOfMails += recipientSize;
        }

        mockCampMan.campaignNames = campaignNames;
        mockCampMan.recipSizes = recipSizes;

        //add single Campaign
        mockCampMan.singleCampaign = singleCampaign;
        mockCampMan.singleSize = singleSize;

        mockCampMan.createCampaigns();
    }


    private void setAddressbookManager() {
        MockAddressbookManager manager = (MockAddressbookManager) ManagerFactory.getAddressbookManager();
        Addressbook testAddrBook = new MockAddressbook("testAddrBook");
        manager.addAddressbook(testAddrBook);
    }

    @After
    public void tearDown() throws Exception {
        tomcatJNDI.tearDown();
        greenMail.stop();
    }

    @AfterClass
    public static void tearDownClass() {
        File privateKey = new File("../private.key");
        privateKey.delete();
        File publicKey = new File("../public.key.pub");
        publicKey.delete();
    }


    @Test
    public void testAllCampaigns() throws InterruptedException, MessagingException, URISyntaxException {
        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("context.xml").toURI()).toFile());
        tomcatJNDI.start();

        setServer();
        ManagerFactory.reset();
        setCampaignManager();
        setAddressbookManager();


        SendPhishMailsTask sendTask = new SendPhishMailsTask();
        Thread sendThread = new Thread(sendTask);
        sendThread.start();

        sendThread.join();

        Message[] messages = greenMail.getReceivedMessages();
        assertEquals(amountOfMails, messages.length);

        JWTVerifier verifier = JWT.require(tokenAlgorithm).build();
        HashSet<String> ids = new HashSet<String>();
        for (Message message : messages) {
            DecodedJWT decodedToken = verifier.verify(message.getHeader("X-Phishy-ID")[0]);
            assertTrue(ids.add(decodedToken.getClaim("nonce").asString()));
        }
        assertEquals(amountOfMails, ids.size());
    }

    @Test
    public void testSingleCampaign() throws InterruptedException, MessagingException, URISyntaxException {
        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("context.xml").toURI()).toFile());
        tomcatJNDI.start();

        setServer();
        ManagerFactory.reset();
        setCampaignManager();
        setAddressbookManager();


        SendPhishMailsTask sendTask = new SendPhishMailsTask(new CampaignDescription(singleCampaign));
        Thread sendThread = new Thread(sendTask);
        sendThread.start();
        sendThread.join();

        Message[] messages = greenMail.getReceivedMessages();
        assertEquals(singleSize, messages.length);

        JWTVerifier verifier = JWT.require(tokenAlgorithm).build();
        HashSet<String> ids = new HashSet<String>();
        for (Message message : messages) {
            DecodedJWT decodedToken = verifier.verify(message.getHeader("X-Phishy-ID")[0]);
            assertTrue(ids.add(decodedToken.getClaim("nonce").asString()));
        }
        assertEquals(singleSize, ids.size());
    }

    @Test
    public void testMissingSessionAllCampaigns() throws InterruptedException, URISyntaxException {
        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("contextFail.xml").toURI()).toFile());
        tomcatJNDI.start();

        setServer();
        ManagerFactory.reset();
        setCampaignManager();
        setAddressbookManager();


        SendPhishMailsTask sendTask = new SendPhishMailsTask();
        Thread sendThread = new Thread(sendTask);
        sendThread.start();

        sendThread.join();

        Message[] messages = greenMail.getReceivedMessages();
        assertEquals(0, messages.length);
    }

    @Test
    public void testMissingSessionSingleCampaign() throws URISyntaxException, InterruptedException {
        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("contextFail.xml").toURI()).toFile());
        tomcatJNDI.start();

        setServer();
        ManagerFactory.reset();
        setCampaignManager();
        setAddressbookManager();


        SendPhishMailsTask sendTask = new SendPhishMailsTask(new CampaignDescription(singleCampaign));
        Thread sendThread = new Thread(sendTask);
        sendThread.start();
        sendThread.join();

        Message[] messages = greenMail.getReceivedMessages();
        assertEquals(0, messages.length);

    }

    @Test
    public void testFailCampaignSingleCampaign() throws URISyntaxException, InterruptedException {
        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("context.xml").toURI()).toFile());
        tomcatJNDI.start();

        setServer();
        ManagerFactory.reset();
        setCampaignManager();
        setAddressbookManager();

        SendPhishMailsTask sendTask = new SendPhishMailsTask(new CampaignDescription("FailCampaign3"));
        Thread sendThread = new Thread(sendTask);
        sendThread.start();
        sendThread.join();

        Message[] messages = greenMail.getReceivedMessages();
        assertEquals(0, messages.length);

    }

    @Test
    public void testFailTemplateCampaignSingleCampaign() throws URISyntaxException, InterruptedException {
        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("context.xml").toURI()).toFile());
        tomcatJNDI.start();

        setServer();
        ManagerFactory.reset();
        setCampaignManager();
        setAddressbookManager();

        SendPhishMailsTask sendTask = new SendPhishMailsTask(new CampaignDescription("FailCampaign2"));
        Thread sendThread = new Thread(sendTask);
        sendThread.start();
        sendThread.join();

        Message[] messages = greenMail.getReceivedMessages();
        assertEquals(0, messages.length);

    }


}
