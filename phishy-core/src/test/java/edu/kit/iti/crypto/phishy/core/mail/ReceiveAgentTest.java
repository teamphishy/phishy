package edu.kit.iti.crypto.phishy.core.mail;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import hthurow.tomcatjndi.TomcatJNDI;
import org.junit.After;
import org.junit.Test;

import java.net.URISyntaxException;
import java.nio.file.Paths;

public class ReceiveAgentTest {

    TomcatJNDI tomcatJNDI = new TomcatJNDI();
    GreenMail greenMailImap;

    @After
    public void tearDown() throws Exception {
        tomcatJNDI.tearDown();

        if (greenMailImap != null) {
            greenMailImap.stop();
        }
    }

    @Test(expected = IllegalStateException.class)
    public void testMissingReceiveSession() throws URISyntaxException {
        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("contextFail.xml").toURI()).toFile());
        tomcatJNDI.start();

        ReceiveAgent agent = new ReceiveAgent();
        agent.receiveMails();
    }

    @Test(expected = IllegalStateException.class)
    public void testWrongConnection() throws URISyntaxException {
        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("contextFail2.xml").toURI()).toFile());
        tomcatJNDI.start();

        ReceiveAgent agent = new ReceiveAgent();
        agent.receiveMails();
    }

    @Test(expected = IllegalStateException.class)
    public void testMissingSecurity() throws URISyntaxException {
        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("contextFail3.xml").toURI()).toFile());
        tomcatJNDI.start();

        ServerSetup setupImap = new ServerSetup(47661, "localhost", ServerSetup.PROTOCOL_IMAP);
        greenMailImap = new GreenMail(setupImap);
        greenMailImap.start();

        ReceiveAgent agent = new ReceiveAgent();
        agent.receiveMails();
    }
}
