package edu.kit.iti.crypto.phishy.core.mail;

import org.junit.Test;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.*;

public class MailSpoolTest {

    AtomicInteger counter = new AtomicInteger(0);

    @Test
    public void testMailSpoolWithThreads() throws InterruptedException, MessagingException {
        int messagesPerThread = 100;
        int threads = 10;
        int totalMessages = messagesPerThread * threads;
        Session session = null;
        MailSpool mailSpool = new MailSpool(totalMessages);
        Thread[] allGenerator = new Thread[threads];
        Thread[] sendAgents = new Thread[threads];
        MimeMessage[] messages = new MimeMessage[totalMessages];
        for (int i = 0; i < messages.length; i++) {
            messages[i] = new MimeMessage(session);
        }

        for (int i = 0; i < allGenerator.length; i++) {
            sendAgents[i] = new Thread(new Runnable() {

                @Override
                public void run() {
                    try {

                        int nextMailIndex = mailSpool.reserveNextMail();
                        if (nextMailIndex == -1) {
                            return;
                        }
                        boolean nextIndexValid;

                        while (nextIndexValid = mailSpool.isNextIndexPossible(nextMailIndex)) {

                            Thread.sleep(10);

                            while (nextIndexValid && mailSpool.isNextMailReady(nextMailIndex)) {
                                mailSpool.getMailToSend(nextMailIndex);
                                counter.getAndIncrement();

                                nextMailIndex = mailSpool.reserveNextMail();
                                if (nextMailIndex == -1) {
                                    break;
                                }

                                nextIndexValid = mailSpool.isNextIndexPossible(nextMailIndex);
                            }
                            if (nextMailIndex == -1) {
                                break;
                            }
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            sendAgents[i].start();
        }


        for (int i = 0; i < allGenerator.length; i++) {
            final int threadIndex = i;
            allGenerator[i] = new Thread(new Runnable() {

                @Override
                public void run() {
                    for (int j = 0; j < messagesPerThread; j++) {
                        MimeMessage message = messages[threadIndex * messagesPerThread + j];
                        try {
                            message.addHeader("testID", "" + (threadIndex * messagesPerThread + j));
                            message.addFrom(new InternetAddress[]{new InternetAddress()});
                            message.addRecipients(RecipientType.TO, new InternetAddress[]{new InternetAddress()});
                        } catch (MessagingException e) {
                            e.printStackTrace();
                        }
                        mailSpool.addMailToSend(message);
                    }
                }
            });
            allGenerator[i].start();
        }

        for (Thread generator : allGenerator) {
            generator.join();
        }
        mailSpool.notifyLastMailAdded();

        for (Thread agent : sendAgents) {
            agent.join();
        }

        assertEquals(totalMessages, counter.get());

        HashSet<String> ids = new HashSet<String>();
        for (MimeMessage mimeMessage : messages) {
            assertTrue(ids.add(mimeMessage.getHeader("testID")[0]));
        }
    }

    @Test
    public void simpleTestMailSpool() throws MessagingException {
        Session session = null;
        MailSpool mailSpool = new MailSpool(2);

        int index = mailSpool.reserveNextMail();
        assertTrue(mailSpool.isNextIndexPossible(index));
        assertFalse(mailSpool.isNextMailReady(index));

        mailSpool.addMailToSend(new MimeMessage(session));
        assertTrue(mailSpool.isNextIndexPossible(index));
        assertTrue(mailSpool.isNextMailReady(index));

        mailSpool.addMailToSend(new MimeMessage(session));
        mailSpool.notifyLastMailAdded();

        assertTrue(mailSpool.isNextIndexPossible(index));
        assertTrue(mailSpool.isNextMailReady(index));
        mailSpool.getMailToSend(index);

        index = mailSpool.reserveNextMail();
        assertTrue(mailSpool.isNextIndexPossible(index));
        assertTrue(mailSpool.isNextMailReady(index));
        mailSpool.getMailToSend(index);

        assertEquals(-1, mailSpool.reserveNextMail());
    }
}
