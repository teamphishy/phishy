package edu.kit.iti.crypto.phishy.core.tasks;

import edu.kit.iti.crypto.phishy.data.ManagerFactory;
import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookDescription;
import hthurow.tomcatjndi.TomcatJNDI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.net.URISyntaxException;
import java.nio.file.Paths;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class UpdateAddressbookTaskTest {

    static final String JNDI_ABMAN_NAME = "java:comp/env/comp/addressbookManager";
    MockAddressbook addressbookA;
    MockAddressbook addressbookB;

    TomcatJNDI tomcatJNDI = new TomcatJNDI();

    @Before
    public void setup() throws URISyntaxException, NamingException {


        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("context.xml").toURI()).toFile());
        tomcatJNDI.start();

        InitialContext ctx = new InitialContext();


        addressbookA = new MockAddressbook("A");
        addressbookB = new MockAddressbook("B");
        ManagerFactory.reset();
        MockAddressbookManager manager = (MockAddressbookManager) ManagerFactory.getAddressbookManager();
        manager.addAddressbook(addressbookA);
        manager.addAddressbook(addressbookB);

    }

    @After
    public void teardown() {

        tomcatJNDI.tearDown();

    }

    @Test
    public void testUpdateAll() {

        assertFalse(addressbookA.getWasUpdated());
        assertFalse(addressbookB.getWasUpdated());

        UpdateAddressbookTask task = new UpdateAddressbookTask();
        task.run();

        assertTrue(addressbookA.getWasUpdated());
        assertTrue(addressbookB.getWasUpdated());

    }

    @Test
    public void testUpdateSingle() {

        assertFalse(addressbookA.getWasUpdated());
        assertFalse(addressbookB.getWasUpdated());

        UpdateAddressbookTask task = new UpdateAddressbookTask(new AddressbookDescription("A"));
        task.run();

        assertTrue(addressbookA.getWasUpdated());
        assertFalse(addressbookB.getWasUpdated());

    }

}