package edu.kit.iti.crypto.phishy.scheduling;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

public class MockScheduledTask extends ScheduledTask {

    public MockScheduledTask(Runnable thread, Collection<Weekday> weekdays, Date time, Map<String, String> parameters) {
        super(thread, weekdays, time, parameters);
    }


}