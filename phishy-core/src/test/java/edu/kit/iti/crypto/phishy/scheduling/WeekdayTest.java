package edu.kit.iti.crypto.phishy.scheduling;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class WeekdayTest {

    @Test
    public void testWeekdayAll() {
        assertArrayEquals(new Weekday[]{Weekday.MONDAY, Weekday.TUESDAY, Weekday.WEDNESDAY,
                        Weekday.THURSDAY, Weekday.FRIDAY, Weekday.SATURDAY, Weekday.SUNDAY},
                Weekday.fromString("monday,tuesday,wednesday,thursday,friday,saturday,sunday").toArray());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalDay() {
        Weekday.fromString("Faultier");
    }

    @Test
    public void testWeekdayList() {

        assertArrayEquals(new Weekday[]{Weekday.MONDAY, Weekday.FRIDAY},
                Weekday.fromString("monday,friday").toArray());

    }

    @Test
    public void testWeekend() {

        assertArrayEquals(new Weekday[]{Weekday.SATURDAY, Weekday.SUNDAY},
                Weekday.fromString("weekend").toArray());

    }

    @Test
    public void testWorkday() {

        assertArrayEquals(new Weekday[]{Weekday.MONDAY, Weekday.TUESDAY, Weekday.WEDNESDAY,
                        Weekday.THURSDAY, Weekday.FRIDAY},
                Weekday.fromString("workday").toArray());

    }

    @Test
    public void testWildcard() {

        assertArrayEquals(new Weekday[]{Weekday.MONDAY, Weekday.TUESDAY, Weekday.WEDNESDAY,
                        Weekday.THURSDAY, Weekday.FRIDAY, Weekday.SATURDAY, Weekday.SUNDAY},
                Weekday.fromString("*").toArray());

    }

    @Test
    public void singleDayFromInt() {
        assertEquals(Weekday.MONDAY, Weekday.singleDayFromInt(1));
        assertEquals(Weekday.TUESDAY, Weekday.singleDayFromInt(2));
        assertEquals(Weekday.WEDNESDAY, Weekday.singleDayFromInt(3));
        assertEquals(Weekday.THURSDAY, Weekday.singleDayFromInt(4));
        assertEquals(Weekday.FRIDAY, Weekday.singleDayFromInt(5));
        assertEquals(Weekday.SATURDAY, Weekday.singleDayFromInt(6));
        assertEquals(Weekday.SUNDAY, Weekday.singleDayFromInt(0));
    }

    @Test
    public void singleDayFromInt1() {
        try {
            Weekday weekday = Weekday.singleDayFromInt(12);
        } catch (IllegalArgumentException e) {
            assertEquals("Invalid weekday", e.getMessage());
        }

    }


}