package edu.kit.iti.crypto.phishy.core.api.mock;

import edu.kit.iti.crypto.phishy.data.addressbook.Addressbook;
import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookDescription;
import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookManager;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import java.util.Collection;
import java.util.List;

public class MockAddressbookManager implements AddressbookManager {

    private Collection<Addressbook> addressbooks;

    public MockAddressbookManager() {

        addressbooks = List.of(new MockAddressbook("a"), new MockAddressbook("b"));

    }

    @Override
    public Collection<Addressbook> listAddressbooks() {
        return addressbooks;
    }

    @Override
    public boolean hasAddressbook(AddressbookDescription description) {

        for (Addressbook a : addressbooks) {
            if (description.getName().equals(a.getName())) {
                return true;
            }
        }

        return false;
    }

    @Override
    public Addressbook getAddressbook(AddressbookDescription description) throws NotFoundException {
        for (Addressbook a : addressbooks) {
            if (description.getName().equals(a.getName())) {
                return a;
            }
        }
        throw new NotFoundException();
    }
}
