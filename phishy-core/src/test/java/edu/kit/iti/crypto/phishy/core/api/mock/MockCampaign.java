package edu.kit.iti.crypto.phishy.core.api.mock;

import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookDescription;
import edu.kit.iti.crypto.phishy.data.addressbook.Recipient;
import edu.kit.iti.crypto.phishy.data.campaign.*;
import edu.kit.iti.crypto.phishy.data.statistics.CampaignStatisticsData;
import edu.kit.iti.crypto.phishy.data.template.TemplateDescription;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MockCampaign extends Campaign {

    private String name;
    private int recipientSetSize = 0;
    private int mailLifetime = 0;
    private CampaignStatus status = CampaignStatus.INACTIVE;
    private AddressbookDescription addressbook = null;
    private Map<TemplateType, TemplateDescription> templates = new HashMap<>();
    private int mailInterval = 30;


    public MockCampaign(String name) {
        this.name = name;
    }

    public MockCampaign(String name, int recipientSetSize, int mailLifetime,
                        CampaignStatus status,
                        AddressbookDescription addressbook, int mailInterval) {
        this.name = name;
        this.recipientSetSize = recipientSetSize;
        this.mailLifetime = mailLifetime;
        this.status = status;
        this.addressbook = addressbook;
        this.mailInterval = mailInterval;
    }

    @Override
    public void linkTemplate(TemplateType type, TemplateDescription description) {
        templates.put(type, description);
    }

    @Override
    public void unlinkTemplate(TemplateType type) {
        templates.remove(type);
    }

    @Override
    public TemplateDescription getTemplate(TemplateType type) throws NotFoundException {
        if (templates.containsKey(type)) {
            return templates.get(type);
        } else {
            throw new NotFoundException();
        }
    }

    @Override
    public boolean hasTemplate(TemplateType type) {
        return templates.containsKey(type);
    }

    @Override
    public CampaignStatisticsData getStatistics() {

        return new MockCampaignStatisticsData(getName(), 20, 10, 5);

    }

    @Override
    public OutstandingTest getOutstandingTest(String id) throws NotFoundException {
        throw new UnsupportedOperationException("Not supported on this mock");
    }

    @Override
    public boolean hasOutstandingTest(String id) {
        throw new UnsupportedOperationException("Not supported on this mock");
    }

    @Override
    public AddressbookDescription getAddressbook() throws NotFoundException {
        if (addressbook == null) {
            throw new NotFoundException();
        } else {
            return addressbook;
        }
    }

    @Override
    public void setAddressbook(AddressbookDescription description) {
        addressbook = description;
    }

    @Override
    public void recordTests(Collection<TestDescription> tests) {
        throw new UnsupportedOperationException("Not supported on this mock");
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getRecipientSetSize() {
        return recipientSetSize;
    }

    @Override
    public void setRecipientSetSize(int recipientSetSize) {
        this.recipientSetSize = recipientSetSize;
    }

    @Override
    public CampaignStatus getStatus() {
        return status;
    }

    @Override
    public void setStatus(CampaignStatus status) {
        this.status = status;
    }

    @Override
    public int getMailLifetime() {
        return mailLifetime;
    }

    @Override
    public void setMailLifetime(int mailLifetime) {
        this.mailLifetime = mailLifetime;
    }

    @Override
    public int getMailInterval() {
        return mailInterval;
    }

    @Override
    public void setMailInterval(int mailInterval) {
        this.mailInterval = mailInterval;
    }

    @Override
    public List<Recipient> deleteTests(List<Recipient> recipients) {
        throw new UnsupportedOperationException("Not supported on this mock");

    }
}
