package edu.kit.iti.crypto.phishy.core.api.mock;

import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookDescription;
import edu.kit.iti.crypto.phishy.data.campaign.Campaign;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignDescription;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignManager;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignStatus;
import edu.kit.iti.crypto.phishy.data.statistics.CampaignStatisticsData;
import edu.kit.iti.crypto.phishy.data.statistics.CumulatedStatisticsData;
import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class MockCampaignManager implements CampaignManager {

    private List<Campaign> campaigns = new ArrayList<>();

    public MockCampaignManager() {
        this.campaigns.add(new MockCampaign("A"));
        this.campaigns.add(new MockCampaign("B", 100, 30,
                CampaignStatus.ACTIVE, new AddressbookDescription("a"), 50));
    }

    @Override
    public Campaign getCampaign(CampaignDescription name) throws NotFoundException {

        assert name != null;
        for (Campaign campaign : campaigns) {
            if (name.equals(new CampaignDescription(campaign.getName()))) {
                return campaign;
            }
        }

        throw new NotFoundException();
    }

    @Override
    public boolean hasCampaign(CampaignDescription name) {
        assert name != null;
        for (Campaign campaign : campaigns) {
            if (name.equals(new CampaignDescription(campaign.getName()))) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Collection<Campaign> listCampaigns() {
        return campaigns;
    }

    @Override
    public Campaign createCampaign(String name) throws CreationException {

        if (!name.matches("[a-zA-Z][a-zA-Z0-9\\-_]*")) {
            throw new CreationException();
        }

        Campaign campaign = new MockCampaign(name);
        this.campaigns.add(campaign);

        return campaign;
    }

    @Override
    public Campaign createCampaign(String name, int recipientSetSize, int mailLifetime, int mailInterval) throws CreationException {

        Campaign campaign = createCampaign(name);

        campaign.setRecipientSetSize(recipientSetSize);
        campaign.setMailLifetime(mailLifetime);
        campaign.setMailInterval(mailInterval);

        return campaign;
    }

    @Override
    public void deleteCampaign(CampaignDescription name) throws DeletionException {

        Campaign cDel = null;
        for (Campaign campaign : campaigns) {
            if (name.equals(new CampaignDescription(campaign.getName()))) {
                cDel = campaign;
            }
        }

        if (cDel == null) {
            throw new DeletionException();
        } else {
            campaigns.remove(cDel);
        }

    }

    @Override
    public CumulatedStatisticsData getCumulatedStatistics() {

        var context = new Object() {
            int sentMails = 0;
            int reportedMails = 0;
            int phishedMails = 0;
        };


        listCampaigns().stream().map(Campaign::getStatistics).forEach(d -> {
            context.sentMails += d.getSentMails();
            context.reportedMails += d.getReportedMails();
            context.phishedMails += d.getPhishedMails();
        });

        return new MockCumulatedStatisticsData(context.sentMails, context.reportedMails, context.phishedMails);

    }

    @Override
    public Collection<CampaignStatisticsData> getCampaignStatisticsData() {
        return listCampaigns().stream().map(c -> {
            return c.getStatistics();
        }).collect(Collectors.toList());
    }
}
