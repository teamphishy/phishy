package edu.kit.iti.crypto.phishy.core.api.mock;

import edu.kit.iti.crypto.phishy.data.addressbook.Addressbook;
import edu.kit.iti.crypto.phishy.data.addressbook.Recipient;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public class MockAddressbook implements Addressbook {

    private String name;

    public MockAddressbook(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Recipient> getRandomRecipients(int num, Date notTestedSince) {
        return List.of();
    }

    @Override
    public boolean checkCredentials(String name, String password) {
        return false;
    }

    @Override
    public void recordTest(Collection<Recipient> recipients) {

    }

    @Override
    public void updateCache() {

    }

    @Override
    public Recipient completeRecipient(Recipient recipient) {
        return recipient;
    }
}
