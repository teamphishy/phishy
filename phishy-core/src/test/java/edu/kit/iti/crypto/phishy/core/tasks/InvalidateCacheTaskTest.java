package edu.kit.iti.crypto.phishy.core.tasks;

import edu.kit.iti.crypto.phishy.data.ManagerFactory;
import hthurow.tomcatjndi.TomcatJNDI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.net.URISyntaxException;
import java.nio.file.Paths;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class InvalidateCacheTaskTest {

    static final String JNDI_TEMPLATE_MANAGER_NAME = "java:comp/env/comp/templateManager";
    TomcatJNDI tomcatJNDI = new TomcatJNDI();

    @Before
    public void setup() throws URISyntaxException {

        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("context.xml").toURI()).toFile());
        tomcatJNDI.start();

    }

    @After
    public void teardown() {
        tomcatJNDI.tearDown();
    }

    @Test
    public void testInvalidateCaches() throws NamingException {

        InitialContext ctx = new InitialContext();
        ManagerFactory.reset();
        MockTemplateManager manager = (MockTemplateManager) ManagerFactory.getTemplateManager();

        InvalidateCacheTask invalidateCacheTask = new InvalidateCacheTask();

        assertFalse(manager.isCacheInvalidated());

        invalidateCacheTask.run();

        assertTrue(manager.isCacheInvalidated());


    }

}