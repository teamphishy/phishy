package edu.kit.iti.crypto.phishy.core.api.mock;

import edu.kit.iti.crypto.phishy.data.template.Template;
import edu.kit.iti.crypto.phishy.data.template.TemplateDescription;
import edu.kit.iti.crypto.phishy.data.template.TemplateFile;
import edu.kit.iti.crypto.phishy.data.template.TemplateGroup;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

public class MockTemplate implements Template {

    private TemplateGroup group;
    private String name;

    public MockTemplate(String name, TemplateGroup group) {
        this.name = name;
        this.group = group;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public TemplateFile getFile(String path) throws NotFoundException {
        throw new UnsupportedOperationException("Not Supported by mock");
    }

    @Override
    public boolean hasFile(String path) {
        throw new UnsupportedOperationException("Not Supported by mock");
    }

    @Override
    public TemplateDescription getDescription() {
        return new TemplateDescription(group.getName(), this.getName());
    }
}
