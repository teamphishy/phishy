package edu.kit.iti.crypto.phishy.core;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class ModalLockTest {

    @Test(timeout = 1000L)
    public void testLocking() throws InterruptedException, ModalLock.ResourceLockedException {

        try (var successfulLease = ModalLock.getInstance().getLeaseOrFail()) {

            ModalLock.getInstance().returnLease(successfulLease);

            try (var anotherLease = ModalLock.getInstance().getLease()) {
                assertNotNull(anotherLease);
            }

        } catch (Exception e) {
            fail("Could not obtain successful lock");
        }

    }

}