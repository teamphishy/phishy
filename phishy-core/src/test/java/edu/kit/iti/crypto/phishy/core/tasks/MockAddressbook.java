package edu.kit.iti.crypto.phishy.core.tasks;

import edu.kit.iti.crypto.phishy.data.addressbook.Addressbook;
import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookDescription;
import edu.kit.iti.crypto.phishy.data.addressbook.Recipient;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class MockAddressbook implements Addressbook {

    private String name;
    private boolean wasUpdated = false;

    public MockAddressbook(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Recipient> getRandomRecipients(int num, Date notTestedSince) {
        List<Recipient> recipients = new ArrayList<Recipient>();
        for (int i = 0; i < num; i++) {
            Recipient recipient = new Recipient("ID-" + i);
            recipient.setMail("test" + i + "@mail.com");
            recipients.add(recipient);
        }
        return recipients;
    }

    @Override
    public boolean checkCredentials(String name, String password) {
        throw new UnsupportedOperationException("Not supported on mock");
    }

    @Override
    public void recordTest(Collection<Recipient> recipients) {
        //test not needed here
    }

    @Override
    public void updateCache() {
        this.wasUpdated = true;
    }

    @Override
    public Recipient completeRecipient(Recipient recipient) {
        throw new UnsupportedOperationException("Not supported on mock");
    }

    public AddressbookDescription getDescription() {
        return new AddressbookDescription(this.getName());
    }

    public boolean getWasUpdated() {
        return wasUpdated;
    }

}
