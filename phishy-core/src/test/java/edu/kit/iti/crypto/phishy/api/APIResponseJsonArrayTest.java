package edu.kit.iti.crypto.phishy.api;

import edu.kit.iti.crypto.phishy.core.api.APIResponseJsonArray;
import edu.kit.iti.crypto.phishy.core.api.HttpResponseType;
import org.junit.Assert;
import org.junit.Test;

import javax.json.*;


public class APIResponseJsonArrayTest {

    @Test
    public void getBooleanTest() {
        JsonArrayBuilder builder = Json.createArrayBuilder();

        builder.add(true);
        builder.add(false);
        JsonArray baseArray = builder.build();
        APIResponseJsonArray array = new APIResponseJsonArray(HttpResponseType.BAD_REQUEST, baseArray);

        Assert.assertEquals(array.getBoolean(0), baseArray.getBoolean(0));
        Assert.assertEquals(array.getBoolean(1), baseArray.getBoolean(1));
        Assert.assertEquals(array.getBoolean(2, true), true);
    }

    @Test
    public void getStringTest() {
        JsonArrayBuilder builder = Json.createArrayBuilder();

        builder.add("Message1");
        builder.add("Message2");
        JsonArray baseArray = builder.build();
        APIResponseJsonArray array = new APIResponseJsonArray(HttpResponseType.BAD_REQUEST, baseArray);

        Assert.assertEquals(array.getString(0), baseArray.getString(0));
        Assert.assertEquals(array.getString(1), baseArray.getString(1));
        Assert.assertEquals(array.getString(2, "Nothing"), "Nothing");
    }

    @Test
    public void getIntTest() {
        JsonArrayBuilder builder = Json.createArrayBuilder();

        builder.add(1);
        builder.add(2);
        JsonArray baseArray = builder.build();
        APIResponseJsonArray array = new APIResponseJsonArray(HttpResponseType.BAD_REQUEST, baseArray);

        Assert.assertEquals(array.getInt(0), baseArray.getInt(0));
        Assert.assertEquals(array.getInt(1), baseArray.getInt(1));
        Assert.assertEquals(array.getInt(2, 42), 42);
    }

    @Test
    public void isNullTest() {
        JsonArrayBuilder builder = Json.createArrayBuilder();

        builder.add("String");
        builder.add(JsonValue.NULL);
        JsonArray baseArray = builder.build();
        APIResponseJsonArray array = new APIResponseJsonArray(HttpResponseType.BAD_REQUEST, baseArray);

        Assert.assertEquals(array.isNull(0), baseArray.isNull(0));
        Assert.assertEquals(array.isNull(1), baseArray.isNull(1));
    }

    @Test
    public void sizeTest() {
        JsonArrayBuilder builder = Json.createArrayBuilder();

        builder.add("String");
        builder.add(JsonValue.NULL);
        JsonArray baseArray = builder.build();
        APIResponseJsonArray array = new APIResponseJsonArray(HttpResponseType.BAD_REQUEST, baseArray);

        Assert.assertEquals(array.size(), baseArray.size());
    }

    @Test
    public void isEmptyTest() {
        JsonArrayBuilder builder = Json.createArrayBuilder();

        builder.add("String");
        builder.add(JsonValue.NULL);
        JsonArray baseArray = builder.build();
        APIResponseJsonArray array = new APIResponseJsonArray(HttpResponseType.BAD_REQUEST, baseArray);

        Assert.assertFalse(array.isEmpty());
    }

    @Test
    public void isEmptyTrueTest() {
        JsonArrayBuilder builder = Json.createArrayBuilder();

        JsonArray baseArray = builder.build();
        APIResponseJsonArray array = new APIResponseJsonArray(HttpResponseType.BAD_REQUEST, baseArray);

        Assert.assertTrue(array.isEmpty());
    }

}
