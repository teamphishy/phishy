package edu.kit.iti.crypto.phishy.core.mail;

import com.icegreen.greenmail.util.DummySSLSocketFactory;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import hthurow.tomcatjndi.TomcatJNDI;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.naming.NamingException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.security.Security;

import static org.junit.Assert.assertEquals;

public class SendAgentTest {

    TomcatJNDI tomcatJNDI = new TomcatJNDI();
    MailSpool mailSpool;
    ServerSetup setupSmtp;
    GreenMail greenMailSmtp;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        Security.setProperty("ssl.SocketFactory.provider", DummySSLSocketFactory.class.getName());

        mailSpool = new MailSpool(1);
        Session session = null;
        MimeMessage message = new MimeMessage(session);
        message.addRecipient(RecipientType.TO, new InternetAddress("test@mail.com"));
        message.setText("content");
        mailSpool.addMailToSend(message);
    }

    @After
    public void tearDown() throws Exception {
        tomcatJNDI.tearDown();
        greenMailSmtp.stop();
    }

    @Test
    public void testSingleSend() throws NamingException, InterruptedException, URISyntaxException {
        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("context.xml").toURI()).toFile());
        tomcatJNDI.start();
        setSmtpServer();

        SendAgent agent = new SendAgent(mailSpool);
        Thread testSend = new Thread(agent);
        testSend.start();
        mailSpool.notifyLastMailAdded();
        testSend.join();


        Message[] messages = greenMailSmtp.getReceivedMessages();
        assertEquals(1, messages.length);
    }

    @Test(expected = NamingException.class)
    public void testMissingSendSession() throws URISyntaxException, InterruptedException, NamingException {
        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("contextFail.xml").toURI()).toFile());
        tomcatJNDI.start();
        setSmtpServer();

        SendAgent agent = new SendAgent(mailSpool);
        Thread testSend = new Thread(agent);
        testSend.start();
        testSend.join();

    }

    @Test
    public void testWrongConnection() throws URISyntaxException, InterruptedException, NamingException {
        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("contextFail2.xml").toURI()).toFile());
        tomcatJNDI.start();
        setSmtpServer();

        SendAgent agent = new SendAgent(mailSpool);
        Thread testSend = new Thread(agent);
        testSend.start();
        testSend.join();
        Message[] messages = greenMailSmtp.getReceivedMessages();
        assertEquals(0, messages.length);
    }

    @Test
    public void testMissingSecurity() throws URISyntaxException, InterruptedException, NamingException {
        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("contextFail3.xml").toURI()).toFile());
        tomcatJNDI.start();

        setupSmtp = new ServerSetup(47621, "localhost", ServerSetup.PROTOCOL_SMTP);
        greenMailSmtp = new GreenMail(setupSmtp);
        greenMailSmtp.setUser("user1@mail.com", "admin", "password");
        greenMailSmtp.start();

        SendAgent agent = new SendAgent(mailSpool);
        Thread testSend = new Thread(agent);
        testSend.start();
        testSend.join();
        Message[] messages = greenMailSmtp.getReceivedMessages();
        assertEquals(0, messages.length);
    }

    private void setSmtpServer() {
        setupSmtp = new ServerSetup(47621, "localhost", ServerSetup.PROTOCOL_SMTPS);
        greenMailSmtp = new GreenMail(setupSmtp);
        greenMailSmtp.setUser("user1@mail.com", "admin", "password");
        greenMailSmtp.start();
    }

}
