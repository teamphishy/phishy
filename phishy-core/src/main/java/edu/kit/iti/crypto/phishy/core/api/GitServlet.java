package edu.kit.iti.crypto.phishy.core.api;

import edu.kit.iti.crypto.phishy.data.ManagerFactory;
import edu.kit.iti.crypto.phishy.data.template.TemplateManager;
import org.eclipse.jgit.transport.resolver.RepositoryResolver;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * Servlet for Git functionality
 */
public class GitServlet extends org.eclipse.jgit.http.server.GitServlet implements ServletConfig {

    private ServletConfig config;

    @Override
    public void init(ServletConfig config) throws ServletException {

        this.config = config;

        TemplateManager manager = ManagerFactory.getTemplateManager();
        assert manager != null;


        RepositoryResolver<HttpServletRequest> resolver = manager.getGitResolver();
        if (resolver == null) {
            throw new ServletException("Git not supported for these templates");
        }

        this.setRepositoryResolver(resolver);
        super.init(this);

    }

    @Override
    public String getServletName() {
        return GitServlet.class.getName();
    }

    @Override
    public ServletContext getServletContext() {
        return config.getServletContext();
    }

    @Override
    public String getInitParameter(String name) {

        if (name.equals("export-all")) {
            return "0";
        }

        return config.getInitParameter(name);

    }

    @Override
    public Enumeration<String> getInitParameterNames() {
        return config.getInitParameterNames();
    }

}
