package edu.kit.iti.crypto.phishy.core.bootstrapper;

import edu.kit.iti.crypto.phishy.scheduling.ScheduledTask;
import edu.kit.iti.crypto.phishy.scheduling.Scheduler;

import javax.naming.*;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.logging.Level;
import java.util.logging.Logger;


@WebListener
public class Bootstrapper implements ServletContextListener {

    private Logger logger = Logger.getLogger(Bootstrapper.class.getName());

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        try {
            initScheduler();
        } catch (NamingException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }

    private void initScheduler() throws NamingException {
        Scheduler scheduler = Scheduler.getInstance();
        InitialContext initialContext = new InitialContext();
        Context scheduledTasks;
        try {
            scheduledTasks = (Context) initialContext.lookup("java:comp/env/phishy/scheduledTasks");
        } catch (NamingException e) {
            logger.log(Level.INFO, "No scheduled tasks");
            return;
        }

        NamingEnumeration<Binding> namingEnumeration = scheduledTasks.listBindings("/");

        while (namingEnumeration.hasMore()) {
            Binding binding = namingEnumeration.nextElement();
            ScheduledTask scheduledTask = (ScheduledTask) binding.getObject();
            scheduler.schedule(scheduledTask);
        }
        namingEnumeration.close();
        scheduler.dispatchInitialTasks();
    }
}
