package edu.kit.iti.crypto.phishy.core.mail;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import edu.kit.iti.crypto.phishy.data.addressbook.Recipient;
import edu.kit.iti.crypto.phishy.data.campaign.TestDescription;
import edu.kit.iti.crypto.phishy.data.template.Template;
import edu.kit.iti.crypto.phishy.data.template.TemplateFile;
import edu.kit.iti.crypto.phishy.data.template.Variable;
import edu.kit.iti.crypto.phishy.data.template.VariableFilterStream;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Generates Info/Phishing-Mails to be sent out.
 */
public class MailGenerator implements Runnable {

    private final Collection<Recipient> recipients;
    private final MailSpool mailSpool;
    private final Template template;
    private final Logger logger;
    private final Algorithm tokenAlgorithm;
    private final int daysTillExpired;

    /**
     * Creates MailGenerator
     *
     * @param mailSpool       will receive generated mails
     * @param template        of the mail type
     * @param recipients      list of containing the data to generate the mails
     * @param tokenAlgorithm  to generate jwt tokens in header if not null
     * @param daysTillExpired for how long the token is valid
     */
    public MailGenerator(MailSpool mailSpool, Template template, Collection<Recipient> recipients, Algorithm tokenAlgorithm, int daysTillExpired) {
        this.mailSpool = mailSpool;
        this.recipients = recipients;
        this.template = template;
        this.tokenAlgorithm = tokenAlgorithm;
        this.daysTillExpired = daysTillExpired;
        logger = Logger.getLogger(this.getClass().getName());
    }

    /**
     * Loads template, generates mails and adds them to the MailSpool to be sent out
     */
    @Override
    public void run() {
        TemplateFile mailTemplate;
        try {
            mailTemplate = template.getFile("mail.eml");
        } catch (NotFoundException e) {
            logger.log(Level.SEVERE, "Couldn't find mail.eml file in '" + template.getName() + "' Template");
            return;
        }
        for (Recipient recipient : recipients) {
            generateMail(mailTemplate, recipient);
        }
    }

    private void generateMail(TemplateFile mailTemplate, Recipient recipient) {
        Collection<Variable> variables = createVariableList(recipient);
        InputStream unprocessedMailStream = mailTemplate.getStream();
        VariableFilterStream varFilterStream = new VariableFilterStream(unprocessedMailStream, variables);
        MimeMessage message;
        try {
            message = createMail(recipient, varFilterStream);
            mailSpool.addMailToSend(message);
        } catch (MessagingException e) {
            logger.log(Level.SEVERE, "Couldn't create mail with '" + template.getName() + "' Template");
        } catch (UnsupportedEncodingException e) {
            logger.log(Level.SEVERE, "UTF-8 not supported");
        }

    }

    private MimeMessage createMail(Recipient recipient, InputStream varFilterStream)
            throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = new MimeMessage(null, varFilterStream);

        if (tokenAlgorithm != null && recipient.getTestId() != null) {
            message.setHeader("X-Phishy-ID", generateJwtToken(recipient));
        }

        InternetAddress address;
        if (recipient.getName() != null) {
            address = new InternetAddress(recipient.getMail(), recipient.getName());
        } else {
            address = new InternetAddress(recipient.getMail());
        }
        message.addRecipient(RecipientType.TO, address);
        message.saveChanges();
        return message;
    }

    private Collection<Variable> createVariableList(Recipient recipient) {
        Collection<Variable> variables = new ArrayList<Variable>();
        TestDescription testDesc = recipient.getTestId();
        if (testDesc != null) {
            variables.add(new Variable("phishy.id", testDesc.toIdentifier()));
        }
        variables.add(new Variable("recipient.mail", recipient.getMail()));
        variables.add(new Variable("recipient.givenname", recipient.getGivenName()));
        variables.add(new Variable("recipient.name", recipient.getName()));
        variables.add(new Variable("recipient.surname", recipient.getSurname()));
        variables.add(new Variable("recipient.mail.formatted", recipient.getName() + " <" + recipient.getMail() + ">"));


        return variables;
    }

    private String generateJwtToken(Recipient recipient) {
        Calendar expiry = Calendar.getInstance();
        expiry.add(Calendar.DAY_OF_MONTH, daysTillExpired);
        return JWT.create()
                .withExpiresAt(expiry.getTime())
                .withClaim("nonce", recipient.getTestId().toIdentifier())
                .withClaim("email", recipient.getMail())
                .withClaim("name", recipient.getName())
                .sign(tokenAlgorithm);
    }
}
