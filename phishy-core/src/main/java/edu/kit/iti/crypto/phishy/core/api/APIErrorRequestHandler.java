package edu.kit.iti.crypto.phishy.core.api;

import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.MutationException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import javax.json.Json;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObjectBuilder;

/**
 * Handler for exceptions
 */
public class APIErrorRequestHandler implements APIRequestHandler {

    private Exception exception;

    public APIErrorRequestHandler(Exception exception) {
        this.exception = exception;
    }

    @Override
    public APIResponseJsonStructure handleRequest(APIRequestJsonObject requestBody) {

        JsonBuilderFactory factory = Json.createBuilderFactory(null);
        JsonObjectBuilder error = factory.createObjectBuilder();
        error.add("message", exception.getMessage() != null ? exception.getMessage() : "");

        HttpResponseType status;

        if (exception instanceof HttpUnsupportedMethodException) {
            status = HttpResponseType.METHOD_NOT_ALLOWED;
        } else if (exception instanceof NotFoundException) {
            status = HttpResponseType.NOT_FOUND;
        } else if (exception instanceof CreationException) {
            status = HttpResponseType.CONFLICT;
        } else if (exception instanceof MutationException) {
            status = HttpResponseType.CONFLICT;
        } else {
            status = HttpResponseType.BAD_REQUEST;
        }

        return new APIResponseJsonObject(status, error.build());
    }
}
