package edu.kit.iti.crypto.phishy.core.api;

import edu.kit.iti.crypto.phishy.data.ManagerFactory;
import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookDescription;
import edu.kit.iti.crypto.phishy.data.campaign.*;
import edu.kit.iti.crypto.phishy.data.template.TemplateDescription;
import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.MutationException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 * Handle Campaign Data Requests
 */
public class APICampaignRequestHandler implements APIRequestHandler {

    private static final String CAMPAIGN_FORMAT = "[a-zA-Z0-9_]+";
    private static final Pattern CAMPAIGN_PATTERN = Pattern.compile(CAMPAIGN_FORMAT);

    CampaignManager manager;
    Logger logger;

    public APICampaignRequestHandler(CampaignManager campaignManager) {
        manager = ManagerFactory.getCampaignManager();
        logger = Logger.getLogger(this.getClass().getName());
    }

    @Override
    public APIResponseJsonStructure handleRequest(APIRequestJsonObject requestBody)
            throws NotFoundException, CreationException, DeletionException, MutationException {

        if (requestBody.getRequestPath().isEmpty()) {
            return handleCampaignList(requestBody);
        } else {
            return handleCampaignAccess(requestBody);
        }

    }

    /**
     * Handle requests for specific campaigns
     */
    private APIResponseJsonStructure handleCampaignAccess(APIRequestJsonObject requestBody)
            throws NotFoundException, CreationException, DeletionException, MutationException {

        assert requestBody != null;

        requestBody.getRequestType()
                .require(HttpRequestType.GET, HttpRequestType.PUT, HttpRequestType.POST, HttpRequestType.DELETE);

        String campaignName = requestBody.getRequestPath();
        if (!CAMPAIGN_PATTERN.matcher(campaignName).matches()) {
            throw new IllegalArgumentException("Campaign name not well-formatted");
        }

        switch (requestBody.getRequestType()) {
            case GET:
                return handleGetCampaign(campaignName);
            case PUT:
                return handlePutCampaign(campaignName, requestBody);
            case POST:
                return handlePostCampaign(campaignName, requestBody);
            case DELETE:
                return handleDeleteCampaign(campaignName);
            default:
                throw new HttpUnsupportedMethodException(requestBody.getRequestType());
        }

    }

    /**
     * Return info for a specific campaign
     *
     * @param campaignName Campaign's name
     */
    private APIResponseJsonStructure handleGetCampaign(String campaignName) throws NotFoundException {

        return new APIResponseJsonObject(HttpResponseType.OK, getCampaignJson(campaignName));

    }

    /**
     * Create JSON Object for a campaign
     *
     * @param campaignName Campaign's name
     */
    private JsonObject getCampaignJson(String campaignName) throws NotFoundException {

        Campaign campaign = manager.getCampaign(new CampaignDescription(campaignName));

        JsonObjectBuilder jsonCampaign = Json.createObjectBuilder();
        jsonCampaign.add("id", campaign.getName());
        try {
            jsonCampaign.add("addressbook", campaign.getAddressbook().getName());
        } catch (NotFoundException e) {
            jsonCampaign.add("addressbook", "");
        }
        jsonCampaign.add("mailLifetime", campaign.getMailLifetime());
        jsonCampaign.add("recipientSetSize", campaign.getRecipientSetSize());
        jsonCampaign.add("mailInterval", campaign.getMailInterval());
        jsonCampaign.add("status", campaign.getStatus().toString());

        JsonObjectBuilder jsonTemplates = Json.createObjectBuilder();
        for (TemplateType type : TemplateType.values()) {
            if (campaign.hasTemplate(type)) {
                jsonTemplates.add(type.toString(), campaign.getTemplate(type).toString());
            } else {
                jsonTemplates.add(type.toString(), "");
            }
        }

        jsonCampaign.add("templates", jsonTemplates);

        logger.log(Level.INFO, "API: Listed Campaign Info for '" + campaignName + "'");

        return jsonCampaign.build();

    }

    /**
     * Update a campaign's data
     */
    private void updateCampaign(Campaign campaign, APIRequestJsonObject requestJson)
            throws DeletionException, MutationException {

        if (requestJson.containsKey("addressbook")) {
            String addressbookName = requestJson.getString("addressbook");
            if (!addressbookName.isEmpty()) {
                campaign.setAddressbook(new AddressbookDescription(addressbookName));
            }
        }

        if (requestJson.containsKey("mailLifetime")) {
            campaign.setMailLifetime(requestJson.getInt("mailLifetime"));
        }

        if (requestJson.containsKey("recipientSetSize")) {
            campaign.setRecipientSetSize(requestJson.getInt("recipientSetSize"));
        }

        if (requestJson.containsKey("mailInterval")) {
            campaign.setMailInterval(requestJson.getInt("mailInterval"));
        }

        if (requestJson.containsKey("status")) {
            campaign.setStatus(CampaignStatus.fromString(requestJson.getString("status")));
        }

        if (requestJson.containsKey("templates")) {
            JsonObject templatesJson = requestJson.getJsonObject("templates");
            for (TemplateType type : TemplateType.values()) {
                if (templatesJson.containsKey(type.toString())) {
                    String templateName = templatesJson.getString(type.toString());
                    if (templateName.isEmpty()) {
                        campaign.unlinkTemplate(type);
                    } else {
                        campaign.linkTemplate(type, TemplateDescription.fromFQN(templateName));
                    }
                }
            }
        }

    }

    /**
     * Updata a campaign
     */
    private APIResponseJsonStructure handlePutCampaign(String campaignName, APIRequestJsonObject requestJson)
            throws NotFoundException, DeletionException, MutationException {

        Campaign campaign = manager.getCampaign(new CampaignDescription(campaignName));
        updateCampaign(campaign, requestJson);

        logger.log(Level.INFO, "API: Updated Campaign '" + campaignName + "'");

        return new APIResponseJsonObject(HttpResponseType.OK, getCampaignJson(campaignName));

    }

    /**
     * Create a new campaign
     */
    private APIResponseJsonStructure handlePostCampaign(String campaignName, APIRequestJsonObject requestJson)
            throws CreationException, NotFoundException, DeletionException, MutationException {

        Campaign campaign = manager.createCampaign(campaignName);
        updateCampaign(campaign, requestJson);

        logger.log(Level.INFO, "API: Created Campaign '" + campaignName + "'");

        return new APIResponseJsonObject(HttpResponseType.CREATED, getCampaignJson(campaignName));


    }

    /**
     * Delete a campaign
     */
    private APIResponseJsonStructure handleDeleteCampaign(String campaignName) throws DeletionException {

        manager.deleteCampaign(new CampaignDescription(campaignName));

        logger.log(Level.INFO, "API: Deleted Campaign '" + campaignName + "'");

        return new APIResponseJsonObject(HttpResponseType.OK, JsonObject.EMPTY_JSON_OBJECT);

    }

    /**
     * List all campaigns
     */
    private APIResponseJsonStructure handleCampaignList(APIRequestJsonObject requestBody) {

        requestBody.getRequestType().require(HttpRequestType.GET);

        Collection<Campaign> campaigns = manager.listCampaigns();
        JsonArrayBuilder jsonCampaigns = Json.createArrayBuilder();

        for (Campaign campaign : campaigns) {

            JsonObjectBuilder jsonCampaign = Json.createObjectBuilder();
            jsonCampaign.add("id", campaign.getName());
            jsonCampaign.add("status", campaign.getStatus().toString());
            jsonCampaigns.add(jsonCampaign);

        }

        logger.log(Level.INFO, "API: List campaigns");

        return new APIResponseJsonArray(HttpResponseType.OK, jsonCampaigns.build());


    }
}
