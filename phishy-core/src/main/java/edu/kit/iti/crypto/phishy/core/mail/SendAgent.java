package edu.kit.iti.crypto.phishy.core.mail;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Sends mails to SMTP Server
 */
public class SendAgent implements Runnable {

    private final MailSpool mailSpool;
    private final Session sendSession;
    private final Logger logger;

    /**
     * Creates SendAgent with connection to SMTP server defined with JNDI
     *
     * @param mailSpool the cache to receive the generated mails from
     * @throws NamingException when SendSession is not correctly defined in JNDI
     */
    public SendAgent(MailSpool mailSpool) throws NamingException {
        this.mailSpool = mailSpool;
        logger = Logger.getLogger(this.getClass().getName());


        Context initialContext = new InitialContext();
        sendSession = (Session) initialContext.lookup("java:comp/env/mail/SendSession");

    }

    /**
     * Takes mails from MailSpool and sends them to the server
     */
    @Override
    public void run() {
        try {
            //set JNDI mail.transport.protocol="PROTOCOL"
            Transport transport = sendSession.getTransport();
            transport.connect();

            int nextMailIndex = mailSpool.reserveNextMail();
            if (nextMailIndex == -1) {
                transport.close();
                return;
            }

            boolean nextIndexValid;

            while (nextIndexValid = mailSpool.isNextIndexPossible(nextMailIndex)) {

                while (nextIndexValid && mailSpool.isNextMailReady(nextMailIndex)) {
                    MimeMessage mail = mailSpool.getMailToSend(nextMailIndex);
                    transport.sendMessage(mail, mail.getAllRecipients());

                    nextMailIndex = mailSpool.reserveNextMail();
                    if (nextMailIndex == -1) {
                        break;
                    }

                    nextIndexValid = mailSpool.isNextIndexPossible(nextMailIndex);
                }
                if (!nextIndexValid || nextMailIndex == -1) {
                    break;
                }
                Thread.sleep(500);
            }

            transport.close();
        } catch (MessagingException e) {
            logger.log(Level.SEVERE, "Unable to send mails. Check if connection credentials are correct: " + e.getMessage());
        } catch (InterruptedException e) {
            logger.log(Level.SEVERE, "Sending mails was interrupted");
        }
    }
}
