package edu.kit.iti.crypto.phishy.scheduling;


import java.util.*;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;


public final class Scheduler {

    private static Scheduler instance;
    private Collection<ScheduledTask> scheduledTasks = new ArrayList<>();
    private final ExecutorService sharedPool;
    private final Logger logger;
    private final ScheduledExecutorService execService;

    private Scheduler() {

        this.logger = Logger.getLogger(this.getClass().getName());

        this.sharedPool = Executors.newFixedThreadPool(
                Math.max(2, Runtime.getRuntime().availableProcessors()));

        this.execService = Executors.newScheduledThreadPool(
                Math.max(2, Runtime.getRuntime().availableProcessors()));

        executeScheduledTasks();

    }


    public static Scheduler getInstance() {

        if (instance == null) {
            instance = new Scheduler();
        }

        return instance;

    }


    public void schedule(ScheduledTask scheduledTask) {

        this.scheduledTasks.add(scheduledTask);

        logger.log(Level.INFO, "Scheduled " + scheduledTask);

    }

    private void executeScheduledTasks() {

        execService.scheduleAtFixedRate(() -> dispatchNextTasks(), getDelayToHalfHour(), 60, TimeUnit.MINUTES);


    }

    public void dispatchInitialTasks() {

        GregorianCalendar currentDate = new GregorianCalendar(TimeZone.getTimeZone("UTC"));

        if (!(currentDate.get(Calendar.MINUTE) > 30)) {
            return;
        }

        Collection<ScheduledTask> tasksToday = getTasksToday(currentDate);
        GregorianCalendar taskTime = new GregorianCalendar(TimeZone.getTimeZone("UTC"));


        for (ScheduledTask scheduledTask : tasksToday) {
            taskTime.setTime(scheduledTask.getTime());
            if (taskTime.get(Calendar.HOUR_OF_DAY) == currentDate.get(Calendar.HOUR_OF_DAY) &&
                    taskTime.get(Calendar.MINUTE) >= currentDate.get(Calendar.MINUTE)) {
                scheduledTask.start();
                logger.log(Level.INFO, "Dispatched" + scheduledTask);
            }
        }
    }


    private int getDelayToHalfHour() {
        int delay;
        GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
        if (calendar.get(Calendar.MINUTE) <= 30) {
            delay = 30 - calendar.get(Calendar.MINUTE);
        } else {
            delay = 30 + (60 - calendar.get(Calendar.MINUTE));
        }

        return delay;
    }


    private Collection<ScheduledTask> getTasksToday(GregorianCalendar date) {

        Collection<ScheduledTask> tasksToday = new ArrayList<>();

        for (ScheduledTask scheduledTask : scheduledTasks) {

            if (scheduledTask.getWeekdays().contains(Weekday.singleDayFromInt(date.get(Calendar.DAY_OF_WEEK)))) {
                tasksToday.add(scheduledTask);
            }
        }
        return tasksToday;
    }

    private void dispatchNextTasks() {
        GregorianCalendar currentDate = new GregorianCalendar(TimeZone.getTimeZone("UTC"));

        Collection<ScheduledTask> tasksToday = getTasksToday(currentDate);


        for (ScheduledTask scheduledTask : tasksToday) {
            GregorianCalendar taskTime = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
            taskTime.setTime(scheduledTask.getTime());
            if (taskTime.get(Calendar.HOUR_OF_DAY) == currentDate.get(Calendar.HOUR_OF_DAY)) {
                scheduledTask.start();
                logger.log(Level.INFO, "Dispatched" + scheduledTask);
            }
        }
    }


    public ExecutorService getThreadPool() {

        return Executors.newFixedThreadPool(
                Math.max(2, Runtime.getRuntime().availableProcessors()));
    }


    public ExecutorService getSingleThread() {
        return Executors.newSingleThreadExecutor();
    }


    public Future<?> dispatchNow(Runnable task) {

        logger.log(Level.INFO, "Dispatched " + task);

        return sharedPool.submit(task);

    }

}
