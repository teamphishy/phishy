package edu.kit.iti.crypto.phishy.core.tasks;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.RSAKeyProvider;
import edu.kit.iti.crypto.phishy.core.JWTKeyProvider;
import edu.kit.iti.crypto.phishy.core.ModalLock;
import edu.kit.iti.crypto.phishy.core.ModalLock.Lease;
import edu.kit.iti.crypto.phishy.core.mail.MailGenerator;
import edu.kit.iti.crypto.phishy.core.mail.MailSpool;
import edu.kit.iti.crypto.phishy.core.mail.ReceiveAgent;
import edu.kit.iti.crypto.phishy.core.mail.SendAgent;
import edu.kit.iti.crypto.phishy.data.ManagerFactory;
import edu.kit.iti.crypto.phishy.data.addressbook.Recipient;
import edu.kit.iti.crypto.phishy.data.campaign.*;
import edu.kit.iti.crypto.phishy.data.template.Template;
import edu.kit.iti.crypto.phishy.data.template.TemplateDescription;
import edu.kit.iti.crypto.phishy.data.template.TemplateManager;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import edu.kit.iti.crypto.phishy.scheduling.Scheduler;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.naming.NamingException;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Evaluates the mails being received from the IMAP server and answers respectively
 */
public class EvaluateReportedMailsTask implements Runnable {

    private static final int PARTITION_DIV = 100;
    private final RSAKeyProvider rsaKeyProvider = new JWTKeyProvider();


    private MailSpool mailSpool;
    private final Logger logger;
    private int amountOfMails = 0;

    /**
     * Creates EvaluateReportedMailsTask
     */
    public EvaluateReportedMailsTask() {
        logger = Logger.getLogger(this.getClass().getName());
    }


    /**
     * Gathers mails from the IMAP server and evaluates them. Sends mails back with info
     */
    @Override
    public void run() {
        try (Lease lease = ModalLock.getInstance().getLease()) {
            Scheduler scheduler = Scheduler.getInstance();
            ExecutorService generatorPool = scheduler.getThreadPool();
            ExecutorService sendThread = scheduler.getSingleThread();
            Collection<Future<?>> allThreads = new ArrayList<Future<?>>();
            Future<?> sendAgentFuture;

            Map<Campaign, List<Recipient>> mappedCampaignMessages = mapToCampaign(getMessages());
            mailSpool = new MailSpool(amountOfMails);


            for (Entry<Campaign, List<Recipient>> campaignMessages : mappedCampaignMessages.entrySet()) {
                allThreads.addAll(handleCampaign(campaignMessages.getKey(), campaignMessages.getValue(), generatorPool));
            }

            sendAgentFuture = sendThread.submit(createSendAgent());

            for (Future<?> future : allThreads) {
                try {
                    future.get();
                } catch (InterruptedException | ExecutionException e) {
                    logger.log(Level.SEVERE, "Couldn't execute processing reported mails correctly");
                }
            }

            generatorPool.shutdownNow();
            mailSpool.notifyLastMailAdded();
            try {
                sendAgentFuture.get();
            } catch (InterruptedException | ExecutionException e) {
                logger.log(Level.SEVERE, "Couldn't execute processing reported mails correctly");
            }
            sendThread.shutdownNow();

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Couldn't execute processing reported mails correctly");
        }
    }


    private Collection<Message> getMessages() {
        ReceiveAgent receiveAgent = new ReceiveAgent();
        Message[] receivedMessages = receiveAgent.receiveMails();
        amountOfMails = receivedMessages.length;
        Collection<Message> messages = Arrays.asList(receivedMessages);
        return messages;
    }

    private SendAgent createSendAgent() {
        try {
            return new SendAgent(mailSpool);
        } catch (NamingException e1) {
            logger.log(Level.SEVERE, "Couldn't find SendSession in JNDI");
            throw new IllegalStateException();
        }
    }

    private Collection<Future<?>> handleCampaign(Campaign campaign, List<Recipient> recipients,
                                                 ExecutorService generatorPool) {

        TemplateManager templateManager = ManagerFactory.getTemplateManager();
        Template template;
        try {
            TemplateDescription templateDesc = campaign.getTemplate(TemplateType.INFO_MAIL);
            template = templateDesc.resolve(templateManager);
        } catch (NotFoundException e) {
            logger.log(Level.SEVERE, "Couldn't find Info-Mail template from campaign: " + campaign.getName());
            return Collections.emptyList();
        }

        List<Recipient> deletedRecipients = campaign.deleteTests(recipients);
        campaign.getStatistics().increaseReportedMail(deletedRecipients.size());

        List<List<Recipient>> partitionedRecipients = partitionRecipients(deletedRecipients);
        Collection<Future<?>> allGenerator = new ArrayList<Future<?>>();

        for (List<Recipient> recipientList : partitionedRecipients) {
            MailGenerator mailGenerator = new MailGenerator(mailSpool, template, recipientList, null, 0);
            allGenerator.add(generatorPool.submit(mailGenerator));
        }
        return allGenerator;
    }

    private Map<Campaign, List<Recipient>> mapToCampaign(Collection<Message> mails) {
        Algorithm tokenAlgorithm = Algorithm.RSA256(rsaKeyProvider);
        JWTVerifier verifier = JWT.require(tokenAlgorithm).build();

        Map<Campaign, List<Recipient>> mappedCampaignRecipients =
                new HashMap<Campaign, List<Recipient>>();

        CampaignManager campaignManager = ManagerFactory.getCampaignManager();
        for (Message message : mails) {
            try {
                evaluateMail(verifier, mappedCampaignRecipients, campaignManager, message);

            } catch (MessagingException | JWTVerificationException e1) {
                logger.log(Level.FINE, "Mail is not a Phishy-Mail");
                amountOfMails--;
            } catch (NotFoundException e) {
                logger.log(Level.WARNING, "Skip E-Mail because couldn't resolve campaign");
                amountOfMails--;
            }
        }
        return mappedCampaignRecipients;
    }


    private void evaluateMail(JWTVerifier verifier, Map<Campaign, List<Recipient>> mappedCampaignRecipients,
                              CampaignManager campaignManager, Message message) throws MessagingException, NotFoundException, JWTVerificationException {

        String[] campaignIdHeader = message.getHeader("X-Phishy-ID");
        if (campaignIdHeader == null) {
            throw new MessagingException("Mail is not a Phishy-Mail");
        }
        DecodedJWT decodedToken = verifier.verify(campaignIdHeader[0]);

        assert decodedToken != null;

        if (decodedToken.getClaim("nonce").isNull()) {
            logger.log(Level.INFO, "nonce not set in JWT Token");
            throw new JWTVerificationException("nonce does not exist");
        }

        TestDescription testDesc = TestDescription.fromIdentifier(decodedToken.getClaim("nonce").asString());
        Campaign campaign = getCampaign(campaignManager, testDesc);

        List<Recipient> recipients = mappedCampaignRecipients.get(campaign);

        if (recipients != null) {

            Recipient recipient = new Recipient("", decodedToken.getClaim("email").asString(),
                    decodedToken.getClaim("name").asString());

            recipient.setTestId(testDesc);
            recipients.add(recipient);
        } else {
            recipients = new ArrayList<Recipient>();

            Recipient recipient = new Recipient("", decodedToken.getClaim("email").asString(),
                    decodedToken.getClaim("name").asString());

            recipient.setTestId(testDesc);
            recipients.add(recipient);

            mappedCampaignRecipients.put(campaign, recipients);
        }
    }

    private Campaign getCampaign(CampaignManager campaignManager, TestDescription testDesc) throws NotFoundException {

        CampaignDescription campaignDesc = testDesc.getCampaignDescription();

        Campaign campaign;
        try {
            campaign = campaignDesc.resolve(campaignManager);
        } catch (NotFoundException e) {
            logger.log(Level.WARNING, "Couldn't find Campaign: " + campaignDesc.getName());
            throw new NotFoundException();
        }
        return campaign;
    }

    private List<List<Recipient>> partitionRecipients(List<Recipient> recipients) {
        List<List<Recipient>> partitionedList = new ArrayList<List<Recipient>>();
        int partitions = recipients.size() / PARTITION_DIV;

        for (int i = 0; i < partitions; i++) {
            partitionedList.add(recipients.subList(PARTITION_DIV * i, PARTITION_DIV * (i + 1)));
        }
        partitionedList.add(recipients.subList(PARTITION_DIV * partitions, recipients.size()));

        return partitionedList;
    }

}
