package edu.kit.iti.crypto.phishy.core.api;

import javax.json.*;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Http JSON Response Object
 */
public class APIResponseJsonObject extends APIResponseJsonStructure implements JsonObject {

    private final JsonObject jsonObject;

    public APIResponseJsonObject(HttpResponseType responseType, JsonObject jsonObject) {

        super(responseType);

        this.jsonObject = jsonObject;
    }


    @Override
    public ValueType getValueType() {
        return jsonObject.getValueType();
    }

    @Override
    public int size() {
        return jsonObject.size();
    }

    @Override
    public boolean isEmpty() {
        return jsonObject.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return jsonObject.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return jsonObject.containsValue(value);
    }

    @Override
    public JsonValue get(Object key) {
        return jsonObject.get(key);
    }

    @Override
    public JsonValue put(String key, JsonValue value) {
        return jsonObject.put(key, value);
    }

    @Override
    public JsonValue remove(Object key) {
        return jsonObject.remove(key);
    }

    @Override
    public void putAll(Map<? extends String, ? extends JsonValue> m) {
        jsonObject.putAll(m);
    }

    @Override
    public void clear() {
        jsonObject.clear();
    }

    @Override
    public Set<String> keySet() {
        return jsonObject.keySet();
    }

    @Override
    public Collection<JsonValue> values() {
        return jsonObject.values();
    }

    @Override
    public Set<Entry<String, JsonValue>> entrySet() {
        return jsonObject.entrySet();
    }

    @Override
    public JsonArray getJsonArray(String name) {
        return jsonObject.getJsonArray(name);
    }

    @Override
    public JsonObject getJsonObject(String name) {
        return jsonObject.getJsonObject(name);
    }

    @Override
    public JsonNumber getJsonNumber(String name) {
        return jsonObject.getJsonNumber(name);
    }

    @Override
    public JsonString getJsonString(String name) {
        return jsonObject.getJsonString(name);
    }

    @Override
    public String getString(String name) {
        return jsonObject.getString(name);
    }

    @Override
    public String getString(String name, String defaultValue) {
        return jsonObject.getString(name, defaultValue);
    }

    @Override
    public int getInt(String name) {
        return jsonObject.getInt(name);
    }

    @Override
    public int getInt(String name, int defaultValue) {
        return jsonObject.getInt(name, defaultValue);
    }

    @Override
    public boolean getBoolean(String name) {
        return jsonObject.getBoolean(name);
    }

    @Override
    public boolean getBoolean(String name, boolean defaultValue) {
        return jsonObject.getBoolean(name, defaultValue);
    }

    @Override
    public boolean isNull(String name) {
        return jsonObject.isNull(name);
    }

}
