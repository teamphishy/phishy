package edu.kit.iti.crypto.phishy.core.api;

import javax.json.JsonStructure;

/**
 * Http JSON Response Structure
 */
public abstract class APIResponseJsonStructure implements JsonStructure {

    private final HttpResponseType responseType;

    public APIResponseJsonStructure(HttpResponseType responseType) {
        this.responseType = responseType;
    }

    public HttpResponseType getResponseType() {
        return responseType;
    }

}
