package edu.kit.iti.crypto.phishy.core.api;

import edu.kit.iti.crypto.phishy.data.addressbook.Addressbook;
import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookManager;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Handles Requests for Addressbook data
 */
public class APIAddressbookRequestHandler implements APIRequestHandler {

    AddressbookManager manager;
    Logger logger;

    public APIAddressbookRequestHandler(AddressbookManager addressbookManager) {
        manager = addressbookManager;
        logger = Logger.getLogger(this.getClass().getName());
    }

    @Override
    public APIResponseJsonStructure handleRequest(APIRequestJsonObject requestBody) {

        requestBody.getRequestType().require(HttpRequestType.GET);

        if (!requestBody.getRequestPath().isEmpty()) {
            throw new IllegalArgumentException("addressbook request must not provide a path");
        }

        JsonArrayBuilder jsonAddressbooks = Json.createArrayBuilder();
        Collection<Addressbook> addressbooks = manager.listAddressbooks();

        for (Addressbook addressbook : addressbooks) {
            jsonAddressbooks.add(addressbook.getName());
        }

        logger.log(Level.INFO, "API: List Addressbooks");

        return new APIResponseJsonArray(HttpResponseType.OK, jsonAddressbooks.build());

    }
}
