package edu.kit.iti.crypto.phishy.scheduling;

import javax.naming.Context;
import javax.naming.Name;
import javax.naming.RefAddr;
import javax.naming.Reference;
import javax.naming.spi.ObjectFactory;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Create a new ScheduledTask from JNDI
 */
public class ScheduledTaskFactory implements ObjectFactory {

    private static final Pattern PARAM_PATTERN = Pattern.compile("param\\..*");

    @Override
    public Object getObjectInstance(Object obj, Name name, Context nameCtx, Hashtable<?, ?> environment)
            throws Exception {

        Reference reference = (Reference) obj;
        Enumeration<RefAddr> references = reference.getAll();

        String weekdays = "";
        String time = "";
        String taskClass = "";
        Map<String, String> parameters = new HashMap<>();

        while (references.hasMoreElements()) {

            RefAddr address = references.nextElement();
            String type = address.getType();
            String content = (String) address.getContent();

            switch (type) {
                case "scheduler.weekdays":
                    weekdays = content;
                    break;
                case "scheduler.time":
                    time = content;
                    break;
                case "scheduler.task":
                    taskClass = content;
                    break;
                default:
                    if (PARAM_PATTERN.matcher(type).matches()) {
                        String paramName = type.substring(6);
                        parameters.put(paramName, content);
                    }
            }

        }

        return ScheduledTask.fromData(taskClass, weekdays, time, parameters);

    }
}
