package edu.kit.iti.crypto.phishy.core.api;

/**
 * Exception for unsupported Http methods
 */
public class HttpUnsupportedMethodException extends RuntimeException {

    public HttpUnsupportedMethodException(HttpRequestType method) {
        super("Unsupported HTTP Method: " + method.toString());
    }
}
