package edu.kit.iti.crypto.phishy.core.tasks;

import edu.kit.iti.crypto.phishy.data.ManagerFactory;
import edu.kit.iti.crypto.phishy.data.template.TemplateManager;

import java.util.logging.Level;
import java.util.logging.Logger;

public class InvalidateCacheTask implements Runnable {


    @Override
    public void run() {

        Logger.getLogger(InvalidateCacheTask.class.getName()).log(Level.INFO, "Invalidating cache cache");
        TemplateManager manager = ManagerFactory.getTemplateManager();

        assert manager != null;

        manager.invalidateCache();

    }
}
