package edu.kit.iti.crypto.phishy.scheduling;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Provides Weekdays for the Scheduler
 */
public enum Weekday {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;

    private static Collection<Weekday> singleDayFromString(String day) {

        switch (day.toLowerCase()) {
            case "monday":
                return List.of(MONDAY);
            case "tuesday":
                return List.of(TUESDAY);
            case "wednesday":
                return List.of(WEDNESDAY);
            case "thursday":
                return List.of(THURSDAY);
            case "friday":
                return List.of(FRIDAY);
            case "saturday":
                return List.of(SATURDAY);
            case "sunday":
                return List.of(SUNDAY);
            case "weekend":
                return List.of(SATURDAY, SUNDAY);
            case "workday":
                return List.of(MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY);
            case "*":
                return List.of(MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY);
            default:
                throw new IllegalArgumentException("Invalid weekday");
        }
    }

    public static Weekday singleDayFromInt(int day) {
        switch (day) {
            case 0:
                return SUNDAY;
            case 1:
                return MONDAY;
            case 2:
                return TUESDAY;
            case 3:
                return WEDNESDAY;
            case 4:
                return THURSDAY;
            case 5:
                return FRIDAY;
            case 6:
                return SATURDAY;
            default:
                throw new IllegalArgumentException("Invalid weekday");
        }
    }

    /**
     * Get a collection of Weekdays from a comma-separated string.
     * Each comma-separated section is parsed into a Weekday.
     * Special names are "weekend", "workday" and "*" for all weekdays
     *
     * @param days comma-separated day-list
     * @return Collection of Weekdays
     */
    public static Collection<Weekday> fromString(String days) {

        Collection<Weekday> weekdays = new ArrayList<>();
        var dayFragments = days.split(",");
        for (var day : dayFragments) {
            weekdays.addAll(singleDayFromString(day.strip()));
        }

        return weekdays;
    }

}
