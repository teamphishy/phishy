package edu.kit.iti.crypto.phishy.core.api;

import edu.kit.iti.crypto.phishy.data.template.Template;
import edu.kit.iti.crypto.phishy.data.template.TemplateDescription;
import edu.kit.iti.crypto.phishy.data.template.TemplateGroup;
import edu.kit.iti.crypto.phishy.data.template.TemplateManager;
import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import javax.json.*;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 * Handles tempalte data requests
 */
public class APITemplateRequestHandler implements APIRequestHandler {

    private static final String TEMPLATE_FORMAT = "^[a-zA-Z][a-zA-Z0-9_]*$";
    private static final Pattern TEMPLATE_PATTERN = Pattern.compile(TEMPLATE_FORMAT);

    private TemplateManager manager;
    private Logger logger;
    private static final String GIT_ROOT = "/phishy/git/";

    public APITemplateRequestHandler(TemplateManager templateManager) {
        Objects.requireNonNull(templateManager);
        this.manager = templateManager;
        this.logger = Logger.getLogger(this.getClass().getName());
    }

    @Override
    public APIResponseJsonStructure handleRequest(APIRequestJsonObject requestBody)
            throws CreationException, NotFoundException, DeletionException {

        if (requestBody.getRequestPath().isEmpty()) {
            requestBody.getRequestType().require(HttpRequestType.GET);
            return listTemplates();
        } else {
            requestBody.getRequestType().require(HttpRequestType.GET, HttpRequestType.POST, HttpRequestType.DELETE);
            return handleTemplateGroup(requestBody);
        }

    }

    private APIResponseJsonStructure handleTemplateGroup(APIRequestJsonObject requestBody)
            throws NotFoundException, CreationException, DeletionException {

        String templateGroupName = requestBody.getRequestPath();
        if (!TEMPLATE_PATTERN.matcher(templateGroupName).matches()) {
            throw new IllegalArgumentException("Template group name malformed");
        }

        switch (requestBody.getRequestType()) {

            case GET:
                return handleGetTemplateGroup(templateGroupName);
            case POST:
                return handlePostTemplateGroup(templateGroupName);
            case DELETE:
                return handleDeleteTemplateGroup(templateGroupName);

            default:
                throw new HttpUnsupportedMethodException(requestBody.getRequestType());

        }

    }

    private APIResponseJsonStructure handleDeleteTemplateGroup(String templateGroupName) throws DeletionException {

        manager.deleteGroup(templateGroupName);

        logger.log(Level.INFO, "API: deleted template group '" + templateGroupName + "'");

        return new APIResponseJsonObject(HttpResponseType.OK, JsonObject.EMPTY_JSON_OBJECT);

    }

    private APIResponseJsonStructure handlePostTemplateGroup(String templateGroupName) throws CreationException {

        TemplateGroup group = manager.createGroup(templateGroupName);

        logger.log(Level.INFO, "API: created template group '" + templateGroupName + "'");

        return new APIResponseJsonObject(HttpResponseType.CREATED, formatGroupInfo(group));

    }

    private APIResponseJsonStructure handleGetTemplateGroup(String templateGroupName) throws NotFoundException {

        TemplateGroup group = manager.getGroup(templateGroupName);

        JsonArrayBuilder jsonArray = Json.createArrayBuilder();

        for (Template t : group.listTemplates()) {
            jsonArray.add(formatTemplateInfo(t.getDescription()));
        }

        logger.log(Level.INFO, "API: list templates for '" + templateGroupName + "'");

        return new APIResponseJsonArray(HttpResponseType.OK, jsonArray.build());

    }

    private JsonObject formatTemplateInfo(TemplateDescription template) {

        JsonObjectBuilder jsonObject = Json.createObjectBuilder();
        jsonObject.add("id", template.toString());
        jsonObject.add("git", GIT_ROOT + template.getGroup() + "/" + template.getName());

        return jsonObject.build();

    }

    private JsonObject formatGroupInfo(TemplateGroup group) {

        JsonObjectBuilder jsonObject = Json.createObjectBuilder();
        jsonObject.add("id", group.getName());
        jsonObject.add("git", GIT_ROOT + group.getName());

        return jsonObject.build();

    }

    private APIResponseJsonStructure listTemplates() {

        JsonObjectBuilder jsonObject = Json.createObjectBuilder();
        jsonObject.add("groups", listGroups());
        jsonObject.add("templates", listAllTemplates());

        logger.log(Level.INFO, "API: List template groups");

        return new APIResponseJsonObject(HttpResponseType.OK, jsonObject.build());

    }

    private JsonArray listGroups() {

        JsonArrayBuilder jsonArray = Json.createArrayBuilder();

        for (TemplateGroup group : manager.listGroups()) {
            jsonArray.add(formatGroupInfo(group));
        }

        return new APIResponseJsonArray(HttpResponseType.OK, jsonArray.build());

    }

    private JsonArray listAllTemplates() {

        JsonArrayBuilder jsonArray = Json.createArrayBuilder();

        for (TemplateGroup group : manager.listGroups()) {
            for (Template t : group.listTemplates()) {
                jsonArray.add(formatTemplateInfo(t.getDescription()));
            }
        }

        return jsonArray.build();

    }

}
