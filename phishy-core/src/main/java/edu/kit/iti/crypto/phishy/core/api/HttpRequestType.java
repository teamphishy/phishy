package edu.kit.iti.crypto.phishy.core.api;

/**
 * Request Methods
 */
public enum HttpRequestType {
    GET,
    POST,
    PUT,
    DELETE;

    public static HttpRequestType fromString(String method) {

        switch (method.toLowerCase()) {
            case "get":
                return GET;
            case "post":
                return POST;
            case "put":
                return PUT;
            case "delete":
                return DELETE;
            default:
                throw new IllegalArgumentException("HTTP Method not supported");
        }

    }

    public String toString() {

        switch (this) {
            case GET:
                return "GET";
            case PUT:
                return "PUT";
            case POST:
                return "POST";
            case DELETE:
                return "DELETE";
            default:
                throw new IllegalStateException("Invalid Enum value");
        }

    }

    public void require(HttpRequestType... methods) {

        for (HttpRequestType method : methods) {
            if (method == this) {
                return;
            }
        }

        throw new HttpUnsupportedMethodException(this);

    }

}
