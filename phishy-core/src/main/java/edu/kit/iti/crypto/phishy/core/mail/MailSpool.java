package edu.kit.iti.crypto.phishy.core.mail;

import javax.mail.internet.MimeMessage;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * A cache between generating mails and sending them out.
 */
public class MailSpool {

    private final AtomicInteger lastEmail;
    private final AtomicInteger nextEmail;
    private final MimeMessage[] mails;
    private boolean lastMailAdded = false;

    /**
     * Creates MailSpool with max amount of possible mails to be sent out.
     *
     * @param mailCount is the amount of how many mails are potentially expected.
     */
    public MailSpool(int mailCount) {
        mails = new MimeMessage[mailCount];
        lastEmail = new AtomicInteger(0);
        nextEmail = new AtomicInteger(0);
    }

    /**
     * Adds new mail to be sent out to the cache
     *
     * @param mail to be sent out
     */
    public void addMailToSend(MimeMessage mail) {
        int indexToPut = lastEmail.getAndIncrement();
        mails[indexToPut] = mail;
    }

    /**
     * Takes next mail to be sent out.
     * {@link #isNextMailReady()} should be called before {@link #getMailToSend()}.
     *
     * @return the next mail to be sent out
     */
    public MimeMessage getMailToSend(int index) {
        MimeMessage mail = mails[index];
        mails[index] = null;
        return mail;
    }

    /**
     * Checks if next mail is ready
     *
     * @return true if next mail is ready
     */
    public boolean isNextMailReady(int index) {
        return mails[index] != null;
    }

    /**
     * Checks if the index is possible. Will only be certain until {@link #notifyLastMailAdded()} was called.
     * Must be called before {@link #isNextMailReady(int)}
     *
     * @return false only when {@link #notifyLastMailAdded()} was called and index is invalid.
     */
    public boolean isNextIndexPossible(int index) {
        return !lastMailAdded || index < lastEmail.get();
    }

    /**
     * Reserves an index for the cache. And checks if IndexOutOFBounds could be caused.
     * Should call {@link #isNextIndexPossible(int)} to check if index is valid.
     *
     * @return index of next mail or -1 when IndexOutOFBoundsExcpetion would be caused.
     */
    public int reserveNextMail() {
        int index = nextEmail.getAndIncrement();
        return index < mails.length ? index : -1;
    }

    /**
     * Should only be called after all mails are generated
     */
    public void notifyLastMailAdded() {
        lastMailAdded = true;
    }
}
