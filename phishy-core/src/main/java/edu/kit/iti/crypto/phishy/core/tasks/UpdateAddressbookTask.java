package edu.kit.iti.crypto.phishy.core.tasks;

import edu.kit.iti.crypto.phishy.core.ModalLock;
import edu.kit.iti.crypto.phishy.core.ModalLock.Lease;
import edu.kit.iti.crypto.phishy.data.ManagerFactory;
import edu.kit.iti.crypto.phishy.data.addressbook.Addressbook;
import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookDescription;
import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookManager;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import edu.kit.iti.crypto.phishy.scheduling.TaskParameter;

import java.util.logging.Level;
import java.util.logging.Logger;

public class UpdateAddressbookTask implements Runnable {

    @TaskParameter("addressbook")
    AddressbookDescription addressbookDescription = null;
    Logger logger;

    public UpdateAddressbookTask() {
        // Empty initializer for Scheduler
        this.logger = Logger.getLogger(this.getClass().getName());
    }

    public UpdateAddressbookTask(AddressbookDescription addressbookDescription) {
        if (addressbookDescription == null) {
            throw new IllegalArgumentException("AddressbookDescription must not be null");
        }
        this.addressbookDescription = addressbookDescription;
        this.logger = Logger.getLogger(this.getClass().getName());
    }

    @Override
    public void run() {
        try (Lease lease = ModalLock.getInstance().getLease()) {
            AddressbookManager manager = ManagerFactory.getAddressbookManager();
            assert manager != null;

            if (addressbookDescription == null) {

                manager.listAddressbooks().forEach(this::updateAddressbook);

            } else {

                try {
                    this.updateAddressbook(addressbookDescription.resolve(manager));
                } catch (NotFoundException e) {
                    logger.log(Level.SEVERE, "Cannot resolve Addressbook '" + addressbookDescription.getName() + "'");
                }

            }
        } catch (InterruptedException e1) {
            logger.log(Level.SEVERE, "Couldn't update addressbook correctly.");
        } catch (Exception e1) {
            logger.log(Level.SEVERE, "Couldn't update addressbook. Waiting for resources was interrupted.");
        }
    }

    private void updateAddressbook(Addressbook addressbook) {
        logger.log(Level.INFO, "Updating Addressbook cache for " + addressbook.getName());
        addressbook.updateCache();
        logger.log(Level.INFO, "Cache updated");
    }

}
