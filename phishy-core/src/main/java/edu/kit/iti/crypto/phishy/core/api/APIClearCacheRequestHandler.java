package edu.kit.iti.crypto.phishy.core.api;

import edu.kit.iti.crypto.phishy.core.tasks.InvalidateCacheTask;
import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import edu.kit.iti.crypto.phishy.scheduling.Scheduler;

import javax.json.JsonObject;
import java.util.logging.Level;
import java.util.logging.Logger;

public class APIClearCacheRequestHandler implements APIRequestHandler {
    @Override
    public APIResponseJsonStructure handleRequest(APIRequestJsonObject requestBody)
            throws NotFoundException, CreationException, DeletionException {

        requestBody.getRequestType().require(HttpRequestType.POST);


        InvalidateCacheTask task = new InvalidateCacheTask();
        Scheduler.getInstance().dispatchNow(task);

        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "API: schedule clear cache now");

        return new APIResponseJsonObject(HttpResponseType.OK, JsonObject.EMPTY_JSON_OBJECT);

    }
}
