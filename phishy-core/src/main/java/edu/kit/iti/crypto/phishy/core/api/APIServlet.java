package edu.kit.iti.crypto.phishy.core.api;

import edu.kit.iti.crypto.phishy.core.ModalLock;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Servlet for API Requests
 */
public class APIServlet extends HttpServlet {
    private APIMainRequestHandler handler = new APIMainRequestHandler();
    private Logger logger;

    public APIServlet() {
        logger = Logger.getLogger(this.getClass().getName());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        handler.delegateRequest(req, resp);
    }

    private void handleWriteOperation(HttpServletRequest req, HttpServletResponse resp) {

        try (var apiLock = ModalLock.getInstance().getLeaseOrFail()) {

            handler.delegateRequest(req, resp);

        } catch (Exception e) { // Catch everything; to avoid DOS Attacks
            logger.log(Level.WARNING, "API is locked for writing");
            resp.setStatus(HttpResponseType.LOCKED.getStatusCode());
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {

        handleWriteOperation(req, resp);

    }


    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) {

        handleWriteOperation(req, resp);

    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) {

        handleWriteOperation(req, resp);

    }

}
