package edu.kit.iti.crypto.phishy.core.api;

import edu.kit.iti.crypto.phishy.core.tasks.UpdateAddressbookTask;
import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.MutationException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import edu.kit.iti.crypto.phishy.scheduling.Scheduler;

import javax.json.JsonObject;
import java.util.logging.Level;
import java.util.logging.Logger;

public class APIUpdateAddressbookHandler implements APIRequestHandler {
    @Override
    public APIResponseJsonStructure handleRequest(APIRequestJsonObject requestBody)
            throws NotFoundException, CreationException, DeletionException, MutationException {

        requestBody.getRequestType().require(HttpRequestType.POST);


        UpdateAddressbookTask task = new UpdateAddressbookTask();
        Scheduler.getInstance().dispatchNow(task);

        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "API: update addressbook now");

        return new APIResponseJsonObject(HttpResponseType.OK, JsonObject.EMPTY_JSON_OBJECT);

    }
}
