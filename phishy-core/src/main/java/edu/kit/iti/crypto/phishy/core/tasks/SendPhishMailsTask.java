package edu.kit.iti.crypto.phishy.core.tasks;

import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.RSAKeyProvider;
import edu.kit.iti.crypto.phishy.core.JWTKeyProvider;
import edu.kit.iti.crypto.phishy.core.ModalLock;
import edu.kit.iti.crypto.phishy.core.ModalLock.Lease;
import edu.kit.iti.crypto.phishy.core.mail.MailGenerator;
import edu.kit.iti.crypto.phishy.core.mail.MailSpool;
import edu.kit.iti.crypto.phishy.core.mail.SendAgent;
import edu.kit.iti.crypto.phishy.data.ManagerFactory;
import edu.kit.iti.crypto.phishy.data.addressbook.Addressbook;
import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookDescription;
import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookManager;
import edu.kit.iti.crypto.phishy.data.addressbook.Recipient;
import edu.kit.iti.crypto.phishy.data.campaign.*;
import edu.kit.iti.crypto.phishy.data.template.Template;
import edu.kit.iti.crypto.phishy.data.template.TemplateDescription;
import edu.kit.iti.crypto.phishy.data.template.TemplateManager;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import edu.kit.iti.crypto.phishy.scheduling.Scheduler;
import edu.kit.iti.crypto.phishy.scheduling.TaskParameter;

import javax.naming.NamingException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Sends all or single campaigns with Phishing-Mails to random recipients to test them.
 */
public class SendPhishMailsTask implements Runnable {

    private static final int PARTITION_DIV = 100;
    private final RSAKeyProvider rsaKeyProvider = new JWTKeyProvider();

    @TaskParameter("campaign")
    private final CampaignDescription campaignDesc;
    private MailSpool mailSpool;
    private final Logger logger;

    /**
     * Creates SendPhishMailsTask for all campaigns
     */
    public SendPhishMailsTask() {
        this(null);
    }

    /**
     * Creates SendPhishMailsTask for single campaign
     *
     * @param campaignDesc used to sent Phishing-Mails
     */
    public SendPhishMailsTask(CampaignDescription campaignDesc) {
        this.campaignDesc = campaignDesc;
        logger = Logger.getLogger(this.getClass().getName());
    }

    /**
     * Prepares single or all campaigns for the mails to be generated accordingly and sent out.
     */
    @Override
    public void run() {
        try (Lease lease = ModalLock.getInstance().getLease()) {
            Algorithm tokenAlgorithm = Algorithm.RSA256(rsaKeyProvider);

            Scheduler scheduler = Scheduler.getInstance();
            ExecutorService generatorPool = scheduler.getThreadPool();
            ExecutorService sendThread = scheduler.getSingleThread();
            CampaignManager campaignManager = ManagerFactory.getCampaignManager();
            Collection<Future<?>> allThreads = new ArrayList<Future<?>>();
            Future<?> sendAgentFuture;

            if (campaignDesc == null) {
                allThreads.addAll(sendAllCampaigns(tokenAlgorithm, generatorPool, sendThread, campaignManager));
            } else {
                allThreads.addAll(sendSingleCampaign(tokenAlgorithm, generatorPool, sendThread, campaignManager));
            }
            sendAgentFuture = sendThread.submit(createSendAgent());

            for (Future<?> future : allThreads) {
                try {
                    future.get();
                } catch (InterruptedException | ExecutionException e) {
                    logger.log(Level.SEVERE, "Couldn't execute sending phishing mails correctly");
                }
            }

            generatorPool.shutdownNow();
            mailSpool.notifyLastMailAdded();
            try {
                sendAgentFuture.get();

            } catch (InterruptedException | ExecutionException e) {
                logger.log(Level.SEVERE, "Couldn't execute sending phishing mails correctly");
            }
            sendThread.shutdownNow();

        } catch (IllegalStateException e) {
            logger.log(Level.SEVERE, "Couldn't execute sending phishing mails correctly");
        } catch (Exception e1) {
            logger.log(Level.SEVERE, "Couldn't execute sending phishing mails. Waiting for resources was interrupted.");
        }
    }

    private Collection<Future<?>> sendSingleCampaign(Algorithm tokenAlgorithm, ExecutorService generatorPool, ExecutorService sendThread,
                                                     CampaignManager campaignManager) {
        Campaign campaign;
        try {
            campaign = campaignDesc.resolve(campaignManager);
        } catch (NotFoundException e) {
            logger.log(Level.SEVERE, "Couldn't find campaign: " + campaignDesc.getName());
            throw new IllegalStateException();
        }
        mailSpool = new MailSpool(campaign.getRecipientSetSize());
        return sendCampaign(campaign, generatorPool, tokenAlgorithm);
    }

    private Collection<Future<?>> sendAllCampaigns(Algorithm tokenAlgorithm, ExecutorService generatorPool, ExecutorService sendThread,
                                                   CampaignManager campaignManager) {

        Collection<Campaign> campaigns = campaignManager.listCampaigns();

        int mailsCount = 0;
        for (Campaign campaign : campaigns) {
            mailsCount += campaign.getRecipientSetSize();
        }
        mailSpool = new MailSpool(mailsCount);
        Collection<Future<?>> allThreads = new ArrayList<Future<?>>();

        for (Campaign campaign : campaigns) {
            try {
                allThreads.addAll(sendCampaign(campaign, generatorPool, tokenAlgorithm));
            } catch (IllegalStateException e) {
                continue;
            }
        }
        return allThreads;
    }

    private SendAgent createSendAgent() {
        try {
            return new SendAgent(mailSpool);
        } catch (NamingException e) {
            logger.log(Level.SEVERE, "Couldn't find SendSession in JNDI");
            throw new IllegalStateException();
        }
    }

    private Collection<Future<?>> sendCampaign(Campaign campaign, ExecutorService threadPool, Algorithm tokenAlgorithm) {

        AddressbookManager addressbookManager = ManagerFactory.getAddressbookManager();
        Addressbook addressbook;
        try {
            AddressbookDescription addressbookDesc = campaign.getAddressbook();
            addressbook = addressbookDesc.resolve(addressbookManager);
        } catch (NotFoundException e) {
            logger.log(Level.SEVERE, "Couldn't find addressbook for campaign: " + campaign.getName());
            throw new IllegalStateException();
        }

        long mailIntervalMiliSecs = convertDaysinMiliSecs(campaign.getMailInterval());
        Date notTestedSince = new Date(System.currentTimeMillis() - mailIntervalMiliSecs);
        List<Recipient> randRecipients = addressbook.getRandomRecipients(campaign.getRecipientSetSize(), notTestedSince);

        List<Recipient> recipients = createRecipientsWithIDs(randRecipients, campaign);
        List<List<Recipient>> partitionedRecipients = partitionRecipients(recipients);

        TemplateManager templateManager = ManagerFactory.getTemplateManager();
        Template template;
        try {
            TemplateDescription templateDesc = campaign.getTemplate(TemplateType.PHISH_MAIL);
            template = templateDesc.resolve(templateManager);
        } catch (NotFoundException e) {
            logger.log(Level.SEVERE, "Couldn't find template for campaign: " + campaign.getName());
            throw new IllegalStateException();
        }

        Collection<Future<?>> allGenerator = new ArrayList<Future<?>>();
        for (List<Recipient> recipientList : partitionedRecipients) {
            MailGenerator mailGenerator = new MailGenerator(mailSpool, template, recipientList, tokenAlgorithm, campaign.getMailLifetime());
            allGenerator.add(threadPool.submit(mailGenerator));
        }

        addressbook.recordTest(recipients);

        CampaignDescription campaignDesc = new CampaignDescription(campaign.getName());
        campaign.recordTests(createTestDescriptions(campaignDesc, recipients));

        return allGenerator;
    }

    private long convertDaysinMiliSecs(int days) {
        return ((long) days) * 86400000L;
    }

    private Collection<TestDescription> createTestDescriptions(CampaignDescription campaign, List<Recipient> recipientList) {
        return recipientList.stream()
                .map(r -> r.getTestId())
                .collect(Collectors.toList());
    }

    private List<Recipient> createRecipientsWithIDs(List<Recipient> randRecipients, Campaign campaign) {
        List<Recipient> recipients = new ArrayList<Recipient>();
        for (Recipient randRecipient : randRecipients) {
            Recipient recipient = randRecipient.clone();
            TestDescription testDesc = new TestDescription(new CampaignDescription(campaign.getName()), UUID.randomUUID().toString());

            recipient.setTestId(testDesc);
            recipients.add(recipient);
        }
        return recipients;
    }

    private List<List<Recipient>> partitionRecipients(List<Recipient> recipients) {
        List<List<Recipient>> partitionedList = new ArrayList<List<Recipient>>();
        int partitions = recipients.size() / PARTITION_DIV;

        for (int i = 0; i < partitions; i++) {
            partitionedList.add(recipients.subList(PARTITION_DIV * i, PARTITION_DIV * (i + 1)));
        }
        if (recipients.size() % PARTITION_DIV != 0) {
            partitionedList.add(recipients.subList(PARTITION_DIV * partitions, recipients.size()));
        }
        return partitionedList;
    }
}
