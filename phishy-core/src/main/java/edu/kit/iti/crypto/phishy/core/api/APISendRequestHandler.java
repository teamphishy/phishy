package edu.kit.iti.crypto.phishy.core.api;

import edu.kit.iti.crypto.phishy.core.tasks.SendPhishMailsTask;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignDescription;
import edu.kit.iti.crypto.phishy.scheduling.Scheduler;

import javax.json.JsonObject;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Handles request to start sending phish mail
 */
public class APISendRequestHandler implements APIRequestHandler {

    @Override
    public APIResponseJsonStructure handleRequest(APIRequestJsonObject requestBody) {

        requestBody.getRequestType().require(HttpRequestType.POST);

        SendPhishMailsTask task;

        if (requestBody.getRequestPath().isEmpty()) {
            task = new SendPhishMailsTask();
        } else {
            task = new SendPhishMailsTask(new CampaignDescription(requestBody.getRequestPath()));
        }

        Scheduler.getInstance().dispatchNow(task);

        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "API: schedule send mails now");

        return new APIResponseJsonObject(HttpResponseType.OK, JsonObject.EMPTY_JSON_OBJECT);

    }

}
