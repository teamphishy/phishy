package edu.kit.iti.crypto.phishy.core;

import java.time.LocalDateTime;

/**
 * Global lock for administrative work
 * Call `getLease()` to obtain an exclusive lease for max one hour and wait until you can get one.
 * Call `getLeaseOrFail()` to obtain an exclusive lock for max one hour or return immediately.
 * Call `releaseLease(Lease)` to return a lease prior to its expiration
 */
public final class ModalLock {

    /**
     * Exclusive lease for global changes
     */
    public final class Lease implements AutoCloseable {
        private final LocalDateTime expires;

        public Lease() {
            // Lease is valid for one hour
            this.expires = LocalDateTime.now().plusHours(1);
        }

        public boolean isValid() {
            return this.expires.isAfter(LocalDateTime.now());
        }

        @Override
        public void close() throws Exception {

            ModalLock.getInstance().returnLease(this);

        }
    }

    public final class ResourceLockedException extends Exception {

        public ResourceLockedException() {
            super("Global API is locked");
        }

    }

    private Lease currentLease = null;
    private static final ModalLock instance = new ModalLock();

    public static ModalLock getInstance() {
        return instance;
    }

    /**
     * Obtain a lease for max one hour or wait until one is available
     *
     * @return Lease valid for one hour
     * @throws InterruptedException if waiting for a lease was interrupted
     */
    public synchronized Lease getLease() throws InterruptedException {

        while ((currentLease != null) && (currentLease.isValid())) {
            wait();
        }

        currentLease = new Lease();
        return currentLease;

    }

    /**
     * Obtain a lease for max one hour or return `null` if currently locked
     *
     * @return Lease valid for one hour or `null` if no lease available
     */
    public synchronized Lease getLeaseOrFail() throws ResourceLockedException {

        if ((currentLease != null) && (currentLease.isValid())) {
            throw new ResourceLockedException();
        }

        currentLease = new Lease();
        return currentLease;

    }

    /**
     * Return a lease
     *
     * @param lease Lease which should be invalidated
     */
    public synchronized void returnLease(Lease lease) {

        if (lease == currentLease) {
            currentLease = null;
            notifyAll();
        }

    }

}
