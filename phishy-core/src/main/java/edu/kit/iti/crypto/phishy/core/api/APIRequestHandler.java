package edu.kit.iti.crypto.phishy.core.api;

import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.MutationException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

/**
 * Handles API Requests
 */
public interface APIRequestHandler {

    /**
     * Handles API request and returns the result
     *
     * @param requestBody Requests's parsed json data
     * @return Json Data of response
     */
    APIResponseJsonStructure handleRequest(APIRequestJsonObject requestBody) throws NotFoundException,
            CreationException, DeletionException, MutationException;

}
