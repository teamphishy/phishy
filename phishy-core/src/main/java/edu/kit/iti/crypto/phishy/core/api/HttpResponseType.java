package edu.kit.iti.crypto.phishy.core.api;

/**
 * Http Response codes
 */
public enum HttpResponseType {
    OK(200),
    CREATED(201),
    BAD_REQUEST(400),
    UNAUTHORIZED(401),
    FORBIDDEN(403),
    NOT_FOUND(404),
    METHOD_NOT_ALLOWED(405),
    CONFLICT(409),
    LOCKED(423),
    INTERNAL_SERVER_ERROR(500);

    private final int statusCode;

    HttpResponseType(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }
}
