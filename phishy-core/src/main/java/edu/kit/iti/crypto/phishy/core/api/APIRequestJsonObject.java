package edu.kit.iti.crypto.phishy.core.api;

import javax.json.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Parsed JSON Request
 */
public class APIRequestJsonObject implements JsonObject {

    private final String requestEndpoint;
    private final HttpRequestType requestType;
    private final JsonObject jsonObject;
    private final String requestPath;

    public APIRequestJsonObject(HttpServletRequest request) throws IOException {

        requestType = HttpRequestType.fromString(request.getMethod());

        // Some requests do not have data attached
        if (request.getContentLength() < 2) {
            jsonObject = JsonObject.EMPTY_JSON_OBJECT;
        } else {
            JsonReader reader = Json.createReader(request.getReader());
            jsonObject = reader.readObject();
        }

        // Normalize path
        String pathInfo = request.getPathInfo();
        while (pathInfo.charAt(0) == '/') {
            pathInfo = pathInfo.substring(1);
        }
        while (pathInfo.charAt(pathInfo.length() - 1) == '/') {
            pathInfo = pathInfo.substring(0, pathInfo.length() - 1);
        }

        // split path into ENDPOINT/PATH(/TO/SPECIFIC/DATA)
        int requestPathDivider = pathInfo.indexOf('/');
        if (requestPathDivider != -1) {
            requestEndpoint = pathInfo.substring(0, requestPathDivider);
            requestPath = pathInfo.substring(requestPathDivider + 1);
        } else {
            requestEndpoint = pathInfo;
            requestPath = "";
        }


    }


    @Override
    public ValueType getValueType() {
        return jsonObject.getValueType();
    }

    @Override
    public int size() {
        return jsonObject.size();
    }

    @Override
    public boolean isEmpty() {
        return jsonObject.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return jsonObject.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return jsonObject.containsValue(value);
    }

    @Override
    public JsonValue get(Object key) {
        return jsonObject.get(key);
    }

    @Override
    public JsonValue put(String key, JsonValue value) {
        return jsonObject.put(key, value);
    }

    @Override
    public JsonValue remove(Object key) {
        return jsonObject.remove(key);
    }

    @Override
    public void putAll(Map<? extends String, ? extends JsonValue> m) {
        jsonObject.putAll(m);
    }

    @Override
    public void clear() {
        jsonObject.clear();
    }

    @Override
    public Set<String> keySet() {
        return jsonObject.keySet();
    }

    @Override
    public Collection<JsonValue> values() {
        return jsonObject.values();
    }

    @Override
    public Set<Entry<String, JsonValue>> entrySet() {
        return jsonObject.entrySet();
    }

    @Override
    public JsonArray getJsonArray(String name) {
        return jsonObject.getJsonArray(name);
    }

    @Override
    public JsonObject getJsonObject(String name) {
        return jsonObject.getJsonObject(name);
    }

    @Override
    public JsonNumber getJsonNumber(String name) {
        return jsonObject.getJsonNumber(name);
    }

    @Override
    public JsonString getJsonString(String name) {
        return jsonObject.getJsonString(name);
    }

    @Override
    public String getString(String name) {
        return jsonObject.getString(name);
    }

    @Override
    public String getString(String name, String defaultValue) {
        return jsonObject.getString(name, defaultValue);
    }

    @Override
    public int getInt(String name) {
        return jsonObject.getInt(name);
    }

    @Override
    public int getInt(String name, int defaultValue) {
        return jsonObject.getInt(name, defaultValue);
    }

    @Override
    public boolean getBoolean(String name) {
        return jsonObject.getBoolean(name);
    }

    @Override
    public boolean getBoolean(String name, boolean defaultValue) {
        return jsonObject.getBoolean(name, defaultValue);
    }

    @Override
    public boolean isNull(String name) {
        return jsonObject.isNull(name);
    }

    public String getRequestEndpoint() {
        return requestEndpoint;
    }

    public HttpRequestType getRequestType() {
        return requestType;
    }

    public String getRequestPath() {
        return requestPath;
    }
}
