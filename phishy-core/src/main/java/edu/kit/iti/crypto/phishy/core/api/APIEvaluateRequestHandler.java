package edu.kit.iti.crypto.phishy.core.api;

import edu.kit.iti.crypto.phishy.core.tasks.EvaluateReportedMailsTask;
import edu.kit.iti.crypto.phishy.scheduling.Scheduler;

import javax.json.JsonObject;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Handles the request to evaluate reported mail now
 */
public class APIEvaluateRequestHandler implements APIRequestHandler {

    @Override
    public APIResponseJsonStructure handleRequest(APIRequestJsonObject requestBody) {

        requestBody.getRequestType().require(HttpRequestType.POST);


        EvaluateReportedMailsTask task = new EvaluateReportedMailsTask();
        Scheduler.getInstance().dispatchNow(task);

        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "API: schedule evaluate mails now");

        return new APIResponseJsonObject(HttpResponseType.OK, JsonObject.EMPTY_JSON_OBJECT);
    }

}
