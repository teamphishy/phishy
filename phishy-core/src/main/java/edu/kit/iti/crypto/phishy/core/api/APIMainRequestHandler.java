package edu.kit.iti.crypto.phishy.core.api;


import edu.kit.iti.crypto.phishy.data.ManagerFactory;
import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookManager;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignManager;
import edu.kit.iti.crypto.phishy.data.template.TemplateManager;
import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import javax.json.Json;
import javax.json.JsonWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Delegates API requests to the corresponding handler
 */
public class APIMainRequestHandler {

    private final TemplateManager templateManager;
    private final CampaignManager campaignManager;
    private final AddressbookManager addressbookManager;

    private final Logger logger;

    private Map<String, APIRequestHandler> handlers;

    public APIMainRequestHandler() {

        this.logger = Logger.getLogger(this.getClass().getName());
        this.campaignManager = ManagerFactory.getCampaignManager();
        this.templateManager = ManagerFactory.getTemplateManager();
        this.addressbookManager = ManagerFactory.getAddressbookManager();

        this.initHandlers();

    }

    private void initHandlers() {

        handlers = new HashMap<>();
        handlers.put("campaign", new APICampaignRequestHandler(campaignManager));
        handlers.put("template", new APITemplateRequestHandler(templateManager));
        handlers.put("addressbook", new APIAddressbookRequestHandler(addressbookManager));
        handlers.put("stat", new APIStatisticsRequestHandler(campaignManager));
        handlers.put("send", new APISendRequestHandler());
        handlers.put("evaluate", new APIEvaluateRequestHandler());
        handlers.put("clearcache", new APIClearCacheRequestHandler());
        handlers.put("updateab", new APIUpdateAddressbookHandler());

    }


    /**
     * Delegates the request to the corresponding handler
     *
     * @param request  Incoming http request
     * @param response Outgoing http response
     */
    public void delegateRequest(HttpServletRequest request, HttpServletResponse response) {

        try {

            logger.log(Level.INFO, "API: " + request.getMethod() + " " + request.getPathInfo());

            APIRequestJsonObject jsonRequest = new APIRequestJsonObject(request);


            APIRequestHandler requestHandler = handlers.get(jsonRequest.getRequestEndpoint());
            if (requestHandler == null) {
                requestHandler = new APIErrorRequestHandler(new UnsupportedOperationException("Unsupported API"));
            }

            APIResponseJsonStructure jsonResponse = null;
            try {
                jsonResponse = requestHandler.handleRequest(jsonRequest);
            } catch (NotFoundException | CreationException | DeletionException e) {
                requestHandler = new APIErrorRequestHandler(e);
                jsonResponse = requestHandler.handleRequest(jsonRequest);
            }

            assert jsonResponse != null;

            response.setStatus(jsonResponse.getResponseType().getStatusCode());
            JsonWriter jsonWriter = Json.createWriter(response.getWriter());
            jsonWriter.write(jsonResponse);

        } catch (Exception e) { // Catch everything; to avoid DOS Attacks
            e.printStackTrace();
            logger.log(Level.SEVERE, "API Error: " + e);
            response.setStatus(HttpResponseType.INTERNAL_SERVER_ERROR.getStatusCode());
        }


    }
}
