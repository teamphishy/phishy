package edu.kit.iti.crypto.phishy.core.api;

import edu.kit.iti.crypto.phishy.data.campaign.CampaignDescription;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignManager;
import edu.kit.iti.crypto.phishy.data.statistics.StatisticsData;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Handles requests for statistics data
 */
public class APIStatisticsRequestHandler implements APIRequestHandler {

    private CampaignManager manager;
    private Logger logger;

    public APIStatisticsRequestHandler(CampaignManager campaignManager) {

        this.manager = campaignManager;
        this.logger = Logger.getLogger(this.getClass().getName());

    }

    @Override
    public APIResponseJsonStructure handleRequest(APIRequestJsonObject requestBody) throws NotFoundException {

        requestBody.getRequestType().require(HttpRequestType.GET);

        if (requestBody.getRequestPath().isEmpty()) {
            return getCumulatedStatistics();
        } else {
            return getCampaignStatistics(new CampaignDescription(requestBody.getRequestPath()));
        }


    }

    /**
     * Get statistics for a specific campaign
     */
    private APIResponseJsonStructure getCampaignStatistics(CampaignDescription campaignDescription)
            throws NotFoundException {

        StatisticsData data = campaignDescription.resolve(manager).getStatistics();

        logger.log(Level.INFO, "API: List campaign statistics for '" + campaignDescription + "'");

        return new APIResponseJsonObject(HttpResponseType.OK, formatStatistics(data));

    }

    /**
     * Get cumulated statistics over all campaigns
     */
    private APIResponseJsonStructure getCumulatedStatistics() {

        StatisticsData data = manager.getCumulatedStatistics();

        logger.log(Level.INFO, "API: List cumulated statistics");

        return new APIResponseJsonObject(HttpResponseType.OK, formatStatistics(data));

    }

    /**
     * Format statistics to JSON Object
     */
    private JsonObject formatStatistics(StatisticsData data) {

        JsonObjectBuilder jsonStatistics = Json.createObjectBuilder();
        jsonStatistics.add("numSentMails", data.getSentMails());
        jsonStatistics.add("numReportedMails", data.getReportedMails());
        jsonStatistics.add("numPhishedMails", data.getPhishedMails());

        return jsonStatistics.build();

    }


}
