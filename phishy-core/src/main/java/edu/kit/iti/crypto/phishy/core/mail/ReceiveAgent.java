package edu.kit.iti.crypto.phishy.core.mail;

import javax.mail.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Receives mails from an IMAP server
 */
public class ReceiveAgent {

    private final Logger logger;

    /**
     * Creates ReceiveAgent
     */
    public ReceiveAgent() {
        logger = Logger.getLogger(this.getClass().getName());
    }

    /**
     * Receives mails from the IMAP server defined with JNDI.
     * Mails will have header with Phishing-ID and mail address prefetched.
     *
     * @return the mails from server
     */
    public Message[] receiveMails() {
        try {
            Context initialContext = new InitialContext();
            Session receiveSession = (Session) initialContext.lookup("java:comp/env/mail/ReceiveSession");

            //set JNDI mail.store.protocol="PROTOCOL"
            Store emailStore = receiveSession.getStore();
            emailStore.connect();

            Folder emailFolder = emailStore.getFolder(this.getFolderName());
            emailFolder.open(Folder.READ_ONLY);
            Message[] messages = emailFolder.getMessages();

            FetchProfile fetchProfile = new FetchProfile();
            fetchProfile.add(FetchProfile.Item.ENVELOPE);
            fetchProfile.add("X-Phishy-ID");
            emailFolder.fetch(messages, fetchProfile);

            return messages;

        } catch (NamingException e) {
            logger.log(Level.SEVERE, "Couldn't find ReceiveSession in JNDI");
        } catch (NoSuchProviderException e) {
            logger.log(Level.SEVERE, "Mail Server doesn't provide Imaps connection");
        } catch (MessagingException e) {
            logger.log(Level.SEVERE, "Unable to receive mails. Check if connection credentials are correct.");
        }
        throw new IllegalStateException();
    }

    private String getFolderName() {

        Context initialContext = null;
        try {
            initialContext = new InitialContext();
            return (String) initialContext.lookup("java:comp/env/phishy/reportFolder");
        } catch (NamingException e) {
            return "inbox";
        }

    }
}
