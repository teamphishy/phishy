package edu.kit.iti.crypto.phishy.scheduling;

import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookDescription;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignDescription;
import edu.kit.iti.crypto.phishy.data.template.TemplateDescription;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Manages a Task and its scheduling information
 */
public class ScheduledTask {

    private Date time = null;
    private Collection<Weekday> weekdays = List.of();
    private Map<String, String> parameters = Map.of();
    private Runnable thread = null;
    private final Logger logger;


    public ScheduledTask(Runnable thread, Collection<Weekday> weekdays, Date time, Map<String, String> parameters) {

        this.thread = thread;
        this.weekdays = weekdays;
        this.time = time;
        this.parameters = parameters;
        this.logger = Logger.getLogger(this.getClass().getName());

    }

    /**
     * Create a new ScheduledTask from the given data
     *
     * @param threadClassName Class name for the Task's Runnable
     * @param weekdays        Comma-separated list of weekdays to run this Task
     * @param time            String representing the time to run this Task (HH:mm)
     * @param parameters      List of Parameters to set prior to task invocation
     * @return ScheduledTask from the given data
     * @throws ParseException            if Time is not a valid Time
     * @throws ClassNotFoundException    if threadClassName is not a valid class
     * @throws NoSuchMethodException     if the thread class does not have an empty constructor
     * @throws IllegalAccessException    if the thread class's constructor is not accessible
     * @throws InvocationTargetException if thread class cannot be instantiated
     * @throws InstantiationException    if thread class cannot be instantiated
     */
    public static ScheduledTask fromData(String threadClassName, String weekdays, String time,
                                         Map<String, String> parameters)
            throws ParseException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException,
            InvocationTargetException, InstantiationException {

        Date dateTime = new SimpleDateFormat("HH:mm").parse(time);
        Collection<Weekday> weekdayList = Weekday.fromString(weekdays);

        // Get thread class
        Class<?> threadClass = Class.forName(threadClassName);
        Constructor<?> constructor = threadClass.getConstructor();
        Runnable runnableTask = (Runnable) constructor.newInstance();

        // Create a "real" ScheduledTask
        return new ScheduledTask(runnableTask, weekdayList, dateTime, parameters);

    }

    /**
     * Set Task's parameters and dispatch task in shared thread pool
     */
    public void start() {

        Class<?> threadClass = thread.getClass();
        for (Field field : threadClass.getDeclaredFields()) {
            if (field.isAnnotationPresent(TaskParameter.class)) {
                try {
                    // Replace parameters
                    field.setAccessible(true);
                    String paramValue = getParameter(field.getAnnotation(TaskParameter.class).value());
                    if (paramValue != null) {

                        if (field.getType() == TemplateDescription.class) {
                            TemplateDescription templateDescription = TemplateDescription.fromFQN(paramValue);
                            field.set(thread, templateDescription);

                        } else if (field.getType() == CampaignDescription.class) {
                            CampaignDescription campaignDescription = new CampaignDescription(paramValue);
                            field.set(thread, campaignDescription);


                        } else if (field.getType() == AddressbookDescription.class) {
                            AddressbookDescription addressbookDescription = new AddressbookDescription(paramValue);
                            field.set(thread, addressbookDescription);

                        } else {
                            field.set(thread, paramValue);
                        }
                    }


                } catch (IllegalAccessException e) {
                    // Silent fail
                    logger.log(Level.SEVERE, e.getMessage());
                }

            }
        }

        // Dispatch runnable on shared thread pool
        Scheduler.getInstance().dispatchNow(thread);

    }

    private String getParameter(String name) {

        return parameters.get(name);

    }

    @Override
    public String toString() {
        return "ScheduledTask{"
                + "time=" + time
                + ", weekdays=" + weekdays
                + ", parameters=" + parameters
                + ", thread=" + thread.getClass().getName() + '}';
    }

    public Date getTime() {
        return time;
    }

    public Collection<Weekday> getWeekdays() {
        return weekdays;
    }
}
