package edu.kit.iti.crypto.phishy.core.api;

import javax.json.*;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Http JSON Array Response
 */
public class APIResponseJsonArray extends APIResponseJsonStructure implements JsonArray {

    private JsonArray jsonArray;

    public APIResponseJsonArray(HttpResponseType responseType, JsonArray jsonArray) {

        super(responseType);
        this.jsonArray = jsonArray;

    }

    @Override
    public JsonObject getJsonObject(int i) {
        return jsonArray.getJsonObject(i);
    }

    @Override
    public JsonArray getJsonArray(int i) {
        return jsonArray.getJsonArray(i);
    }

    @Override
    public JsonNumber getJsonNumber(int i) {
        return jsonArray.getJsonNumber(i);
    }

    @Override
    public JsonString getJsonString(int i) {
        return jsonArray.getJsonString(i);
    }

    @Override
    public <T extends JsonValue> List<T> getValuesAs(Class<T> aClass) {
        return jsonArray.getValuesAs(aClass);
    }

    @Override
    public String getString(int i) {
        return jsonArray.getString(i);
    }

    @Override
    public String getString(int i, String s) {
        return jsonArray.getString(i, s);
    }

    @Override
    public int getInt(int i) {
        return jsonArray.getInt(i);
    }

    @Override
    public int getInt(int i, int i1) {
        return jsonArray.getInt(i, i1);
    }

    @Override
    public boolean getBoolean(int i) {
        return jsonArray.getBoolean(i);
    }

    @Override
    public boolean getBoolean(int i, boolean b) {
        return jsonArray.getBoolean(i, b);
    }

    @Override
    public boolean isNull(int i) {
        return jsonArray.isNull(i);
    }

    @Override
    public int size() {
        return jsonArray.size();
    }

    @Override
    public boolean isEmpty() {
        return jsonArray.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return jsonArray.contains(o);
    }

    @Override
    public Iterator<JsonValue> iterator() {
        return jsonArray.iterator();
    }

    @Override
    public Object[] toArray() {
        return jsonArray.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return jsonArray.toArray(a);
    }

    @Override
    public boolean add(JsonValue jsonValue) {
        return jsonArray.add(jsonValue);
    }

    @Override
    public boolean remove(Object o) {
        return jsonArray.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return jsonArray.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends JsonValue> c) {
        return jsonArray.addAll(c);
    }

    @Override
    public boolean addAll(int index, Collection<? extends JsonValue> c) {
        return jsonArray.addAll(index, c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return jsonArray.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return jsonArray.retainAll(c);
    }

    @Override
    public void clear() {
        jsonArray.clear();
    }

    @Override
    public JsonValue get(int index) {
        return jsonArray.get(index);
    }

    @Override
    public JsonValue set(int index, JsonValue element) {
        return jsonArray.set(index, element);
    }

    @Override
    public void add(int index, JsonValue element) {
        jsonArray.add(index, element);
    }

    @Override
    public JsonValue remove(int index) {
        return jsonArray.remove(index);
    }

    @Override
    public int indexOf(Object o) {
        return jsonArray.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return jsonArray.lastIndexOf(o);
    }

    @Override
    public ListIterator<JsonValue> listIterator() {
        return jsonArray.listIterator();
    }

    @Override
    public ListIterator<JsonValue> listIterator(int index) {
        return jsonArray.listIterator(index);
    }

    @Override
    public List<JsonValue> subList(int fromIndex, int toIndex) {
        return jsonArray.subList(fromIndex, toIndex);
    }

    @Override
    public ValueType getValueType() {
        return jsonArray.getValueType();
    }
}
