# Phishy - **P**reventive **H**acking to **I**ncrease **S**ecurity in **H**andling **Y**our data

Phishy is an open-source tool for automatically sending training phishing emails and monitoring the response of their recipients. 
It is meant to be used for training and preparation so that an organization can increase the chances of an employee detecting a malicious email.

**Phishy is not meant, nor allowed to be used for malicious purposes ([License](https://git.scc.kit.edu/phishermansfriend/phishy/-/blob/trunk/LICENSE)).**

Features:

- Automatically send emails to a randomly selected group of recipients
- Managing of multiple different phishing campaigns
- Customizable minimum mail intervals and amount of recipient
- Fully customizable email and website templates, either versioned through **Git** or just sitting on a hard drive
- Automated feedback emails and website
- Support for **LDAP** and **CSV** addressbooks
- Support for **SQLite** and **MariaDB** databases
- **[Prometheus](https://prometheus.io/)** interface for evaluation
- Deployable as a [Docker Container](https://www.docker.com/resources/what-container)
- **REST API** and **command-line interface** for administation and configuration

Supported Platforms:
- Linux/POSIX compatible OS

Requirements:
- Java 11 or later
- Maven
- Docker

See [wiki](https://git.scc.kit.edu/phishermansfriend/phishy/-/wikis/home) for more information.