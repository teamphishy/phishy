package edu.kit.iti.crypto.phishy.cli.template;

import picocli.CommandLine;

@CommandLine.Command(
        name = "group",
        description = "Template group related requests",
        subcommands = {
                CLITemplateGroupCreate.class,
                CLITemplateGroupGit.class,
                CLITemplateGroupList.class,
                CLITemplateGroupRemove.class
        }
)
public class CLITemplateGroup {
}
