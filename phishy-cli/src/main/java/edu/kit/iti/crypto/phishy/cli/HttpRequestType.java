package edu.kit.iti.crypto.phishy.cli;

public enum HttpRequestType {
    GET,
    PUT,
    POST,
    DELETE
}
