package edu.kit.iti.crypto.phishy.cli.statistics;

import edu.kit.iti.crypto.phishy.cli.*;
import picocli.CommandLine;

import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.validation.constraints.Pattern;
import java.util.concurrent.Callable;

@CommandLine.Command(
        name = "statistics",
        description = "Outputs statistics about the phishing mails"
)
public class CLIStatistics implements Callable<Integer> {
    private static final String REQUEST_ENDPOINT = "/phishy/api/stat/";

    @Pattern(regexp = CLIMain.NAME_PATTERN, message = "Invalid string format for CAMPAIGN!")
    @CommandLine.Option(
            names = "-c",
            paramLabel = "CAMPAIGN",
            description = "The name of the Campaign"
    )
    private String campaign;

    @CommandLine.Option(
            names = "--sentMail",
            description = "Outputs the amount of sent mails as a single unformatted number"
    )
    private boolean sentMail;

    @CommandLine.Option(
            names = "--phishedMail",
            description = "Outputs the amount of phished mails as a single unformatted number"
    )
    private boolean phishedMail;

    @CommandLine.Option(
            names = "--reportedMail",
            description = "Outputs the amount of reported mails as a single unformatted number"
    )
    private boolean reportedMail;

    @CommandLine.Spec
    private CommandLine.Model.CommandSpec spec;

    @Override
    public Integer call() {
        CLIMain.validate(this, spec.commandLine());
        CLIRequestJsonObject request = new CLIRequestJsonObject(REQUEST_ENDPOINT, HttpRequestType.GET);
        if (campaign != null) {
            request.setRequestEndpoint(REQUEST_ENDPOINT + campaign);
        }

        CLIResponseJsonObject response = HttpHandler.sendRequest(request);
        if (!HttpResponseErrorChecker.checkForErrors(response, JsonValue.ValueType.OBJECT)) {
            return -1;
        }

        int sentMails;
        int reportedMails;
        int phishedMails;

        try {
            JsonObject jsonResponse = response.getJsonStructure().asJsonObject();
            sentMails = jsonResponse.getInt("numSentMails");
            reportedMails = jsonResponse.getInt("numReportedMails");
            phishedMails = jsonResponse.getInt("numPhishedMails");
        } catch (NullPointerException | ClassCastException | JsonException e) {
            System.err.println("Error while parsing JSON");
            return -1;
        }

        if (sentMail) {
            System.out.println(sentMails);
            return 0;
        } else if (phishedMail) {
            System.out.println(phishedMails);
            return 0;
        } else if (reportedMail) {
            System.out.println(reportedMails);
            return 0;
        }

        if (campaign == null) {
            System.out.println("There were " + sentMails + " mails sent, " + phishedMails + " of them successfully phished their recipients and " + reportedMails + " mails were reported.");
        } else {
            System.out.println("Statistics for campaign: " + campaign);
            System.out.println("Number of sent mails: " + sentMails);
            System.out.println("Number of reported mails: " + reportedMails);
            System.out.println("Number of phished mails: " + phishedMails);
        }
        return 0;
    }
}
