package edu.kit.iti.crypto.phishy.cli.campaign;

import edu.kit.iti.crypto.phishy.cli.*;
import picocli.CommandLine;

import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.validation.constraints.Pattern;
import java.util.concurrent.Callable;

@CommandLine.Command(
        name = "print",
        description = "Shows information about the specified campaign"
)
public class CLICampaignPrint implements Callable<Integer> {

    @Pattern(regexp = CLIMain.NAME_PATTERN, message = "Invalid string format for NAME!")
    @CommandLine.Parameters(
            paramLabel = "NAME",
            description = "The name of the Campaign"
    )
    private String name;

    @CommandLine.Spec
    private CommandLine.Model.CommandSpec spec;

    @Override
    public Integer call() {
        CLIMain.validate(this, spec.commandLine());

        CLIRequestJsonObject request = new CLIRequestJsonObject(CLICampaign.REQUEST_ENDPOINT + this.name, HttpRequestType.GET);

        CLIResponseJsonObject response = HttpHandler.sendRequest(request);
        if (!HttpResponseErrorChecker.checkForErrors(response, JsonValue.ValueType.OBJECT)) {
            return -1;
        }

        try {
            JsonObject responseJson = response.getJsonStructure().asJsonObject();
            System.out.println("Name: " + CLIMain.getValueOrDefault(responseJson.getString("id")));
            System.out.println("Addressbook: " + CLIMain.getValueOrDefault(responseJson.getString("addressbook")));
            System.out.println("Mail Lifetime: " + responseJson.getInt("mailLifetime"));
            System.out.println("Recipient Set Size: " + responseJson.getInt("recipientSetSize"));
            System.out.println("Mail Interval: " + responseJson.getInt("mailInterval"));
            System.out.println("Status: " + CLIMain.getValueOrDefault(responseJson.getString("status")));
            JsonObject templates = responseJson.getJsonObject("templates");
            System.out.println("Templates:");
            System.out.println("    infoMail: " + CLIMain.getValueOrDefault(templates.getString("infoMail")));
            System.out.println("    infoWebsite: " + CLIMain.getValueOrDefault(templates.getString("infoWebsite")));
            System.out.println("    phishMail: " + CLIMain.getValueOrDefault(templates.getString("phishMail")));
            System.out.println("    phishWebsite: " + CLIMain.getValueOrDefault(templates.getString("phishWebsite")));
        } catch (NullPointerException | ClassCastException | JsonException e) {
            System.err.println("Error while parsing JSON");
            return -1;
        }
        return 0;
    }
}
