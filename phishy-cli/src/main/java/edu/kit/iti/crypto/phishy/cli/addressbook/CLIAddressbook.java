package edu.kit.iti.crypto.phishy.cli.addressbook;

import picocli.CommandLine;

@CommandLine.Command(
        name = "addressbook",
        description = "Addressbook related requests",
        subcommands = {
                CLIAddressbookList.class,
                CLIAddressbookUpdate.class
        }
)
public class CLIAddressbook {
    public static final String REQUEST_ENDPOINT = "/phishy/api/addressbook/";
    public static final String UPDATE_ENDPOINT = "/phishy/api/updateab/";
}
