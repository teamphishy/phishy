package edu.kit.iti.crypto.phishy.cli.campaign;

import picocli.CommandLine;

@CommandLine.Command(
        name = "campaign",
        description = "Campaign related requests",
        subcommands = {
                CLICampaignCreate.class,
                CLICampaignLink.class,
                CLICampaignList.class,
                CLICampaignPause.class,
                CLICampaignPrint.class,
                CLICampaignRemove.class,
                CLICampaignResume.class,
                CLICampaignUnlink.class,
                CLICampaignUpdate.class
        }
)
public class CLICampaign {
    public static final String REQUEST_ENDPOINT = "/phishy/api/campaign/";
}
