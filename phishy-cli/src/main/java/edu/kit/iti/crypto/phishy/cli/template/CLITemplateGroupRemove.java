package edu.kit.iti.crypto.phishy.cli.template;

import edu.kit.iti.crypto.phishy.cli.*;
import picocli.CommandLine;

import javax.json.JsonValue;
import javax.validation.constraints.Pattern;
import java.util.concurrent.Callable;

@CommandLine.Command(
        name = "remove",
        description = "Removes a template group"
)
public class CLITemplateGroupRemove implements Callable<Integer> {
    @Pattern(regexp = CLIMain.TEMPLATE_GROUP_NAME_PATTERN, message = "Invalid string format for NAME!")
    @CommandLine.Parameters(
            paramLabel = "NAME",
            description = "The name of the Template Group"
    )
    private String name;

    @CommandLine.Option(
            names = "--yes",
            description = "Disables confirmation prompt"
    )
    private boolean yes;

    @CommandLine.Spec
    private CommandLine.Model.CommandSpec spec;

    @Override
    public Integer call() {
        CLIMain.validate(this, spec.commandLine());
        CLIRequestJsonObject request = new CLIRequestJsonObject(CLITemplate.REQUEST_ENDPOINT + this.name, HttpRequestType.DELETE);

        if (!yes) {
            boolean choice = CLIMain.showConfirmationPrompt();
            if (!choice) return 0;
        }

        CLIResponseJsonObject response = HttpHandler.sendRequest(request);
        if (!HttpResponseErrorChecker.checkForErrors(response, JsonValue.ValueType.OBJECT)) {
            return -1;
        }
        return 0;
    }
}
