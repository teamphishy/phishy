package edu.kit.iti.crypto.phishy.cli.template;

import edu.kit.iti.crypto.phishy.cli.*;
import picocli.CommandLine;

import javax.json.JsonArray;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.validation.constraints.Pattern;
import java.util.concurrent.Callable;

@CommandLine.Command(
        name = "git",
        description = "Outputs the git repository of the template group"
)
public class CLITemplateGroupGit implements Callable<Integer> {
    @Pattern(regexp = CLIMain.TEMPLATE_GROUP_NAME_PATTERN, message = "Invalid string format for NAME!")
    @CommandLine.Parameters(
            paramLabel = "NAME",
            description = "The name of the Template Group"
    )
    private String name;

    @CommandLine.Spec
    private CommandLine.Model.CommandSpec spec;

    @Override
    public Integer call() {
        CLIMain.validate(this, spec.commandLine());
        CLIRequestJsonObject request = new CLIRequestJsonObject(CLITemplate.REQUEST_ENDPOINT, HttpRequestType.GET);

        CLIResponseJsonObject response = HttpHandler.sendRequest(request);
        if (!HttpResponseErrorChecker.checkForErrors(response, JsonValue.ValueType.OBJECT)) {
            return -1;
        }

        try {
            JsonObject responseJson = response.getJsonStructure().asJsonObject();
            JsonArray groups = responseJson.getJsonArray("groups");
            for (int i = 0; i < groups.size(); i++) {
                JsonObject current = groups.getJsonObject(i);
                if (current.getString("id").equals(this.name)) {
                    System.out.println(current.getString("git"));
                    return 0;
                }
            }
        } catch (NullPointerException | ClassCastException | JsonException e) {
            System.err.println("Error while parsing JSON");
            return -1;
        }
        System.err.println("Group not found");
        return -1;
    }
}
