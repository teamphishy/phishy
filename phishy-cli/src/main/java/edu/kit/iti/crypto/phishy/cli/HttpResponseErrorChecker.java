package edu.kit.iti.crypto.phishy.cli;

import javax.json.JsonValue;

public class HttpResponseErrorChecker {
    private static boolean verbose;

    public static boolean checkForErrors(CLIResponseJsonObject response, JsonValue.ValueType jsonType) {
        final String NO_RESPONSE = "Received no response, please try again later.";
        final String DEFAULT_RESPONSE = "The server responded: ";
        final String EXPECTED_JSON = "Response json object is empty";

        if (response == null) {
            System.err.println("ERROR: " + NO_RESPONSE);
            return false;
        }

        if (verbose) {
            System.out.println(DEFAULT_RESPONSE + response.getResponseType().getStatusCode() + " "
                    + response.getResponseType().name());
        }
        verbose = false;

        switch (response.getResponseType()) {
            case BAD_REQUEST:
                System.err.println("ERROR: 400 BAD_REQUEST");
                return false;
            case NOT_FOUND:
                System.err.println("ERROR: 404 NOT_FOUND");
                return false;
            case METHOD_NOT_ALLOWED:
                System.err.println("ERROR: 405 METHOD_NOT_ALLOWED");
                return false;
            case CONFLICT:
                System.err.println("ERROR: 409 CONFLICT");
                return false;
            default: //no operation
                break;
        }

        if (response.getJsonStructure().getValueType() != jsonType) {
            System.err.println("Expected JSON Type " + jsonType.name()
                    + " but got " + response.getJsonStructure().getValueType().name());
            return false;
        }

        return true;
    }

    public static void setVerbose() {
        HttpResponseErrorChecker.verbose = true;
    }
}
