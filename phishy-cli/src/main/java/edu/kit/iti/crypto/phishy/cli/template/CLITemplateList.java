package edu.kit.iti.crypto.phishy.cli.template;

import edu.kit.iti.crypto.phishy.cli.*;
import picocli.CommandLine;

import javax.json.JsonArray;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonValue;
import java.util.concurrent.Callable;

@CommandLine.Command(
        name = "list",
        description = "Lists all groups and templates"
)
public class CLITemplateList implements Callable<Integer> {
    @Override
    public Integer call() {
        CLIRequestJsonObject request = new CLIRequestJsonObject(CLITemplate.REQUEST_ENDPOINT, HttpRequestType.GET);
        CLIResponseJsonObject response = HttpHandler.sendRequest(request);
        if (!HttpResponseErrorChecker.checkForErrors(response, JsonValue.ValueType.OBJECT)) {
            return -1;
        }

        try {
            JsonObject responseJson = response.getJsonStructure().asJsonObject();
            JsonArray groups = responseJson.getJsonArray("groups");

            // Only print template groups
            for (int i = 0; i < groups.size(); i++) {
                JsonObject o = groups.getJsonObject(i);
                System.out.println(CLIMain.getValueOrDefault(o.getString("id")));
            }
        } catch (NullPointerException | ClassCastException | JsonException e) {
            System.err.println("Error while parsing JSON");
            return -1;
        }
        return 0;
    }
}
