package edu.kit.iti.crypto.phishy.cli.campaign;

import edu.kit.iti.crypto.phishy.cli.*;
import picocli.CommandLine;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import java.util.concurrent.Callable;

@CommandLine.Command(
        name = "update",
        description = "Updates the parameters of a campaign"
)
public class CLICampaignUpdate implements Callable<Integer> {
    private final String statusPattern = "active|inactive|ACTIVE|INACTIVE";

    @Pattern(regexp = CLIMain.NAME_PATTERN, message = "Invalid string format!")
    @CommandLine.Parameters(
            paramLabel = "NAME",
            description = "The name of the Campaign"
    )
    private String name;

    @Pattern(regexp = CLIMain.NAME_PATTERN, message = "Invalid string format for ADDRESSBOOK!")
    @CommandLine.Option(
            names = "-a",
            paramLabel = "ADDRESSBOOK",
            description = "The name of the Addressbook"
    )
    private String addressbook;

    @Positive(message = "mailLifetime must be greater 0")
    @CommandLine.Option(
            names = "--mailLifetime",
            description = "The amount of days a phishing Mail is valid"
    )
    private Integer mailLifetime;

    @Positive(message = "recipientSetSize must be greater 0")
    @CommandLine.Option(
            names = "--recipientSetSize",
            description = "The amount of recipients that are messaged in a cycle"
    )
    private Integer recipientSetSize;

    @Positive(message = "mailInterval must be greater 0")
    @CommandLine.Option(
            names = "--mailInterval",
            description = "The minimum amount of days between two phishing mails to the same user"
    )
    private Integer mailInterval;

    @Pattern(regexp = statusPattern, message = "Invalid string format for status!")
    @CommandLine.Option(
            names = "--status",
            description = "The status of the Campaign (active, inactive)"
    )
    private String status;

    @Pattern(regexp = CLIMain.TEMPLATE_GROUP_NAME_PATTERN + "-" + CLIMain.NAME_PATTERN, message = "Invalid string format for phishMail!")
    @CommandLine.Option(
            names = "--phishMail",
            paramLabel = "TEMPLATE",
            description = "The name of the phishing mail template"
    )
    private String phishMail;

    @Pattern(regexp = CLIMain.TEMPLATE_GROUP_NAME_PATTERN + "-" + CLIMain.NAME_PATTERN, message = "Invalid string format for phishWebsite!")
    @CommandLine.Option(
            names = "--phishWebsite",
            paramLabel = "TEMPLATE",
            description = "The name of the phishing website template"
    )
    private String phishWebsite;

    @Pattern(regexp = CLIMain.TEMPLATE_GROUP_NAME_PATTERN + "-" + CLIMain.NAME_PATTERN, message = "Invalid string format for infoMail!")
    @CommandLine.Option(
            names = "--infoMail",
            paramLabel = "TEMPLATE",
            description = "The name of the info mail template"
    )
    private String infoMail;

    @Pattern(regexp = CLIMain.TEMPLATE_GROUP_NAME_PATTERN + "-" + CLIMain.NAME_PATTERN, message = "Invalid string format for infoWebsite!")
    @CommandLine.Option(
            names = "--infoWebsite",
            paramLabel = "TEMPLATE",
            description = "The name of the info website template"
    )
    private String infoWebsite;

    @CommandLine.Spec
    private CommandLine.Model.CommandSpec spec;

    @Override
    public Integer call() {
        CLIMain.validate(this, spec.commandLine());
        CLIRequestJsonObject request = new CLIRequestJsonObject(CLICampaign.REQUEST_ENDPOINT + this.name, HttpRequestType.PUT);
        JsonObjectBuilder requestJson = Json.createObjectBuilder();

        if (addressbook != null) {
            requestJson.add("addressbook", this.addressbook);
        }

        JsonObjectBuilder jsonTemplates = Json.createObjectBuilder();
        if (phishMail != null) {
            jsonTemplates.add("phishMail", this.phishMail);
        }
        if (phishWebsite != null) {
            jsonTemplates.add("phishWebsite", this.phishWebsite);
        }
        if (infoMail != null) {
            jsonTemplates.add("infoMail", this.infoMail);
        }
        if (infoWebsite != null) {
            jsonTemplates.add("infoWebsite", this.infoWebsite);
        }

        if (mailLifetime != null) {
            requestJson.add("mailLifetime", mailLifetime);
        }
        if (recipientSetSize != null) {
            requestJson.add("recipientSetSize", recipientSetSize);
        }
        if (mailInterval != null) {
            requestJson.add("mailInterval", mailInterval);
        }
        if (status != null) {
            requestJson.add("status", status);
        }

        requestJson.add("templates", jsonTemplates);
        request.addJsonObject(requestJson.build());

        CLIResponseJsonObject response = HttpHandler.sendRequest(request);
        if (!HttpResponseErrorChecker.checkForErrors(response, JsonValue.ValueType.OBJECT)) {
            return -1;
        }
        return 0;
    }
}
