package edu.kit.iti.crypto.phishy.cli;

import javax.json.*;
import javax.json.stream.JsonParsingException;
import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Base64;

public class HttpHandler {
    private static String hostAddress;
    private static String username;
    private static String password;

    public static void setAuthenticationMethod(String user, String pass) {
        HttpHandler.username = user;
        HttpHandler.password = pass;
    }

    public static void setHost(String host) {
        HttpHandler.hostAddress = host;
    }

    public static CLIResponseJsonObject sendRequest(CLIRequestJsonObject request) {
        //try loading from config file
        if (hostAddress == null) {
            hostAddress = ConfigHandler.loadConfigHost();
        }
        if (username == null || password == null) {
            String[] auth = ConfigHandler.loadConfigAuth();
            username = auth[0];
            password = auth[1];
        }

        if (hostAddress == null) {
            System.err.println("The host address is not set");
            System.exit(-1);
        }
        if (username == null || password == null) {
            System.err.println("Username and password are not set");
            System.exit(-1);
        }
        try {
            if (hostAddress.endsWith("/")) {
                hostAddress = hostAddress.substring(0, hostAddress.length() - 1);
            }
            URL url = new URL(hostAddress + request.getRequestEndpoint());
            JsonObject requestJsonBody = request.getJsonObject();

            HttpClient client = HttpClient.newBuilder()
                    .followRedirects(HttpClient.Redirect.NORMAL)
                    .build();

            HttpRequest.Builder httpRequestBuilder = HttpRequest.newBuilder()
                    .uri(url.toURI())
                    .timeout(Duration.of(15, ChronoUnit.SECONDS))
                    .method(request.getRequestType().name(), HttpRequest.BodyPublishers.ofString(requestJsonBody.toString()))
                    .setHeader("Content-Type", "application/json;charset=UTF-8")
                    .setHeader("Accept", "application/json")
                    .setHeader("Authorization", basicAuth(username, password));

            if (requestJsonBody.isEmpty()) {
                httpRequestBuilder.method(request.getRequestType().name(), HttpRequest.BodyPublishers.noBody());
            } else {
                httpRequestBuilder.method(request.getRequestType().name(), HttpRequest.BodyPublishers.ofString(requestJsonBody.toString()));
            }
            HttpRequest httpRequest = httpRequestBuilder.build();

            HttpResponse<String> response = client.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            HttpResponseType statusCode = HttpResponseType.fromInt(response.statusCode());
            JsonStructure jsonStructure = convertToJsonObject(response);
            return new CLIResponseJsonObject(statusCode, jsonStructure);

        } catch (MalformedURLException | URISyntaxException e) {
            System.err.println("Invalid host address format: " + e.getMessage());
            System.exit(-1);
            return null;
        } catch (ProtocolException e) {
            System.err.println("TCP Error: " + e.getMessage());
            System.exit(-1);
            return null;
        } catch (IOException e) {
            System.err.println("Error while opening HTTP connection: " + e.getMessage());
            System.exit(-1);
            return null;
        } catch (JsonParsingException e) {
            System.err.println("Error while parsing Json: " + e.getMessage());
            System.exit(-1);
            return null;
        } catch (InterruptedException e) {
            System.err.println("Error while sending request");
            System.exit(-1);
            return null;
        }
    }

    private static String basicAuth(String username, String password) {
        return "Basic " + Base64.getEncoder().encodeToString((username + ":" + password).getBytes());
    }

    private static JsonStructure convertToJsonObject(HttpResponse<String> response) throws JsonParsingException {
        String body = response.body();
        if (body.isEmpty()) {
            return JsonObject.EMPTY_JSON_OBJECT;
        }
        try {
            JsonReader jsonReader = Json.createReader(new StringReader(body));
            JsonObject object = jsonReader.readObject();
            jsonReader.close();
            return object;
        } catch (JsonParsingException e) {
            try {
                JsonReader jsonReader = Json.createReader(new StringReader(body));
                JsonArray array = jsonReader.readArray();
                jsonReader.close();
                return array;
            } catch (JsonParsingException ex) {
                System.err.println("Error while parsing json");
                System.exit(-1);
                return null;
            }
        }
    }
}
