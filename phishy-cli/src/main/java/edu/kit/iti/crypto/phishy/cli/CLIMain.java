package edu.kit.iti.crypto.phishy.cli;

import edu.kit.iti.crypto.phishy.cli.addressbook.CLIAddressbook;
import edu.kit.iti.crypto.phishy.cli.campaign.CLICampaign;
import edu.kit.iti.crypto.phishy.cli.evaluate.CLIEvaluate;
import edu.kit.iti.crypto.phishy.cli.send.CLISend;
import edu.kit.iti.crypto.phishy.cli.statistics.CLIStatistics;
import edu.kit.iti.crypto.phishy.cli.template.CLITemplate;
import picocli.CommandLine;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.logging.Level;
import java.util.regex.Pattern;

@CommandLine.Command(
        name = "phishy",
        description = "The command-line interface for Phishy",
        subcommands = {
                CLIAddressbook.class,
                CLICampaign.class,
                CLIEvaluate.class,
                CLISend.class,
                CLIStatistics.class,
                CLITemplate.class
        }
)
public class CLIMain {
    public static final String NAME_PATTERN = "[a-zA-Z0-9_]+";
    public static final String TEMPLATE_PATTERN = "[a-zA-Z][a-zA-Z0-9_]*-[a-zA-Z][a-zA-Z0-9_]*";
    public static final String TEMPLATE_GROUP_NAME_PATTERN = "^[a-zA-Z][a-zA-Z0-9_]*";
    public static final String NO_ITEM = "-";
    private static final String SEPARATOR = ":";
    @CommandLine.Spec
    private CommandLine.Model.CommandSpec spec;

    public static void main(String[] args) {
        CLIMain main = new CLIMain();
        CommandLine commandLine = new CommandLine(main);
        main.validateSelf();
        ConfigHandler.createDefaultConfig();
        int exitCode = commandLine.execute(args);
        System.exit(exitCode);
    }

    /**
     * Displays a confirmation prompt
     *
     * @return boolean: whether the user accepted or not
     */
    public static boolean showConfirmationPrompt() {
        final String CONFIRMATION_PROMPT = "Do you really want to execute this command? (y/N): ";
        final String USER_CHOICE_FORMAT = "[yYnN]";
        final String USER_CHOICE_YES = "[yY]";
        final String USER_CHOICE_NO = "[nN]";
        final String ABORT = "Stopping, command will not be executed!";

        BufferedReader buffReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print(CONFIRMATION_PROMPT);

        String choice;
        try {
            choice = buffReader.readLine();
        } catch (IOException e) {
            return false;
        }
        choice = choice.toLowerCase();

        if (!choice.matches(USER_CHOICE_FORMAT) || choice.matches(USER_CHOICE_NO)) {
            System.out.println(ABORT);
            return false;
        }
        return true;
    }

    /**
     * validates parameters of CLI classes
     *
     * @param instance    the CLI class object (normally just call 'this')
     * @param commandLine the picocli commandline
     */
    public static void validate(Object instance, CommandLine commandLine) {
        //turn off logging for validation library to prevent spam
        java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.OFF);
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<Object>> violations = validator.validate(instance);

        if (!violations.isEmpty()) {
            StringBuilder errorMsg = new StringBuilder();
            for (ConstraintViolation<Object> violation : violations) {
                errorMsg.append("ERROR: ").append(violation.getMessage()).append("\n");
            }
            throw new CommandLine.ParameterException(commandLine, errorMsg.toString());
        }
    }

    public static void checkHost(String host) {
        if (host == null || host.equals("")) {
            throw new IllegalArgumentException("Host cannot be empty");
        }
        Pattern regex = Pattern.compile("^(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");

        if (!regex.matcher(host).matches()) {
            throw new IllegalArgumentException("Url format not accepted");
        }
    }

    private static String[] formatAuthentication(String usernameAndPassword) {
        String[] output = new String[2];
        if (!usernameAndPassword.contains(SEPARATOR)) {
            throw new IllegalArgumentException("Wrong username and password format");
        }
        int indexOfFirstColon = usernameAndPassword.indexOf(SEPARATOR);

        output[0] = usernameAndPassword.substring(0, indexOfFirstColon);
        output[1] = usernameAndPassword.substring(indexOfFirstColon + 1);
        return output;
    }

    public static String getValueOrDefault(String value) {
        return value.isEmpty() ? NO_ITEM : value;
    }

    public void validateSelf() {
        CLIMain.validate(this, spec.commandLine());
    }

    @CommandLine.Option(
            names = {"-u"},
            description = "Username and password separated by a single colon.",
            paramLabel = "USER:PASSWORD",
            scope = CommandLine.ScopeType.INHERIT
    )
    public void authenticate(String usernameAndPassword) {
        assert usernameAndPassword != null;
        String[] formattedAuth = null;
        try {
            formattedAuth = formatAuthentication(usernameAndPassword);
        } catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
            System.exit(-1);
        }
        HttpHandler.setAuthenticationMethod(formattedAuth[0], formattedAuth[1]);
    }

    @CommandLine.Option(
            names = {"-h"},
            description = "The url of the Phishy-Server",
            paramLabel = "HOST",
            scope = CommandLine.ScopeType.INHERIT
    )
    public void setHost(String host) {
        try {
            checkHost(host);
        } catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
            System.exit(-1);
        }
        HttpHandler.setHost(host);
    }

    @CommandLine.Option(
            names = {"-v", "--verbose"},
            description = "Displays additional information",
            scope = CommandLine.ScopeType.INHERIT
    )
    public void verbose(boolean verbose) {
        if (verbose) {
            HttpResponseErrorChecker.setVerbose();
        }
    }
}
