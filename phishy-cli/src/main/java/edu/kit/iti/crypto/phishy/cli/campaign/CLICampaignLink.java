package edu.kit.iti.crypto.phishy.cli.campaign;

import edu.kit.iti.crypto.phishy.cli.*;
import picocli.CommandLine;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.validation.constraints.Pattern;
import java.util.concurrent.Callable;

@CommandLine.Command(
        name = "link",
        description = "Links a template to a campaign"
)
public class CLICampaignLink implements Callable<Integer> {
    private final String templateTypePattern = "infoMail|infoWebsite|phishMail|phishWebsite";

    @Pattern(regexp = CLIMain.NAME_PATTERN, message = "Invalid string format for NAME!")
    @CommandLine.Parameters(
            paramLabel = "NAME",
            description = "The name of the Campaign"
    )
    private String name;

    @Pattern(regexp = templateTypePattern, message = "Invalid string format for TEMPLATE_TYPE (expected: phishMail, phishWebsite, infoMail, infoWebsite)!")
    @CommandLine.Parameters(
            paramLabel = "TEMPLATE_TYPE",
            description = "The type of template (phishMail, phishWebsite, infoMail, infoWebsite)"
    )
    private String templateType;

    @Pattern(regexp = CLIMain.TEMPLATE_PATTERN, message = "Invalid string format for TEMPLATE (expected: <group>-<template>)!")
    @CommandLine.Parameters(
            paramLabel = "TEMPLATE",
            description = "The name of the template (<group>-<template>)"
    )
    private String template;

    @CommandLine.Spec
    private CommandLine.Model.CommandSpec spec;

    @Override
    public Integer call() {
        CLIMain.validate(this, spec.commandLine());
        CLIRequestJsonObject request = new CLIRequestJsonObject(CLICampaign.REQUEST_ENDPOINT + this.name, HttpRequestType.PUT);
        JsonObjectBuilder requestJson = Json.createObjectBuilder();

        JsonObjectBuilder templates = Json.createObjectBuilder();
        templates.add(templateType, template);
        requestJson.add("templates", templates);
        request.addJsonObject(requestJson.build());

        CLIResponseJsonObject response = HttpHandler.sendRequest(request);
        if (!HttpResponseErrorChecker.checkForErrors(response, JsonValue.ValueType.OBJECT)) {
            return -1;
        }
        return 0;
    }
}
