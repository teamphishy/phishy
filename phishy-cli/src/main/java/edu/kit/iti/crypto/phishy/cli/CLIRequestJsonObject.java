package edu.kit.iti.crypto.phishy.cli;

import javax.json.JsonObject;

public class CLIRequestJsonObject {
    private String requestEndpoint;
    private HttpRequestType requestType;
    private JsonObject jsonObject;

    public CLIRequestJsonObject(String requestEndpoint, HttpRequestType requestType) {
        this.requestEndpoint = requestEndpoint;
        this.requestType = requestType;
        this.jsonObject = JsonObject.EMPTY_JSON_OBJECT;
    }

    public String getRequestEndpoint() {
        return requestEndpoint;
    }

    public void setRequestEndpoint(String requestEndpoint) {
        this.requestEndpoint = requestEndpoint;
    }

    public HttpRequestType getRequestType() {
        return requestType;
    }

    public JsonObject getJsonObject() {
        return jsonObject;
    }

    public void addJsonObject(JsonObject jsonObject) {
        this.jsonObject = jsonObject;
    }
}
