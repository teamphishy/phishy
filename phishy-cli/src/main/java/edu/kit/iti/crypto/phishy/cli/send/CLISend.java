package edu.kit.iti.crypto.phishy.cli.send;

import edu.kit.iti.crypto.phishy.cli.*;
import picocli.CommandLine;

import javax.json.JsonValue;
import javax.validation.constraints.Pattern;
import java.util.concurrent.Callable;

@CommandLine.Command(
        name = "send",
        description = "Sends phishing mails to a set of recipients"
)
public class CLISend implements Callable<Integer> {

    private static final String REQUEST_ENDPOINT = "/phishy/api/send/";

    @Pattern(regexp = CLIMain.NAME_PATTERN, message = "Invalid string format for CAMPAIGN!")
    @CommandLine.Option(
            names = "-c",
            paramLabel = "CAMPAIGN",
            description = "The name of the Campaign"
    )
    private String campaign;

    @CommandLine.Option(
            names = "--yes",
            description = "Disables confirmation prompt"
    )
    private boolean yes;

    @CommandLine.Spec
    private CommandLine.Model.CommandSpec spec;

    @Override
    public Integer call() {
        CLIMain.validate(this, spec.commandLine());
        CLIRequestJsonObject request = new CLIRequestJsonObject(REQUEST_ENDPOINT, HttpRequestType.POST);
        if (campaign != null) {
            request.setRequestEndpoint(REQUEST_ENDPOINT + campaign);
        }

        if (!yes) {
            boolean choice = CLIMain.showConfirmationPrompt();
            if (!choice) return 0;
        }

        CLIResponseJsonObject response = HttpHandler.sendRequest(request);
        if (!HttpResponseErrorChecker.checkForErrors(response, JsonValue.ValueType.OBJECT)) {
            return -1;
        }
        return 0;
    }
}
