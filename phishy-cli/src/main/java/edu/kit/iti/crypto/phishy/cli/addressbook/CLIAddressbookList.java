package edu.kit.iti.crypto.phishy.cli.addressbook;

import edu.kit.iti.crypto.phishy.cli.*;
import picocli.CommandLine;

import javax.json.JsonArray;
import javax.json.JsonException;
import javax.json.JsonValue;
import java.util.ArrayList;
import java.util.concurrent.Callable;

@CommandLine.Command(
        name = "list",
        description = "Lists all addressbooks."
)
public class CLIAddressbookList implements Callable<Integer> {
    @Override
    public Integer call() {
        CLIRequestJsonObject request = new CLIRequestJsonObject(CLIAddressbook.REQUEST_ENDPOINT, HttpRequestType.GET);
        CLIResponseJsonObject response = HttpHandler.sendRequest(request);
        if (!HttpResponseErrorChecker.checkForErrors(response, JsonValue.ValueType.ARRAY)) {
            return -1;
        }

        //print response
        ArrayList<String> addressbooks = new ArrayList<>();
        try {
            JsonArray jsonAddressbooks = response.getJsonStructure().asJsonArray();
            for (int i = 0; i < jsonAddressbooks.size(); i++) {
                addressbooks.add(jsonAddressbooks.getString(i));
            }
        } catch (NullPointerException | ClassCastException | JsonException e) {
            System.err.println("Error while parsing JSON");
            return -1;
        }
        addressbooks.forEach(System.out::println);
        return 0;
    }
}
