package edu.kit.iti.crypto.phishy.cli.campaign;

import edu.kit.iti.crypto.phishy.cli.*;
import picocli.CommandLine;

import javax.json.JsonArray;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonValue;
import java.util.concurrent.Callable;

@CommandLine.Command(
        name = "list",
        description = "Lists all campaigns"
)
public class CLICampaignList implements Callable<Integer> {
    @Override
    public Integer call() {
        CLIRequestJsonObject request = new CLIRequestJsonObject(CLICampaign.REQUEST_ENDPOINT, HttpRequestType.GET);
        CLIResponseJsonObject response = HttpHandler.sendRequest(request);
        if (!HttpResponseErrorChecker.checkForErrors(response, JsonValue.ValueType.ARRAY)) {
            return -1;
        }

        try {
            JsonArray responseJson = response.getJsonStructure().asJsonArray();
            for (int i = 0; i < responseJson.size(); i++) {
                JsonObject o = responseJson.getJsonObject(i);
                System.out.println(("active".equals(o.getString("status")) ? "+" : "-")
                        + o.getString("id") + " , status: " + o.getString("status"));
            }
        } catch (NullPointerException | ClassCastException | JsonException e) {
            System.err.println("Error while parsing JSON");
            return -1;
        }
        return 0;
    }
}
