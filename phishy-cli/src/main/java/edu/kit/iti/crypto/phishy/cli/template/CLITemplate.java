package edu.kit.iti.crypto.phishy.cli.template;

import picocli.CommandLine;

@CommandLine.Command(
        name = "template",
        description = "Template related requests",
        subcommands = {
                CLITemplateGroup.class,
                CLITemplateList.class,
                CLITemplateUpdate.class
        }
)
public class CLITemplate {
    public static final String REQUEST_ENDPOINT = "/phishy/api/template/";
    public static final String UPDATE_ENDPOINT = "/phishy/api/clearcache/";
}
