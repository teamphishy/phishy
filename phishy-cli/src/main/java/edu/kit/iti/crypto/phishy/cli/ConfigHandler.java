package edu.kit.iti.crypto.phishy.cli;

import javax.json.*;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ConfigHandler {
    private static final String CONFIG_NAME = ".phishy.cnf";
    private static final Path CONFIG_PATH = Paths.get(System.getProperty("user.home"));

    private static final String CONFIG_HOST_DEFAULT = "http://localhost:8888";
    private static final String CONFIG_USERNAME_DEFAULT = "root";
    private static final String CONFIG_PASSWORD_DEFAULT = "admin";
    private static boolean warningAlreadyShown = false;

    public static String[] loadConfigAuth() {
        String[] auth = new String[2];
        JsonObject json = loadConfig();
        auth[0] = json.getString("username");
        auth[1] = json.getString("password");
        return auth;
    }

    public static String loadConfigHost() {
        JsonObject json = loadConfig();
        String host = json.getString("host");
        CLIMain.checkHost(host);
        return host;
    }

    private static JsonObject loadConfig() {
        Path configFile = CONFIG_PATH.resolve(CONFIG_NAME);
        try (JsonReader reader = Json.createReader(new FileInputStream(configFile.toFile()))) {
            return reader.readObject();
        } catch (FileNotFoundException | JsonException e) {
            if (!warningAlreadyShown) {
                System.out.println("WARNING: Config invalid, falling back to default values");
                warningAlreadyShown = true;
            }
            return Json.createObjectBuilder()
                    .add("host", CONFIG_HOST_DEFAULT)
                    .add("username", CONFIG_USERNAME_DEFAULT)
                    .add("password", CONFIG_PASSWORD_DEFAULT)
                    .build();
        }
    }

    public static void createDefaultConfig() {
        File configFile = CONFIG_PATH.resolve(CONFIG_NAME).toFile();
        if (configFile.exists() && !configFile.isDirectory()) {
            return;
        }
        try (JsonWriter writer = Json.createWriter(new FileOutputStream(configFile))) {
            configFile.createNewFile();
            JsonObject defaultJson = Json.createObjectBuilder()
                    .add("host", CONFIG_HOST_DEFAULT)
                    .add("username", CONFIG_USERNAME_DEFAULT)
                    .add("password", CONFIG_PASSWORD_DEFAULT)
                    .build();
            writer.writeObject(defaultJson);
        } catch (IOException e) {
            System.err.println("Error while creating default config");
        }
    }
}
