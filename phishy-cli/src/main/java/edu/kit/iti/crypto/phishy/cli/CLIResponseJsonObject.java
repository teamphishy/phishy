package edu.kit.iti.crypto.phishy.cli;

import javax.json.JsonStructure;


public class CLIResponseJsonObject {

    private final HttpResponseType responseType;
    private final JsonStructure jsonStructure;

    public CLIResponseJsonObject(HttpResponseType responseType, JsonStructure jsonStructure) {
        this.responseType = responseType;
        this.jsonStructure = jsonStructure;
    }

    public HttpResponseType getResponseType() {
        return responseType;
    }

    public JsonStructure getJsonStructure() {
        return jsonStructure;
    }
}
