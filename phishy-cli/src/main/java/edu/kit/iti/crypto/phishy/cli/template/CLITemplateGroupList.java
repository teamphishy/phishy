package edu.kit.iti.crypto.phishy.cli.template;

import edu.kit.iti.crypto.phishy.cli.*;
import picocli.CommandLine;

import javax.json.JsonArray;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.validation.constraints.Pattern;
import java.util.concurrent.Callable;

@CommandLine.Command(
        name = "list",
        description = "Lists all templates of a given group"
)
public class CLITemplateGroupList implements Callable<Integer> {
    @Pattern(regexp = CLIMain.TEMPLATE_GROUP_NAME_PATTERN, message = "Invalid string format for NAME!")
    @CommandLine.Parameters(
            paramLabel = "NAME",
            description = "The name of the Template Group"
    )
    private String name;

    @CommandLine.Spec
    private CommandLine.Model.CommandSpec spec;

    @Override
    public Integer call() {
        CLIMain.validate(this, spec.commandLine());
        CLIRequestJsonObject request = new CLIRequestJsonObject(CLITemplate.REQUEST_ENDPOINT + this.name, HttpRequestType.GET);

        CLIResponseJsonObject response = HttpHandler.sendRequest(request);
        if (!HttpResponseErrorChecker.checkForErrors(response, JsonValue.ValueType.ARRAY)) {
            return -1;
        }

        try {
            JsonArray templates = response.getJsonStructure().asJsonArray();
            for (int i = 0; i < templates.size(); i++) {
                JsonObject o = templates.getJsonObject(i);
                System.out.println(CLIMain.getValueOrDefault(o.getString("id")));
            }
        } catch (NullPointerException | ClassCastException | JsonException e) {
            System.err.println("Error while parsing JSON");
            return -1;
        }
        return 0;
    }
}
