package edu.kit.iti.crypto.phishy.cli.campaign;

import edu.kit.iti.crypto.phishy.cli.*;
import picocli.CommandLine;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.validation.constraints.Pattern;
import java.util.concurrent.Callable;

@CommandLine.Command(
        name = "unlink",
        description = "Unlinks a template from a campaign"
)
public class CLICampaignUnlink implements Callable<Integer> {
    private final String templateTypePattern = "infoMail|infoWebsite|phishMail|phishWebsite";

    @Pattern(regexp = CLIMain.NAME_PATTERN, message = "Invalid string format for NAME!")
    @CommandLine.Parameters(
            paramLabel = "NAME",
            description = "The name of the Campaign"
    )
    private String name;

    @Pattern(regexp = templateTypePattern, message = "Invalid string format for TEMPLATE_TYPE!")
    @CommandLine.Parameters(
            paramLabel = "TEMPLATE_TYPE",
            description = "The type of template (phishMail, phishWebsite, infoMail, infoWebsite)"
    )
    private String templateType;

    @CommandLine.Spec
    private CommandLine.Model.CommandSpec spec;

    @Override
    public Integer call() {
        CLIMain.validate(this, spec.commandLine());
        CLIRequestJsonObject request = new CLIRequestJsonObject(CLICampaign.REQUEST_ENDPOINT + this.name, HttpRequestType.PUT);
        JsonObjectBuilder requestJson = Json.createObjectBuilder();

        JsonObjectBuilder templates = Json.createObjectBuilder();
        templates.add(templateType, "");
        requestJson.add("templates", templates);
        request.addJsonObject(requestJson.build());

        CLIResponseJsonObject response = HttpHandler.sendRequest(request);
        if (!HttpResponseErrorChecker.checkForErrors(response, JsonValue.ValueType.OBJECT)) {
            return -1;
        }
        return 0;
    }
}
