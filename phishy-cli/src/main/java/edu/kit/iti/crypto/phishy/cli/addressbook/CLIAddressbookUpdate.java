package edu.kit.iti.crypto.phishy.cli.addressbook;

import edu.kit.iti.crypto.phishy.cli.*;
import picocli.CommandLine;

import javax.json.JsonValue;
import java.util.concurrent.Callable;

@CommandLine.Command(
        name = "update",
        description = "Update all addressbook caches."
)
public class CLIAddressbookUpdate implements Callable<Integer> {
    @Override
    public Integer call() {
        CLIRequestJsonObject request = new CLIRequestJsonObject(CLIAddressbook.UPDATE_ENDPOINT, HttpRequestType.POST);
        CLIResponseJsonObject response = HttpHandler.sendRequest(request);
        if (!HttpResponseErrorChecker.checkForErrors(response, JsonValue.ValueType.OBJECT)) {
            return -1;
        }
        return 0;
    }
}
