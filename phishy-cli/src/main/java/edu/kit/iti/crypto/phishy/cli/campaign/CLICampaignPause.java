package edu.kit.iti.crypto.phishy.cli.campaign;

import edu.kit.iti.crypto.phishy.cli.*;
import picocli.CommandLine;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.validation.constraints.Pattern;
import java.util.concurrent.Callable;

@CommandLine.Command(
        name = "pause",
        description = "Pauses the specified campaign from automatically being sent"
)
public class CLICampaignPause implements Callable<Integer> {
    private static final String STATUS_INACTIVE = "inactive";

    @Pattern(regexp = CLIMain.NAME_PATTERN, message = "Invalid string format for NAME!")
    @CommandLine.Parameters(
            paramLabel = "NAME",
            description = "The name of the Campaign"
    )
    private String name;

    @CommandLine.Spec
    private CommandLine.Model.CommandSpec spec;

    @Override
    public Integer call() {
        CLIMain.validate(this, spec.commandLine());

        CLIRequestJsonObject request = new CLIRequestJsonObject(CLICampaign.REQUEST_ENDPOINT + this.name, HttpRequestType.PUT);

        JsonObjectBuilder requestJson = Json.createObjectBuilder();
        requestJson.add("status", STATUS_INACTIVE);
        request.addJsonObject(requestJson.build());

        CLIResponseJsonObject response = HttpHandler.sendRequest(request);
        if (!HttpResponseErrorChecker.checkForErrors(response, JsonValue.ValueType.OBJECT)) {
            return -1;
        }
        return 0;
    }
}
