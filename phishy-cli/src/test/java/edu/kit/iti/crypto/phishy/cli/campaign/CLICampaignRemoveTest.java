package edu.kit.iti.crypto.phishy.cli.campaign;

import edu.kit.iti.crypto.phishy.cli.CLIMain;
import edu.kit.iti.crypto.phishy.cli.ConfigHandler;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockserver.client.MockServerClient;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import picocli.CommandLine;

import java.io.*;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertTrue;

public class CLICampaignRemoveTest {
    private MockServerClient server;

    @Before
    public void setUp() throws Exception {
        server = ClientAndServer.startClientAndServer(8888);
        //turn off annoying logging
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver.mockserver"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver.proxy"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ConfigHandler.createDefaultConfig();
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
    }

    @Test
    public void call() {
        server.when(HttpRequest.request()
                .withMethod("DELETE")
                .withPath("/phishy/api/campaign/test123"))
                .respond(HttpResponse.response()
                        .withStatusCode(200));

        String[] args = {"campaign", "remove", "test123",
                "--yes",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        server.verify(HttpRequest.request()
                .withMethod("DELETE")
                .withPath("/phishy/api/campaign/test123"));
    }

    @Test
    public void callIllegalParameterFormats() {
        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"campaign", "remove", "--yes", "campai$)/$&gn",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        String replaced = cliOutput.toString().trim().replace("\r\n", "\n");
        assertTrue(replaced.contains("Invalid string format for NAME!"));
    }

    @Test
    public void callWithPrompt() {
        server.when(HttpRequest.request()
                .withMethod("DELETE")
                .withPath("/phishy/api/campaign/test123"))
                .respond(HttpResponse.response()
                        .withStatusCode(200));

        InputStream stream = new ByteArrayInputStream(("y\n").getBytes(StandardCharsets.UTF_8));
        InputStream stdin = System.in;
        System.setIn(stream);

        String[] args = {"campaign", "remove", "test123",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setIn(stdin);

        server.verify(HttpRequest.request()
                .withMethod("DELETE")
                .withPath("/phishy/api/campaign/test123"));
    }

}