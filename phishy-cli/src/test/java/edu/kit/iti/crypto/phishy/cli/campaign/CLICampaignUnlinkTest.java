package edu.kit.iti.crypto.phishy.cli.campaign;

import edu.kit.iti.crypto.phishy.cli.CLIMain;
import edu.kit.iti.crypto.phishy.cli.ConfigHandler;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockserver.client.MockServerClient;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import picocli.CommandLine;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertTrue;

public class CLICampaignUnlinkTest {
    private MockServerClient server;

    @Before
    public void setUp() throws Exception {
        server = ClientAndServer.startClientAndServer(8888);
        //turn off annoying logging
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver.mockserver"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver.proxy"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ConfigHandler.createDefaultConfig();
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
    }

    @Test
    public void call() {
        JsonObjectBuilder templates = Json.createObjectBuilder()
                .add("phishMail", "");

        JsonObject jsonObject = Json.createObjectBuilder()
                .add("templates", templates)
                .build();

        server.when(HttpRequest.request()
                .withMethod("PUT")
                .withPath("/phishy/api/campaign/test123"))
                .respond(HttpResponse.response()
                        .withStatusCode(200));

        String[] args = {"campaign", "unlink", "test123",
                "phishMail",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        server.verify(HttpRequest.request()
                .withMethod("PUT")
                .withPath("/phishy/api/campaign/test123")
                .withBody(jsonObject.toString()));
    }

    @Test
    public void callIllegalParameterFormats() {
        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"campaign", "unlink", "campai$)/$&gn",
                "phishMailu",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        String replaced = cliOutput.toString().trim().replace("\r\n", "\n");
        assertTrue(replaced.contains("Invalid string format for NAME!"));
        assertTrue(replaced.contains("Invalid string format for TEMPLATE_TYPE!"));
    }

}