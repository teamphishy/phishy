package edu.kit.iti.crypto.phishy.cli.campaign;

import edu.kit.iti.crypto.phishy.cli.CLIMain;
import edu.kit.iti.crypto.phishy.cli.ConfigHandler;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockserver.client.MockServerClient;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import picocli.CommandLine;

import javax.json.Json;
import javax.json.JsonArray;
import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;

public class CLICampaignListTest {

    private MockServerClient server;

    @Before
    public void setUp() throws Exception {
        server = ClientAndServer.startClientAndServer(8888);
        //turn off annoying logging
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver.mockserver"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver.proxy"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ConfigHandler.createDefaultConfig();
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
    }

    @Test
    public void call() {
        JsonArray jsonArray = Json.createArrayBuilder()
                .add(Json.createObjectBuilder()
                        .add("id", "test123")
                        .add("status", "active"))
                .build();

        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/campaign/"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withBody(jsonArray.toString()));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"campaign", "list",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals("+test123 , status: active", cliOutput.toString().trim().replace("\r\n", "\n"));

        server.verify(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/campaign/"));
    }

    @Test
    public void callNoCampaigns() {
        JsonArray jsonArray = JsonArray.EMPTY_JSON_ARRAY;

        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/campaign/"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withBody(jsonArray.toString()));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"campaign", "list",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals("", cliOutput.toString().trim().replace("\r\n", "\n"));

        server.verify(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/campaign/"));
    }

    @Test
    public void callNoJson() {
        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/campaign/"))
                .respond(HttpResponse.response()
                        .withStatusCode(200));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"campaign", "list",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals("Expected JSON Type ARRAY but got OBJECT", cliOutput.toString().trim().replace("\r\n", "\n"));
    }

    @Test
    public void callWrongJson() {
        JsonArray jsonArray = Json.createArrayBuilder().add(1337).add("failing").build();

        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/campaign/"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withBody(jsonArray.toString()));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"campaign", "list",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals("Error while parsing JSON", cliOutput.toString().trim().replace("\r\n", "\n"));
    }

}