package edu.kit.iti.crypto.phishy.cli;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.mockserver.client.MockServerClient;
import org.mockserver.configuration.ConfigurationProperties;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import picocli.CommandLine;

import java.io.*;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.*;
import static org.mockserver.model.HttpRequest.request;

public class CLIMainTest {

    private MockServerClient server;

    @Rule
    public ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Before
    public void setUp() throws Exception {
        server = ClientAndServer.startClientAndServer(8888);
        //turn off annoying logging
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver.mockserver"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver.proxy"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ConfigHandler.createDefaultConfig();
    }

    @Test
    public void main() throws Exception {
        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"campaign"};
        exit.expectSystemExitWithStatus(2);
        CLIMain.main(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.err)));
        String replaced = cliOutput.toString().trim().replace("\r\n", "\n");
        assertTrue(replaced.contains("Missing required subcommand"));
    }

    @Test
    public void showConfirmationPrompt() throws InterruptedException {
        InputStream stream = new ByteArrayInputStream(("y\n").getBytes(StandardCharsets.UTF_8));
        InputStream stdin = System.in;
        System.setIn(stream);
        assertTrue(CLIMain.showConfirmationPrompt());
        System.setIn(stdin);
    }

    @Test
    public void showConfirmationPromptDecline() throws InterruptedException {
        InputStream stream = new ByteArrayInputStream(("n\n").getBytes(StandardCharsets.UTF_8));
        InputStream stdin = System.in;
        System.setIn(stream);
        assertFalse(CLIMain.showConfirmationPrompt());
        System.setIn(stdin);
    }

    @Test
    public void checkHost() {
        String host = "http://example.com";
        CLIMain.checkHost(host);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkHostFail() {
        CLIMain.checkHost(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkHostWrong() {
        String host = "http:/example.com";
        CLIMain.checkHost(host);
    }

    @Test
    public void getValueOrDefault() {
        assertEquals("-", CLIMain.getValueOrDefault(""));
        assertEquals("asdasd", CLIMain.getValueOrDefault("asdasd"));
    }

    @Test
    public void authenticate() {
        ConfigurationProperties.proxyAuthenticationUsername("user");
        ConfigurationProperties.proxyAuthenticationPassword("Password");

        server.when(HttpRequest.request()
                .withMethod("POST")
                .withPath("/phishy/api/send/")
                .withHeader("Authorization", "Basic dXNlcjpQYXNzd29yZA=="))
                .respond(HttpResponse.response()
                        .withStatusCode(200));

        String[] args = {"send", "--yes", "-h", "http://localhost:8888", "-u", "user:Password"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        server.verify(request()
                .withMethod("POST")
                .withPath("/phishy/api/send/")
                .withHeader("Authorization", "Basic dXNlcjpQYXNzd29yZA=="));
    }

    @Test
    public void authenticateWrong() {
        ConfigurationProperties.proxyAuthenticationUsername("user");
        ConfigurationProperties.proxyAuthenticationPassword("Password");

        server.when(HttpRequest.request()
                .withMethod("POST")
                .withPath("/phishy/api/send/")
                .withHeader("Authorization", "Basic definitelyWrong"))
                .respond(HttpResponse.response()
                        .withStatusCode(200));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"send", "--yes", "-h", "http://localhost:8888", "-u", "user:Password"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        String replaced = cliOutput.toString().trim().replace("\r\n", "\n");
        assertTrue(replaced.contains("ERROR: 404 NOT_FOUND"));

        server.verify(request()
                .withMethod("POST")
                .withPath("/phishy/api/send/")
                .withHeader("Authorization", "Basic dXNlcjpQYXNzd29yZA=="));
    }

    @Test
    public void authenticateWrongFormat() {
        ConfigurationProperties.proxyAuthenticationUsername("user");
        ConfigurationProperties.proxyAuthenticationPassword("Password");

        server.when(HttpRequest.request()
                .withMethod("POST")
                .withPath("/phishy/api/send/")
                .withHeader("Authorization", "Basic dXNlcjpQYXNzd29yZA=="))
                .respond(HttpResponse.response()
                        .withStatusCode(200));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"send", "--yes", "-h", "http://localhost:8888", "-u", "userPassword"};
        exit.expectSystemExitWithStatus(-1);
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertTrue(cliOutput.toString().trim().replace("\r\n", "\n").contains("Wrong username and password format"));
    }

    @Test
    public void setHost() {
        ClientAndServer serverLocal = ClientAndServer.startClientAndServer(8889);

        serverLocal.when(HttpRequest.request()
                .withMethod("POST")
                .withPath("/phishy/api/send/"))
                .respond(HttpResponse.response()
                        .withStatusCode(200));

        String[] args = {"send", "--yes", "-h", "http://localhost:8889"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        serverLocal.verify(request()
                .withMethod("POST")
                .withPath("/phishy/api/send/"));
        serverLocal.stop();
    }

    @Test
    public void setHostWrong2() {
        ClientAndServer serverLocal = ClientAndServer.startClientAndServer(8889);

        serverLocal.when(HttpRequest.request()
                .withMethod("POST")
                .withPath("/phishy/api/send/"))
                .respond(HttpResponse.response()
                        .withStatusCode(200));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"send", "--yes", "-h", ""};
        CLIMain cliMain = new CLIMain();
        exit.expectSystemExitWithStatus(-1);
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertTrue(cliOutput.toString().trim().replace("\r\n", "\n").contains("Host cannot be empty"));

        serverLocal.stop();
    }

    //@Ignore("CLI does not like this!")
    @Test
    public void setHostWrong() {
        ClientAndServer serverLocal = ClientAndServer.startClientAndServer(8889);
        serverLocal.when(HttpRequest.request()
                .withMethod("POST")
                .withPath("/phishy/api/send/"))
                .respond(HttpResponse.response()
                        .withStatusCode(200));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));


        String[] args = {"send", "--yes", "-h", "http:/localhost:8889"};
        CLIMain cliMain = new CLIMain();
        exit.expectSystemExitWithStatus(-1);
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertTrue(cliOutput.toString().trim().replace("\r\n", "\n").contains("Url format not accepted"));

        serverLocal.stop();
    }

    @Test
    public void verbose() {
        server.when(HttpRequest.request()
                .withMethod("POST")
                .withPath("/phishy/api/send/"))
                .respond(HttpResponse.response()
                        .withStatusCode(200));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"send", "--yes", "-h", "http://localhost:8888", "-v"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals("The server responded: 200 OK", cliOutput.toString().trim());

        server.verify(request()
                .withMethod("POST")
                .withPath("/phishy/api/send/"));
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
    }
}