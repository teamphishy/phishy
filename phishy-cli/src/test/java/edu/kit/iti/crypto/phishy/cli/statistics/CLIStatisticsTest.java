package edu.kit.iti.crypto.phishy.cli.statistics;

import edu.kit.iti.crypto.phishy.cli.CLIMain;
import edu.kit.iti.crypto.phishy.cli.ConfigHandler;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockserver.client.MockServerClient;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import picocli.CommandLine;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CLIStatisticsTest {

    private MockServerClient server;

    @Before
    public void setUp() throws Exception {
        server = ClientAndServer.startClientAndServer(8888);
        //turn off annoying logging
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver.mockserver"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver.proxy"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ConfigHandler.createDefaultConfig();
    }

    @Test
    public void callStatAll() {
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("numSentMails", 13)
                .add("numReportedMails", 10)
                .add("numPhishedMails", 3)
                .build();

        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/stat/"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withBody(jsonObject.toString()));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"statistics", "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals(("There were 13 mails sent, 3 of them successfully phished their recipients and 10 mails were reported."), cliOutput.toString().trim().replace("\r\n", "\n"));

        server.verify(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/stat/"));
    }

    @Test
    public void callStatOne() {
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("numSentMails", 3)
                .add("numReportedMails", 1)
                .add("numPhishedMails", 2)
                .build();

        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/stat/test123"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withBody(jsonObject.toString()));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"statistics", "-c", "test123", "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals(("Statistics for campaign: test123\n" +
                "Number of sent mails: 3\n" +
                "Number of reported mails: 1\n" +
                "Number of phished mails: 2"), cliOutput.toString().trim().replace("\r\n", "\n"));

        server.verify(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/stat/test123"));
    }

    @Test
    public void callStatSingleScriptFriendlyReported() {
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("numSentMails", 3)
                .add("numReportedMails", 1)
                .add("numPhishedMails", 2)
                .build();

        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/stat/test123"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withBody(jsonObject.toString()));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"statistics", "-c", "test123", "--reportedMail", "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals("1", cliOutput.toString().trim());

        server.verify(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/stat/test123"));
    }

    @Test
    public void callStatSingleScriptFriendlyPhished() {
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("numSentMails", 3)
                .add("numReportedMails", 1)
                .add("numPhishedMails", 2)
                .build();

        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/stat/test123"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withBody(jsonObject.toString()));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"statistics", "-c", "test123", "--phishedMail", "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals("2", cliOutput.toString().trim());

        server.verify(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/stat/test123"));
    }

    @Test
    public void callStatSingleScriptFriendlySent() {
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("numSentMails", 3)
                .add("numReportedMails", 1)
                .add("numPhishedMails", 2)
                .build();

        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/stat/test123"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withBody(jsonObject.toString()));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"statistics", "-c", "test123", "--sentMail", "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals("3", cliOutput.toString().trim());

        server.verify(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/stat/test123"));
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
    }

    @Test
    public void callIllegalParameterFormats() {
        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"statistics", "-c", "campai$)/$&gn",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        String replaced = cliOutput.toString().trim().replace("\r\n", "\n");
        assertTrue(replaced.contains("Invalid string format for CAMPAIGN!"));
    }

    @Test
    public void callWrongJson() {
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("wrong", 13)
                .add("format", 10)
                .add("here", 3)
                .build();

        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/stat/"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withBody(jsonObject.toString()));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"statistics", "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals(("Error while parsing JSON"), cliOutput.toString().trim().replace("\r\n", "\n"));
    }

    @Test
    public void callWrongJson2() {
        JsonArray jsonObject = Json.createArrayBuilder()
                .add("wrong")
                .add("format")
                .add("here")
                .build();

        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/stat/"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withBody(jsonObject.toString()));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"statistics", "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals(("Expected JSON Type OBJECT but got ARRAY"), cliOutput.toString().trim().replace("\r\n", "\n"));
    }
}