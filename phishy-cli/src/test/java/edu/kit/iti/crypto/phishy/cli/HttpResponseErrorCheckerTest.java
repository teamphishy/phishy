package edu.kit.iti.crypto.phishy.cli;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockserver.client.MockServerClient;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import picocli.CommandLine;

import javax.json.JsonObject;
import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;

public class HttpResponseErrorCheckerTest {

    private MockServerClient server;

    @Before
    public void setUp() throws Exception {
        server = ClientAndServer.startClientAndServer(8888);

        //turn off annoying logging
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver.mockserver"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver.proxy"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ConfigHandler.createDefaultConfig();
    }

    @After
    public void tearDown() {
        server.stop();
    }

    @Test
    public void call400() {
        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/addressbook/"))
                .respond(HttpResponse.response().withStatusCode(400));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"addressbook", "list",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.err)));
        assertEquals(("ERROR: 400 BAD_REQUEST"), cliOutput.toString().trim().replace("\r\n", "\n"));
    }

    @Test
    public void call404() {
        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/addressbook/"))
                .respond(HttpResponse.response().withStatusCode(404));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"addressbook", "list",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.err)));
        assertEquals(("ERROR: 404 NOT_FOUND"), cliOutput.toString().trim().replace("\r\n", "\n"));
    }

    @Test
    public void call405() {
        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/addressbook/"))
                .respond(HttpResponse.response().withStatusCode(405));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"addressbook", "list",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.err)));
        assertEquals(("ERROR: 405 METHOD_NOT_ALLOWED"), cliOutput.toString().trim().replace("\r\n", "\n"));
    }

    @Test
    public void call409() {
        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/addressbook/"))
                .respond(HttpResponse.response().withStatusCode(409));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"addressbook", "list",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.err)));
        assertEquals(("ERROR: 409 CONFLICT"), cliOutput.toString().trim().replace("\r\n", "\n"));
    }

    @Test
    public void callExpectedWrongJsonType() {
        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/campaign/test123"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withBody(JsonObject.EMPTY_JSON_ARRAY.toString()));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"campaign", "print", "test123",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.err)));
        assertEquals("Expected JSON Type OBJECT but got ARRAY", cliOutput.toString().trim().replace("\r\n", "\n"));
    }

}