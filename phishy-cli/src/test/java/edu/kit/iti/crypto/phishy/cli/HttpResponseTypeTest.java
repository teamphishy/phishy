package edu.kit.iti.crypto.phishy.cli;

import org.junit.Test;

public class HttpResponseTypeTest {

    @Test(expected = IllegalArgumentException.class)
    public void fromInt() {
        HttpResponseType.fromInt(-500);
    }
}