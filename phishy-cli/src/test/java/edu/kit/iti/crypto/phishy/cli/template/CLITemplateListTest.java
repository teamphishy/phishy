package edu.kit.iti.crypto.phishy.cli.template;

import edu.kit.iti.crypto.phishy.cli.CLIMain;
import edu.kit.iti.crypto.phishy.cli.ConfigHandler;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockserver.client.MockServerClient;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import picocli.CommandLine;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;

public class CLITemplateListTest {

    private MockServerClient server;

    @Before
    public void setUp() throws Exception {
        server = ClientAndServer.startClientAndServer(8888);

        //turn off annoying logging
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver.mockserver"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver.proxy"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ConfigHandler.createDefaultConfig();
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
    }

    @Test
    public void call() {
        JsonArray groups = Json.createArrayBuilder()
                .add(Json.createObjectBuilder()
                        .add("id", "testGroup")
                        .add("git", "/git/testGroup")
                        .build())
                .build();

        JsonArray templates = Json.createArrayBuilder()
                .add(Json.createObjectBuilder()
                        .add("id", "testTemplate")
                        .add("git", "/git/testGroup")
                        .build())
                .add(Json.createObjectBuilder()
                        .add("id", "testTemplate2")
                        .add("git", "/git/testGroup")
                        .build())
                .build();
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("groups", groups)
                .add("templates", templates)
                .build();

        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/template/"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withBody(jsonObject.toString()));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"template", "list",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals(("testGroup"), cliOutput.toString().trim().replace("\r\n", "\n"));

        server.verify(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/template/"));
    }

    @Test
    public void callNoResult() {
        JsonArray groups = Json.createArrayBuilder()
                .build();

        JsonArray templates = Json.createArrayBuilder()
                .build();
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("groups", groups)
                .add("templates", templates)
                .build();

        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/template/"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withBody(jsonObject.toString()));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"template", "list",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals((""), cliOutput.toString().trim().replace("\r\n", "\n"));

        server.verify(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/template/"));
    }

    @Test
    public void callWrongJson() {
        JsonArray groups = Json.createArrayBuilder()
                .add(Json.createObjectBuilder()
                        .add("fail", "testGroup")
                        .add("format", "/git/testGroup")
                        .build())
                .build();

        JsonArray templates = Json.createArrayBuilder()
                .add(Json.createObjectBuilder()
                        .add("not", "testTemplate")
                        .add("working", "/git/testGroup")
                        .build())
                .add(Json.createObjectBuilder()
                        .add("never", "testTemplate2")
                        .add("ever", "/git/testGroup")
                        .build())
                .build();
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("groups", groups)
                .add("templates", templates)
                .build();

        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/template/"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withBody(jsonObject.toString()));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"template", "list",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals(("Error while parsing JSON"), cliOutput.toString().trim().replace("\r\n", "\n"));
    }

    @Test
    public void callWrongJson2() {
        JsonArray templates = Json.createArrayBuilder()
                .add(Json.createObjectBuilder()
                        .add("not", "testTemplate")
                        .add("working", "/git/testGroup")
                        .build())
                .add(Json.createObjectBuilder()
                        .add("never", "testTemplate2")
                        .add("ever", "/git/testGroup")
                        .build())
                .build();

        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/template/"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withBody(templates.toString()));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"template", "list",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals(("Expected JSON Type OBJECT but got ARRAY"), cliOutput.toString().trim().replace("\r\n", "\n"));
    }
}