package edu.kit.iti.crypto.phishy.cli.template;

import edu.kit.iti.crypto.phishy.cli.CLIMain;
import edu.kit.iti.crypto.phishy.cli.ConfigHandler;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockserver.client.MockServerClient;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import picocli.CommandLine;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CLITemplateGroupListTest {

    private MockServerClient server;

    @Before
    public void setUp() throws Exception {
        server = ClientAndServer.startClientAndServer(8888);

        //turn off annoying logging
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver.mockserver"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver.proxy"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ConfigHandler.createDefaultConfig();
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
    }

    @Test
    public void call() {
        JsonArray array = Json.createArrayBuilder()
                .add(Json.createObjectBuilder()
                        .add("id", "testTemplate")
                        .add("git", "/git/testGroup")
                        .build())
                .add(Json.createObjectBuilder()
                        .add("id", "testTemplate2")
                        .add("git", "/git/testGroup")
                        .build())
                .build();

        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/template/testGroup"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withBody(array.toString()));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"template", "group", "list", "testGroup",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals(("testTemplate\n" +
                "testTemplate2"), cliOutput.toString().trim().replace("\r\n", "\n"));

        server.verify(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/template/testGroup"));
    }

    @Test
    public void callNoResult() {
        JsonArray array = Json.createArrayBuilder()
                .build();

        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/template/testGroup"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withBody(array.toString()));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"template", "group", "list", "testGroup",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals((""), cliOutput.toString().trim().replace("\r\n", "\n"));

        server.verify(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/template/testGroup"));
    }

    @Test
    public void callIllegalName() {
        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"template", "group", "list", "testGrou+ä#,.123p",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertTrue(cliOutput.toString().trim().replace("\r\n", "\n").contains("ERROR: Invalid string format for NAME!"));
    }

    @Test
    public void callWrongJson() {
        JsonObject object = Json.createObjectBuilder()
                .build();

        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/template/testGroup"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withBody(object.toString()));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"template", "group", "list", "testGroup",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals(("Expected JSON Type ARRAY but got OBJECT"), cliOutput.toString().trim().replace("\r\n", "\n"));
    }

    @Test
    public void callWrongJson2() {
        JsonArray array = Json.createArrayBuilder()
                .add(Json.createObjectBuilder()
                        .add("fail", "testTemplate")
                        .add("now", "/git/testGroup")
                        .build())
                .add(Json.createObjectBuilder()
                        .add("fail", "testTemplate2")
                        .add("now", "/git/testGroup")
                        .build())
                .build();

        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/template/testGroup"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withBody(array.toString()));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"template", "group", "list", "testGroup",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals(("Error while parsing JSON"), cliOutput.toString().trim().replace("\r\n", "\n"));
    }
}