package edu.kit.iti.crypto.phishy.cli;

import org.junit.Before;
import org.junit.Test;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ConfigHandlerTest {

    private final String CONFIG_NAME = ".phishy.cnf";
    private final Path CONFIG_PATH = Paths.get(System.getProperty("user.home"));

    @Before
    public void setUp() throws Exception {
        ConfigHandler.createDefaultConfig();
    }

    @Test
    public void loadConfigHost() throws IOException {
        JsonObject backup = createBackup();
        loadBackup(Json.createObjectBuilder()
                .add("host", "http://host123.de")
                .add("username", "peter")
                .add("password", "Password")
                .build(), false);
        String output = ConfigHandler.loadConfigHost();
        assertEquals("http://host123.de", output);
        loadBackup(backup, backup == null);
    }

    @Test
    public void loadConfigHostFaulty() throws IOException {
        JsonObject backup = createBackup();
        FileWriter fileWriter = new FileWriter(CONFIG_PATH.resolve(CONFIG_NAME).toFile());
        fileWriter.write("big bullshit)(/%/()%!/");
        fileWriter.close();

        String output = ConfigHandler.loadConfigHost();

        assertEquals("http://localhost:8888", output);
        loadBackup(backup, backup == null);
    }

    @Test
    public void loadConfigAuth() throws IOException {
        JsonObject backup = createBackup();
        loadBackup(Json.createObjectBuilder()
                .add("host", "host123")
                .add("username", "peter")
                .add("password", "Password")
                .build(), false);
        String[] output = ConfigHandler.loadConfigAuth();
        assertEquals("peter", output[0]);
        assertEquals("Password", output[1]);
        loadBackup(backup, backup == null);
    }

    @Test
    public void createDefaultConfig() throws IOException {
        File configFile = CONFIG_PATH.resolve(CONFIG_NAME).toFile();
        JsonObject backup = createBackup();

        ConfigHandler.createDefaultConfig();
        assertTrue(configFile.exists() && !configFile.isDirectory());

        loadBackup(backup, backup == null);
    }

    @Test
    public void createDefaultConfigWithNoPreviousOne() throws IOException {
        File configFile = CONFIG_PATH.resolve(CONFIG_NAME).toFile();
        JsonObject backup = createBackup();
        Files.deleteIfExists(configFile.toPath());


        ConfigHandler.createDefaultConfig();
        assertTrue(configFile.exists() && !configFile.isDirectory());

        loadBackup(backup, backup == null);
    }

    private JsonObject createBackup() throws IOException {
        File configFile = CONFIG_PATH.resolve(CONFIG_NAME).toFile();
        boolean isNewFile = configFile.createNewFile();
        JsonObject backup = null;
        if (!isNewFile) {
            JsonReader reader = Json.createReader(new FileInputStream(configFile));
            backup = reader.readObject();
            reader.close();
        }
        Files.deleteIfExists(configFile.toPath());
        return backup;
    }

    private void loadBackup(JsonObject backup, boolean isNewFile) throws FileNotFoundException {
        final String CONFIG_NAME = ".phishy.cnf";
        final Path CONFIG_PATH = Paths.get(System.getProperty("user.home"));
        File configFile = CONFIG_PATH.resolve(CONFIG_NAME).toFile();
        if (!isNewFile) {
            JsonWriter writer = Json.createWriter(new FileOutputStream(configFile));
            writer.writeObject(backup);
            writer.close();
        }
    }
}