package edu.kit.iti.crypto.phishy.cli;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

import java.nio.file.Path;
import java.nio.file.Paths;

public class HttpHandlerTest {

    private final String CONFIG_NAME = ".phishy.cnf";
    private final Path CONFIG_PATH = Paths.get(System.getProperty("user.home"));

    @Rule
    public ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Test
    public void sendRequestHostNull() {
        exit.expectSystemExitWithStatus(-1);
        HttpHandler.setHost(null);
        HttpHandler.sendRequest(new CLIRequestJsonObject(null, HttpRequestType.GET));
    }

    @Test
    public void sendRequestUserPassNull() {
        exit.expectSystemExitWithStatus(-1);
        HttpHandler.setAuthenticationMethod(null, null);
        HttpHandler.sendRequest(new CLIRequestJsonObject("null", HttpRequestType.GET));
    }
}