package edu.kit.iti.crypto.phishy.cli.addressbook;

import edu.kit.iti.crypto.phishy.cli.CLIMain;
import edu.kit.iti.crypto.phishy.cli.ConfigHandler;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockserver.client.MockServerClient;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import picocli.CommandLine;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;

public class CLIAddressbookListTest {

    private MockServerClient server;

    @Before
    public void setUp() throws Exception {
        server = ClientAndServer.startClientAndServer(8888);

        //turn off annoying logging
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver.mockserver"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver.proxy"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ConfigHandler.createDefaultConfig();
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
    }

    @Test
    public void call() {
        JsonArray addressbooks = Json.createArrayBuilder()
                .add("KIT-LDAP")
                .add("Test-CSV")
                .build();

        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/addressbook/"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withBody(addressbooks.toString()));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"addressbook", "list",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals(("KIT-LDAP\n" +
                "Test-CSV"), cliOutput.toString().trim().replace("\r\n", "\n"));

        server.verify(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/addressbook/"));
    }

    @Test
    public void callNoResults() {
        JsonArray addressbooks = Json.createArrayBuilder()
                .build();

        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/addressbook/"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withBody(addressbooks.toString()));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"addressbook", "list",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals((""), cliOutput.toString().trim().replace("\r\n", "\n"));

        server.verify(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/addressbook/"));
    }

    @Test
    public void callWrongJson() {
        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/addressbook/"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withBody(JsonObject.EMPTY_JSON_OBJECT.toString()));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"addressbook", "list",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals(("Expected JSON Type ARRAY but got OBJECT"), cliOutput.toString().trim().replace("\r\n", "\n"));
    }

    @Test
    public void callWrongJson2() {
        JsonArray addressbooks = Json.createArrayBuilder()
                .add(1)
                .add(3)
                .build();

        server.when(HttpRequest.request()
                .withMethod("GET")
                .withPath("/phishy/api/addressbook/"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withBody(addressbooks.toString()));

        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"addressbook", "list",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        assertEquals(("Error while parsing JSON"), cliOutput.toString().trim().replace("\r\n", "\n"));
    }
}