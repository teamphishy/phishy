package edu.kit.iti.crypto.phishy.cli;

import java.security.Permission;

public class SystemExitCatchSecurityManager extends SecurityManager {
    @Override
    public void checkExit(int status) {
        throw new SecurityException();
    }

    @Override
    public void checkPermission(Permission perm) {
        // Allow other activities by default
    }
}
