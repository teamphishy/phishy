package edu.kit.iti.crypto.phishy.cli.evaluate;

import edu.kit.iti.crypto.phishy.cli.CLIMain;
import edu.kit.iti.crypto.phishy.cli.ConfigHandler;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockserver.client.MockServerClient;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import picocli.CommandLine;

import java.io.*;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertTrue;
import static org.mockserver.model.HttpRequest.request;

public class CLIEvaluateTest {

    private MockServerClient server;

    @Before
    public void setUp() throws Exception {
        server = ClientAndServer.startClientAndServer(8888);
        //turn off annoying logging
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver.mockserver"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger("org.mockserver.proxy"))
                .setLevel(ch.qos.logback.classic.Level.OFF);
        ConfigHandler.createDefaultConfig();
    }

    @Test
    public void callEvalAll() {
        server.when(HttpRequest.request()
                .withMethod("POST")
                .withPath("/phishy/api/evaluate/"))
                .respond(HttpResponse.response()
                        .withStatusCode(200));

        String[] args = {"evaluate", "--yes", "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        server.verify(request()
                .withMethod("POST")
                .withPath("/phishy/api/evaluate/"));
    }

    @Test
    public void callEvalOne() {
        server.when(HttpRequest.request()
                .withMethod("POST")
                .withPath("/phishy/api/evaluate/test123"))
                .respond(HttpResponse.response()
                        .withStatusCode(200));

        String[] args = {"evaluate", "--yes", "-c", "test123", "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        server.verify(request()
                .withMethod("POST")
                .withPath("/phishy/api/evaluate/test123"));
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
    }

    @Test
    public void callIllegalParameterFormats() {
        ByteArrayOutputStream cliOutput = new ByteArrayOutputStream();
        System.setErr(new PrintStream(cliOutput, true, StandardCharsets.UTF_8));

        String[] args = {"evaluate", "-c", "campai$)/$&gn",
                "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setErr(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        String replaced = cliOutput.toString().trim().replace("\r\n", "\n");
        assertTrue(replaced.contains("Invalid string format for CAMPAIGN!"));
    }

    @Test
    public void callWithPrompt() {
        server.when(HttpRequest.request()
                .withMethod("POST")
                .withPath("/phishy/api/evaluate/test123"))
                .respond(HttpResponse.response()
                        .withStatusCode(200));

        InputStream stream = new ByteArrayInputStream(("y\n").getBytes(StandardCharsets.UTF_8));
        InputStream stdin = System.in;
        System.setIn(stream);

        String[] args = {"evaluate", "-c", "test123", "-h", "http://localhost:8888"};
        CLIMain cliMain = new CLIMain();
        new CommandLine(cliMain).execute(args);

        System.setIn(stdin);

        server.verify(request()
                .withMethod("POST")
                .withPath("/phishy/api/evaluate/test123"));
    }
}