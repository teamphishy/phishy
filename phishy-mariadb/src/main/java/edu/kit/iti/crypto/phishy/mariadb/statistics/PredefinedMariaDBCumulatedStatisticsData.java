package edu.kit.iti.crypto.phishy.mariadb.statistics;

import edu.kit.iti.crypto.phishy.data.statistics.CumulatedStatisticsData;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PredefinedMariaDBCumulatedStatisticsData implements CumulatedStatisticsData {

    private static final Logger logger = Logger.getLogger(PredefinedMariaDBCumulatedStatisticsData.class.getName());

    private final int sentMails;
    private final int reportedMails;
    private final int phishedMails;

    public PredefinedMariaDBCumulatedStatisticsData(int sentMails, int reportedMails, int phishedMails) {
        this.sentMails = sentMails;
        this.reportedMails = reportedMails;
        this.phishedMails = phishedMails;
    }

    @Override
    public int getSentMails() {
        return this.sentMails;
    }

    @Override
    public int getReportedMails() {
        return this.reportedMails;
    }

    @Override
    public int getPhishedMails() {
        return this.phishedMails;
    }

    public static CumulatedStatisticsData get(DataSource dataSource) {
        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt
                    = conn.prepareStatement(
                    "select sum(sentMails) as cumulatedSentMails, sum(reportedMails) as cumulatedReportedMails,"
                            + " sum(phishedMails) as cumulatedPhishedMails from Statistics;");

            ResultSet resultSet = stmt.executeQuery();
            if (!resultSet.next()) {
                throw new IllegalStateException("Database has no statistics items");
            }

            return new PredefinedMariaDBCumulatedStatisticsData(resultSet.getInt("cumulatedSentMails"),
                    resultSet.getInt("cumulatedReportedMails"),
                    resultSet.getInt("cumulatedPhishedMails"));

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error reading from MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }
    }
}
