package edu.kit.iti.crypto.phishy.mariadb.statistics;

import edu.kit.iti.crypto.phishy.data.campaign.CampaignDescription;
import edu.kit.iti.crypto.phishy.data.statistics.CampaignStatisticsData;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PredefinedMariaDBCampaignStatisticsData implements CampaignStatisticsData {

    private static final String SQL_NAME = "[a-zA-Z][a-zA-Z_0-9]*";

    private static final Logger logger = Logger.getLogger(PredefinedMariaDBCampaignStatisticsData.class.getName());

    private final CampaignDescription campaign;
    private final int sentMails;
    private final int reportedMails;
    private final int phishedMails;

    public PredefinedMariaDBCampaignStatisticsData(CampaignDescription campaign, int sentMails, int reportedMails,
                                                   int phishedMails) {
        this.campaign = campaign;
        this.sentMails = sentMails;
        this.reportedMails = reportedMails;
        this.phishedMails = phishedMails;
    }

    @Override
    public String getCampaignName() {
        return campaign.getName();
    }

    @Override
    public void increaseSentMails(int amount) {
        throw new UnsupportedOperationException("Predefined Statistics Data can't be increased");
    }

    @Override
    public void increaseReportedMail(int amount) {
        throw new UnsupportedOperationException("Predefined Statistics Data can't be increased");
    }

    @Override
    public void increasePhishedMail(int amount) {
        throw new UnsupportedOperationException("Predefined Statistics Data can't be increased");
    }

    @Override
    public int getSentMails() {
        return sentMails;
    }

    @Override
    public int getReportedMails() {
        return reportedMails;
    }

    @Override
    public int getPhishedMails() {
        return phishedMails;
    }

    public static List<CampaignStatisticsData> getAll(DataSource dataSource) {

        List<CampaignStatisticsData> statistics = new ArrayList<>();

        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt = conn.prepareStatement("select campaign, sentMails, reportedMails, phishedMails from Statistics");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                statistics.add(new PredefinedMariaDBCampaignStatisticsData(
                        new CampaignDescription(result.getString("campaign")),
                        result.getInt("sentMails"),
                        result.getInt("reportedMails"),
                        result.getInt("phishedMails"))
                );
            }

            return statistics;

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }
    }

}
