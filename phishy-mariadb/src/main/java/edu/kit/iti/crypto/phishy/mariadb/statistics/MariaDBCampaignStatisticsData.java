package edu.kit.iti.crypto.phishy.mariadb.statistics;

import edu.kit.iti.crypto.phishy.data.campaign.CampaignDescription;
import edu.kit.iti.crypto.phishy.data.statistics.CampaignStatisticsData;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class MariaDBCampaignStatisticsData implements CampaignStatisticsData {

    private static final String SQL_NAME = "[a-zA-Z][a-zA-Z_0-9]*";
    private static final Pattern SQL_NAME_PATTERN = Pattern.compile(SQL_NAME);

    private final DataSource dataSource;
    private final CampaignDescription campaign;
    private final Logger logger;

    public MariaDBCampaignStatisticsData(String campaign, DataSource dataSource) {
        this(new CampaignDescription(campaign), dataSource);
    }

    public MariaDBCampaignStatisticsData(CampaignDescription campaign, DataSource dataSource) {
        this.dataSource = dataSource;
        this.campaign = campaign;
        this.logger = Logger.getLogger(this.getClass().getName());

        if (!this.checkExistence()) {
            logger.log(Level.SEVERE, "Statistics for Campaign " + campaign + " do not exist");
            throw new IllegalArgumentException("campaign does not exist");
        }
    }

    @Override
    public String getCampaignName() {
        return campaign.getName();
    }

    @Override
    public void increaseSentMails(int amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("amount must be positive");
        }
        updateStatistics("sentMails", amount);
    }

    @Override
    public void increaseReportedMail(int amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("amount must be positive");
        }
        updateStatistics("reportedMails", amount);
    }

    @Override
    public void increasePhishedMail(int amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("amount must be positive");
        }
        updateStatistics("phishedMails", amount);
    }

    @Override
    public int getSentMails() {
        return getStatistics("sentMails");
    }

    @Override
    public int getReportedMails() {
        return getStatistics("reportedMails");
    }

    @Override
    public int getPhishedMails() {
        return getStatistics("phishedMails");
    }

    private boolean checkExistence() {
        // since MariaDB triggers create statistics tuple upon Campaign creation, we can simply check for statistics existence

        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt = conn.prepareStatement("select campaign from Statistics where campaign = ?;");
            stmt.setString(1, campaign.getName());
            ResultSet resultSet = stmt.executeQuery();

            return resultSet.next();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error reading from MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }

    }

    private void updateStatistics(String field, int amount) {

        if (!SQL_NAME_PATTERN.matcher(field).matches()) {
            throw new IllegalArgumentException("field is not a valid SQL filed name");
        }

        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt = conn.prepareStatement("update Statistics set " + field + " = " + field + " + ? where campaign = ?;");
            stmt.setInt(1, amount);
            stmt.setString(2, campaign.getName());
            stmt.executeUpdate();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }

    }

    private int getStatistics(String field) {

        if (!SQL_NAME_PATTERN.matcher(field).matches()) {
            throw new IllegalArgumentException("field is not a valid SQL filed name");
        }

        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt = conn.prepareStatement("select " + field + " from Statistics where campaign = ?");
            stmt.setString(1, campaign.getName());
            ResultSet resultSet = stmt.executeQuery();

            if (!resultSet.next()) {
                throw new IllegalStateException("Statistics item in database was deleted");
            }

            return resultSet.getInt(field);

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }
    }


}
