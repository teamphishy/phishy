package edu.kit.iti.crypto.phishy.mariadb.campaign;

import edu.kit.iti.crypto.phishy.data.campaign.CampaignDescription;
import edu.kit.iti.crypto.phishy.data.campaign.OutstandingTest;
import edu.kit.iti.crypto.phishy.data.statistics.CampaignStatisticsData;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MariaDBOutstandingTest implements OutstandingTest {

    private final DataSource dataSource;
    private final CampaignDescription description;
    private final CampaignStatisticsData statistics;
    private final String testId;
    private final Logger logger;

    public MariaDBOutstandingTest(String testId, CampaignDescription description, CampaignStatisticsData statistics, DataSource dataSource) {
        this.dataSource = dataSource;
        this.description = description;
        this.statistics = statistics;
        this.testId = testId;
        this.logger = Logger.getLogger(this.getClass().getName());
        this.createIfNotExists();
    }

    @Override
    public void delete() {

        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt = conn.prepareStatement("delete from OutstandingTest where id = ?;");
            stmt.setString(1, testId);
            stmt.executeUpdate();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }

    }

    @Override
    public void increaseLoginAttempt() throws NotFoundException {

        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt = conn.prepareStatement("update OutstandingTest set loginAttempts = loginAttempts + 1 where id = ?;");
            stmt.setString(1, testId);
            stmt.executeUpdate();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }

    }

    @Override
    public String getId() {
        return testId;
    }

    @Override
    public Date getDate() throws NotFoundException {

        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt = conn.prepareStatement("select `date` from OutstandingTest where id = ?");
            stmt.setString(1, testId);
            ResultSet result = stmt.executeQuery();

            if (!result.next()) {
                throw new NotFoundException();
            }

            return result.getDate("date");


        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }

    }

    @Override
    public int getLoginAttempt() throws NotFoundException {

        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt = conn.prepareStatement("select loginAttempts from OutstandingTest where id = ?");
            stmt.setString(1, testId);
            ResultSet result = stmt.executeQuery();

            if (!result.next()) {
                throw new NotFoundException();
            }

            return result.getInt("loginAttempts");


        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }

    }

    private void createIfNotExists() {

        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt = conn.prepareStatement("insert ignore into OutstandingTest (id, campaign, date) values(?,?,?)");
            stmt.setString(1, testId);
            stmt.setString(2, this.description.getName());

            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDateTime now = LocalDateTime.now();
            stmt.setString(3, dtf.format(now));

            int status = stmt.executeUpdate();

            if (status == 1) {
                //initially increase statistics
                statistics.increaseSentMails(1);
            }

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }
    }
}
