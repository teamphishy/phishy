package edu.kit.iti.crypto.phishy.mariadb.campaign;

import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookDescription;
import edu.kit.iti.crypto.phishy.data.addressbook.Recipient;
import edu.kit.iti.crypto.phishy.data.campaign.*;
import edu.kit.iti.crypto.phishy.data.statistics.CampaignStatisticsData;
import edu.kit.iti.crypto.phishy.data.template.TemplateDescription;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.MutationException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import edu.kit.iti.crypto.phishy.mariadb.statistics.MariaDBCampaignStatisticsData;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class MariaDBCampaign extends Campaign {

    private static final String SQL_NAME = "[a-zA-Z][a-zA-Z_0-9]*";
    private static final Pattern SQL_NAME_PATTERN = Pattern.compile(SQL_NAME);

    private final DataSource dataSource;
    private final Logger logger;
    private final CampaignDescription campaignDescription;
    private final MariaDBCampaignStatisticsData statistics;

    public MariaDBCampaign(String name, DataSource dataSource) {
        this(new CampaignDescription(name), dataSource);
    }

    public MariaDBCampaign(CampaignDescription campaignDescription, DataSource dataSource) {
        this.dataSource = dataSource;
        this.logger = Logger.getLogger(this.getClass().getName());
        this.campaignDescription = campaignDescription;
        createCampaignIfNotExist();
        statistics = new MariaDBCampaignStatisticsData(campaignDescription.getName(), dataSource);
    }

    private void createCampaignIfNotExist() {
        final String query = "INSERT IGNORE INTO Campaign (id) VALUES (?)";

        try (Connection connection = this.dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, this.campaignDescription.getName());
            statement.executeUpdate();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while inserting into Campaign");
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void linkTemplate(TemplateType type, TemplateDescription description) {

        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt = conn.prepareStatement(
                    "replace into CampaignTemplateRelation (campaign, type, template) values (?, ?, ?);");
            stmt.setString(1, this.getName());
            stmt.setInt(2, type.toInt());
            stmt.setString(3, description.toString());
            stmt.executeUpdate();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }

    }

    @Override
    public void unlinkTemplate(TemplateType type) throws DeletionException {
        Objects.requireNonNull(type);

        if (!hasTemplate(type)) {
            throw new DeletionException();
        }

        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt =
                    conn.prepareStatement("delete from CampaignTemplateRelation where campaign = ? and type = ?;");
            stmt.setString(1, this.getName());
            stmt.setInt(2, type.toInt());
            stmt.executeUpdate();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing MariaDB: " + e.getMessage());
            throw new DeletionException();
        }

    }

    @Override
    public TemplateDescription getTemplate(TemplateType type) throws NotFoundException {

        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt = conn.prepareStatement(
                    "select template from CampaignTemplateRelation where campaign = ? and type = ?;");
            stmt.setString(1, this.getName());
            stmt.setInt(2, type.toInt());
            ResultSet resultSet = stmt.executeQuery();

            if (!resultSet.next()) {
                throw new NotFoundException();
            }

            return TemplateDescription.fromFQN(resultSet.getString("template"));

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }

    }

    @Override
    public boolean hasTemplate(TemplateType type) {

        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt = conn.prepareStatement(
                    "select template from CampaignTemplateRelation where campaign = ? and type = ?;");
            stmt.setString(1, this.getName());
            stmt.setInt(2, type.toInt());
            ResultSet resultSet = stmt.executeQuery();

            return resultSet.next();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }

    }

    @Override
    public CampaignStatisticsData getStatistics() {
        return new MariaDBCampaignStatisticsData(campaignDescription, dataSource);
    }

    @Override
    public OutstandingTest getOutstandingTest(String id) throws NotFoundException {

        if (!hasOutstandingTest(id)) {
            throw new NotFoundException();
        }
        return new MariaDBOutstandingTest(id, campaignDescription, statistics, dataSource);

    }

    @Override
    public boolean hasOutstandingTest(String id) {

        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt =
                    conn.prepareStatement("select id from OutstandingTest where campaign = ? and id = ?;");
            stmt.setString(1, this.getName());
            stmt.setString(2, id);
            ResultSet resultSet = stmt.executeQuery();

            return resultSet.next();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }

    }

    @Override
    public AddressbookDescription getAddressbook() throws NotFoundException {

        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt = conn.prepareStatement("select addressbook from Campaign where id = ?");
            stmt.setString(1, this.getName());
            ResultSet resultSet = stmt.executeQuery();

            if (!resultSet.next()) {
                logger.log(Level.SEVERE, "Campaign " + campaignDescription + " was deleted while accessing");
                throw new IllegalStateException("Campaign does not exits");
            }

            String addressbookName = resultSet.getString("addressbook");
            if (addressbookName == null) {
                throw new NotFoundException();
            }

            return new AddressbookDescription(addressbookName);

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }

    }

    @Override
    public void setAddressbook(AddressbookDescription description) {

        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt = conn.prepareStatement("update Campaign set addressbook = ? where id = ?;");
            stmt.setString(1, description.getName());
            stmt.setString(2, this.getName());
            stmt.executeUpdate();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }

    }

    @Override
    public void recordTests(Collection<TestDescription> tests) {

        try (Connection conn = dataSource.getConnection()) {
            conn.createStatement().execute("START TRANSACTION;");
            PreparedStatement stmt =
                    conn.prepareStatement("insert into OutstandingTest (id, campaign, `date`) values (?, ?, ?)");

            for (TestDescription test : tests) {

                stmt.setString(1, test.getTestID());
                stmt.setString(2, this.getName());
                stmt.setDate(3, new Date(System.currentTimeMillis()));
                stmt.executeUpdate();
            }
            conn.createStatement().execute("COMMIT;");

            statistics.increaseSentMails(tests.size());
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }

    }

    @Override
    public List<Recipient> deleteTests(List<Recipient> recipients) {
        try (Connection conn = dataSource.getConnection()) {

            List<Recipient> deleteRecipients = new ArrayList<Recipient>();

            conn.createStatement().execute("START TRANSACTION;");

            PreparedStatement stmt = conn.prepareStatement("DELETE FROM OutstandingTest WHERE id = ?;");

            for (Recipient recipient : recipients) {
                stmt.setString(1, recipient.getTestId().getTestID());
                if (stmt.executeUpdate() > 0) {
                    deleteRecipients.add(recipient);
                }
            }

            conn.createStatement().execute("COMMIT;");

            return deleteRecipients;

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }

    }

    @Override
    public String getName() {
        return campaignDescription.getName();
    }

    @Override
    public int getRecipientSetSize() {
        return getIntValue("recipientSetSize");
    }

    @Override
    public void setRecipientSetSize(int recipientSetSize) {
        if (recipientSetSize <= 0) {
            throw new IllegalArgumentException("Recipient set size must be greater zero.");
        }
        setIntValue("recipientSetSize", recipientSetSize);
    }

    @Override
    public CampaignStatus getStatus() {
        return CampaignStatus.fromInt(getIntValue("status"));
    }

    @Override
    public void setStatus(CampaignStatus status) throws MutationException {

        if (status == null) {
            throw new IllegalArgumentException("status must not be null");
        }

        if (status == CampaignStatus.ACTIVE && !this.isSetUp()) {
            throw new MutationException();
        }
        setIntValue("status", status.toInt());
    }

    @Override
    public int getMailLifetime() {
        return getIntValue("mailLifetime");
    }

    @Override
    public void setMailLifetime(int mailLifetime) {
        if (mailLifetime <= 0) {
            throw new IllegalArgumentException("Mail lifetime must be greater zero.");
        }
        setIntValue("mailLifetime", mailLifetime);
    }

    @Override
    public int getMailInterval() {
        return getIntValue("mailInterval");
    }

    @Override
    public void setMailInterval(int mailInterval) {
        if (mailInterval <= 0) {
            throw new IllegalArgumentException("Mail interval must be greater zero.");
        }
        setIntValue("mailInterval", mailInterval);
    }

    private int getIntValue(String field) {

        if (!SQL_NAME_PATTERN.matcher(field).matches()) {
            throw new IllegalArgumentException("Illegal SQL field name");
        }

        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt = conn.prepareStatement("select " + field + " from Campaign where id = ?;");
            stmt.setString(1, this.getName());
            ResultSet result = stmt.executeQuery();

            if (!result.next()) {
                logger.log(Level.SEVERE, "Campaign " + campaignDescription + " was deleted while accessing");
                throw new IllegalStateException("Campaign does not exits");
            }

            return result.getInt(field);

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }

    }

    private void setIntValue(String field, int value) {

        if (!SQL_NAME_PATTERN.matcher(field).matches()) {
            throw new IllegalArgumentException("Illegal SQL field name");
        }

        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt = conn.prepareStatement("update Campaign set " + field + " = ? where id = ?;");
            stmt.setInt(1, value);
            stmt.setString(2, this.getName());
            stmt.executeUpdate();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }

    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MariaDBCampaign that = (MariaDBCampaign) o;
        return Objects.equals(getName(), that.getName());
    }
}
