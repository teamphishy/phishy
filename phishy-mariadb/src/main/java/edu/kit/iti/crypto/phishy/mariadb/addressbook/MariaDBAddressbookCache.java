package edu.kit.iti.crypto.phishy.mariadb.addressbook;

import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookCache;
import edu.kit.iti.crypto.phishy.data.addressbook.Recipient;

import javax.sql.DataSource;
import java.sql.*;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MariaDBAddressbookCache implements AddressbookCache {

    private final DataSource dataSource;
    private final Logger logger;

    public MariaDBAddressbookCache(DataSource dataSource) {
        this.dataSource = dataSource;
        this.logger = Logger.getLogger(this.getClass().getName());
    }

    @Override
    public void storeRecipients(Collection<Recipient> recipients) {

        logger.log(Level.INFO, "Storing in AB cache " + recipients.size());

        try (Connection conn = dataSource.getConnection()) {
            conn.createStatement().execute("START TRANSACTION;");
            PreparedStatement stmt = conn.prepareStatement("replace into Recipients (id, lastTest) values (?, ?);");

            for (Recipient r : recipients) {
                stmt.setString(1, r.getId());
                Date latestContact = r.getLatestContact();
                if (latestContact == null) {
                    stmt.setNull(2, Types.DATE);
                } else {
                    java.sql.Date sqlDate = new java.sql.Date(latestContact.getTime());
                    stmt.setDate(2, sqlDate);
                }
                stmt.executeUpdate();
            }
            conn.createStatement().execute("COMMIT;");
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error writing to MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }

        logger.log(Level.INFO, "Finished storing");

    }

    @Override
    public List<Recipient> getRandomRecipients(int num, Date notTestedSince) {

        List<Recipient> recipients = new ArrayList<>();

        try (Connection conn = dataSource.getConnection()) {

            // num is integer -> no sql injection possible
            PreparedStatement stmt = conn.prepareStatement("select id from Recipients where lastTest <= ? OR lastTest IS NULL order by RAND() limit " + num + ";");
            stmt.setObject(1, notTestedSince.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                recipients.add(new Recipient(result.getString("id")));
            }

            return recipients;

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error reading from MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }

    }

    @Override
    public Recipient completeRecipient(Recipient recipient) {

        Recipient completedRecipient = recipient.clone();

        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt = conn.prepareStatement("select lastTest from Recipients where id = ?");
            stmt.setString(1, recipient.getId());
            ResultSet resultSet = stmt.executeQuery();

            if (resultSet.next()) {
                java.sql.Date lastContact = resultSet.getDate("lastTest");
                completedRecipient.setLatestContact(lastContact);
            }

            return completedRecipient;

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error reading from MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }

    }
}
