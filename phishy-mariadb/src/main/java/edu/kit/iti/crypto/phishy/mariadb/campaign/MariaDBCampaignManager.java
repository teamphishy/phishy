package edu.kit.iti.crypto.phishy.mariadb.campaign;

import edu.kit.iti.crypto.phishy.data.campaign.Campaign;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignDescription;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignManager;
import edu.kit.iti.crypto.phishy.data.statistics.CampaignStatisticsData;
import edu.kit.iti.crypto.phishy.data.statistics.CumulatedStatisticsData;
import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import edu.kit.iti.crypto.phishy.mariadb.statistics.PredefinedMariaDBCampaignStatisticsData;
import edu.kit.iti.crypto.phishy.mariadb.statistics.PredefinedMariaDBCumulatedStatisticsData;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class MariaDBCampaignManager implements CampaignManager {

    private static final String NAME_FORMAT = "^[a-zA-Z][a-zA-Z0-9_]*$";
    private static final Pattern NAME_PATTERN = Pattern.compile(NAME_FORMAT);

    private final DataSource dataSource;
    private final Logger logger;

    public MariaDBCampaignManager(DataSource dataSource) {
        this.dataSource = dataSource;
        this.logger = Logger.getLogger(this.getClass().getName());
    }

    @Override
    public Campaign getCampaign(CampaignDescription campaignDescription) throws NotFoundException {

        if (!hasCampaign(campaignDescription)) {
            throw new NotFoundException();
        }

        return new MariaDBCampaign(campaignDescription, dataSource);

    }

    @Override
    public boolean hasCampaign(CampaignDescription name) {

        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt = conn.prepareStatement("select id from Campaign where id = ?;");
            stmt.setString(1, name.getName());
            ResultSet result = stmt.executeQuery();

            return result.next();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }

    }

    @Override
    public Collection<Campaign> listCampaigns() {

        List<Campaign> campaigns = new ArrayList<>();

        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt = conn.prepareStatement("select id from Campaign;");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {

                campaigns.add(new MariaDBCampaign(result.getString("id"), dataSource));

            }

            return campaigns;

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing MariaDB: " + e.getMessage());
            throw new IllegalStateException("Error accessing MariaDB");
        }

    }

    @Override
    public Campaign createCampaign(String name) throws CreationException {
        if (!NAME_PATTERN.matcher(name).matches()) {
            throw new IllegalArgumentException("Name must match: " + NAME_FORMAT);
        }
        if (hasCampaign(new CampaignDescription(name))) {
            throw new CreationException();
        }
        return new MariaDBCampaign(name, dataSource);
    }

    @Override
    public Campaign createCampaign(String name, int recipientSetSize, int mailLifetime, int mailInterval)
            throws CreationException {
        if (!NAME_PATTERN.matcher(name).matches()) {
            throw new IllegalArgumentException("Name must match: " + NAME_FORMAT);
        }
        if (hasCampaign(new CampaignDescription(name))) {
            throw new CreationException();
        }

        Campaign campaign = new MariaDBCampaign(name, dataSource);
        campaign.setRecipientSetSize(recipientSetSize);
        campaign.setMailLifetime(mailLifetime);
        campaign.setMailInterval(mailInterval);
        return campaign;
    }

    @Override
    public void deleteCampaign(CampaignDescription name) throws DeletionException {

        if (!hasCampaign(name)) {
            throw new DeletionException();
        }

        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt = conn.prepareStatement("delete from Campaign where id = ?;");
            stmt.setString(1, name.getName());
            stmt.executeUpdate();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing MariaDB: " + e.getMessage());
            throw new DeletionException();
        }

    }

    @Override
    public CumulatedStatisticsData getCumulatedStatistics() {
        return PredefinedMariaDBCumulatedStatisticsData.get(dataSource);
    }

    @Override
    public Collection<CampaignStatisticsData> getCampaignStatisticsData() {
        return PredefinedMariaDBCampaignStatisticsData.getAll(dataSource);
    }
}
