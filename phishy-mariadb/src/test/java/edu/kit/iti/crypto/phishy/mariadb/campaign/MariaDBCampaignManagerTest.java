package edu.kit.iti.crypto.phishy.mariadb.campaign;

import edu.kit.iti.crypto.phishy.data.campaign.Campaign;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignDescription;
import edu.kit.iti.crypto.phishy.data.statistics.CampaignStatisticsData;
import edu.kit.iti.crypto.phishy.data.statistics.CumulatedStatisticsData;
import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

public class MariaDBCampaignManagerTest {

    private MariaDBCampaignManager manager;
    private MockDatasourceMariaDB dataSource;

    @Before
    public void setUp() throws Exception {
        dataSource = new MockDatasourceMariaDB();
        Connection con = dataSource.getConnection();

        manager = new MariaDBCampaignManager(dataSource);

        //create basic table structure with some test values
        con.createStatement().executeUpdate("create table Campaign (id varchar(32) not null, status boolean not null default 0, recipientSetSize smallint not null default 30, mailLifetime smallint not null default 14, mailInterval int not null default 30, addressbook varchar(32) default null, primary key (id));");
        con.createStatement().executeUpdate("create table Statistics (campaign varchar(32) not null, sentMails int not null default 0, reportedMails int not null default 0, phishedMails int not null default 0, primary key (campaign), foreign key (campaign) references Campaign (id));");
        con.createStatement().executeUpdate("create trigger statistics_create after insert on Campaign for each row insert ignore into Statistics (campaign) values (NEW.id);");
        con.createStatement().executeUpdate("create trigger statistics_delete before delete on Campaign for each row delete from Statistics where campaign = OLD.id;");

        con.createStatement().executeUpdate("insert into Campaign (id, recipientSetSize, status, mailLifetime, mailInterval, addressbook) values ('test', 25, 0, 5, 60, 'testAddressbook')");
        con.createStatement().executeUpdate("insert into Campaign (id, recipientSetSize, status, mailLifetime, mailInterval, addressbook) values ('otherTest', 7, 1, 3, 100, 'addressbook2')");

        con.createStatement().executeUpdate("update Statistics set sentMails = 5, reportedMails = 2, phishedMails = 3 where campaign = 'test';");
        con.createStatement().executeUpdate("update Statistics set sentMails = 1, reportedMails = 0, phishedMails = 1 where campaign = 'otherTest';");
    }

    @After
    public void shutdownDocker() {
        dataSource.closeDocker();
        dataSource = null;
        manager = null;
    }

    @Test
    public void getCampaign() throws NotFoundException {
        Campaign campaign = manager.getCampaign(new CampaignDescription("test"));
        assertEquals("test", campaign.getName());
        assertEquals(25, campaign.getRecipientSetSize());
        assertEquals(0, campaign.getStatus().toInt());
        assertEquals(5, campaign.getMailLifetime());
        assertEquals("testAddressbook", campaign.getAddressbook().getName());
    }

    @Test(expected = NotFoundException.class)
    public void getCampaignNotFound() throws NotFoundException {
        Campaign campaign = manager.getCampaign(new CampaignDescription("test4"));
    }

    @Test
    public void hasCampaign() {
        assertTrue(manager.hasCampaign(new CampaignDescription("test")));
    }

    @Test
    public void hasCampaignFail() {
        assertFalse(manager.hasCampaign(new CampaignDescription("test6")));
    }

    @Test
    public void listCampaigns() {
        Collection<Campaign> campaigns = manager.listCampaigns();
        ArrayList<String> expectedContents = new ArrayList<>(Arrays.asList("test", "otherTest"));

        campaigns.forEach(campaign -> {
            if (!expectedContents.contains(campaign.getName())) {
                fail();
            }
            expectedContents.remove(campaign.getName());
        });
    }

    @Test
    public void createCampaign() throws CreationException {
        Campaign campaign = manager.createCampaign("test2");
        assertEquals("test2", campaign.getName());
    }

    @Test(expected = CreationException.class)
    public void createCampaignCreationExeption() throws CreationException {
        Campaign campaign = manager.createCampaign("test");
    }

    @Test
    public void testCreateCampaign() throws CreationException {
        Campaign campaign = manager.createCampaign("test2", 42, 39, 32);
        assertEquals("test2", campaign.getName());
        assertEquals(42, campaign.getRecipientSetSize());
        assertEquals(39, campaign.getMailLifetime());
        assertEquals(32, campaign.getMailInterval());
    }

    @Test(expected = CreationException.class)
    public void testCreateCampaignCreationException() throws CreationException {
        Campaign campaign = manager.createCampaign("test", 42, 39, 32);
    }

    @Test
    public void deleteCampaign() throws DeletionException, SQLException {
        manager.deleteCampaign(new CampaignDescription("test"));
        ResultSet set = dataSource.getConnection().prepareStatement("SELECT id FROM Campaign WHERE id='test'").executeQuery();
        assertFalse(set.next());
    }

    @Test(expected = DeletionException.class)
    public void deleteCampaignDeletionException() throws DeletionException {
        manager.deleteCampaign(new CampaignDescription("test3"));
    }

    @Test
    public void getCumulatedStatistics() {
        CumulatedStatisticsData statistics = manager.getCumulatedStatistics();
        assertEquals(6, statistics.getSentMails());
        assertEquals(2, statistics.getReportedMails());
        assertEquals(4, statistics.getPhishedMails());
    }

    @Test
    public void getCampaignStatisticsData() {
        Collection<CampaignStatisticsData> statistics = manager.getCampaignStatisticsData();

        assertEquals(2, statistics.size());
        boolean[] contains = new boolean[2];
        statistics.forEach(stat -> {
            if (stat.getCampaignName().equals("test")) {
                assertEquals(5, stat.getSentMails());
                assertEquals(2, stat.getReportedMails());
                assertEquals(3, stat.getPhishedMails());
                contains[0] = true;
            } else if (stat.getCampaignName().equals("otherTest")) {
                assertEquals(1, stat.getSentMails());
                assertEquals(0, stat.getReportedMails());
                assertEquals(1, stat.getPhishedMails());
                contains[1] = true;
            } else {
                Assert.fail();
            }
        });
        assertTrue(contains[0]);
        assertTrue(contains[1]);
    }
}
