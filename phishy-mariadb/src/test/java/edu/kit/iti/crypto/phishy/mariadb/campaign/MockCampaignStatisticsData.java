package edu.kit.iti.crypto.phishy.mariadb.campaign;

import edu.kit.iti.crypto.phishy.data.statistics.CampaignStatisticsData;

public class MockCampaignStatisticsData implements CampaignStatisticsData {

    private int sentMails = 0;
    private int reportedMails = 0;
    private int phishedMails = 0;

    @Override
    public String getCampaignName() {
        return null;
    }

    @Override
    public void increaseSentMails(int amount) {
        sentMails += amount;
    }

    @Override
    public void increaseReportedMail(int amount) {
        reportedMails += amount;
    }

    @Override
    public void increasePhishedMail(int amount) {
        phishedMails += amount;
    }

    @Override
    public int getSentMails() {
        return sentMails;
    }

    @Override
    public int getReportedMails() {
        return reportedMails;
    }

    @Override
    public int getPhishedMails() {
        return phishedMails;
    }
}
