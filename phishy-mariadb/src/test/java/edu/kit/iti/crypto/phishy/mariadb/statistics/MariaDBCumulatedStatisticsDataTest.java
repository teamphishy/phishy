package edu.kit.iti.crypto.phishy.mariadb.statistics;

import edu.kit.iti.crypto.phishy.data.statistics.CumulatedStatisticsData;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

public class MariaDBCumulatedStatisticsDataTest {

    private MockDatasourceMariaDB dataSource;

    @Before
    public void setupDatasource() throws SQLException {
        dataSource = new MockDatasourceMariaDB();
        Connection con = dataSource.getConnection();

        //create basic table structure with some test values
        con.createStatement().executeUpdate("create table Campaign (id varchar(32) not null, status boolean not null default 0, recipientSetSize smallint not null default 30, mailLifetime smallint not null default 14, mailInterval int not null default 30, addressbook varchar(32) default null, primary key (id));");
        con.createStatement().executeUpdate("create table Statistics (campaign varchar(32) not null, sentMails int not null default 0, reportedMails int not null default 0, phishedMails int not null default 0, primary key (campaign), foreign key (campaign) references Campaign (id));");
        con.createStatement().executeUpdate("create trigger statistics_create after insert on Campaign for each row insert ignore into Statistics (campaign) values (NEW.id);");
        con.createStatement().executeUpdate("create trigger statistics_delete before delete on Campaign for each row delete from Statistics where campaign = OLD.id;");

        con.createStatement().executeUpdate("insert into Campaign (id) values ('test')");
        con.createStatement().executeUpdate("update Statistics set sentMails = 25, reportedMails = 10, phishedMails = 5 where campaign = 'test';");
        con.createStatement().executeUpdate("insert into Campaign (id) values ('otherTest')");
        con.createStatement().executeUpdate("update Statistics set sentMails = 7, reportedMails = 2, phishedMails = 3 where campaign = 'otherTest';");
    }

    @After
    public void shutdownDocker() {
        dataSource.closeDocker();
        dataSource = null;
    }

    @Test
    public void testGetData() throws SQLException {
        CumulatedStatisticsData data = PredefinedMariaDBCumulatedStatisticsData.get(dataSource);

        Assert.assertEquals(data.getSentMails(), 25 + 7);
        Assert.assertEquals(data.getReportedMails(), 10 + 2);
        Assert.assertEquals(data.getPhishedMails(), 5 + 3);
    }


    @Test
    public void testGet() throws SQLException {
        CumulatedStatisticsData data = PredefinedMariaDBCumulatedStatisticsData.get(dataSource);

        Assert.assertEquals(data.getSentMails(), 25 + 7);
        Assert.assertEquals(data.getReportedMails(), 10 + 2);
        Assert.assertEquals(data.getPhishedMails(), 5 + 3);
    }

}
