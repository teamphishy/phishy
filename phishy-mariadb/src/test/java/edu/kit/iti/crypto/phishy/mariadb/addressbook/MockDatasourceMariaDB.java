package edu.kit.iti.crypto.phishy.mariadb.addressbook;

import org.junit.Rule;
import org.testcontainers.containers.MariaDBContainer;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

public class MockDatasourceMariaDB implements DataSource {

    @Rule
    public MariaDBContainer mariaDB = new MariaDBContainer().withUsername("admin").withPassword("admin").withDatabaseName("phishy");
    private Connection con;

    public MockDatasourceMariaDB() throws SQLException {
        mariaDB.start();
    }

    public void closeDocker() {
        mariaDB.close();
    }

    @Override
    public Connection getConnection() throws SQLException {
        if (true || con == null || con.isClosed()) {
            con = DriverManager.getConnection(mariaDB.getJdbcUrl(), "admin", "admin");
        }
        return con;
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return getConnection();
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return null;
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {

    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {

    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return 0;
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return null;
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return false;
    }
}
