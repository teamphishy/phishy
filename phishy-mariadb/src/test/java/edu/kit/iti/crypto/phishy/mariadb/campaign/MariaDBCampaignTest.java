package edu.kit.iti.crypto.phishy.mariadb.campaign;

import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookDescription;
import edu.kit.iti.crypto.phishy.data.addressbook.Recipient;
import edu.kit.iti.crypto.phishy.data.campaign.*;
import edu.kit.iti.crypto.phishy.data.statistics.StatisticsData;
import edu.kit.iti.crypto.phishy.data.template.TemplateDescription;
import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.MutationException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class MariaDBCampaignTest {

    private MariaDBCampaign campaign;
    private MariaDBCampaign campaign2;
    private MariaDBCampaign campaignFailing;


    private MockDatasourceMariaDB dataSource;

    @Before
    public void setupDatasource() throws SQLException {
        dataSource = new MockDatasourceMariaDB();
        try (Connection con = this.dataSource.getConnection()) {

            //create basic table structure with some test values
            con.createStatement().executeUpdate("create table Campaign (id varchar(32) not null, status boolean not null default 0, recipientSetSize smallint not null default 30, mailLifetime smallint not null default 14, mailInterval int not null default 30, addressbook varchar(32) default null, primary key (id));");
            con.createStatement().executeUpdate("create table CampaignTemplateRelation (campaign varchar(32) not null, type tinyint(2) not null, template varchar(32) not null, primary key (campaign, type), foreign key (campaign) references Campaign(id));");
            con.createStatement().executeUpdate("create table OutstandingTest(id varchar(36) not null, campaign varchar(32) not null, `date` Date not null, loginAttempts tinyint(2) not null default 0, primary key (id), foreign key (campaign) references Campaign(id));");
            con.createStatement().executeUpdate("create table Statistics (campaign varchar(32) not null, sentMails int not null default 0, reportedMails int not null default 0, phishedMails int not null default 0, primary key (campaign), foreign key (campaign) references Campaign (id));");
            con.createStatement().executeUpdate("create trigger statistics_create after insert on Campaign for each row insert ignore into Statistics (campaign) values (NEW.id);");
            con.createStatement().executeUpdate("create trigger statistics_delete before delete on Campaign for each row delete from Statistics where campaign = OLD.id;");
            con.createStatement().executeUpdate("create trigger templates_delete before delete on Campaign for each row delete from CampaignTemplateRelation where campaign = OLD.id;");
            con.createStatement().executeUpdate("create trigger outstandingtests_delete before delete on Campaign for each row delete from OutstandingTest where campaign = OLD.id;");

            con.createStatement().executeUpdate("insert into Campaign (id) values ('test123')");
            con.createStatement().executeUpdate("insert into Campaign (id) values ('test456')");
            campaign = new MariaDBCampaign("test123", dataSource);
            campaign2 = new MariaDBCampaign("test123", dataSource);
            campaignFailing = new MariaDBCampaign("test13", dataSource);

            con.createStatement().executeUpdate("update Statistics set sentMails = 1, reportedMails = 2, phishedMails = 3 where campaign = 'test123';");

            con.createStatement().executeUpdate("insert into Campaign (id) values ('otherTest')");
            con.createStatement().executeUpdate("update Campaign set recipientSetSize = 15, status = 1, mailLifetime = 1337, mailInterval = 60, addressbook='ldap' where id = 'test123';");


            //manually rename campaign to create error (doesn't work in MariaDB)
            con.createStatement().executeUpdate("update Campaign set recipientSetSize = 999, status = 0, mailLifetime = 42, mailInterval = 40, addressbook='keins' where id = 'test456';");

            con.createStatement().executeUpdate("insert into CampaignTemplateRelation (campaign, type, template) values ('test123', 3, 'group-template1')");
            con.createStatement().executeUpdate("insert into OutstandingTest (id, campaign, date, loginAttempts) values ('oTest1', 'test123', '2020-07-13', 9)");
        }
    }

    @After
    public void shutdownDocker() {
        dataSource.closeDocker();
        dataSource = null;
        campaign = null;
        campaignFailing = null;
    }

    @Test
    public void isSetUpFail() {
        assertFalse(campaign.isSetUp());
    }

    @Test
    public void isSetUp() throws CreationException {
        campaign.linkTemplate(TemplateType.INFO_MAIL, new TemplateDescription("test123", "testTemplate1"));
        campaign.linkTemplate(TemplateType.INFO_WEBSITE, new TemplateDescription("test123", "testTemplate2"));
        //campaign.linkTemplate(TemplateType.PHISH_MAIL, new TemplateDescription("test123", "testTemplate3"));
        campaign.linkTemplate(TemplateType.PHISH_WEBSITE, new TemplateDescription("test123", "testTemplate4"));
        campaign.setAddressbook(new AddressbookDescription("testAddressbook"));

        assertTrue(campaign.isSetUp());
    }

    @Test
    public void isAlmostSetUp() throws CreationException, SQLException {
        try (Connection con = dataSource.getConnection()) {
            campaign.linkTemplate(TemplateType.INFO_MAIL, new TemplateDescription("test123", "testTemplate1"));
            campaign.linkTemplate(TemplateType.INFO_WEBSITE, new TemplateDescription("test123", "testTemplate2"));
            campaign.linkTemplate(TemplateType.PHISH_MAIL, new TemplateDescription("test123", "testTemplate3"));
            campaign.linkTemplate(TemplateType.PHISH_WEBSITE, new TemplateDescription("test123", "testTemplate4"));
            con.createStatement().executeUpdate("UPDATE Campaign SET addressbook=NULL WHERE id='test123'");
            assertFalse(campaign.isSetUp());
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void createIllegalCampaignName() {
        new SQLCampaign("test/)!(/)=", dataSource);
    }

    @Test
    public void linkTemplate() throws SQLException {
        try (Connection con = this.dataSource.getConnection()) {
            campaign.linkTemplate(TemplateType.INFO_MAIL, new TemplateDescription("test123", "testTemplate"));
            ResultSet set = con.prepareStatement("SELECT template FROM CampaignTemplateRelation WHERE campaign='test123'").executeQuery();
            assertTrue(set.next());
            assertEquals("test123-testTemplate", set.getString(1));
        }
    }

    @Test
    public void unlinkTemplate() throws DeletionException, SQLException {
        try (Connection con = this.dataSource.getConnection()) {
            campaign.unlinkTemplate(TemplateType.PHISH_MAIL);
            ResultSet set = con.prepareStatement("SELECT template FROM CampaignTemplateRelation WHERE campaign='test123'").executeQuery();
            assertFalse(set.next());
        }
    }

    @Test(expected = DeletionException.class)
    public void unlinkTemplateFail() throws DeletionException {
        campaign.unlinkTemplate(TemplateType.INFO_MAIL);
    }

    @Test
    public void getTemplate() throws NotFoundException {
        TemplateDescription description = campaign.getTemplate(TemplateType.PHISH_MAIL);
        assertEquals("template1", description.getName());
        assertEquals("group", description.getGroup());
    }

    @Test(expected = NotFoundException.class)
    public void getTemplateFail() throws NotFoundException {
        campaign.getTemplate(TemplateType.INFO_MAIL);
    }

    @Test(expected = NotFoundException.class)
    public void getTemplateFail2() throws NotFoundException, SQLException {
        try (Connection con = dataSource.getConnection()) {
            con.createStatement().executeUpdate("UPDATE CampaignTemplateRelation SET template='testTemplateFailing' WHERE campaign='test123' AND type = 1");
            campaign.getTemplate(TemplateType.INFO_MAIL);
        }
    }

    @Test
    public void hasTemplate() {
        assertTrue(campaign.hasTemplate(TemplateType.PHISH_MAIL));
    }

    @Test
    public void hasTemplateFail() {
        assertFalse(campaign.hasTemplate(TemplateType.INFO_MAIL));
    }

    @Test
    public void getStatistics() {
        StatisticsData statistics = campaign.getStatistics();
        assertEquals(1, statistics.getSentMails());
        assertEquals(2, statistics.getReportedMails());
        assertEquals(3, statistics.getPhishedMails());
    }

    @Test
    public void getOutstandingTest() throws NotFoundException {
        OutstandingTest test = campaign.getOutstandingTest("oTest1");
        assertEquals(test.getId(), "oTest1");
    }

    @Test(expected = NotFoundException.class)
    public void getOutstandingTestFail() throws NotFoundException {
        OutstandingTest test = campaign.getOutstandingTest("oTest");
    }

    @Test
    public void hasOutstandingTest() {
        assertTrue(campaign.hasOutstandingTest("oTest1"));
    }

    @Test
    public void hasOutstandingTestFail() {
        assertFalse(campaign.hasOutstandingTest("oTest"));
    }

    @Test
    public void getAddressbook() throws NotFoundException {
        assertEquals("ldap", campaign.getAddressbook().getName());
    }

    @Test(expected = NotFoundException.class)
    public void getAddressbookNotFound() throws NotFoundException {
        AddressbookDescription a = campaignFailing.getAddressbook();
        System.out.println(a.getName());
    }

    @Test
    public void setAddressbook() throws SQLException {
        try (Connection con = this.dataSource.getConnection()) {
            campaign.setAddressbook(new AddressbookDescription("a"));
            ResultSet set = con.prepareStatement("SELECT addressbook FROM Campaign WHERE id='test123'").executeQuery();
            assertTrue(set.next());
            assertEquals("a", set.getString(1));
        }
    }

    @Test
    public void recordTest() throws SQLException {
        try (Connection con = this.dataSource.getConnection()) {
            ArrayList<TestDescription> tests = new ArrayList<>();
            tests.add(new TestDescription(new CampaignDescription("test123"), "oTest2"));
            tests.add(new TestDescription(new CampaignDescription("test456"), "oTest3"));
            campaign.recordTests(tests);
            ResultSet set = con.prepareStatement("SELECT id, date FROM OutstandingTest WHERE id='oTest2'").executeQuery();
            assertTrue(set.next());
            assertEquals("oTest2", set.getString(1));
            ResultSet set2 = con.prepareStatement("SELECT id FROM OutstandingTest WHERE id='oTest3'").executeQuery();
            assertTrue(set2.next());
            assertEquals("oTest3", set2.getString(1));
        }
    }

    @Test
    public void recordTestEmpty() {
        ArrayList<TestDescription> tests = new ArrayList<>();
        campaign.recordTests(tests);
    }

    @Test
    public void deleteTestsTest() throws SQLException {
        try (Connection con = this.dataSource.getConnection()) {
            //insert Outstanding test data

            ArrayList<Recipient> rList = new ArrayList<>();
            Recipient r1 = new Recipient("test@mail.com");
            r1.setTestId(new TestDescription(new CampaignDescription("test123"), "oTest1"));
            rList.add(r1);

            campaign.deleteTests(rList);
            ResultSet set = con.prepareStatement("SELECT id FROM OutstandingTest WHERE id='oTest1'").executeQuery();
            assertFalse(set.next());
        }
    }

    @Test
    public void deleteTestsEmptyTest() {
        ArrayList<Recipient> tests = new ArrayList<>();
        campaign.deleteTests(tests);
    }

    @Test
    public void getName() {
        assertEquals("test123", campaign.getName());
    }

    @Test
    public void getRecipientSetSize() {
        assertEquals(15, campaign.getRecipientSetSize());
    }

    @Test
    public void setRecipientSetSize() throws SQLException {
        try (Connection con = this.dataSource.getConnection()) {
            campaign.setRecipientSetSize(20);
            ResultSet set = con.prepareStatement("SELECT recipientSetSize FROM Campaign WHERE id='test123'").executeQuery();
            assertTrue(set.next());
            assertEquals(20, set.getInt(1));
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void setRecipientSetSizeFail() throws SQLException {
        campaign.setRecipientSetSize(-50);
    }

    @Test
    public void getStatus() {
        assertEquals(CampaignStatus.ACTIVE, campaign.getStatus());
    }

    @Test
    public void setStatus() throws SQLException, MutationException {
        try (Connection con = this.dataSource.getConnection()) {
            campaign.setStatus(CampaignStatus.INACTIVE);
            ResultSet set = con.prepareStatement("SELECT status FROM Campaign WHERE id='test123'").executeQuery();
            assertTrue(set.next());
            assertEquals(CampaignStatus.INACTIVE.toInt(), set.getInt(1));
        }
    }

    @Test(expected = MutationException.class)
    public void setStatusFail() throws SQLException, MutationException {
        campaign.setStatus(CampaignStatus.ACTIVE);
    }

    @Test
    public void getMailLifetime() {
        assertEquals(1337, campaign.getMailLifetime());
    }

    @Test
    public void setMailLifetime() throws SQLException {
        try (Connection con = this.dataSource.getConnection()) {
            campaign.setMailLifetime(50);
            ResultSet set = con.prepareStatement("SELECT mailLifetime FROM Campaign WHERE id='test123'").executeQuery();
            assertTrue(set.next());
            assertEquals(50, set.getInt(1));
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void setMailLifetimeFail() {
        campaign.setMailLifetime(-42);
    }

    @Test
    public void getMailInterval() {
        assertEquals(60, campaign.getMailInterval());
    }

    @Test
    public void setMailInterval() throws SQLException {
        try (Connection con = this.dataSource.getConnection()) {
            campaign.setMailInterval(50);
            ResultSet set = con.prepareStatement("SELECT mailInterval FROM Campaign WHERE id='test123'").executeQuery();
            assertTrue(set.next());
            assertEquals(50, set.getInt(1));
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void setMailIntervalFail() {
        campaign.setMailInterval(-500);
    }

    @Test
    public void equalsSameTest() {
        MariaDBCampaign campaignCopy = campaign;
        assertEquals(campaign, campaignCopy);
    }

    @Test
    public void equalsDifferentClassTest() {
        assertNotEquals(campaign, new BigDecimal(42));
    }

    @Test
    public void equalsTest() {
        assertEquals(campaign, campaign2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setNullStatus() throws MutationException {
        campaign.setStatus(null);
    }
}
