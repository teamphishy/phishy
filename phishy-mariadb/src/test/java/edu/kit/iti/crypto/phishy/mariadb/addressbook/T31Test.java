package edu.kit.iti.crypto.phishy.mariadb.addressbook;

import edu.kit.iti.crypto.phishy.data.addressbook.Addressbook;
import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookCache;
import edu.kit.iti.crypto.phishy.data.addressbook.CSVAddressbook;
import edu.kit.iti.crypto.phishy.data.addressbook.Recipient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

import static org.junit.Assert.assertEquals;

/* T31 Testcase
 * See SQLite version for documentation in phishy.data.addressbook
 */
public class T31Test {
    private MockDatasourceMariaDB dataSource = null;

    @Before
    public void setUp() throws Exception {
        dataSource = new MockDatasourceMariaDB();
        try (Connection con = dataSource.getConnection()) {
            con.createStatement().executeUpdate("create table Recipients (id varchar(100) not null, lastTest Date default null, primary key (id));");
        }
    }

    @After
    public void shutdownDocker() {
        dataSource.closeDocker();
        dataSource = null;
    }


    @Test
    public void testT31MariaDB() throws URISyntaxException, SQLException {

        final int RECIPIENT_NUM = 20;
        final int EXPECTED_RECIPIENT_NUM = 15;

        Path csvFile = Paths.get(this.getClass().getResource("t31.csv").toURI());
        AddressbookCache cache = new MariaDBAddressbookCache(dataSource);
        Addressbook addressbook = new CSVAddressbook("addressbook", cache, csvFile);

        addressbook.updateCache();

        // first set 10 Recipients to 65 days old
        Calendar calender65DaysOld;
        try (Connection con = dataSource.getConnection()) {

            calender65DaysOld = new GregorianCalendar();
            calender65DaysOld.add(Calendar.DAY_OF_MONTH, -65);

            PreparedStatement stmt = con.prepareStatement("update Recipients set lastTest = ? LIMIT 10");
            stmt.setDate(1, new java.sql.Date(calender65DaysOld.getTime().getTime()));
            stmt.executeUpdate();
        }

        // then set 5 of those 10 Recipients to dates older than 90
        try (Connection con = dataSource.getConnection()) {

            Calendar cal = new GregorianCalendar();
            cal.add(Calendar.DAY_OF_MONTH, -95);

            PreparedStatement stmt = con.prepareStatement("update Recipients set lastTest = ? where lastTest = ? LIMIT 5");
            stmt.setDate(1, new java.sql.Date(cal.getTime().getTime()));
            stmt.setDate(2, new java.sql.Date(calender65DaysOld.getTime().getTime()));
            stmt.executeUpdate();

        }

        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DAY_OF_MONTH, -90);

        List<Recipient> recipients = addressbook.getRandomRecipients(RECIPIENT_NUM, cal.getTime());

        assertEquals(EXPECTED_RECIPIENT_NUM, recipients.size());

        // Check for uniqueness
        Set<Recipient> recipientSet = new HashSet<>(recipients);
        assertEquals(EXPECTED_RECIPIENT_NUM, recipientSet.size());

    }
}
