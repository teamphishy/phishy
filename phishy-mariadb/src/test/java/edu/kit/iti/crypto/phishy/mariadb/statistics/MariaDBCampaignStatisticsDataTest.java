package edu.kit.iti.crypto.phishy.mariadb.statistics;

import edu.kit.iti.crypto.phishy.data.campaign.CampaignDescription;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

public class MariaDBCampaignStatisticsDataTest {

    private MockDatasourceMariaDB dataSource;

    @Before
    public void setupDatasource() throws SQLException {
        dataSource = new MockDatasourceMariaDB();
        Connection con = dataSource.getConnection();

        //create basic table structure with some test values
        con.createStatement().executeUpdate("create table Campaign (id varchar(32) not null, status boolean not null default 0, recipientSetSize smallint not null default 30, mailLifetime smallint not null default 14, mailInterval int not null default 30, addressbook varchar(32) default null, primary key (id));");
        con.createStatement().executeUpdate("create table Statistics (campaign varchar(32) not null, sentMails int not null default 0, reportedMails int not null default 0, phishedMails int not null default 0, primary key (campaign), foreign key (campaign) references Campaign (id));");
        con.createStatement().executeUpdate("create trigger statistics_create after insert on Campaign for each row insert ignore into Statistics (campaign) values (NEW.id);");
        con.createStatement().executeUpdate("create trigger statistics_delete before delete on Campaign for each row delete from Statistics where campaign = OLD.id;");

        con.createStatement().executeUpdate("insert into Campaign (id) values ('test')");
        con.createStatement().executeUpdate("update Statistics set sentMails = 25, reportedMails = 10, phishedMails = 5 where campaign = 'test';");
        con.createStatement().executeUpdate("insert into Campaign (id) values ('otherTest')");
        con.createStatement().executeUpdate("update Statistics set sentMails = 7, reportedMails = 2, phishedMails = 3 where campaign = 'otherTest';");
        con.createStatement().executeUpdate("insert into Campaign (id) values ('justCampaign')");
    }

    @After
    public void shutdownDocker() {
        dataSource.closeDocker();
        dataSource = null;
    }

    @Test
    public void testGetData() throws SQLException {
        MariaDBCampaignStatisticsData data = new MariaDBCampaignStatisticsData("test", dataSource);

        Assert.assertEquals(data.getCampaignName(), "test");
        Assert.assertEquals(data.getSentMails(), 25);
        Assert.assertEquals(data.getReportedMails(), 10);
        Assert.assertEquals(data.getPhishedMails(), 5);
    }

    @Test
    public void secondConstructor() throws SQLException {
        MariaDBCampaignStatisticsData data = new MariaDBCampaignStatisticsData(new CampaignDescription("test"), dataSource);

        Assert.assertEquals(data.getCampaignName(), "test");
        Assert.assertEquals(data.getSentMails(), 25);
        Assert.assertEquals(data.getReportedMails(), 10);
        Assert.assertEquals(data.getPhishedMails(), 5);
    }


    @Test
    public void testGetOtherData() throws SQLException {
        MariaDBCampaignStatisticsData otherData = new MariaDBCampaignStatisticsData("otherTest", dataSource);

        Assert.assertEquals(otherData.getCampaignName(), "otherTest");
        Assert.assertEquals(otherData.getSentMails(), 7);
        Assert.assertEquals(otherData.getReportedMails(), 2);
        Assert.assertEquals(otherData.getPhishedMails(), 3);
    }

    @Test
    public void testCreateDatabaseEntry() throws SQLException {
        MariaDBCampaignStatisticsData data = new MariaDBCampaignStatisticsData("justCampaign", dataSource);

        Assert.assertEquals(data.getCampaignName(), "justCampaign");
        Assert.assertEquals(data.getSentMails(), 0);
        Assert.assertEquals(data.getReportedMails(), 0);
        Assert.assertEquals(data.getPhishedMails(), 0);

        data.increaseSentMails(42);
        data.increaseReportedMail(11);
        data.increasePhishedMail(7);

        Assert.assertEquals(data.getSentMails(), 42);
        Assert.assertEquals(data.getReportedMails(), 11);
        Assert.assertEquals(data.getPhishedMails(), 7);
    }

    @Test
    public void testIncreaseData() throws SQLException {
        MariaDBCampaignStatisticsData data = new MariaDBCampaignStatisticsData("test", dataSource);

        data.increaseSentMails(42);
        data.increaseReportedMail(11);
        data.increasePhishedMail(7);

        Assert.assertEquals(data.getSentMails(), 25 + 42);
        Assert.assertEquals(data.getReportedMails(), 10 + 11);
        Assert.assertEquals(data.getPhishedMails(), 5 + 7);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIncreaseByNegativeSent() throws SQLException {
        MariaDBCampaignStatisticsData data = new MariaDBCampaignStatisticsData("test", dataSource);
        data.increaseSentMails(-42);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIncreaseByNegativeReported() throws SQLException {
        MariaDBCampaignStatisticsData data = new MariaDBCampaignStatisticsData("test", dataSource);
        data.increaseReportedMail(-42);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIncreaseByNegativePhished() throws SQLException {
        MariaDBCampaignStatisticsData data = new MariaDBCampaignStatisticsData("test", dataSource);
        data.increasePhishedMail(-42);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNotExistingCampaign() throws SQLException {
        MariaDBCampaignStatisticsData data = new MariaDBCampaignStatisticsData("DoesNotExist", dataSource);
        data.getPhishedMails();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSQLInjection() throws SQLException {
        MariaDBCampaignStatisticsData data = new MariaDBCampaignStatisticsData("test AND NOT campaign=test OR campaign=otherTest", dataSource);
        data.getPhishedMails();
    }
}
