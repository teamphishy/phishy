package edu.kit.iti.crypto.phishy.mariadb.statistics;

import edu.kit.iti.crypto.phishy.data.campaign.CampaignDescription;
import edu.kit.iti.crypto.phishy.data.statistics.CampaignStatisticsData;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;

public class PredefinedMariaDBCampaignStatisticsDataTest {

    @Test
    public void testGetData() {
        CampaignStatisticsData data = new PredefinedMariaDBCampaignStatisticsData(new CampaignDescription("test"), 25, 10, 5);

        Assert.assertEquals(data.getCampaignName(), "test");
        Assert.assertEquals(data.getSentMails(), 25);
        Assert.assertEquals(data.getReportedMails(), 10);
        Assert.assertEquals(data.getPhishedMails(), 5);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testIncreaseDataSent() {
        CampaignStatisticsData data = new PredefinedMariaDBCampaignStatisticsData(new CampaignDescription("test"), 25, 10, 5);

        data.increaseSentMails(42);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testIncreaseDataReported() {
        CampaignStatisticsData data = new PredefinedMariaDBCampaignStatisticsData(new CampaignDescription("test"), 25, 10, 5);

        data.increaseReportedMail(11);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testIncreaseDataPhished() {
        CampaignStatisticsData data = new PredefinedMariaDBCampaignStatisticsData(new CampaignDescription("test"), 25, 10, 5);

        data.increasePhishedMail(7);
    }

    @Test
    public void testGetAll() throws SQLException {
        MockDatasourceMariaDB dataSource = new MockDatasourceMariaDB();
        Connection con = dataSource.getConnection();

        //create basic table structure with some test values
        con.createStatement().executeUpdate("create table Campaign (id varchar(32) not null, status boolean not null default 0, recipientSetSize smallint not null default 30, mailLifetime smallint not null default 14, mailInterval int not null default 30, addressbook varchar(32) default null, primary key (id));");
        con.createStatement().executeUpdate("create table Statistics (campaign varchar(32) not null, sentMails int not null default 0, reportedMails int not null default 0, phishedMails int not null default 0, primary key (campaign), foreign key (campaign) references Campaign (id));");
        con.createStatement().executeUpdate("create trigger statistics_create after insert on Campaign for each row insert ignore into Statistics (campaign) values (NEW.id);");
        con.createStatement().executeUpdate("create trigger statistics_delete before delete on Campaign for each row delete from Statistics where campaign = OLD.id;");

        con.createStatement().executeUpdate("insert into Campaign (id) values ('test')");
        con.createStatement().executeUpdate("update Statistics set sentMails = 25, reportedMails = 10, phishedMails = 5 where campaign = 'test';");
        con.createStatement().executeUpdate("insert into Campaign (id) values ('otherTest')");
        con.createStatement().executeUpdate("update Statistics set sentMails = 7, reportedMails = 2, phishedMails = 3 where campaign = 'otherTest';");

        Collection<CampaignStatisticsData> data = PredefinedMariaDBCampaignStatisticsData.getAll(dataSource);

        Assert.assertEquals(data.size(), 2);
        // because of java
        final boolean[] hasTest = new boolean[1];
        final boolean[] hasOtherTest = new boolean[1];

        data.forEach(d -> {
            if (d.getCampaignName().equals("test")) {
                Assert.assertEquals(d.getSentMails(), 25);
                Assert.assertEquals(d.getReportedMails(), 10);
                Assert.assertEquals(d.getPhishedMails(), 5);
                hasTest[0] = true;
            } else if (d.getCampaignName().equals("otherTest")) {
                Assert.assertEquals(d.getSentMails(), 7);
                Assert.assertEquals(d.getReportedMails(), 2);
                Assert.assertEquals(d.getPhishedMails(), 3);
                hasOtherTest[0] = true;
            } else {
                Assert.fail("Only two test were created");
            }
        });
        Assert.assertTrue(hasTest[0]);
        Assert.assertTrue(hasOtherTest[0]);
        dataSource.closeDocker();
    }
}
