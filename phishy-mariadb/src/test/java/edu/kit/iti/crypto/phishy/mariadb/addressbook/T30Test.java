package edu.kit.iti.crypto.phishy.mariadb.addressbook;

import edu.kit.iti.crypto.phishy.data.addressbook.Addressbook;
import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookCache;
import edu.kit.iti.crypto.phishy.data.addressbook.CSVAddressbook;
import edu.kit.iti.crypto.phishy.data.addressbook.Recipient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/* T30 Testcase
 * See SQLite version for documentation in phishy.data.addressbook
 */
public class T30Test {

    private MockDatasourceMariaDB dataSource = null;

    @Before
    public void setUp() throws Exception {
        dataSource = new MockDatasourceMariaDB();
        Connection con = dataSource.getConnection();
        con.createStatement().executeUpdate("create table Recipients (id varchar(100) not null, lastTest Date default null, primary key (id));");
    }

    @After
    public void shutdownDocker() {
        dataSource.closeDocker();
        dataSource = null;
    }

    @Test
    public void testT30MariaDB() throws URISyntaxException {

        final int RECIPIENT_NUM = 20;

        Path csvFile = Paths.get(this.getClass().getResource("t30.csv").toURI());
        AddressbookCache cache = new MariaDBAddressbookCache(dataSource);
        Addressbook addressbook = new CSVAddressbook("addressbook", cache, csvFile);

        addressbook.updateCache();

        List<Recipient> recipients = addressbook.getRandomRecipients(RECIPIENT_NUM, new Date());

        assertEquals(RECIPIENT_NUM, recipients.size());

        // Check for uniqueness
        Set<Recipient> recipientSet = new HashSet<>(recipients);
        assertEquals(RECIPIENT_NUM, recipientSet.size());
    }

}
