package edu.kit.iti.crypto.phishy.mariadb.campaign;

import edu.kit.iti.crypto.phishy.data.campaign.CampaignDescription;
import edu.kit.iti.crypto.phishy.data.statistics.CampaignStatisticsData;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static org.junit.Assert.*;

public class MariaDBOutstandingTestTest {
    private MariaDBOutstandingTest test;
    private MariaDBOutstandingTest testFailing;
    private MockDatasourceMariaDB dataSource;
    private CampaignStatisticsData c1Stat;

    @Before
    public void setUp() throws Exception {
        dataSource = new MockDatasourceMariaDB();
        try (Connection con = this.dataSource.getConnection()) {
            con.createStatement().executeUpdate("create table Campaign (id varchar(32) not null, status boolean not null default 0, recipientSetSize smallint not null default 30, mailLifetime smallint not null default 14, mailInterval int not null default 30, addressbook varchar(32) default null, primary key (id));");
            con.createStatement().executeUpdate("create table OutstandingTest(id varchar(36) not null, campaign varchar(32) not null, `date` Date not null, loginAttempts tinyint(2) not null default 0, primary key (id), foreign key (campaign) references Campaign(id));");
            con.createStatement().executeUpdate("create table Statistics (campaign varchar(32) not null, sentMails int not null default 0, reportedMails int not null default 0, phishedMails int not null default 0, primary key (campaign), foreign key (campaign) references Campaign (id));");
            con.createStatement().executeUpdate("create trigger statistics_create after insert on Campaign for each row insert ignore into Statistics (campaign) values (NEW.id);");
            con.createStatement().executeUpdate("create trigger statistics_delete before delete on Campaign for each row delete from Statistics where campaign = OLD.id;");
            con.createStatement().executeUpdate("create trigger outstandingtests_delete before delete on Campaign for each row delete from OutstandingTest where campaign = OLD.id;");


            con.createStatement().executeUpdate("insert into Campaign (id) values ('c1')");
            c1Stat = new MockCampaignStatisticsData();

            test = new MariaDBOutstandingTest("test123", new CampaignDescription("c1"), c1Stat, dataSource);
            testFailing = new MariaDBOutstandingTest("test13", new CampaignDescription("c1"), c1Stat, dataSource);
            //this one already exists and shouldn't increase the statistics
            MariaDBOutstandingTest testFailing2 = new MariaDBOutstandingTest("test13", new CampaignDescription("c1"), c1Stat, dataSource);

            con.createStatement().executeUpdate("UPDATE OutstandingTest SET date='2020-07-13', loginAttempts=2 WHERE id='test123';");
            con.createStatement().executeUpdate("UPDATE OutstandingTest SET id='test456', campaign='c1', date='2020-07-14', loginAttempts=6 WHERE id='test13';");
        }
    }

    @After
    public void shutdownDocker() {
        dataSource.closeDocker();
        dataSource = null;
        test = null;
        testFailing = null;
        c1Stat = null;
    }

    @Test
    public void increaseLoginAttempt() throws NotFoundException, SQLException {
        try (Connection con = this.dataSource.getConnection()) {
            test.increaseLoginAttempt();
            ResultSet set = con.prepareStatement("SELECT loginAttempts FROM OutstandingTest WHERE id='test123'").executeQuery();
            assertTrue(set.next());
            assertEquals(3, set.getInt("loginAttempts"));
        }
    }

    @Test
    public void getId() {
        assertEquals("test123", test.getId());
    }

    @Test
    public void getDate() throws NotFoundException {
        Date date = test.getDate();
        long expectedDate = LocalDateTime.parse("2020-07-13T00:00:00").atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        assertEquals(expectedDate, date.getTime());
    }

    @Test(expected = NotFoundException.class)
    public void getDateNotFound() throws NotFoundException {
        testFailing.getDate();
    }

    @Test
    public void getLoginAttempt() throws NotFoundException {
        assertEquals(2, test.getLoginAttempt());
    }

    @Test(expected = NotFoundException.class)
    public void getLoginAttemptNotFound() throws NotFoundException {
        testFailing.getLoginAttempt();
    }

    @Test
    public void delete() throws SQLException {
        try (Connection con = this.dataSource.getConnection()) {
            test.delete();
            ResultSet set = con.prepareStatement("SELECT loginAttempts FROM OutstandingTest WHERE id='test123'").executeQuery();
            assertFalse(set.next());
        }
    }

    @Test
    public void statIncrease() {
        Assert.assertEquals(2, c1Stat.getSentMails());
        testFailing = new MariaDBOutstandingTest("new", new CampaignDescription("c1"), c1Stat, dataSource);
        Assert.assertEquals(3, c1Stat.getSentMails());
    }
}
