package edu.kit.iti.crypto.phishy.data.campaign;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CampaignStatusTest {

    @Test
    public void fromInt() {
        assertEquals(CampaignStatus.INACTIVE, CampaignStatus.fromInt(0));
        assertEquals(CampaignStatus.ACTIVE, CampaignStatus.fromInt(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void fromIntFail() {
        CampaignStatus.fromInt(-1);
    }

    @Test
    public void testToString() {
        assertEquals("inactive", CampaignStatus.INACTIVE.toString());
        assertEquals("active", CampaignStatus.ACTIVE.toString());
    }

    @Test
    public void fromString() {
        assertEquals(CampaignStatus.INACTIVE, CampaignStatus.fromString("inactive"));
        assertEquals(CampaignStatus.ACTIVE, CampaignStatus.fromString("active"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void fromStringFail() {
        CampaignStatus.fromString("inactivead");

    }
}