package edu.kit.iti.crypto.phishy.data.campaign;

import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookDescription;
import edu.kit.iti.crypto.phishy.data.statistics.StatisticsData;
import edu.kit.iti.crypto.phishy.data.template.TemplateDescription;
import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.MutationException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class SQLCampaignTest {
    private DataSource dataSource;
    private SQLCampaign campaign;
    private SQLCampaign campaignFailing;


    @Before
    public void setUp() throws Exception {
        dataSource = new MockSQLDatasource();
        try (Connection con = this.dataSource.getConnection()) {
            con.createStatement().executeUpdate("DROP TABLE IF Exists Campaign");
            con.createStatement().executeUpdate("DROP TABLE IF Exists Statistics");
            con.createStatement().executeUpdate("DROP TABLE IF Exists CampaignTemplateRelation");
            con.createStatement().executeUpdate("DROP TABLE IF Exists OutstandingTest");

            con.createStatement().executeUpdate("create table Campaign (id text not null, recipientSetSize int default 100 not null, status int default 0 not null, mailLifetime int default 14 not null, mailInterval int default 30 not null, addressbook text default null, primary key(id));");

            con.createStatement().executeUpdate("create table Statistics (campaign text not null, sentMails int default 0 not null, reportedMails int default 0 not null, phishedMails int default 0 not null, primary key(campaign), foreign key(campaign) references campaign(id))");
            con.createStatement().executeUpdate("INSERT INTO Statistics(campaign, sentMails, reportedMails, phishedMails) VALUES('test123', 1, 2, 3)");

            campaign = new SQLCampaign("test123", dataSource);
            campaignFailing = new SQLCampaign("test13", dataSource);

            con.createStatement().executeUpdate("UPDATE Campaign SET recipientSetSize=15, status=1, mailLifetime=1337, mailInterval=60, addressbook='ldap' WHERE id='test123'");
            //manually rename campaign to create error
            con.createStatement().executeUpdate("UPDATE Campaign SET id='test456', recipientSetSize=999, status=0, mailLifetime=42, mailInterval=40, addressbook='keins' WHERE id='test13'");

            con.createStatement().executeUpdate("create table CampaignTemplateRelation(campaign text not null, type int not null, template text not null, primary key(campaign, type), foreign key(campaign) references campaign(id))");
            con.createStatement().executeUpdate("INSERT INTO CampaignTemplateRelation(campaign, type, template) VALUES('test123', 3, 'group-template1')");

            con.createStatement().executeUpdate("create table OutstandingTest (id text not null primary key, campaign text not null, \"date\" date, status int default 0 not null, loginAttempts int default 0 not null);");
            con.createStatement().executeUpdate("INSERT INTO OutstandingTest(id, campaign, date, status, loginAttempts) VALUES('oTest1', 'test123', '1594626452', 1, 9)");
        }
    }

    @Test
    public void isSetUpFail() {
        assertFalse(campaign.isSetUp());
    }

    @Test
    public void isSetUp() throws CreationException {
        campaign.linkTemplate(TemplateType.INFO_MAIL, new TemplateDescription("test123", "testTemplate1"));
        campaign.linkTemplate(TemplateType.INFO_WEBSITE, new TemplateDescription("test123", "testTemplate2"));
        //campaign.linkTemplate(TemplateType.PHISH_MAIL, new TemplateDescription("test123", "testTemplate3"));
        campaign.linkTemplate(TemplateType.PHISH_WEBSITE, new TemplateDescription("test123", "testTemplate4"));
        campaign.setAddressbook(new AddressbookDescription("testAddressbook"));

        assertTrue(campaign.isSetUp());
    }

    @Test
    public void isAlmostSetUp() throws CreationException, SQLException {
        try (Connection con = dataSource.getConnection()) {
            campaign.linkTemplate(TemplateType.INFO_MAIL, new TemplateDescription("test123", "testTemplate1"));
            campaign.linkTemplate(TemplateType.INFO_WEBSITE, new TemplateDescription("test123", "testTemplate2"));
            campaign.linkTemplate(TemplateType.PHISH_MAIL, new TemplateDescription("test123", "testTemplate3"));
            campaign.linkTemplate(TemplateType.PHISH_WEBSITE, new TemplateDescription("test123", "testTemplate4"));
            con.createStatement().executeUpdate("UPDATE Campaign SET addressbook=NULL WHERE id='test123'");
            assertFalse(campaign.isSetUp());
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void createIllegalCampaignName() {
        new SQLCampaign("test/)!(/)=", dataSource);
    }

    @Test
    public void linkTemplate() throws SQLException {
        try (Connection con = this.dataSource.getConnection()) {
            campaign.linkTemplate(TemplateType.INFO_MAIL, new TemplateDescription("test123", "testTemplate"));
            ResultSet set = con.prepareStatement("SELECT template FROM CampaignTemplateRelation WHERE campaign='test123'").executeQuery();
            assertEquals("test123-testTemplate", set.getString(1));
        }
    }

    @Test
    public void unlinkTemplate() throws DeletionException, SQLException {
        try (Connection con = this.dataSource.getConnection()) {
            campaign.unlinkTemplate(TemplateType.PHISH_MAIL);
            ResultSet set = con.prepareStatement("SELECT template FROM CampaignTemplateRelation WHERE campaign='test123'").executeQuery();
            assertFalse(set.next());
        }
    }

    @Test(expected = DeletionException.class)
    public void unlinkTemplateFail() throws DeletionException {
        campaign.unlinkTemplate(TemplateType.INFO_MAIL);
    }

    @Test
    public void getTemplate() throws NotFoundException {
        TemplateDescription description = campaign.getTemplate(TemplateType.PHISH_MAIL);
        assertEquals("template1", description.getName());
        assertEquals("group", description.getGroup());
    }

    @Test(expected = NotFoundException.class)
    public void getTemplateFail() throws NotFoundException {
        campaign.getTemplate(TemplateType.INFO_MAIL);
    }

    @Test(expected = NotFoundException.class)
    public void getTemplateFail2() throws NotFoundException, SQLException {
        try (Connection con = dataSource.getConnection()) {
            con.createStatement().executeUpdate("UPDATE CampaignTemplateRelation SET template='testTemplateFailing' WHERE campaign='test123' AND type = 1");
            campaign.getTemplate(TemplateType.INFO_MAIL);
        }
    }

    @Test
    public void hasTemplate() {
        assertTrue(campaign.hasTemplate(TemplateType.PHISH_MAIL));
    }

    @Test
    public void hasTemplateFail() {
        assertFalse(campaign.hasTemplate(TemplateType.INFO_MAIL));
    }

    @Test
    public void getStatistics() {
        StatisticsData statistics = campaign.getStatistics();
        assertEquals(1, statistics.getSentMails());
        assertEquals(2, statistics.getReportedMails());
        assertEquals(3, statistics.getPhishedMails());
    }

    @Test
    public void getOutstandingTest() throws NotFoundException {
        OutstandingTest test = campaign.getOutstandingTest("oTest1");
        assertEquals(test.getId(), "oTest1");
    }

    @Test(expected = NotFoundException.class)
    public void getOutstandingTestFail() throws NotFoundException {
        OutstandingTest test = campaign.getOutstandingTest("oTest");
    }

    @Test
    public void hasOutstandingTest() {
        assertTrue(campaign.hasOutstandingTest("oTest1"));
    }

    @Test
    public void hasOutstandingTestFail() {
        assertFalse(campaign.hasOutstandingTest("oTest"));
    }

    @Test
    public void getAddressbook() throws NotFoundException {
        assertEquals("ldap", campaign.getAddressbook().getName());
    }

    @Test(expected = NotFoundException.class)
    public void getAddressbookNotFound() throws NotFoundException {
        campaignFailing.getAddressbook();
    }

    @Test
    public void setAddressbook() throws SQLException {
        try (Connection con = this.dataSource.getConnection()) {
            campaign.setAddressbook(new AddressbookDescription("a"));
            ResultSet set = con.prepareStatement("SELECT addressbook FROM Campaign WHERE id='test123'").executeQuery();
            assertEquals("a", set.getString(1));
        }
    }

    @Test
    public void recordTest() throws SQLException {
        try (Connection con = this.dataSource.getConnection()) {
            ArrayList<TestDescription> tests = new ArrayList<>();
            tests.add(new TestDescription(new CampaignDescription("test123"), "oTest2"));
            tests.add(new TestDescription(new CampaignDescription("test13"), "oTest3"));
            campaign.recordTests(tests);
            ResultSet set = con.prepareStatement("SELECT id FROM OutstandingTest WHERE id='oTest2'").executeQuery();
            assertEquals("oTest2", set.getString(1));
            ResultSet set2 = con.prepareStatement("SELECT id FROM OutstandingTest WHERE id='oTest3'").executeQuery();
            assertEquals("oTest3", set2.getString(1));
        }
    }

    @Test
    public void recordTestEmpty() {
        ArrayList<TestDescription> tests = new ArrayList<>();
        campaign.recordTests(tests);
    }

    @Test
    public void getName() {
        assertEquals("test123", campaign.getName());
    }

    @Test
    public void getRecipientSetSize() {
        assertEquals(15, campaign.getRecipientSetSize());
    }

    @Test
    public void setRecipientSetSize() throws SQLException {
        try (Connection con = this.dataSource.getConnection()) {
            campaign.setRecipientSetSize(20);
            ResultSet set = con.prepareStatement("SELECT recipientSetSize FROM Campaign WHERE id='test123'").executeQuery();
            assertEquals(20, set.getInt(1));
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void setRecipientSetSizeFail() throws SQLException {
        campaign.setRecipientSetSize(-50);
    }

    @Test
    public void getStatus() {
        assertEquals(CampaignStatus.ACTIVE, campaign.getStatus());
    }

    @Test
    public void setStatus() throws SQLException, MutationException {
        try (Connection con = this.dataSource.getConnection()) {
            campaign.setStatus(CampaignStatus.INACTIVE);
            ResultSet set = con.prepareStatement("SELECT status FROM Campaign WHERE id='test123'").executeQuery();
            assertEquals(CampaignStatus.INACTIVE.toInt(), set.getInt(1));
        }
    }

    @Test(expected = MutationException.class)
    public void setStatusFail() throws SQLException, MutationException {
        campaign.setStatus(CampaignStatus.ACTIVE);
    }

    @Test
    public void getMailLifetime() {
        assertEquals(1337, campaign.getMailLifetime());
    }

    @Test
    public void setMailLifetime() throws SQLException {
        try (Connection con = this.dataSource.getConnection()) {
            campaign.setMailLifetime(50);
            ResultSet set = con.prepareStatement("SELECT mailLifetime FROM Campaign WHERE id='test123'").executeQuery();
            assertEquals(50, set.getInt(1));
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void setMailLifetimeFail() {
        campaign.setMailLifetime(-42);
    }

    @Test
    public void getMailInterval() {
        assertEquals(60, campaign.getMailInterval());
    }

    @Test
    public void setMailInterval() throws SQLException {
        try (Connection con = this.dataSource.getConnection()) {
            campaign.setMailInterval(50);
            ResultSet set = con.prepareStatement("SELECT mailInterval FROM Campaign WHERE id='test123'").executeQuery();
            assertEquals(50, set.getInt(1));
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void setMailIntervalFail() {
        campaign.setMailInterval(-500);
    }

    @Test
    public void equalsSameTest() {
        SQLCampaign campaignCopy = campaign;
        assertEquals(campaign, campaignCopy);
    }

    @Test
    public void equalsDifferentClassTest() {
        assertNotEquals(campaign, new BigDecimal(10));
    }
}