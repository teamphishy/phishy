package edu.kit.iti.crypto.phishy.data.statistics;

import edu.kit.iti.crypto.phishy.data.campaign.CampaignDescription;
import org.junit.Assert;
import org.junit.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SQLCampaignStatisticsDataTest {

    private DataSource setupDatabase() throws SQLException {
        MockDatasourceSQL dataSource = new MockDatasourceSQL();
        Connection con = dataSource.getConnection();

        con.createStatement().executeUpdate("DROP TABLE IF Exists Campaign");
        con.createStatement().executeUpdate("DROP TABLE IF Exists Statistics");

        // create Statistics table with some test values
        con.createStatement().executeUpdate("create table Campaign (id text not null, recipientSetSize int default 100 not null, status int default 0 not null, mailLifetime int default 14 not null, primary key(id));\n");
        con.createStatement().executeUpdate("create table Statistics (campaign text not null, sentMails int default 0 not null, reportedMails int default 0 not null, phishedMails int default 0 not null, primary key(campaign), foreign key(campaign) references campaign(id));\n");
        con.createStatement().executeUpdate("insert into Statistics (campaign, sentMails, reportedMails, phishedMails) values ('test', 25, 10, 5)");
        con.createStatement().executeUpdate("insert into Campaign (id) values ('test')");
        con.createStatement().executeUpdate("insert into Statistics (campaign, sentMails, reportedMails, phishedMails) values ('otherTest', 7, 2, 3)");
        con.createStatement().executeUpdate("insert into Campaign (id) values ('otherTest')");
        con.createStatement().executeUpdate("insert into Campaign (id) values ('justCampaign')");

        return dataSource;
    }

    @Test
    public void secondConstructor() throws SQLException {
        new SQLCampaignStatisticsData(new CampaignDescription("test"), setupDatabase());
    }

    @Test
    public void testGetData() throws SQLException {
        CampaignStatisticsData data = new SQLCampaignStatisticsData("test", setupDatabase());

        Assert.assertEquals(data.getCampaignName(), "test");
        Assert.assertEquals(data.getSentMails(), 25);
        Assert.assertEquals(data.getReportedMails(), 10);
        Assert.assertEquals(data.getPhishedMails(), 5);
    }

    @Test
    public void testGetOtherData() throws SQLException {
        CampaignStatisticsData otherData = new SQLCampaignStatisticsData("otherTest", setupDatabase());

        Assert.assertEquals(otherData.getCampaignName(), "otherTest");
        Assert.assertEquals(otherData.getSentMails(), 7);
        Assert.assertEquals(otherData.getReportedMails(), 2);
        Assert.assertEquals(otherData.getPhishedMails(), 3);
    }

    @Test
    public void testCreateDatabaseEntry() throws SQLException {
        CampaignStatisticsData data = new SQLCampaignStatisticsData("justCampaign", setupDatabase());

        Assert.assertEquals(data.getCampaignName(), "justCampaign");
        Assert.assertEquals(data.getSentMails(), 0);
        Assert.assertEquals(data.getReportedMails(), 0);
        Assert.assertEquals(data.getPhishedMails(), 0);

        data.increaseSentMails(42);
        data.increaseReportedMail(11);
        data.increasePhishedMail(7);

        Assert.assertEquals(data.getSentMails(), 42);
        Assert.assertEquals(data.getReportedMails(), 11);
        Assert.assertEquals(data.getPhishedMails(), 7);
    }

    @Test
    public void testIncreaseData() throws SQLException {
        CampaignStatisticsData data = new SQLCampaignStatisticsData("test", setupDatabase());

        data.increaseSentMails(42);
        data.increaseReportedMail(11);
        data.increasePhishedMail(7);

        Assert.assertEquals(data.getSentMails(), 25 + 42);
        Assert.assertEquals(data.getReportedMails(), 10 + 11);
        Assert.assertEquals(data.getPhishedMails(), 5 + 7);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIncreaseByNegativeSent() throws SQLException {
        CampaignStatisticsData data = new SQLCampaignStatisticsData("test", setupDatabase());
        data.increaseSentMails(-42);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIncreaseByNegativeReported() throws SQLException {
        CampaignStatisticsData data = new SQLCampaignStatisticsData("test", setupDatabase());
        data.increaseReportedMail(-42);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIncreaseByNegativePhished() throws SQLException {
        CampaignStatisticsData data = new SQLCampaignStatisticsData("test", setupDatabase());
        data.increasePhishedMail(-42);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNotExistingCampaign() throws SQLException {
        CampaignStatisticsData data = new SQLCampaignStatisticsData("DoesNotExist", setupDatabase());
        data.getPhishedMails();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSQLInjection() throws SQLException {
        CampaignStatisticsData data = new SQLCampaignStatisticsData("test AND NOT campaign=test OR campaign=otherTest", setupDatabase());
        data.getPhishedMails();
    }

    @Test
    public void removeStatistic() throws SQLException {
        DataSource dataSource = setupDatabase();
        CampaignStatisticsData statistics = new SQLCampaignStatisticsData("test", dataSource);
        SQLCampaignStatisticsData.removeStatistic(new CampaignDescription("test"), dataSource);

        ResultSet set = dataSource.getConnection().createStatement().executeQuery("SELECT campaign FROM Statistics WHERE campaign='test'");
        Assert.assertFalse(set.next());
    }
}
