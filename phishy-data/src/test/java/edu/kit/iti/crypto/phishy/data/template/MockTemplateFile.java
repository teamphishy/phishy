package edu.kit.iti.crypto.phishy.data.template;

import java.io.InputStream;

public class MockTemplateFile implements TemplateFile {

    private String virtualPath;
    private String mimeType;
    private int estimatedSize;
    private InputStream stream;

    public MockTemplateFile(String virtualPath, String mimeType, int estimatedSize, InputStream stream) {
        this.virtualPath = virtualPath;
        this.mimeType = mimeType;
        this.estimatedSize = estimatedSize;
        this.stream = stream;
    }

    @Override
    public String getVirtualPath() {
        return virtualPath;
    }

    @Override
    public String getMimeType() {
        return mimeType;
    }

    @Override
    public long getEstimatedSize() {
        return estimatedSize;
    }

    @Override
    public InputStream getStream() {
        return stream;
    }
}
