package edu.kit.iti.crypto.phishy.data.statistics;

import org.junit.Assert;
import org.junit.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;

public class PredefinedSQLCampaignStatisticsDataTest {

    private DataSource setupDatabase() throws SQLException {
        MockDatasourceSQL dataSource = new MockDatasourceSQL();
        Connection con = dataSource.getConnection();

        // create Statistics table with some test values
        con.createStatement().executeUpdate("create table Campaign (id text not null, recipientSetSize int default 100 not null, status int default 0 not null, mailLifetime int default 14 not null, primary key(id));\n");
        con.createStatement().executeUpdate("create table Statistics (campaign text not null, sentMails int default 0 not null, reportedMails int default 0 not null, phishedMails int default 0 not null, primary key(campaign), foreign key(campaign) references campaign(id));\n");
        con.createStatement().executeUpdate("insert into Statistics (campaign, sentMails, reportedMails, phishedMails) values ('test', 25, 10, 5)");
        con.createStatement().executeUpdate("insert into Campaign (id) values ('test')");
        con.createStatement().executeUpdate("insert into Statistics (campaign, sentMails, reportedMails, phishedMails) values ('otherTest', 7, 2, 3)");
        con.createStatement().executeUpdate("insert into Campaign (id) values ('otherTest')");

        return dataSource;
    }

    @Test
    public void testGetData() {
        CampaignStatisticsData data = new PredefinedSQLCampaignStatisticsData("test", 25, 10, 5);

        Assert.assertEquals(data.getCampaignName(), "test");
        Assert.assertEquals(data.getSentMails(), 25);
        Assert.assertEquals(data.getReportedMails(), 10);
        Assert.assertEquals(data.getPhishedMails(), 5);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testIncreaseDataSent() {
        CampaignStatisticsData data = new PredefinedSQLCampaignStatisticsData("test", 25, 10, 5);

        data.increaseSentMails(42);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testIncreaseDataReported() {
        CampaignStatisticsData data = new PredefinedSQLCampaignStatisticsData("test", 25, 10, 5);

        data.increaseReportedMail(11);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testIncreaseDataPhished() {
        CampaignStatisticsData data = new PredefinedSQLCampaignStatisticsData("test", 25, 10, 5);

        data.increasePhishedMail(7);
    }

    @Test
    public void testGetAll() throws SQLException {
        Collection<CampaignStatisticsData> data = PredefinedSQLCampaignStatisticsData.getAll(setupDatabase());

        Assert.assertEquals(data.size(), 2);
        // because of java
        final boolean[] hasTest = new boolean[1];
        final boolean[] hasOtherTest = new boolean[1];

        data.forEach(d -> {
            if (d.getCampaignName().equals("test")) {
                Assert.assertEquals(d.getSentMails(), 25);
                Assert.assertEquals(d.getReportedMails(), 10);
                Assert.assertEquals(d.getPhishedMails(), 5);
                hasTest[0] = true;
            } else if (d.getCampaignName().equals("otherTest")) {
                Assert.assertEquals(d.getSentMails(), 7);
                Assert.assertEquals(d.getReportedMails(), 2);
                Assert.assertEquals(d.getPhishedMails(), 3);
                hasOtherTest[0] = true;
            } else {
                Assert.fail("Only two test were created");
            }
        });
        Assert.assertTrue(hasTest[0]);
        Assert.assertTrue(hasOtherTest[0]);
    }
}
