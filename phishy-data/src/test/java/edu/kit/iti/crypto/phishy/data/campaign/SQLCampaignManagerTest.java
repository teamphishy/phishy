package edu.kit.iti.crypto.phishy.data.campaign;

import edu.kit.iti.crypto.phishy.data.statistics.CampaignStatisticsData;
import edu.kit.iti.crypto.phishy.data.statistics.CumulatedStatisticsData;
import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

public class SQLCampaignManagerTest {

    private SQLCampaignManager manager;
    private DataSource dataSource;

    @Before
    public void setUp() throws Exception {
        dataSource = new MockSQLDatasource();
        Connection con = dataSource.getConnection();

        con.createStatement().executeUpdate("DROP TABLE IF Exists Campaign");
        con.createStatement().executeUpdate("DROP TABLE IF Exists Statistics");
        manager = new SQLCampaignManager(dataSource);

        // create Statistics table with some test values
        con.createStatement().executeUpdate("create table Campaign (id text not null, recipientSetSize int default 100 not null, status int default 0 not null, mailLifetime int default 14 not null, mailInterval int default 30 not null, addressbook text default null, primary key(id));");
        con.createStatement().executeUpdate("INSERT INTO Campaign (id, recipientSetSize, status, mailLifetime, mailInterval, addressbook) VALUES('test', 25, 0, 5, 60, 'testAddressbook')");
        con.createStatement().executeUpdate("INSERT INTO Campaign (id, recipientSetSize, status, mailLifetime, mailInterval, addressbook) VALUES('otherTest', 7, 1, 3, 100, 'addressbook2')");

        con.createStatement().executeUpdate("create table Statistics (campaign text not null, sentMails int default 0 not null, reportedMails int default 0 not null, phishedMails int default 0 not null, primary key(campaign), foreign key(campaign) references campaign(id));");
        con.createStatement().executeUpdate("INSERT INTO Statistics (campaign, sentMails, reportedMails, phishedMails) VALUES('test', 5, 2, 3)");
        con.createStatement().executeUpdate("INSERT INTO Statistics (campaign, sentMails, reportedMails, phishedMails) VALUES('otherTest', 1, 0, 1)");
    }

    @Test
    public void getCampaign() throws NotFoundException {
        Campaign campaign = manager.getCampaign(new CampaignDescription("test"));
        assertEquals("test", campaign.getName());
        assertEquals(25, campaign.getRecipientSetSize());
        assertEquals(0, campaign.getStatus().toInt());
        assertEquals(5, campaign.getMailLifetime());
        assertEquals("testAddressbook", campaign.getAddressbook().getName());
    }

    @Test(expected = NotFoundException.class)
    public void getCampaignNotFound() throws NotFoundException {
        Campaign campaign = manager.getCampaign(new CampaignDescription("test4"));
    }

    @Test
    public void hasCampaign() {
        assertTrue(manager.hasCampaign(new CampaignDescription("test")));
    }

    @Test
    public void hasCampaignFail() {
        assertFalse(manager.hasCampaign(new CampaignDescription("test6")));
    }

    @Test
    public void listCampaigns() {
        Collection<Campaign> campaigns = manager.listCampaigns();
        ArrayList<String> expectedContents = new ArrayList<>(Arrays.asList("test", "otherTest"));

        campaigns.forEach(campaign -> {
            if (!expectedContents.contains(campaign.getName())) {
                fail();
            }
            expectedContents.remove(campaign.getName());
        });
    }

    @Test
    public void createCampaign() throws CreationException {
        Campaign campaign = manager.createCampaign("test2");
        assertEquals("test2", campaign.getName());
    }

    @Test(expected = CreationException.class)
    public void createCampaignCreationExeption() throws CreationException {
        Campaign campaign = manager.createCampaign("test");
    }

    @Test
    public void testCreateCampaign() throws CreationException {
        Campaign campaign = manager.createCampaign("test2", 42, 39, 32);
        assertEquals("test2", campaign.getName());
        assertEquals(42, campaign.getRecipientSetSize());
        assertEquals(39, campaign.getMailLifetime());
        assertEquals(32, campaign.getMailInterval());
    }

    @Test(expected = CreationException.class)
    public void testCreateCampaignCreationException() throws CreationException {
        Campaign campaign = manager.createCampaign("test", 42, 39, 32);
    }

    @Test
    public void deleteCampaign() throws DeletionException, SQLException {
        manager.deleteCampaign(new CampaignDescription("test"));
        ResultSet set = dataSource.getConnection().prepareStatement("SELECT id FROM Campaign WHERE id='test'").executeQuery();
        assertFalse(set.next());
    }

    @Test(expected = DeletionException.class)
    public void deleteCampaignDeletionException() throws DeletionException {
        manager.deleteCampaign(new CampaignDescription("test3"));
    }

    @Test
    public void getCumulatedStatistics() {
        CumulatedStatisticsData statistics = manager.getCumulatedStatistics();
        assertEquals(6, statistics.getSentMails());
        assertEquals(2, statistics.getReportedMails());
        assertEquals(4, statistics.getPhishedMails());
    }

    @Test
    public void getCampaignStatisticsData() {
        Collection<CampaignStatisticsData> statistics = manager.getCampaignStatisticsData();

        //we can't convert to array and compare that because Collection doesn't guarantee order
        assertEquals(2, statistics.size());
        boolean[] contains = new boolean[2];
        statistics.forEach(stat -> {
            if (stat.getCampaignName().equals("test")) {
                assertEquals(5, stat.getSentMails());
                assertEquals(2, stat.getReportedMails());
                assertEquals(3, stat.getPhishedMails());
                contains[0] = true;
            } else if (stat.getCampaignName().equals("otherTest")) {
                assertEquals(1, stat.getSentMails());
                assertEquals(0, stat.getReportedMails());
                assertEquals(1, stat.getPhishedMails());
                contains[1] = true;
            } else {
                Assert.fail();
            }
        });
        assertTrue(contains[0]);
        assertTrue(contains[1]);
    }
}