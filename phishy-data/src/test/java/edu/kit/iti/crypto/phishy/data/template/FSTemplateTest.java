package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.junit.Test;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.*;

public class FSTemplateTest {

    private Path getRootPath() {
        try {
            return Paths.get(this.getClass().getResource("root").toURI());
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Root Path invalid");
        }
    }

    private Path getGroupPath(String name) {
        return getRootPath().resolve(name);
    }

    private Template getTemplate(String group, String name) {

        Path groupPath = getGroupPath(group);
        TemplateGroup tGroup = new FSTemplateGroup(groupPath);
        return new FSTemplate(groupPath, name, tGroup);

    }

    public void testCreation() {

        Path groupPath = getGroupPath("abc");
        TemplateGroup group = new FSTemplateGroup(groupPath);
        Template template = new FSTemplate(groupPath, "a", group);

        assertEquals("a", template.getName());

    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreationNotFound() {

        Path groupPath = getGroupPath("abc");
        TemplateGroup group = new FSTemplateGroup(groupPath);
        new FSTemplate(groupPath, "x", group);

    }

    @Test
    public void testGetFile() throws NotFoundException {

        Template a = getTemplate("abc", "a");
        TemplateFile file = a.getFile("mail.eml");
        assertEquals("mail.eml", file.getVirtualPath());

        Template b = getTemplate("abc", "b");
        TemplateFile fileB = b.getFile("assets/styles.css");
        assertEquals("assets/styles.css", fileB.getVirtualPath());

    }

    @Test(expected = NotFoundException.class)
    public void testGetFileNotFound() throws NotFoundException {

        Template a = getTemplate("abc", "a");
        TemplateFile file = a.getFile("none.ex");

    }

    @Test
    public void testHasFile() {

        Template a = getTemplate("abc", "a");
        assertTrue(a.hasFile("mail.eml"));
        assertFalse(a.hasFile("none.ex"));

        Template b = getTemplate("abc", "b");
        assertTrue(b.hasFile("assets/styles.css"));

    }

    @Test
    public void testGetDescription() {

        Template a = getTemplate("abc", "a");
        TemplateDescription desc = a.getDescription();

        assertEquals("abc", desc.getGroup());
        assertEquals("a", desc.getName());

    }

}