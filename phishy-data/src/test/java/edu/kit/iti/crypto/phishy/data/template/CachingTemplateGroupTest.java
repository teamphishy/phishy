package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class CachingTemplateGroupTest {

    @Test
    public void testGetName() {

        var group = getCachingTemplateGroup();

        assertEquals("abc", group.getName());

    }

    @Test
    public void testGetTemplate() throws NotFoundException {

        TemplateGroup group = getCachingTemplateGroup();
        assertEquals("a", group.getTemplate(new TemplateDescription("abc", "a")).getName());
        assertEquals("b", group.getTemplate(new TemplateDescription("abc", "b")).getName());

    }

    @Test(expected = NotFoundException.class)
    public void testGetTemplateNotFound() throws NotFoundException {

        TemplateGroup group = getCachingTemplateGroup();
        group.getTemplate(new TemplateDescription("abc", "none")).getName();

    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetTemplateWrongGroup() throws NotFoundException {

        TemplateGroup group = getCachingTemplateGroup();
        group.getTemplate(new TemplateDescription("xyz", "x")).getName();

    }

    @Test
    public void testHasTemplate() {

        TemplateGroup group = getCachingTemplateGroup();

        assertTrue(group.hasTemplate(new TemplateDescription("abc", "a")));
        assertTrue(group.hasTemplate(new TemplateDescription("abc", "b")));
        assertFalse(group.hasTemplate(new TemplateDescription("abc", "none")));

    }

    @Test(expected = IllegalArgumentException.class)
    public void testHasTemplateWrongGroup() {

        TemplateGroup group = getCachingTemplateGroup();
        assertTrue(group.hasTemplate(new TemplateDescription("xyz", "a")));

    }

    private CachingTemplateGroup getCachingTemplateGroup() {
        var cache = new TemplateCache();
        var originalGroup = new MockTemplateGroup("abc", List.of(
                new MockTemplate("abc", "a", List.of()),
                new MockTemplate("abc", "b", List.of())
        ));

        return new CachingTemplateGroup(cache, originalGroup);
    }

}