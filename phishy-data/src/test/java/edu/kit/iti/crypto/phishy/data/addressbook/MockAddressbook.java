package edu.kit.iti.crypto.phishy.data.addressbook;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public class MockAddressbook implements Addressbook {

    private String name;
    private List<Recipient> recipients;

    public MockAddressbook(String name, List<Recipient> recipients) {
        this.name = name;
        this.recipients = recipients;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Recipient> getRandomRecipients(int num, Date notTestedSince) {
        return recipients;
    }

    @Override
    public boolean checkCredentials(String name, String password) {
        throw new UnsupportedOperationException("not supported on mock");
    }

    @Override
    public void recordTest(Collection<Recipient> recipients) {
        throw new UnsupportedOperationException("not supported on mock");
    }

    @Override
    public void updateCache() {
        throw new UnsupportedOperationException("not supported on mock");
    }

    @Override
    public Recipient completeRecipient(Recipient recipient) {
        return recipient;
    }
}
