package edu.kit.iti.crypto.phishy.data.addressbook;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import java.util.Collection;

public class MockAddressbookManager implements AddressbookManager {

    private Collection<Addressbook> addressbooks;

    public MockAddressbookManager(Collection<Addressbook> addressbooks) {
        this.addressbooks = addressbooks;
    }

    @Override
    public Collection<Addressbook> listAddressbooks() {
        return addressbooks;
    }

    @Override
    public boolean hasAddressbook(AddressbookDescription description) {

        for (Addressbook ab : addressbooks) {
            if (description.getName().equals(ab.getName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Addressbook getAddressbook(AddressbookDescription description) throws NotFoundException {

        for (Addressbook ab : addressbooks) {
            if (description.getName().equals(ab.getName())) {
                return ab;
            }
        }

        throw new NotFoundException();
    }
}
