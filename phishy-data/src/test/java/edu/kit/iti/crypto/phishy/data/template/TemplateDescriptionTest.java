package edu.kit.iti.crypto.phishy.data.template;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TemplateDescriptionTest {

    @Test
    public void testFromFQN() {

        var descr = TemplateDescription.fromFQN("abc-a");

        assertEquals("abc", descr.getGroup());
        assertEquals("a", descr.getName());

    }

    @Test(expected = IllegalArgumentException.class)
    public void testFromFQNMultipleHypen() {

        TemplateDescription.fromFQN("abc-a-test");

    }

    @Test(expected = IllegalArgumentException.class)
    public void testFromFQNNoHypen() {

        TemplateDescription.fromFQN("abc");

    }

}