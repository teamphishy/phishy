package edu.kit.iti.crypto.phishy.data.addressbook;

import org.junit.Before;
import org.junit.Test;

import javax.sql.DataSource;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

import static org.junit.Assert.assertEquals;

/* T31 Testfall
 *
 * Voraussetzungen:
 * CSV Adressbuch mit 20 Einträgen.
 * Es wurden 5 Empfänger innerhalb der letzten 90 Tage angeschrieben.
 * Es wurden 5 Empfänger vor mehr als 90 Tagen angeschrieben.
 * Die restlichen Empfänger wurden noch nicht angeschrieben.
 *
 * Testfall:
 * Für eine Empfängersatzgröße von 20 werden Empfänger ausgewählt.
 * Der notTestedSince Parameter ist auf NOW - 90 Tage gesetzt.
 * Die Ergebnisliste enthält 15 verschiedene Empfänger,
 * von denen keiner innerhalb der letzten 90 Tage angeschrieben wurde.
 */
public class T31Test {

    private DataSource dataSource = null;

    @Before
    public void setUp() throws Exception {
        dataSource = new MockDataSource();
        try (Connection con = dataSource.getConnection()) {

            con.createStatement().executeUpdate("drop table if exists Recipients");
            con.createStatement().executeUpdate(
                    "create table Recipients (id int not null, lastTest date default null, primary key(id))");
        }

    }

    @Test
    public void testT31SQLite() throws URISyntaxException, SQLException {

        final int RECIPIENT_NUM = 20;
        final int EXPECTED_RECIPIENT_NUM = 15;

        Path csvFile = Paths.get(this.getClass().getResource("t31.csv").toURI());
        AddressbookCache cache = new SQLAddressbookCache(dataSource);
        Addressbook addressbook = new CSVAddressbook("addressbook", cache, csvFile);

        addressbook.updateCache();

        // set 5 Recipients to dates older than 90
        try (Connection con = dataSource.getConnection()) {

            Calendar cal = new GregorianCalendar();
            cal.add(Calendar.DAY_OF_MONTH, -95);

            PreparedStatement stmt = con.prepareStatement("update Recipients set lastTest = ? where rowid <= 5");
            stmt.setDate(1, new java.sql.Date(cal.getTime().getTime()));
            stmt.executeUpdate();

        }

        try (Connection con = dataSource.getConnection()) {

            Calendar cal = new GregorianCalendar();
            cal.add(Calendar.DAY_OF_MONTH, -65);

            PreparedStatement stmt = con.prepareStatement("update Recipients set lastTest = ? where rowid > 5 and rowid <= 10");
            stmt.setDate(1, new java.sql.Date(cal.getTime().getTime()));
            stmt.executeUpdate();

        }

        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DAY_OF_MONTH, -90);

        List<Recipient> recipients = addressbook.getRandomRecipients(RECIPIENT_NUM, cal.getTime());

        assertEquals(EXPECTED_RECIPIENT_NUM, recipients.size());

        // Check for uniqueness
        Set<Recipient> recipientSet = new HashSet<>(recipients);
        assertEquals(EXPECTED_RECIPIENT_NUM, recipientSet.size());

    }

}
