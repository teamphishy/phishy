package edu.kit.iti.crypto.phishy.data;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

public class MockDataSource implements DataSource {

    private Connection createConnection() throws SQLException {
        try {
            String tempPath = this.getClass().getResource("test.db").toURI().getPath();
            return DriverManager.getConnection("jdbc:sqlite:" + tempPath);
        } catch (URISyntaxException e) {
            throw new SQLException();
        }
    }

    public MockDataSource(Path script) throws SQLException, IOException {

        try {
            Path tmpDB = Paths.get(this.getClass().getResource("test.db").toURI());
            Files.deleteIfExists(tmpDB);
            Files.createFile(tmpDB);
        } catch (URISyntaxException | IOException e) {
            // No op
        }

        Connection connection = createConnection();

        BufferedReader reader = Files.newBufferedReader(script);
        String line;
        while ((line = reader.readLine()) != null) {
            connection.prepareStatement(line).executeUpdate();
        }

    }

    @Override
    public Connection getConnection() throws SQLException {
        return createConnection();
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return getConnection();
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return null;
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {

    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {

    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return 0;
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return null;
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return false;
    }
}
