package edu.kit.iti.crypto.phishy.data.addressbook;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class SQLAddressbookCacheTest {

    MockDatasourceSQL dataSource;

    @Before
    public void setupDatabase() throws SQLException {
        dataSource = new MockDatasourceSQL();
        try (Connection con = dataSource.getConnection()) {

            con.createStatement().executeUpdate("DROP TABLE IF Exists Recipients");

            // create Recipients table with some test values
            con.createStatement().executeUpdate("create table Recipients (id int not null, lastTest date default null, primary key(id));");
            con.createStatement().executeUpdate("insert into Recipients (id, lastTest) values ('a@test.com', '1601510400000')");
            con.createStatement().executeUpdate("insert into Recipients (id, lastTest) values ('b@test.com', null)");
            con.createStatement().executeUpdate("insert into Recipients (id, lastTest) values ('c@test.com', '915148800000')");
        }
    }

    @After
    public void deleteDatabase() {
        dataSource = null;
    }

    @Test
    public void storeNullUpdateTestA() throws SQLException {
        SQLAddressbookCache cache = new SQLAddressbookCache(dataSource);

        Collection<Recipient> rList = new ArrayList<>();
        rList.add(new Recipient("a@test.com"));

        cache.storeRecipients(rList);

        try (Connection con = dataSource.getConnection()) {
            ResultSet resultSet = con.createStatement().executeQuery("select lastTest from Recipients where id = 'a@test.com'");
            Assert.assertNull(resultSet.getObject("lastTest"));
        }
    }

    @Test
    public void storeNullUpdateTestB() throws SQLException {
        SQLAddressbookCache cache = new SQLAddressbookCache(dataSource);

        Collection<Recipient> rList = new ArrayList<>();
        rList.add(new Recipient("b@test.com"));

        cache.storeRecipients(rList);
        try (Connection con = dataSource.getConnection()) {
            ResultSet resultSet = con.createStatement().executeQuery("select lastTest from Recipients where id = 'b@test.com'");
            Assert.assertNull(resultSet.getObject("lastTest"));
        }
    }

    @Test
    public void storeNullInsertTest() throws SQLException {
        SQLAddressbookCache cache = new SQLAddressbookCache(dataSource);

        Collection<Recipient> rList = new ArrayList<>();
        rList.add(new Recipient("new@test.com"));

        cache.storeRecipients(rList);

        try (Connection con = dataSource.getConnection()) {
            ResultSet resultSet = con.createStatement().executeQuery("select lastTest from Recipients where id = 'new@test.com'");
            Assert.assertNull(resultSet.getObject("lastTest"));
        }
    }

    @Test
    public void storeDateUpdateTestA() throws SQLException {
        SQLAddressbookCache cache = new SQLAddressbookCache(dataSource);

        Collection<Recipient> rList = new ArrayList<>();
        Date cDate = new Date();
        rList.add(new Recipient("a@test.com", cDate));

        cache.storeRecipients(rList);

        try (Connection con = dataSource.getConnection()) {
            ResultSet resultSet = con.createStatement().executeQuery("select lastTest from Recipients where id = 'a@test.com'");
            Assert.assertEquals(resultSet.getDate("lastTest"), cDate);
        }
    }

    @Test
    public void storeDateUpdateTestB() throws SQLException {
        SQLAddressbookCache cache = new SQLAddressbookCache(dataSource);

        Collection<Recipient> rList = new ArrayList<>();
        Date cDate = new Date();
        rList.add(new Recipient("b@test.com", cDate));

        cache.storeRecipients(rList);

        try (Connection con = dataSource.getConnection()) {
            ResultSet resultSet = con.createStatement().executeQuery("select lastTest from Recipients where id = 'b@test.com'");
            Assert.assertEquals(resultSet.getDate("lastTest"), cDate);
        }
    }

    @Test
    public void storeDateInsertTest() throws SQLException {
        SQLAddressbookCache cache = new SQLAddressbookCache(dataSource);

        Collection<Recipient> rList = new ArrayList<>();
        Date cDate = new Date();
        rList.add(new Recipient("new@test.com", cDate));

        cache.storeRecipients(rList);

        try (Connection con = dataSource.getConnection()) {
            ResultSet resultSet = con.createStatement().executeQuery("select lastTest from Recipients where id = 'new@test.com'");
            Assert.assertEquals(resultSet.getDate("lastTest"), cDate);
        }
    }

    @Test
    public void bulkStoreTest() throws SQLException {
        SQLAddressbookCache cache = new SQLAddressbookCache(dataSource);

        Collection<Recipient> rList = new ArrayList<>();

        rList.add(new Recipient("a@test.com"));

        Date cDate1 = new GregorianCalendar(2020, Calendar.JANUARY, 7).getTime();
        rList.add(new Recipient("b@test.com", cDate1));

        rList.add(new Recipient("new1@test.com"));

        Date cDate2 = new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime();
        rList.add(new Recipient("new2@test.com", cDate2));


        cache.storeRecipients(rList);

        try (Connection con = dataSource.getConnection()) {
            ResultSet resultSet1 = con.createStatement().executeQuery("select lastTest from Recipients where id = 'a@test.com'");
            Assert.assertNull(resultSet1.getObject("lastTest"));

            ResultSet resultSet2 = con.createStatement().executeQuery("select lastTest from Recipients where id = 'b@test.com'");
            Assert.assertEquals(resultSet2.getDate("lastTest"), cDate1);

            ResultSet resultSet3 = con.createStatement().executeQuery("select lastTest from Recipients where id = 'new1@test.com'");
            Assert.assertNull(resultSet3.getObject("lastTest"));

            ResultSet resultSet4 = con.createStatement().executeQuery("select lastTest from Recipients where id = 'new2@test.com'");
            Assert.assertEquals(resultSet4.getDate("lastTest"), cDate2);
        }
    }

    @Test
    public void getRandomRecipientNoneTest() throws SQLException {
        SQLAddressbookCache cache = new SQLAddressbookCache(dataSource);

        List<Recipient> recipients = cache.getRandomRecipients(0, new Date());
        Assert.assertEquals(0, recipients.size());
    }

    @Test
    public void getRandomRecipientNullDateTest() throws SQLException {
        SQLAddressbookCache cache = new SQLAddressbookCache(dataSource);

        List<Recipient> recipients = cache.getRandomRecipients(1, new GregorianCalendar(1990, Calendar.JANUARY, 1).getTime());
        Assert.assertEquals(1, recipients.size());
        Assert.assertEquals("b@test.com", recipients.get(0).getId());
    }

    @Test
    public void getRandomRecipientNullPlusDateTest() throws SQLException {
        SQLAddressbookCache cache = new SQLAddressbookCache(dataSource);

        List<Recipient> recipients = cache.getRandomRecipients(2, new GregorianCalendar(2000, Calendar.JANUARY, 1).getTime());
        Assert.assertEquals(2, recipients.size());

        List<String> idList = recipients.stream().map(Recipient::getId).collect(Collectors.toList());
        Assert.assertTrue(idList.contains("b@test.com"));
        Assert.assertTrue(idList.contains("c@test.com"));
    }

    @Test
    public void getRandomRecipientOversizeTest() throws SQLException {
        SQLAddressbookCache cache = new SQLAddressbookCache(dataSource);

        List<Recipient> recipients = cache.getRandomRecipients(100, new GregorianCalendar(1990, Calendar.JANUARY, 1).getTime());
        Assert.assertEquals(1, recipients.size());
        Assert.assertEquals("b@test.com", recipients.get(0).getId());
    }

    @Test
    public void getRandomRecipientAllDataTest() throws SQLException {
        SQLAddressbookCache cache = new SQLAddressbookCache(dataSource);

        List<Recipient> recipients = cache.getRandomRecipients(3, new GregorianCalendar(2040, Calendar.JANUARY, 1).getTime());
        Assert.assertEquals(3, recipients.size());

        List<String> idList = recipients.stream().map(Recipient::getId).collect(Collectors.toList());
        Assert.assertTrue(idList.contains("a@test.com"));
        Assert.assertTrue(idList.contains("b@test.com"));
        Assert.assertTrue(idList.contains("c@test.com"));
    }

    @Test
    public void completeRecipientTest() throws SQLException {
        SQLAddressbookCache cache = new SQLAddressbookCache(dataSource);

        Recipient recipient = new Recipient("a@test.com");

        Recipient completedRecipient = cache.completeRecipient(recipient);
        Assert.assertEquals(1601510400000L, completedRecipient.getLatestContact().getTime());
    }

    @Test
    public void completeRecipientNullTest() throws SQLException {
        SQLAddressbookCache cache = new SQLAddressbookCache(dataSource);

        Recipient recipient = new Recipient("b@test.com");

        Recipient completedRecipient = cache.completeRecipient(recipient);

        Assert.assertNull(completedRecipient.getLatestContact());
    }

}
