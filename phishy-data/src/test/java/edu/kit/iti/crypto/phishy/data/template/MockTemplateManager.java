package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.eclipse.jgit.transport.resolver.RepositoryResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Collections;

public class MockTemplateManager implements TemplateManager {

    private Collection<TemplateGroup> groups;

    public MockTemplateManager(Collection<TemplateGroup> groups) {
        this.groups = groups;
    }

    @Override
    public TemplateGroup getGroup(String name) throws NotFoundException {

        for (var group : groups) {
            if (name.equals(group.getName())) {
                return group;
            }
        }

        throw new NotFoundException();

    }

    @Override
    public boolean hasGroup(String name) {
        for (var group : groups) {
            if (name.equals(group.getName())) {
                return true;
            }
        }

        return false;
    }

    @Override
    public TemplateGroup createGroup(String name) throws CreationException {

        var group = new MockTemplateGroup(name, Collections.emptyList());
        this.groups.add(group);
        return group;

    }

    @Override
    public void deleteGroup(String name) throws DeletionException {
        throw new UnsupportedOperationException("Not supported on mock");
    }

    @Override
    public Collection<TemplateGroup> listGroups() {
        return groups;
    }

    @Override
    public Template getTemplate(TemplateDescription description) throws NotFoundException {
        return getGroup(description.getGroup()).getTemplate(description);
    }

    @Override
    public boolean hasTemplate(TemplateDescription description) {
        try {
            return getGroup(description.getGroup()).hasTemplate(description);
        } catch (NotFoundException e) {
            return false;
        }
    }

    @Override
    public RepositoryResolver<HttpServletRequest> getGitResolver() {
        throw new UnsupportedOperationException("Not supported on mock");
    }

    @Override
    public void invalidateCache() {
        throw new UnsupportedOperationException("Not supported on mock");
    }
}
