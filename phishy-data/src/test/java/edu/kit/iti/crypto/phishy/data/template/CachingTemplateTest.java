package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.util.List;

import static org.junit.Assert.*;

public class CachingTemplateTest {

    private Template getCachingTemplate() {
        TemplateCache cache = new TemplateCache();
        Template original = new MockTemplate("abc", "a", List.of(
                new MockTemplateFile("mail.eml", "text/plain",
                        200, new ByteArrayInputStream("Hello".getBytes())),
                new MockTemplateFile("b/long.txt", "text/plain",
                        2000000, new ByteArrayInputStream("Very long text".getBytes()))
        ));

        return new CachingTemplate(cache, original);
    }

    @Test
    public void testGetName() {

        var template = getCachingTemplate();

        assertEquals("a", template.getName());

    }

    @Test
    public void testGetFile() throws NotFoundException {

        var template = getCachingTemplate();

        assertEquals("mail.eml", template.getFile("mail.eml").getVirtualPath());
        assertEquals("b/long.txt", template.getFile("b/long.txt").getVirtualPath());

    }

    @Test(expected = NotFoundException.class)
    public void testGetFileNotFound() throws NotFoundException {

        var template = getCachingTemplate();
        template.getFile("none");

    }

    @Test
    public void testHasFile() {

        var template = getCachingTemplate();
        assertTrue(template.hasFile("mail.eml"));
        assertTrue(template.hasFile("b/long.txt"));
        assertFalse(template.hasFile("none"));

    }

    @Test
    public void testGetDescription() {

        var template = getCachingTemplate();
        var descr = template.getDescription();

        assertEquals("abc", descr.getGroup());
        assertEquals("a", descr.getName());
    }

}