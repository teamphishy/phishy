package edu.kit.iti.crypto.phishy.data.addressbook;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import hthurow.tomcatjndi.TomcatJNDI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Collection;

import static org.junit.Assert.*;

public class JNDIAddressbookManagerTest {

    private TomcatJNDI tomcatJNDI = new TomcatJNDI();

    @Before
    public void setupTests() throws URISyntaxException {

        tomcatJNDI.processContextXml(new File(this.getClass().getResource("context.xml").toURI()));
        tomcatJNDI.processWebXml(new File(this.getClass().getResource("web.xml").toURI()));
        tomcatJNDI.start();

    }

    @After
    public void teardownTests() {

        tomcatJNDI.tearDown();

    }

    @Test
    public void testListAddressbooks() {

        AddressbookManager manager = new JNDIAddressbookManager("java:comp/env/test/ab/");
        Collection<Addressbook> books = manager.listAddressbooks();

        assertEquals(2, books.size());

        for (var book : books) {

            assertTrue("a".equals(book.getName()) || "b".equals(book.getName()));

        }

    }

    @Test
    public void testGetAddressbook() throws NotFoundException {

        AddressbookManager manager = new JNDIAddressbookManager("java:comp/env/test/ab/");
        Addressbook abA = manager.getAddressbook(new AddressbookDescription("a"));

        assertEquals("a", abA.getName());

    }

    @Test(expected = NotFoundException.class)
    public void testGetAddressbookNotFound() throws NotFoundException {

        AddressbookManager manager = new JNDIAddressbookManager("java:comp/env/test/ab/");
        manager.getAddressbook(new AddressbookDescription("none"));

    }

    @Test
    public void testHasAddressbook() throws NotFoundException {

        AddressbookManager manager = new JNDIAddressbookManager("java:comp/env/test/ab/");
        assertTrue(manager.hasAddressbook(new AddressbookDescription("a")));
        assertTrue(manager.hasAddressbook(new AddressbookDescription("b")));
        assertFalse(manager.hasAddressbook(new AddressbookDescription("none")));

    }


}