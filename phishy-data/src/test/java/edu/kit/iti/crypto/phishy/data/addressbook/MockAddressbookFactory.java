package edu.kit.iti.crypto.phishy.data.addressbook;

import javax.naming.Context;
import javax.naming.Name;
import javax.naming.spi.ObjectFactory;
import java.util.Hashtable;
import java.util.List;

public class MockAddressbookFactory implements ObjectFactory {
    @Override
    public Object getObjectInstance(Object obj, Name name, Context nameCtx, Hashtable<?, ?> environment)
            throws Exception {

//        Reference reference (Reference) obj;

        return new MockAddressbook(name.get(0), List.of());


    }
}
