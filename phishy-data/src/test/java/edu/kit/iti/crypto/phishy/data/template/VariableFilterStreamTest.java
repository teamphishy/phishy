package edu.kit.iti.crypto.phishy.data.template;

import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class VariableFilterStreamTest {

    private String stringFromStream(InputStream stream) {

        return new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))
                .lines()
                .collect(Collectors.joining("\n"));

    }

    @Test
    public void testUnmodifiedStream() {

        String original = "Hello World!\n How are you?";
        byte[] data = original.getBytes(StandardCharsets.UTF_8);
        InputStream stream = new ByteArrayInputStream(data);

        VariableFilterStream filterStream = new VariableFilterStream(stream, Collections.emptyList());

        String output = stringFromStream(filterStream);

        assertEquals(original, output);

    }

    @Test
    public void testModifiedStream() {

        String original = "Hello ${recipient.name}!\n Your age is ${recipient.age}";
        String expected = "Hello John Doe!\n Your age is 22";
        byte[] data = original.getBytes(StandardCharsets.UTF_8);
        InputStream stream = new ByteArrayInputStream(data);

        Collection<Variable> variables = List.of(
                new Variable("recipient.name", "John Doe"),
                new Variable("recipient.age", "22"));

        VariableFilterStream filterStream = new VariableFilterStream(stream, variables);

        String output = stringFromStream(filterStream);

        assertEquals(expected, output);

    }

    @Test
    public void testInvalidVariableStream() {

        String original = "Hello ${recipient.name}!\n Your age is ${t\u8fd9st}";
        String expected = "Hello ${recipient.name}!\n Your age is ${t\u8fd9st}";
        byte[] data = original.getBytes(StandardCharsets.UTF_8);
        InputStream stream = new ByteArrayInputStream(data);

        Collection<Variable> variables = List.of(
                new Variable("t\u8fd9st", "T\u8fd9st"),
                new Variable("recipient.age", "22"));

        VariableFilterStream filterStream = new VariableFilterStream(stream, variables);

        String output = stringFromStream(filterStream);

        assertEquals(expected, output);

    }

    @Test
    public void testEscaping() {

        String original = "Hello $${recipient.name}! ${recipient.name} $$${test}";
        String expected = "Hello $${recipient.name}! John Doe $$${test}";
        byte[] data = original.getBytes(StandardCharsets.UTF_8);
        InputStream stream = new ByteArrayInputStream(data);

        Collection<Variable> variables = List.of(
                new Variable("recipient.name", "John Doe"));

        VariableFilterStream filterStream = new VariableFilterStream(stream, variables);

        String output = stringFromStream(filterStream);

        assertEquals(expected, output);

    }

    @Test
    public void testUnicodeReplace() {

        String original = "Hello ${recipient.name}!";
        String expected = "Hello \uC7A5\uB3D9\uBBFC!";
        byte[] data = original.getBytes(StandardCharsets.UTF_8);
        InputStream stream = new ByteArrayInputStream(data);

        Collection<Variable> variables = List.of(
                new Variable("recipient.name", "\uC7A5\uB3D9\uBBFC"));

        VariableFilterStream filterStream = new VariableFilterStream(stream, variables);

        String output = stringFromStream(filterStream);

        assertEquals(expected, output);

    }

    @Test
    public void markSupported() {
        String original = "Hello $${recipient.name}! ${recipient.name} $$${test}";
        String expected = "Hello $${recipient.name}! John Doe $$${test}";
        byte[] data = original.getBytes(StandardCharsets.UTF_8);
        InputStream stream = new ByteArrayInputStream(data);

        Collection<Variable> variables = List.of(
                new Variable("recipient.name", "John Doe"));

        VariableFilterStream filterStream = new VariableFilterStream(stream, variables);

        assertFalse(filterStream.markSupported());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void mark() {
        String original = "Hello $${recipient.name}! ${recipient.name} $$${test}";
        String expected = "Hello $${recipient.name}! John Doe $$${test}";
        byte[] data = original.getBytes(StandardCharsets.UTF_8);
        InputStream stream = new ByteArrayInputStream(data);

        Collection<Variable> variables = List.of(
                new Variable("recipient.name", "John Doe"));

        VariableFilterStream filterStream = new VariableFilterStream(stream, variables);

        filterStream.mark(6);
    }

    @Test(expected = IOException.class)
    public void reset() throws IOException {
        String original = "Hello $${recipient.name}! ${recipient.name} $$${test}";
        String expected = "Hello $${recipient.name}! John Doe $$${test}";
        byte[] data = original.getBytes(StandardCharsets.UTF_8);
        InputStream stream = new ByteArrayInputStream(data);

        Collection<Variable> variables = List.of(
                new Variable("recipient.name", "John Doe"));

        VariableFilterStream filterStream = new VariableFilterStream(stream, variables);

        filterStream.reset();
    }

    @Test
    public void skip() throws IOException {
        String original = "Hello $${recipient.name}! ${recipient.name} $$${test}";
        String expected = "llo $${recipient.name}! John Doe $$${test}";
        byte[] data = original.getBytes(StandardCharsets.UTF_8);
        InputStream stream = new ByteArrayInputStream(data);

        Collection<Variable> variables = List.of(
                new Variable("recipient.name", "John Doe"));

        VariableFilterStream filterStream = new VariableFilterStream(stream, variables);

        filterStream.skip(2);
        String output = stringFromStream(filterStream);

        assertEquals(expected, output);
    }

    @Test
    public void shortInvalidVar() throws IOException {
        String original = "Hello $}!\nYou are ${} years old.";
        String expected = "Hello $}!\nYou are ${} years old.";
        byte[] data = original.getBytes(StandardCharsets.UTF_8);
        InputStream stream = new ByteArrayInputStream(data);

        Collection<Variable> variables = List.of(
                new Variable("recipient.name", "John Doe"),
                new Variable("recipient.age", "22"));

        VariableFilterStream filterStream = new VariableFilterStream(stream, variables);

        String output = stringFromStream(filterStream);

        assertEquals(expected, output);
    }

    @Test
    public void shortVar() throws IOException {
        String original = "Hello ${a}!";
        String expected = "Hello John Doe!";
        byte[] data = original.getBytes(StandardCharsets.UTF_8);
        InputStream stream = new ByteArrayInputStream(data);

        Collection<Variable> variables = List.of(
                new Variable("a", "John Doe"),
                new Variable("recipient.age", "22"));

        VariableFilterStream filterStream = new VariableFilterStream(stream, variables);

        String output = stringFromStream(filterStream);

        assertEquals(expected, output);
    }

    @Test(expected = NullPointerException.class)
    public void readNullTest() throws IOException {
        String original = "Hello ${a}!";
        byte[] data = original.getBytes(StandardCharsets.UTF_8);
        InputStream stream = new ByteArrayInputStream(data);

        Collection<Variable> variables = List.of(
                new Variable("a", "John Doe"),
                new Variable("recipient.age", "22"));

        VariableFilterStream filterStream = new VariableFilterStream(stream, variables);

        filterStream.read(null, 0, 10);
    }

    @Test
    public void readLengthZeroTest() throws IOException {
        String original = "Hello ${a}!";
        byte[] data = original.getBytes(StandardCharsets.UTF_8);
        InputStream stream = new ByteArrayInputStream(data);

        Collection<Variable> variables = List.of(
                new Variable("a", "John Doe"),
                new Variable("recipient.age", "22"));

        VariableFilterStream filterStream = new VariableFilterStream(stream, variables);

        Assert.assertEquals(filterStream.read(new byte[10], 0, 0), 0);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void readNegativeOffsetTest() throws IOException {
        String original = "Hello ${a}!";
        byte[] data = original.getBytes(StandardCharsets.UTF_8);
        InputStream stream = new ByteArrayInputStream(data);

        Collection<Variable> variables = List.of(
                new Variable("a", "John Doe"),
                new Variable("recipient.age", "22"));

        VariableFilterStream filterStream = new VariableFilterStream(stream, variables);

        filterStream.read(new byte[10], -10, 10);
    }
}