package edu.kit.iti.crypto.phishy.data.template;

import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.Repository;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

public class GitTemplateFileTest {

    private Path getShadowFile(String path) {
        try {
            return Paths.get(this.getClass().getResource("root").toURI()).resolve(path);
        } catch (URISyntaxException e) {
            throw new IllegalStateException("Cannot get Shadow file");
        }
    }

    private Path getRootPath() {
        try {
            return Paths.get(this.getClass().getResource("gitroot").toURI());
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Root Path invalid");
        }
    }

    private Path getGroupPath(String name) {
        return getRootPath().resolve(name);
    }

    private Path getTemplatePath(String group, String template) {
        return getGroupPath(group).resolve(template);
    }

    private TemplateFile getTemplateFile(String group, String template, String file) throws IOException {
        Repository repo = new FileRepository(getGroupPath(group).toFile());
        return new GitTemplateFile(repo, template, file);
    }


    @Test
    public void testCreation() throws IOException {

        Repository repo = new FileRepository(getGroupPath("abc").toFile());
        TemplateFile file = new GitTemplateFile(repo, "a", "mail.eml");

        assertEquals("mail.eml", file.getVirtualPath());

    }

    @Test
    public void testCreationDeep() throws IOException {

        Repository repo = new FileRepository(getGroupPath("abc").toFile());
        TemplateFile file = new GitTemplateFile(repo, "a", "assets/styles.css");

        assertEquals("assets/styles.css", file.getVirtualPath());

    }


    @Test
    public void testMimeType() throws IOException {

        TemplateFile fileMail = getTemplateFile("abc", "a", "mail.eml");
        assertEquals("message/rfc822", fileMail.getMimeType());

        TemplateFile fileCSS = getTemplateFile("abc", "b", "assets/styles.css");
        assertEquals("text/css", fileCSS.getMimeType());

        TemplateFile fileHtml = getTemplateFile("abc", "b", "index.html");
        assertEquals("text/html", fileHtml.getMimeType());

    }

    @Test
    public void testGetStream() throws IOException {

        TemplateFile fileMail = getTemplateFile("abc", "a", "mail.eml");
        Path mailPath = getShadowFile("abc/a/mail.eml");

        InputStream templateStream = fileMail.getStream();
        InputStream fileStream = Files.newInputStream(mailPath);

        for (int i = 0; i < Files.size(mailPath); ++i) {
            assertEquals(fileStream.read(), templateStream.read());
        }

    }

    @Test
    public void getEstimatedSize() throws IOException {
        Repository repo = new FileRepository(getGroupPath("abc").toFile());
        TemplateFile file = new GitTemplateFile(repo, "a", "mail.eml");

        assertEquals(618, file.getEstimatedSize());
    }
}