package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.junit.Test;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;

import static org.junit.Assert.*;

public class FSTemplateGroupTest {

    private Path getRootPath() {
        try {
            return Paths.get(this.getClass().getResource("root").toURI());
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Root Path invalid");
        }
    }

    private Path getGroupPath(String name) {
        return getRootPath().resolve(name);
    }

    @Test
    public void testGetName() {

        TemplateGroup groupABC = new FSTemplateGroup(getGroupPath("abc"));
        assertEquals("abc", groupABC.getName());

        TemplateGroup groupXYZ = new FSTemplateGroup(getGroupPath("xyz"));
        assertEquals("xyz", groupXYZ.getName());

    }

    @Test(expected = Exception.class)
    public void testInvalidGroup() {

        new FSTemplateGroup(getGroupPath("none"));

    }

    @Test
    public void testGetTemplate() throws NotFoundException {

        TemplateGroup group = new FSTemplateGroup(getGroupPath("abc"));
        Template a = group.getTemplate(new TemplateDescription("abc", "a"));

        assertEquals("a", a.getName());

    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetTemplateWrongGroup() throws NotFoundException {

        TemplateGroup group = new FSTemplateGroup(getGroupPath("abc"));
        group.getTemplate(new TemplateDescription("xyz", "a"));

    }

    @Test(expected = NotFoundException.class)
    public void testGetTemplateNotFound() throws NotFoundException {

        TemplateGroup group = new FSTemplateGroup(getGroupPath("abc"));
        group.getTemplate(new TemplateDescription("abc", "none"));

    }

    @Test
    public void testHasTemplate() {

        TemplateGroup groupABC = new FSTemplateGroup(getGroupPath("abc"));
        assertTrue(groupABC.hasTemplate(new TemplateDescription("abc", "a")));
        assertFalse(groupABC.hasTemplate(new TemplateDescription("abc", "x")));
        assertFalse(groupABC.hasTemplate(new TemplateDescription("abc", "none")));

    }

    @Test(expected = IllegalArgumentException.class)
    public void testHasTemplateWrongGroup() {

        TemplateGroup groupABC = new FSTemplateGroup(getGroupPath("abc"));
        assertFalse(groupABC.hasTemplate(new TemplateDescription("xyz", "a")));

    }

    @Test
    public void testListTemplates() {

        TemplateGroup groupABC = new FSTemplateGroup(getGroupPath("abc"));

        Collection<Template> templates = groupABC.listTemplates();
        assertEquals(2, templates.size());

        for (Template t : templates) {
            assertTrue("a".equals(t.getName()) || "b".equals(t.getName()));
        }

    }

}