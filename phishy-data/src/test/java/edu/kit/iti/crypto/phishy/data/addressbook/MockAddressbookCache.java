package edu.kit.iti.crypto.phishy.data.addressbook;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class MockAddressbookCache implements AddressbookCache {

    ArrayList<Recipient> recipients = new ArrayList<>();

    @Override
    public void storeRecipients(Collection<Recipient> recipients) {

        for (var recipient : recipients) {

            int idx = hasRecipient(recipient.getId());
            if (idx == -1) { // add recipient
                this.recipients.add(recipient);
            } else {
                this.recipients.set(idx, recipient);
            }

        }

    }

    private int hasRecipient(String id) {

        for (int i = 0; i < recipients.size(); ++i) {
            if (recipients.get(i).getId().equals(id)) {
                return i;
            }
        }

        return -1;

    }

    @Override
    public List<Recipient> getRandomRecipients(int num, Date notTestedSince) {

        // This is definitely not random; but its a mock
        List<Recipient> list = new ArrayList<>();

        for (int i = 0; i < Math.min(num, recipients.size()); ++i) {
            list.add(recipients.get(i));
        }

        return list;

    }

    @Override
    public Recipient completeRecipient(Recipient recipient) {

        int idx = hasRecipient(recipient.getId());
        if (idx == -1) {
            return recipient;
        } else {
            return recipients.get(idx);
        }

    }
}
