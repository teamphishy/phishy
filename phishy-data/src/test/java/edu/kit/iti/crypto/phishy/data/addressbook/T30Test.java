package edu.kit.iti.crypto.phishy.data.addressbook;

import org.junit.Before;
import org.junit.Test;

import javax.sql.DataSource;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/* T30 Testfall
 *
 * Voraussetzungen:
 * CSV-Adressbuch mit 100 Einträgen.
 * Keiner der Empfänger wurde bisher angeschrieben.
 *
 * Testfall:
 * Aus dem Adressbuch werden 20 Empfänger ausgewählt.
 * Dabei enthält die erzeugte Liste 20 verschiedene Empfänger.
 * Es gibt kein offensichtliches Muster, nach dem die Empfänger
 * ausgewählt wurden. Dies wird durch manuelle Revision der Liste geprüft.
 * Bei einem größeren Verzeichnis wird dies durch Code-Review gewährleistet.
 *
 * Die Beispielnamen stammen aus einer Veröffentlichung der Stadt Bremen.
 *
 * Die Zufälligkeit bei Verwendung des SQLAddressbookCache wurde manuell überprüft.
 */
public class T30Test {

    private DataSource dataSource = null;

    @Before
    public void setUp() throws Exception {
        dataSource = new MockDataSource();
        Connection con = dataSource.getConnection();

        con.createStatement().executeUpdate("drop table if exists Recipients");
        con.createStatement().executeUpdate("create table Recipients (id int not null, lastTest date default null, primary key(id))");

    }


    @Test
    public void testT30() throws URISyntaxException {

        final int RECIPIENT_NUM = 20;

        Path csvFile = Paths.get(this.getClass().getResource("t30.csv").toURI());
        AddressbookCache cache = new MockAddressbookCache();
        Addressbook addressbook = new CSVAddressbook("addressbook", cache, csvFile);

        addressbook.updateCache();

        List<Recipient> recipients = addressbook.getRandomRecipients(RECIPIENT_NUM, new Date());

        assertEquals(RECIPIENT_NUM, recipients.size());

        // Check for uniqueness
        Set<Recipient> recipientSet = new HashSet<>(recipients);
        assertEquals(RECIPIENT_NUM, recipientSet.size());


    }

    @Test
    public void testT30Sqlite() throws URISyntaxException {

        final int RECIPIENT_NUM = 20;

        Path csvFile = Paths.get(this.getClass().getResource("t30.csv").toURI());
        AddressbookCache cache = new SQLAddressbookCache(dataSource);
        Addressbook addressbook = new CSVAddressbook("addressbook", cache, csvFile);

        addressbook.updateCache();

        List<Recipient> recipients = addressbook.getRandomRecipients(RECIPIENT_NUM, new Date());

        assertEquals(RECIPIENT_NUM, recipients.size());

        // Check for uniqueness
        Set<Recipient> recipientSet = new HashSet<>(recipients);
        assertEquals(RECIPIENT_NUM, recipientSet.size());


    }

}
