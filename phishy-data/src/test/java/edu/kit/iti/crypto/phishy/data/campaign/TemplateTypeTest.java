package edu.kit.iti.crypto.phishy.data.campaign;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TemplateTypeTest {

    private TemplateType templateType1 = TemplateType.INFO_MAIL;
    private TemplateType templateType2 = TemplateType.INFO_WEBSITE;
    private TemplateType templateType3 = TemplateType.PHISH_MAIL;
    private TemplateType templateType4 = TemplateType.PHISH_WEBSITE;


    @Before
    public void Setup() {
        templateType1 = TemplateType.INFO_MAIL;
        templateType2 = TemplateType.INFO_WEBSITE;
        templateType3 = TemplateType.PHISH_MAIL;
        templateType4 = TemplateType.PHISH_WEBSITE;
    }

    @Test
    public void toInt() {
        assertEquals(1, templateType1.toInt());
        assertEquals(2, templateType2.toInt());
        assertEquals(3, templateType3.toInt());
        assertEquals(4, templateType4.toInt());
    }

    @Test
    public void fromInt() {
        assertEquals(TemplateType.INFO_MAIL, TemplateType.fromInt(0));
        assertEquals(TemplateType.INFO_WEBSITE, TemplateType.fromInt(1));
        assertEquals(TemplateType.PHISH_MAIL, TemplateType.fromInt(2));
        assertEquals(TemplateType.PHISH_WEBSITE, TemplateType.fromInt(3));
        try {
            TemplateType templateType = TemplateType.fromInt(5);
        } catch (IllegalArgumentException e) {
            assertEquals("invalid value", e.getMessage());
        }

    }

    @Test
    public void toString1() {
        assertEquals("infoMail", templateType1.toString());
        assertEquals("infoWebsite", templateType2.toString());
        assertEquals("phishMail", templateType3.toString());
        assertEquals("phishWebsite", templateType4.toString());
    }

    @Test
    public void fromString() {
        assertEquals(TemplateType.INFO_MAIL, TemplateType.fromString("infomail"));
        assertEquals(TemplateType.INFO_WEBSITE, TemplateType.fromString("infowebsite"));
        assertEquals(TemplateType.PHISH_MAIL, TemplateType.fromString("phishmail"));
        assertEquals(TemplateType.PHISH_WEBSITE, TemplateType.fromString("phishywebsite"));
        try {
            TemplateType templateType = TemplateType.fromString("error");
        } catch (IllegalArgumentException e) {
            assertEquals("invalid value", e.getMessage());
        }

    }
}