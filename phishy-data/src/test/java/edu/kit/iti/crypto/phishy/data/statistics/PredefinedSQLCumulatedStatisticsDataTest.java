package edu.kit.iti.crypto.phishy.data.statistics;

import org.junit.Assert;
import org.junit.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class PredefinedSQLCumulatedStatisticsDataTest {

    private DataSource setupDatabase() throws SQLException {
        MockDatasourceSQL dataSource = new MockDatasourceSQL();
        Connection con = dataSource.getConnection();
        con.createStatement().executeUpdate("DROP TABLE IF Exists Campaign");
        con.createStatement().executeUpdate("DROP TABLE IF Exists Statistics");

        // create Statistics table with some test values
        con.createStatement().executeUpdate("create table Campaign (id text not null, recipientSetSize int default 100 not null, status int default 0 not null, mailLifetime int default 14 not null, primary key(id));\n");
        con.createStatement().executeUpdate("create table Statistics (campaign text not null, sentMails int default 0 not null, reportedMails int default 0 not null, phishedMails int default 0 not null, primary key(campaign), foreign key(campaign) references campaign(id));\n");
        con.createStatement().executeUpdate("insert into Statistics (campaign, sentMails, reportedMails, phishedMails) values ('test', 25, 10, 5)");
        con.createStatement().executeUpdate("insert into Campaign (id) values ('test')");
        con.createStatement().executeUpdate("insert into Statistics (campaign, sentMails, reportedMails, phishedMails) values ('otherTest', 7, 2, 3)");
        con.createStatement().executeUpdate("insert into Campaign (id) values ('otherTest')");

        return dataSource;
    }

    @Test
    public void testGetData() {
        CumulatedStatisticsData data = new PredefinedSQLCumulatedStatisticsData(25, 10, 5);

        Assert.assertEquals(data.getSentMails(), 25);
        Assert.assertEquals(data.getReportedMails(), 10);
        Assert.assertEquals(data.getPhishedMails(), 5);
    }

    @Test
    public void testGet() throws SQLException {
        CumulatedStatisticsData data = PredefinedSQLCumulatedStatisticsData.get(setupDatabase());

        Assert.assertEquals(data.getSentMails(), 25 + 7);
        Assert.assertEquals(data.getReportedMails(), 10 + 2);
        Assert.assertEquals(data.getPhishedMails(), 5 + 3);
    }
}
