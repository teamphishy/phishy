package edu.kit.iti.crypto.phishy.data;

import javax.naming.Context;
import javax.naming.Name;
import javax.naming.RefAddr;
import javax.naming.Reference;
import javax.naming.spi.ObjectFactory;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.Hashtable;

public class MockDataSourceFactory implements ObjectFactory {
    @Override
    public Object getObjectInstance(Object obj, Name name, Context nameCtx, Hashtable<?, ?> environment)
            throws Exception {

        Reference reference = (Reference) obj;
        Enumeration<RefAddr> references = reference.getAll();

        String path = "";
        while (references.hasMoreElements()) {

            RefAddr address = references.nextElement();
            String type = address.getType();
            String content = (String) address.getContent();


            if ("path".equals(type)) {
                path = content;
            }
        }

        return new MockDataSource(Paths.get(this.getClass().getResource(path).toURI()));
    }
}
