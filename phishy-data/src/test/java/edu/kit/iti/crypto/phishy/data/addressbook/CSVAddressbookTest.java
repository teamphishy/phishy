package edu.kit.iti.crypto.phishy.data.addressbook;

import org.junit.Ignore;
import org.junit.Test;

import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class CSVAddressbookTest {

    private Addressbook createAddressbook() throws URISyntaxException {
        return new CSVAddressbook("a", new MockAddressbookCache(),
                Paths.get(this.getClass().getResource("a.csv").toURI()));
    }


    @Test
    public void testGetName() throws URISyntaxException {

        Addressbook ab = createAddressbook();

        assertEquals("a", ab.getName());

    }

    @Test
    public void testGetRandomRecipients() throws URISyntaxException, ParseException {

        Addressbook ab = createAddressbook();

        ab.updateCache();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        for (int i = 0; i < 10; ++i) {

            var rcpts = ab.getRandomRecipients(i, dateFormat.parse("1990-01-01"));

            assertTrue(rcpts.size() <= Math.min(i, 5));

        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetRandomRecipientsNegative() throws URISyntaxException, ParseException {

        Addressbook ab = createAddressbook();

        ab.getRandomRecipients(-42, new Date());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRecordTestNull() throws URISyntaxException, ParseException {

        Addressbook ab = createAddressbook();

        ab.recordTest(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void tesCompleteRecipientNull() throws URISyntaxException, ParseException {

        Addressbook ab = createAddressbook();

        ab.completeRecipient(null);
    }

    @Test
    public void testCompleteRecipient() throws URISyntaxException {

        Addressbook ab = createAddressbook();
        ab.updateCache();

        Recipient rcpt = ab.completeRecipient(new Recipient("1"));
        assertEquals("Doe", rcpt.getSurname());
        assertEquals("John", rcpt.getGivenName());
        assertEquals("John Doe", rcpt.getName());
        assertEquals("john.doe@example.org", rcpt.getMail());

    }

    @Test
    @Ignore
    public void testRecordTest() throws URISyntaxException {

        Addressbook ab = createAddressbook();
        ab.updateCache();

        Recipient johnDoe = ab.completeRecipient(new Recipient("1"));
        assertNull(johnDoe.getLatestContact());

        ab.recordTest(List.of(johnDoe));
        johnDoe = ab.completeRecipient(new Recipient("id"));
        assertNotNull(johnDoe.getLatestContact());

    }

}