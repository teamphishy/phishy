package edu.kit.iti.crypto.phishy.data.template;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CachingTemplateFileTest {

    private TemplateFile getTemplateFile(TemplateCache cache) {
        TemplateFile original = new MockTemplateFile("a.txt", "text/plain",
                200, new ByteArrayInputStream("Hello".getBytes()));
        return new CachingTemplateFile(cache,
                new TemplateDescription("abc", "a"),
                original);
    }

    @Test
    public void testGetVirtualPath() {

        TemplateCache cache = new TemplateCache();
        TemplateFile file = getTemplateFile(cache);

        assertEquals("a.txt", file.getVirtualPath());

    }

    @Test
    public void testGetMimeType() {

        TemplateCache cache = new TemplateCache();
        TemplateFile file = getTemplateFile(cache);

        assertEquals("text/plain", file.getMimeType());

    }

    @Test
    public void testGetEstimatedSize() {

        TemplateCache cache = new TemplateCache();
        TemplateFile file = getTemplateFile(cache);

        assertEquals(200, file.getEstimatedSize());

    }

    @Test
    public void testGetStream() throws IOException {

        // 0: PUT, 1: FETCH
        final boolean[] cacheAccessed = {false, false};
        TemplateCache cache = new TemplateCache() {
            @Override
            public synchronized void putItem(TemplateDescription description, String path, byte[] data) {
                super.putItem(description, path, data);
                cacheAccessed[0] = true;
            }

            @Override
            public synchronized byte[] fetchItem(TemplateDescription description, String path) {
                byte[] item = super.fetchItem(description, path);
                if (item != null) {
                    cacheAccessed[1] = true;
                }
                return item;
            }
        };

        String source = "Hello World!";

        TemplateFile originalTemplate = new MockTemplateFile("a", "text/plain",
                source.length(), new ByteArrayInputStream(source.getBytes()));
        TemplateFile cachedFile = new CachingTemplateFile(cache, new TemplateDescription("abc", "a"),
                originalTemplate);

        /* First round possibly not from cache */

        InputStream originalStream = new ByteArrayInputStream(source.getBytes());
        InputStream cachedStream = cachedFile.getStream();

        for (int i = 0; i < source.length(); ++i) {
            assertEquals(originalStream.read(), cachedStream.read());
        }

        assertTrue(cacheAccessed[0]); // cache was PUT


        /* This time from cache */

        InputStream originalStream2 = new ByteArrayInputStream(source.getBytes());
        InputStream cachedStream2 = cachedFile.getStream(); // now definitely from cache

        for (int i = 0; i < source.length(); ++i) {
            assertEquals(originalStream2.read(), cachedStream2.read());
        }

        assertTrue(cacheAccessed[1]); // cache was FETCHED


    }

}