package edu.kit.iti.crypto.phishy.data.campaign;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class SQLOutstandingTestTest {

    private SQLOutstandingTest test;
    private SQLOutstandingTest testFailing;
    private DataSource dataSource;
    private MockCampaignStatisticsData statistics;

    @Before
    public void setUp() throws Exception {
        dataSource = new MockSQLDatasource();
        try (Connection con = this.dataSource.getConnection()) {
            con.createStatement().executeUpdate("DROP TABLE IF Exists Campaign");
            con.createStatement().executeUpdate("DROP TABLE IF Exists Statistics");
            con.createStatement().executeUpdate("DROP TABLE IF Exists OutstandingTest");

            con.createStatement().executeUpdate("create table OutstandingTest (id text not null, campaign text not null, \"date\" date, status int default 0 not null, loginAttempts int default 0 not null, primary key(id));");
            con.createStatement().executeUpdate("create table Campaign (id text not null, recipientSetSize int default 100 not null, status int default 0 not null, mailLifetime int default 14 not null, mailInterval int default 30 not null, addressbook text default null, primary key(id));");
            con.createStatement().executeUpdate("create table Statistics (campaign text not null, sentMails int default 0 not null, reportedMails int default 0 not null, phishedMails int default 0 not null, primary key(campaign), foreign key(campaign) references campaign(id))");

            con.createStatement().executeUpdate("insert into Campaign (id) values ('c1')");
            statistics = new MockCampaignStatisticsData("c1", dataSource);

            test = new SQLOutstandingTest("test123", new CampaignDescription("c1"), statistics, dataSource);
            testFailing = new SQLOutstandingTest("test13", new CampaignDescription("c1"), statistics, dataSource);

            con.createStatement().executeUpdate("UPDATE OutstandingTest SET id='test123', campaign='c1', date='1594626452000', status=1, loginAttempts=2 WHERE id='test123'");
            con.createStatement().executeUpdate("UPDATE OutstandingTest SET id='test456', campaign='c2', date='1594626452000', status=2, loginAttempts=6 WHERE id='test13'");
        }
    }

    @Test
    public void increaseLoginAttempt() throws NotFoundException, SQLException {
        try (Connection con = this.dataSource.getConnection()) {
            test.increaseLoginAttempt();
            ResultSet set = con.prepareStatement("SELECT loginAttempts FROM OutstandingTest WHERE id='test123'").executeQuery();
            assertEquals(3, set.getInt("loginAttempts"));
        }
    }

    @Test(expected = IllegalStateException.class)
    public void increaseLoginAttemptNotFound() throws IllegalStateException {
        testFailing.increaseLoginAttempt();
    }

    @Test
    public void getId() {
        assertEquals("test123", test.getId());
    }

    @Test
    public void getDate() throws NotFoundException {
        Date date = test.getDate();
        assertEquals(1594626452000L, date.getTime());
    }

    @Test(expected = NotFoundException.class)
    public void getDateNotFound() throws NotFoundException {
        testFailing.getDate();
    }

    @Test
    public void getLoginAttempt() {
        assertEquals(2, test.getLoginAttempt());
    }

    @Test(expected = IllegalStateException.class)
    public void getLoginAttemptNotFound() throws IllegalStateException {
        testFailing.getLoginAttempt();
    }

    @Test
    public void delete() throws SQLException {
        try (Connection con = this.dataSource.getConnection()) {
            test.delete();
            ResultSet set = con.prepareStatement("SELECT loginAttempts FROM OutstandingTest WHERE id='test123'").executeQuery();
            assertFalse(set.next());
        }
    }


    @Test
    public void statIncrease() {
        Assert.assertEquals(2, statistics.getSentMails());
        testFailing = new SQLOutstandingTest("new", new CampaignDescription("c1"), statistics, dataSource);
        Assert.assertEquals(3, statistics.getSentMails());
    }
}