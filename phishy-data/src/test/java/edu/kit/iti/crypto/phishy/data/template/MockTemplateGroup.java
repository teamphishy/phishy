package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import java.util.Collection;

public class MockTemplateGroup implements TemplateGroup {

    private String name;
    private Collection<Template> templates;

    public MockTemplateGroup(String name, Collection<Template> templates) {
        this.name = name;
        this.templates = templates;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Template getTemplate(TemplateDescription description) throws NotFoundException {

        if (!name.equals(description.getGroup())) {
            throw new IllegalArgumentException();
        }

        for (Template t : templates) {
            if (description.getName().equals(t.getName())) {
                return t;
            }
        }

        throw new NotFoundException();

    }

    @Override
    public boolean hasTemplate(TemplateDescription description) {

        if (!name.equals(description.getGroup())) {
            throw new IllegalArgumentException();
        }

        for (Template t : templates) {
            if (description.getName().equals(t.getName())) {
                return true;
            }
        }

        return false;

    }

    @Override
    public Collection<Template> listTemplates() {

        return templates;

    }
}
