package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.eclipse.jgit.errors.RepositoryNotFoundException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.ServiceMayNotContinueException;
import org.eclipse.jgit.transport.resolver.RepositoryResolver;
import org.eclipse.jgit.transport.resolver.ServiceNotAuthorizedException;
import org.eclipse.jgit.transport.resolver.ServiceNotEnabledException;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Comparator;

import static org.junit.Assert.*;

public class FSTemplateManagerTest {

    private Path getRootPath() {
        try {
            return Paths.get(this.getClass().getResource("root").toURI());
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Root Path invalid");
        }
    }

    @Test
    public void testGetGroup() throws NotFoundException {

        Path rootPath = getRootPath();
        TemplateManager manager = new FSTemplateManager(rootPath);
        var groupABC = manager.getGroup("abc");
        assertEquals("abc", groupABC.getName());

        var groupXYZ = manager.getGroup("xyz");
        assertEquals("xyz", groupXYZ.getName());

    }

    @Test
    public void testHasGroup() {

        Path rootPath = getRootPath();
        TemplateManager manager = new FSTemplateManager(rootPath);

        assertTrue(manager.hasGroup("abc"));
        assertTrue(manager.hasGroup("xyz"));
        assertFalse(manager.hasGroup("root"));
        assertFalse(manager.hasGroup(""));

    }

    @Test(expected = NotFoundException.class)
    public void testNotFoundGroup() throws NotFoundException {

        Path rootPath = getRootPath();
        TemplateManager manager = new FSTemplateManager(rootPath);

        manager.getGroup("none");

    }

    @Test(expected = NotFoundException.class)
    public void testEmptyNameGroup() throws NotFoundException {

        Path rootPath = getRootPath();
        TemplateManager manager = new FSTemplateManager(rootPath);

        manager.getGroup("");

    }

    @Test
    public void testListGroups() {

        Path rootPath = getRootPath();
        TemplateManager manager = new FSTemplateManager(rootPath);

        Collection<TemplateGroup> groups = manager.listGroups();

        assertEquals(2, groups.size());

        for (TemplateGroup group : groups) {
            assertTrue("abc".equals(group.getName()) || "xyz".equals(group.getName()));
        }

    }

    @Test
    public void testGetTemplate() throws NotFoundException {

        Path rootPath = getRootPath();
        TemplateManager manager = new FSTemplateManager(rootPath);

        Template a = manager.getTemplate(new TemplateDescription("abc", "a"));
        assertEquals("a", a.getName());

        Template b = manager.getTemplate(new TemplateDescription("abc", "b"));
        assertEquals("b", b.getName());

        Template x = manager.getTemplate(new TemplateDescription("xyz", "x"));
        assertEquals("x", x.getName());

    }

    @Test
    public void testHasTemplate() {

        Path rootPath = getRootPath();
        TemplateManager manager = new FSTemplateManager(rootPath);

        assertTrue(manager.hasTemplate(new TemplateDescription("abc", "a")));
        assertTrue(manager.hasTemplate(new TemplateDescription("abc", "b")));
        assertTrue(manager.hasTemplate(new TemplateDescription("xyz", "x")));
        assertFalse(manager.hasTemplate(new TemplateDescription("abc", "x")));
        assertFalse(manager.hasTemplate(new TemplateDescription("xyz", "a")));
        assertFalse(manager.hasTemplate(new TemplateDescription("abc", "")));
        assertFalse(manager.hasTemplate(new TemplateDescription("", "a")));

    }

    @Test
    public void testCreateGroup() throws CreationException, IOException {

        Path rootPath = getRootPath();
        TemplateManager manager = new FSTemplateManager(rootPath);

        manager.createGroup("test");

        assertTrue(Files.isDirectory(rootPath.resolve("test")));

    }

    @Test
    public void testDeleteGroup() throws IOException, DeletionException {

        Path rootPath = getRootPath();
        TemplateManager manager = new FSTemplateManager(rootPath);

        Files.createDirectory(rootPath.resolve("test"));

        manager.deleteGroup("test");

        assertFalse(Files.isDirectory(rootPath.resolve("test")));
    }

    @Test(expected = RepositoryNotFoundException.class)
    public void testRepositoryResolver()
            throws CreationException, ServiceNotAuthorizedException, ServiceNotEnabledException,
            ServiceMayNotContinueException, RepositoryNotFoundException {

        Path rootPath = getRootPath();
        TemplateManager manager = new FSTemplateManager(rootPath);

        manager.createGroup("test");

        RepositoryResolver<HttpServletRequest> resolver = manager.getGitResolver();
        Repository repo = resolver.open(null, "test");
    }

    @Test
    public void invalidateTest() {
        Path rootPath = getRootPath();
        TemplateManager manager = new FSTemplateManager(rootPath);
        //should do nothing (e.g. not crash)
        manager.invalidateCache();
    }

    @Before
    public void removeTestGroup() throws IOException {

        Path rootPath = getRootPath();

        if (Files.isDirectory(rootPath.resolve("test"))) {
            Files.walk(rootPath.resolve("test"))
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        }
    }
}