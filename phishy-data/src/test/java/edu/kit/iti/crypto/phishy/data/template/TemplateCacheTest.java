package edu.kit.iti.crypto.phishy.data.template;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNull;

public class TemplateCacheTest {

    @Test
    public void testPutAndFetch() {

        var cache = new TemplateCache();
        cache.putItem(new TemplateDescription("abc", "a"), "a.txt", "Hello".getBytes());
        cache.putItem(new TemplateDescription("abc", "b"), "a.txt", "World".getBytes());

        var itemWorld = cache.fetchItem(new TemplateDescription("abc", "b"), "a.txt");
        var itemHello = cache.fetchItem(new TemplateDescription("abc", "a"), "a.txt");

        assertArrayEquals("Hello".getBytes(), itemHello);
        assertArrayEquals("World".getBytes(), itemWorld);

    }

    @Test
    public void testOverride() {

        var cache = new TemplateCache();
        var descr = new TemplateDescription("abc", "a");
        cache.putItem(descr, "txt", "Hello".getBytes());
        cache.putItem(descr, "txt", "World".getBytes());

        var item = cache.fetchItem(descr, "txt");
        assertArrayEquals("World".getBytes(), item);

    }

    @Test
    public void testNotFound() {

        var cache = new TemplateCache();
        assertNull(cache.fetchItem(new TemplateDescription("abc", "a"), "txt"));

    }

    @Test
    public void testInvalidate() {

        var cache = new TemplateCache();
        var descr = new TemplateDescription("abc", "a");

        cache.putItem(descr, "txt", "Hello".getBytes());
        assertArrayEquals("Hello".getBytes(), cache.fetchItem(descr, "txt"));

        cache.invalidate();

        assertNull(cache.fetchItem(descr, "txt"));


    }

}