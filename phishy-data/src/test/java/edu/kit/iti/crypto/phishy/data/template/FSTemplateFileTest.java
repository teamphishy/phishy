package edu.kit.iti.crypto.phishy.data.template;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

public class FSTemplateFileTest {

    private Path getRootPath() {
        try {
            return Paths.get(this.getClass().getResource("root").toURI());
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Root Path invalid");
        }
    }

    private Path getGroupPath(String name) {
        return getRootPath().resolve(name);
    }

    private Path getTemplatePath(String group, String template) {
        return getGroupPath(group).resolve(template);
    }

    private TemplateFile getTemplateFile(String group, String template, String file) {
        Path templatePath = getTemplatePath(group, template);
        return new FSTemplateFile(templatePath, templatePath.resolve(file));
    }


    @Test
    public void testCreation() {

        Path templatePath = getTemplatePath("abc", "a");
        TemplateFile file = new FSTemplateFile(templatePath, templatePath.resolve("mail.eml"));

        assertEquals("mail.eml", file.getVirtualPath());

    }

    @Test
    public void testCreationDeep() {

        Path templatePath = getTemplatePath("abc", "b");
        TemplateFile file = new FSTemplateFile(templatePath, templatePath.resolve("assets/styles.css"));

        assertEquals("assets/styles.css", file.getVirtualPath());

    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreationInvalid() {

        Path templatePath = getTemplatePath("abc", "a");
        new FSTemplateFile(templatePath, templatePath.resolve("none.ex"));

    }

    @Test
    public void testMimeType() {

        TemplateFile fileMail = getTemplateFile("abc", "a", "mail.eml");
        assertEquals("message/rfc822", fileMail.getMimeType());

        TemplateFile fileCSS = getTemplateFile("abc", "b", "assets/styles.css");
        assertEquals("text/css", fileCSS.getMimeType());

        TemplateFile fileHtml = getTemplateFile("abc", "b", "index.html");
        assertEquals("text/html", fileHtml.getMimeType());

    }

    @Test
    public void testGetStream() throws IOException {

        TemplateFile fileMail = getTemplateFile("abc", "a", "mail.eml");
        Path mailPath = getTemplatePath("abc", "a").resolve("mail.eml");

        InputStream templateStream = fileMail.getStream();
        InputStream fileStream = Files.newInputStream(mailPath);

        for (int i = 0; i < Files.size(mailPath); ++i) {
            assertEquals(fileStream.read(), templateStream.read());
        }

    }


    @Test
    public void getEstimatedSize() {
        Path templatePath = getTemplatePath("abc", "b");
        TemplateFile file = new FSTemplateFile(templatePath, templatePath.resolve("assets/styles.css"));

        assertEquals(28, file.getEstimatedSize());
    }
}