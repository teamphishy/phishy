package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import java.util.Collection;

public class MockTemplate implements Template {

    private String groupName;
    private String name;
    private Collection<TemplateFile> files;

    public MockTemplate(String groupName, String name,
                        Collection<TemplateFile> files) {
        this.groupName = groupName;
        this.name = name;
        this.files = files;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public TemplateFile getFile(String path) throws NotFoundException {

        for (TemplateFile file : files) {
            if (path.equals(file.getVirtualPath())) {
                return file;
            }
        }

        throw new NotFoundException();

    }

    @Override
    public boolean hasFile(String path) {

        for (TemplateFile file : files) {
            if (path.equals(file.getVirtualPath())) {
                return true;
            }
        }

        return false;

    }

    @Override
    public TemplateDescription getDescription() {

        return new TemplateDescription(groupName, name);

    }
}
