package edu.kit.iti.crypto.phishy.data.campaign;

import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookDescription;
import edu.kit.iti.crypto.phishy.data.addressbook.Recipient;
import edu.kit.iti.crypto.phishy.data.statistics.CampaignStatisticsData;
import edu.kit.iti.crypto.phishy.data.template.TemplateDescription;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import java.util.*;

public class MockCampaign extends Campaign {

    private final String name;
    private int recipientSetSize;
    private int mailLifetime;
    private CampaignStatus status;
    private int mailInterval;

    private ArrayList<MockOutstandingTest> outstandingTests = new ArrayList<>();
    private Map<TemplateType, TemplateDescription> templates = new HashMap<>();

    public MockCampaign(String name) {
        this.name = name;
        this.status = CampaignStatus.ACTIVE;
    }

    public MockCampaign(String name, int recipientSetSize, int mailLifetime, int mailInterval) {
        this.name = name;
        this.recipientSetSize = recipientSetSize;
        this.mailLifetime = mailLifetime;
        this.mailInterval = mailInterval;
        this.status = CampaignStatus.ACTIVE;
    }

    @Override
    public void linkTemplate(TemplateType type, TemplateDescription description) {
        templates.put(type, description);
    }

    @Override
    public void unlinkTemplate(TemplateType type) {
        templates.remove(type);
    }

    @Override
    public TemplateDescription getTemplate(TemplateType type) throws NotFoundException {
        if (!templates.containsKey(type)) {
            throw new NotFoundException();
        }
        return templates.get(type);
    }

    @Override
    public boolean hasTemplate(TemplateType type) {
        return templates.containsKey(type);
    }

    @Override
    public CampaignStatisticsData getStatistics() {
        throw new UnsupportedOperationException("Not implemented!");
    }

    @Override
    public OutstandingTest getOutstandingTest(String id) throws NotFoundException {
        for (OutstandingTest test : outstandingTests) {
            if (test.getId().equals(id)) {
                return test;
            }
        }
        throw new NotFoundException();
    }

    @Override
    public boolean hasOutstandingTest(String id) {
        for (OutstandingTest test : outstandingTests) {
            if (test.getId().equals(id)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public AddressbookDescription getAddressbook() throws NotFoundException {
        throw new UnsupportedOperationException("Not implemented!");
    }

    @Override
    public void setAddressbook(AddressbookDescription description) {
        throw new UnsupportedOperationException("Not implemented!");
    }

    @Override
    public void recordTests(Collection<TestDescription> tests) {
        for (TestDescription t : tests) {
            outstandingTests.add(new MockOutstandingTest(t.getTestID()));
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getRecipientSetSize() {
        return recipientSetSize;
    }

    @Override
    public void setRecipientSetSize(int recipientSetSize) {
        if (recipientSetSize < 1) {
            throw new IllegalArgumentException("RecipientSetSize must be one or higher");
        }
        this.recipientSetSize = recipientSetSize;
    }

    @Override
    public CampaignStatus getStatus() {
        return status;
    }

    @Override
    public void setStatus(CampaignStatus status) {
        if (status == null) {
            throw new IllegalArgumentException("CampaignStatus cannot be null");
        }
        this.status = status;
    }

    @Override
    public int getMailLifetime() {
        return mailLifetime;
    }

    @Override
    public void setMailLifetime(int mailLifetime) {
        if (mailLifetime < 1) {
            throw new IllegalArgumentException("mailLifetime must be greater than zero");
        }
        this.mailLifetime = mailLifetime;
    }

    @Override
    public int getMailInterval() {
        return mailInterval;
    }

    @Override
    public void setMailInterval(int mailInterval) {
        if (mailInterval < 1) {
            throw new IllegalArgumentException("mailInterval must be greater than zero");
        }
        this.mailInterval = mailInterval;
    }

    @Override
    public List<Recipient> deleteTests(List<Recipient> recipients) {
        throw new UnsupportedOperationException("Not implemented!");
    }
}
