package edu.kit.iti.crypto.phishy.data;

import edu.kit.iti.crypto.phishy.data.addressbook.*;
import edu.kit.iti.crypto.phishy.data.campaign.Campaign;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignDescription;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignManager;
import edu.kit.iti.crypto.phishy.data.statistics.CampaignStatisticsData;
import edu.kit.iti.crypto.phishy.data.statistics.CumulatedStatisticsData;
import edu.kit.iti.crypto.phishy.data.template.Template;
import edu.kit.iti.crypto.phishy.data.template.TemplateDescription;
import edu.kit.iti.crypto.phishy.data.template.TemplateGroup;
import edu.kit.iti.crypto.phishy.data.template.TemplateManager;
import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.eclipse.jgit.transport.resolver.RepositoryResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class MockManager implements AddressbookManager, TemplateManager, CampaignManager, AddressbookCache {
    @Override
    public Collection<Addressbook> listAddressbooks() {
        return null;
    }

    @Override
    public boolean hasAddressbook(AddressbookDescription description) {
        return false;
    }

    @Override
    public Addressbook getAddressbook(AddressbookDescription description) throws NotFoundException {
        return null;
    }

    @Override
    public Campaign getCampaign(CampaignDescription name) throws NotFoundException {
        return null;
    }

    @Override
    public boolean hasCampaign(CampaignDescription name) {
        return false;
    }

    @Override
    public Collection<Campaign> listCampaigns() {
        return null;
    }

    @Override
    public Campaign createCampaign(String name) throws CreationException {
        return null;
    }

    @Override
    public Campaign createCampaign(String name, int recipientSetSize, int mailLifetime, int mailInterval) throws CreationException {
        return null;
    }

    @Override
    public void deleteCampaign(CampaignDescription name) throws DeletionException {

    }

    @Override
    public CumulatedStatisticsData getCumulatedStatistics() {
        return null;
    }

    @Override
    public Collection<CampaignStatisticsData> getCampaignStatisticsData() {
        return null;
    }

    @Override
    public TemplateGroup getGroup(String name) throws NotFoundException {
        return null;
    }

    @Override
    public boolean hasGroup(String name) {
        return false;
    }

    @Override
    public TemplateGroup createGroup(String name) throws CreationException {
        return null;
    }

    @Override
    public void deleteGroup(String name) throws DeletionException {

    }

    @Override
    public Collection<TemplateGroup> listGroups() {
        return null;
    }

    @Override
    public Template getTemplate(TemplateDescription description) throws NotFoundException {
        return null;
    }

    @Override
    public boolean hasTemplate(TemplateDescription description) {
        return false;
    }

    @Override
    public RepositoryResolver<HttpServletRequest> getGitResolver() {
        return null;
    }

    @Override
    public void invalidateCache() {

    }

    @Override
    public void storeRecipients(Collection<Recipient> recipients) {

    }

    @Override
    public List<Recipient> getRandomRecipients(int num, Date notTestedSince) {
        return null;
    }

    @Override
    public Recipient completeRecipient(Recipient recipient) {
        return null;
    }
}
