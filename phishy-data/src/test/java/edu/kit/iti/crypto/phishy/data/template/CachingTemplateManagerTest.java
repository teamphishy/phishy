package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class CachingTemplateManagerTest {

    MockTemplateManager tm;

    private CachingTemplateManager populateCache() {

        var cache = new TemplateCache();
        List<TemplateGroup> groups = List.of(
                new MockTemplateGroup("abc", List.of(
                        new MockTemplate("abc", "a", List.of()),
                        new MockTemplate("abc", "b", List.of())
                )),
                new MockTemplateGroup("xyz", List.of())
        );

        tm = new MockTemplateManager(groups);

        return new CachingTemplateManager(cache, tm);

    }

    @Test
    public void testGetGroup() throws NotFoundException {

        var tm = populateCache();

        assertEquals("abc", tm.getGroup("abc").getName());
        assertEquals("xyz", tm.getGroup("xyz").getName());

    }

    @Test(expected = NotFoundException.class)
    public void testGetGroupNotFound() throws NotFoundException {

        var tm = populateCache();

        tm.getGroup("none").getName();

    }

    @Test
    public void testHasGroup() {

        var tm = populateCache();

        assertTrue(tm.hasGroup("abc"));
        assertTrue(tm.hasGroup("xyz"));
        assertFalse(tm.hasGroup("none"));

    }


    @Test
    public void testGetTemplate() throws NotFoundException {

        var tm = populateCache();

        assertEquals("a", tm.getTemplate(new TemplateDescription("abc", "a")).getName());
        assertEquals("b", tm.getTemplate(new TemplateDescription("abc", "b")).getName());

    }

    @Test(expected = NotFoundException.class)
    public void testGetTemplateNotFound() throws NotFoundException {

        var tm = populateCache();

        tm.getTemplate(new TemplateDescription("abc", "none"));

    }

    @Test
    public void testHasTemplate() {

        var tm = populateCache();

        assertTrue(tm.hasTemplate(new TemplateDescription("abc", "a")));
        assertTrue(tm.hasTemplate(new TemplateDescription("abc", "b")));
        assertFalse(tm.hasTemplate(new TemplateDescription("abc", "none")));

    }

    @Test
    public void testInvalidateCache() {

        final boolean[] cacheInvalidated = {false};
        TemplateCache cache = new TemplateCache() {
            @Override
            public void invalidate() {

                super.invalidate();
                cacheInvalidated[0] = true;
            }
        };

        TemplateManager original = new MockTemplateManager(List.of());
        TemplateManager manager = new CachingTemplateManager(cache, original);

        manager.invalidateCache();

        // check if invalidate was called on TemplateCache
        assertTrue(cacheInvalidated[0]);

    }

    @Test
    public void listGroupsTest() {

        var ctm = populateCache();

        assertEquals(ctm.listGroups(), tm.listGroups());
    }
}