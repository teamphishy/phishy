package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.Repository;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.*;

public class GitTemplateTest {

    private Path getRootPath() {
        try {
            return Paths.get(this.getClass().getResource("gitroot").toURI());
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Root Path invalid");
        }
    }

    private Path getGroupPath(String name) {
        return getRootPath().resolve(name);
    }

    private Template getTemplate(String group, String name) throws IOException {

        Path groupPath = getGroupPath(group);
        Repository repo = new FileRepository(groupPath.toFile());
        return new GitTemplate(repo, group, name);

    }

    public void testCreation() throws IOException {

        Path groupPath = getGroupPath("abc");
        Repository repo = new FileRepository(groupPath.toFile());
        Template template = new GitTemplate(repo, "abc", "a");

        assertEquals("a", template.getName());
        assertEquals(new TemplateDescription("abc", "a"), template.getDescription());

    }

    @Test
    public void testGetFile() throws NotFoundException, IOException {

        Template a = getTemplate("abc", "a");
        TemplateFile file = a.getFile("mail.eml");
        assertEquals("mail.eml", file.getVirtualPath());

        Template b = getTemplate("abc", "b");
        TemplateFile fileB = b.getFile("assets/styles.css");
        assertEquals("assets/styles.css", fileB.getVirtualPath());

    }

    @Test(expected = NotFoundException.class)
    public void testGetFileNotFound() throws NotFoundException, IOException {

        Template a = getTemplate("abc", "a");
        TemplateFile file = a.getFile("none.ex");

    }

    @Test
    public void testHasFile() throws IOException {

        Template a = getTemplate("abc", "a");
        assertTrue(a.hasFile("mail.eml"));
        assertFalse(a.hasFile("none.ex"));

        Template b = getTemplate("abc", "b");
        assertTrue(b.hasFile("assets/styles.css"));

    }

    @Test
    public void testGetDescription() throws IOException {

        Template a = getTemplate("abc", "a");
        TemplateDescription desc = a.getDescription();

        assertEquals("abc", desc.getGroup());
        assertEquals("a", desc.getName());

    }

}