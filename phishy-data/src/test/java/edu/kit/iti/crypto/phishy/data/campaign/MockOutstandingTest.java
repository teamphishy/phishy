package edu.kit.iti.crypto.phishy.data.campaign;

import java.util.Date;

public class MockOutstandingTest implements OutstandingTest {

    private int loginAttempt;
    private String id;
    private Date date;

    public MockOutstandingTest(String id) {
        this.id = id;
        loginAttempt = 0;
        this.date = new Date();
    }

    @Override
    public void delete() {
        this.id = "--deleted--";
    }

    @Override
    public void increaseLoginAttempt() {
        loginAttempt++;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Date getDate() {
        return date;
    }

    @Override
    public int getLoginAttempt() {
        return loginAttempt;
    }
}
