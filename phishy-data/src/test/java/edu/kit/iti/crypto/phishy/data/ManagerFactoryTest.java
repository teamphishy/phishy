package edu.kit.iti.crypto.phishy.data;

import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookCache;
import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookManager;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignManager;
import edu.kit.iti.crypto.phishy.data.template.TemplateManager;
import hthurow.tomcatjndi.TomcatJNDI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.net.URISyntaxException;
import java.nio.file.Paths;

import static org.junit.Assert.assertNotNull;

public class ManagerFactoryTest {

    TomcatJNDI tomcatJNDI = new TomcatJNDI();

    @Before
    public void setup() throws URISyntaxException {
        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("contextEmpty.xml").toURI()).toFile());
        tomcatJNDI.start();

        ManagerFactory.reset();

    }

    @After
    public void teardown() {
        tomcatJNDI.tearDown();
    }

    @Test
    public void testGetCampaignManager() {

        CampaignManager manager = ManagerFactory.getCampaignManager();
        assertNotNull(manager);

    }

    @Test
    public void testGetTemplateManager() {

        TemplateManager manager = ManagerFactory.getTemplateManager();
        assertNotNull(manager);

    }

    @Test
    public void testGetAddressbookManager() {

        AddressbookManager manager = ManagerFactory.getAddressbookManager();
        assertNotNull(manager);

    }

    @Test
    public void testGetAddressbookCache() {

        AddressbookCache cache = ManagerFactory.getAddressbookCache();
        assertNotNull(cache);

    }


}