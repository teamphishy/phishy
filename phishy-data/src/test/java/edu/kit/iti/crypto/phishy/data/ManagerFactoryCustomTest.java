package edu.kit.iti.crypto.phishy.data;

import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookCache;
import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookManager;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignManager;
import edu.kit.iti.crypto.phishy.data.template.TemplateManager;
import hthurow.tomcatjndi.TomcatJNDI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.net.URISyntaxException;
import java.nio.file.Paths;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ManagerFactoryCustomTest {

    TomcatJNDI tomcatJNDI = new TomcatJNDI();

    @Before
    public void setup() throws URISyntaxException {

        tomcatJNDI.processContextXml(Paths.get(this.getClass().getResource("contextCustom.xml").toURI()).toFile());
        tomcatJNDI.start();

        ManagerFactory.reset();

    }

    @After
    public void teardown() {
        tomcatJNDI.tearDown();
    }

    @Test
    public void testGetCampaignManager() {

        CampaignManager manager = ManagerFactory.getCampaignManager();
        assertNotNull(manager);
        assertTrue(manager instanceof MockManager);

    }

    @Test
    public void testGetTemplateManager() {

        TemplateManager manager = ManagerFactory.getTemplateManager();
        assertNotNull(manager);
        assertTrue(manager instanceof MockManager);

    }

    @Test
    public void testGetAddressbookManager() {

        AddressbookManager manager = ManagerFactory.getAddressbookManager();
        assertNotNull(manager);
        assertTrue(manager instanceof MockManager);

    }

    @Test
    public void testGetAddressbookCache() {

        AddressbookCache cache = ManagerFactory.getAddressbookCache();
        assertNotNull(cache);
        assertTrue(cache instanceof MockManager);

    }


}