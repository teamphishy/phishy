package edu.kit.iti.crypto.phishy.data.campaign;

import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.junit.Before;
import org.junit.Test;

import java.util.Objects;

import static org.junit.Assert.*;

public class CampaignDescriptionTest {

    private CampaignDescription description;
    private MockCampaignManager manager;

    @Before
    public void setUp() throws CreationException {
        manager = new MockCampaignManager();
        manager.createCampaign("test123");
        description = new CampaignDescription("test123");
    }

    @Test
    public void resolve() throws NotFoundException {
        assertEquals("test123", description.resolve(manager).getName());
    }

    @Test
    public void getName() {
        assertEquals("test123", description.getName());
    }

    @Test
    public void testEquals() {
        CampaignDescription other = new CampaignDescription("test123");
        assertTrue(description.equals(description));
        assertTrue(description.equals(other));
        assertFalse(description.equals(manager));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateException1() {
        CampaignDescription other = new CampaignDescription(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateException2() {
        CampaignDescription other = new CampaignDescription("42");
    }


    @Test
    public void testHashCode() {
        assertEquals(description.hashCode(), description.hashCode());
        assertEquals(description.hashCode(), Objects.hashCode(description));
    }

    @Test
    public void testToString() {
        assertEquals("test123", description.toString());
    }
}