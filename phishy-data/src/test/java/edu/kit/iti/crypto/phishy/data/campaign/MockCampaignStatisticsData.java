package edu.kit.iti.crypto.phishy.data.campaign;


import edu.kit.iti.crypto.phishy.data.statistics.SQLCampaignStatisticsData;

import javax.sql.DataSource;

public class MockCampaignStatisticsData extends SQLCampaignStatisticsData {

    private String name;
    private int sent;
    private int reported;
    private int phished;

    public MockCampaignStatisticsData(String name, DataSource dataSource) {
        super(name, dataSource);
        this.name = name;
    }

    @Override
    public String getCampaignName() {
        return name;
    }

    @Override
    public void increaseSentMails(int amount) {
        sent = sent + amount;
    }

    @Override
    public void increaseReportedMail(int amount) {
        reported = reported + amount;
    }

    @Override
    public void increasePhishedMail(int amount) {
        phished = phished + amount;
    }

    @Override
    public int getSentMails() {
        return sent;
    }

    @Override
    public int getReportedMails() {
        return reported;
    }

    @Override
    public int getPhishedMails() {
        return phished;
    }
}
