package edu.kit.iti.crypto.phishy.data.campaign;

import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class TestDescriptionTest {
    private TestDescription testDescription;
    private String testID;
    private String campaignName;
    private String base64CampaignName;
    private CampaignManager manager;
    private Campaign campaign;

    @Before
    public void init() throws CreationException {
        campaignName = "test123";
        testID = "123e4567-e89b-12d3-a456-426614174000";
        base64CampaignName = "dGVzdDEyMw==";
        manager = new MockCampaignManager();

        campaign = manager.createCampaign(campaignName);
        CampaignDescription campaignDescription = new CampaignDescription(campaignName);
        testDescription = new TestDescription(campaignDescription, testID);
    }

    @Test
    public void testFromIdentifier() {
        String identifier = base64CampaignName + "_" + testID;
        TestDescription test = TestDescription.fromIdentifier(identifier);
        Assert.assertEquals(testID, test.getTestID());
        Assert.assertEquals(campaignName, test.getCampaignDescription().getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFromIdentifierWrongSeparator() {
        String identifier = base64CampaignName + "/" + testID;
        TestDescription test = TestDescription.fromIdentifier(identifier);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFromIdentifierWrongUUIDFormat() {
        String wrongTestID = "1234";
        String identifier = base64CampaignName + "_" + wrongTestID;
        TestDescription test = TestDescription.fromIdentifier(identifier);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFromIdentifierWrongBase64Format() {
        //the base64 name is missing a "=" at the end and therefore is not valid
        String wrongBase64Name = "dGVzdDEyMw=";
        String identifier = base64CampaignName + "_" + wrongBase64Name;
        TestDescription test = TestDescription.fromIdentifier(identifier);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFromIdentifierTwoSeparatorsFormat() {
        String identifier = base64CampaignName + "__" + testID;
        TestDescription test = TestDescription.fromIdentifier(identifier);
    }

    @Test
    public void testResolveCampaign() throws NotFoundException {
        Campaign testCampaign = testDescription.resolveCampaign(manager);
        Assert.assertEquals(campaign.getName(), testCampaign.getName());
    }

    @Test
    public void testResolveTest() throws NotFoundException {
        ArrayList<TestDescription> tests = new ArrayList<>();
        tests.add(testDescription);
        campaign.recordTests(tests);

        OutstandingTest outstandingTest = testDescription.resolveTest(campaign);
        Assert.assertEquals(outstandingTest.getId(), testID);
    }

    @Test
    public void testToIdentifier() {
        String identifier = testDescription.toIdentifier();
        Assert.assertEquals(base64CampaignName + "_" + testID, identifier);
    }

}
