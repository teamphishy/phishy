package edu.kit.iti.crypto.phishy.data.campaign;

import edu.kit.iti.crypto.phishy.data.statistics.CampaignStatisticsData;
import edu.kit.iti.crypto.phishy.data.statistics.CumulatedStatisticsData;
import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import java.util.ArrayList;
import java.util.Collection;

public class MockCampaignManager implements CampaignManager {

    private ArrayList<Campaign> campaigns;

    public MockCampaignManager() {
        campaigns = new ArrayList<>();
    }

    @Override
    public Campaign getCampaign(CampaignDescription name) throws NotFoundException {
        for (Campaign campaign : campaigns) {
            if (name.getName().equals(campaign.getName())) {
                return campaign;
            }
        }
        throw new NotFoundException();
    }

    @Override
    public boolean hasCampaign(CampaignDescription name) {
        for (Campaign campaign : campaigns) {
            if (name.getName().equals(campaign.getName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Collection<Campaign> listCampaigns() {
        return campaigns;
    }

    @Override
    public Campaign createCampaign(String name) throws CreationException {
        if (name == null) {
            throw new CreationException();
        }
        Campaign campaign = new MockCampaign(name);
        campaigns.add(campaign);
        return campaign;
    }

    @Override
    public Campaign createCampaign(String name, int recipientSetSize, int mailLifetime, int mailInterval) throws CreationException {
        if (name == null) {
            throw new CreationException();
        }
        Campaign campaign = new MockCampaign(name, recipientSetSize, mailLifetime, mailInterval);
        campaigns.add(campaign);
        return campaign;
    }

    @Override
    public void deleteCampaign(CampaignDescription name) throws DeletionException {
        for (Campaign campaign : campaigns) {
            if (name.getName().equals(campaign.getName())) {
                campaigns.remove(campaign);
                return;
            }
        }
        throw new DeletionException();
    }

    @Override
    public CumulatedStatisticsData getCumulatedStatistics() {
        throw new UnsupportedOperationException("Not implemented!");
    }

    @Override
    public Collection<CampaignStatisticsData> getCampaignStatisticsData() {
        throw new UnsupportedOperationException("Not implemented!");
    }
}
