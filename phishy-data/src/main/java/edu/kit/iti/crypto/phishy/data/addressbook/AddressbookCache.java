package edu.kit.iti.crypto.phishy.data.addressbook;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Cache for Addressbooks
 * Provides possibility of storing information not storeable in original source
 * The AddressbookCache MUST NOT store testID
 * The AddressbookCache SHOULD store as little information as possible
 */
public interface AddressbookCache {

    /**
     * Store the Recipients in Cache
     * Only information that cannot be fetched from original source should be stored
     *
     * @param recipients recipients to store
     */
    void storeRecipients(Collection<Recipient> recipients);

    /**
     * Get a random set of recipients not contacted since `notTestedSince`
     * This returns at max `num` recipients but might return less
     *
     * @param num            Max number of recipients to return
     * @param notTestedSince Returned Recipients must not be contacted since this date
     * @return list of recipients
     */
    List<Recipient> getRandomRecipients(int num, Date notTestedSince);

    /**
     * Complete a given recipient with the data from this cache
     *
     * @param recipient Incomplete recipient (id must be set)
     * @return Completed Recipient if possible; input otherwise
     */
    Recipient completeRecipient(Recipient recipient);

}
