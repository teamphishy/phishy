package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;

public class TemplateDescription {

    private final String name;
    private final String group;

    public String getName() {
        return name;
    }

    public String getGroup() {
        return group;
    }

    public TemplateDescription(String group, String name) {
        this.group = group;
        this.name = name;
    }

    public Template resolve(TemplateManager manager) throws NotFoundException {

        return manager.getTemplate(this);

    }

    public static TemplateDescription fromFQN(String fqn) {

        int splitPos = fqn.indexOf("-");
        if (splitPos == -1) {
            throw new IllegalArgumentException("FQN must contain exactly one hypen");
        }
        if (fqn.lastIndexOf("-") != splitPos) {
            throw new IllegalArgumentException("FQN must not contain multiple hyphen");
        }

        String group = fqn.substring(0, splitPos);
        String template = fqn.substring(splitPos + 1);

        return new TemplateDescription(group, template);

    }

    @Override
    public String toString() {
        return group + "-" + name;
    }
}
