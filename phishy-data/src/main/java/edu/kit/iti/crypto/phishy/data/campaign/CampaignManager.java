package edu.kit.iti.crypto.phishy.data.campaign;

import edu.kit.iti.crypto.phishy.data.statistics.CampaignStatisticsData;
import edu.kit.iti.crypto.phishy.data.statistics.CumulatedStatisticsData;
import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import java.util.Collection;

/**
 * Provides methods for managing the Campaigns
 */
public interface CampaignManager {

    /**
     * returns a Campaign for a given CampaignDescription
     *
     * @param name the CampaignDescription
     * @return the matching Campaign
     * @throws NotFoundException when the Campaign with the given CampaignDescription is not found
     */
    Campaign getCampaign(CampaignDescription name) throws NotFoundException;

    /**
     * determines whether the CampaignManager possesses a Campaign with the matching CampaignDescription
     *
     * @param name the CampaignDescription
     * @return if there is a matching Campaign
     */
    boolean hasCampaign(CampaignDescription name);

    /**
     * returns a list of all Campaigns known to the CampaignManager
     *
     * @return a list of all Campaigns
     */
    Collection<Campaign> listCampaigns();

    /**
     * creates a new Campaign with a given name
     *
     * @param name the name of the Campaign
     * @return the new Campaign
     * @throws CreationException when the creation of a new campaign was not possible
     */
    Campaign createCampaign(String name) throws CreationException;

    /**
     * creates a new Campaign with a given name, recipientSetSize and mailLifetime
     *
     * @param name             the name of the Campaign
     * @param recipientSetSize the set-size of recipients
     * @param mailLifetime     the amount of time in which the phishing mails are valid
     * @return the new Campaign
     * @throws CreationException when the creation of a new campaign was not possible
     */
    @Deprecated
    Campaign createCampaign(String name, int recipientSetSize, int mailLifetime, int mailInterval) throws CreationException;

    /**
     * deletes a Campaign with a given CampaignDescription
     *
     * @param name the CampaignDescription
     * @throws DeletionException when the deletion of the campaign was not possible
     */
    void deleteCampaign(CampaignDescription name) throws DeletionException;

    /**
     * returns the cumulated statistics for the whole campaign
     *
     * @return a CumulatedStatisticsData Object
     */
    CumulatedStatisticsData getCumulatedStatistics();

    /**
     * returns a Collection of statistics for all Campaigns separately
     *
     * @return a Collection of CampaignStatisticsData
     */
    Collection<CampaignStatisticsData> getCampaignStatisticsData();
}
