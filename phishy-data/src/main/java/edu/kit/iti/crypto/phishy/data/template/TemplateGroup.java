package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import java.util.Collection;

public interface TemplateGroup {

    String getName();

    Template getTemplate(TemplateDescription description) throws NotFoundException;

    boolean hasTemplate(TemplateDescription description);

    Collection<Template> listTemplates();

}
