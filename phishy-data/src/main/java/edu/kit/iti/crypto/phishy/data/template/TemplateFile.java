package edu.kit.iti.crypto.phishy.data.template;

import java.io.InputStream;

public interface TemplateFile {


    String getVirtualPath();

    String getMimeType();

    long getEstimatedSize();

    InputStream getStream();

}
