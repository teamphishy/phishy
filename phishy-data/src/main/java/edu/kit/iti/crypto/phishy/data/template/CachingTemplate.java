package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;

public class CachingTemplate implements Template {

    private final TemplateCache cache;
    private final Template template;

    public CachingTemplate(TemplateCache cache, Template template) {
        this.cache = cache;
        this.template = template;
    }

    @Override
    public String getName() {
        return template.getName();
    }

    @Override
    public TemplateFile getFile(String path) throws NotFoundException {
        return new CachingTemplateFile(cache, this.getDescription(), template.getFile(path));
    }

    @Override
    public boolean hasFile(String path) {
        return template.hasFile(path);
    }

    @Override
    public TemplateDescription getDescription() {
        return template.getDescription();
    }
}
