package edu.kit.iti.crypto.phishy.data.template;

import org.eclipse.jgit.errors.RepositoryNotFoundException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.ServiceMayNotContinueException;
import org.eclipse.jgit.transport.resolver.RepositoryResolver;
import org.eclipse.jgit.transport.resolver.ServiceNotAuthorizedException;
import org.eclipse.jgit.transport.resolver.ServiceNotEnabledException;

public class NullTemplateRepositoryResolver implements RepositoryResolver {
    @Override
    public Repository open(Object req, String name)
            throws RepositoryNotFoundException, ServiceNotAuthorizedException, ServiceNotEnabledException,
            ServiceMayNotContinueException {
        throw new RepositoryNotFoundException("Repositories are not enabled");
    }
}
