package edu.kit.iti.crypto.phishy.data.campaign;

import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookDescription;
import edu.kit.iti.crypto.phishy.data.addressbook.Recipient;
import edu.kit.iti.crypto.phishy.data.statistics.CampaignStatisticsData;
import edu.kit.iti.crypto.phishy.data.statistics.SQLCampaignStatisticsData;
import edu.kit.iti.crypto.phishy.data.template.TemplateDescription;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.MutationException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class SQLCampaign extends Campaign {

    private static final String ATTRIBUTE_FORMAT = "^[a-zA-Z][a-zA-Z0-9_]*";
    private static final Pattern ATTRIBUTE_PATTERN = Pattern.compile(ATTRIBUTE_FORMAT);

    private final DataSource dataSource;
    private final String name;
    private final SQLCampaignStatisticsData statistics;
    private final Logger logger;

    public SQLCampaign(CampaignDescription campaignDescription, DataSource dataSource) {
        this(campaignDescription.getName(), dataSource);
    }

    public SQLCampaign(String name, DataSource dataSource) {
        if (!ATTRIBUTE_PATTERN.matcher(name).matches()) {
            throw new IllegalArgumentException("Illegal name format");
        }
        this.name = name;
        this.dataSource = dataSource;
        logger = Logger.getLogger(this.getClass().getName());
        if (!hasCampaign()) {
            createCampaign();
        }
        statistics = new SQLCampaignStatisticsData(this.name, dataSource);
    }

    @Override
    public void linkTemplate(TemplateType type, TemplateDescription description) {
        Objects.requireNonNull(type);
        Objects.requireNonNull(description);

        if (hasSQLTemplate(type)) {
            removeSQLTemplate(type);
        }

        addSQLTemplate(type, description);
    }

    @Override
    public void unlinkTemplate(TemplateType type) throws DeletionException {
        Objects.requireNonNull(type);

        if (!hasTemplate(type)) {
            throw new DeletionException();
        }
        removeSQLTemplate(type);
    }

    @Override
    public TemplateDescription getTemplate(TemplateType type) throws NotFoundException {
        Objects.requireNonNull(type);
        String templateName = getSQLTemplateName(type);
        if (templateName == null) {
            throw new NotFoundException();
        }
        try {
            return TemplateDescription.fromFQN(templateName);
        } catch (IllegalArgumentException e) {
            throw new NotFoundException();
        }
    }

    @Override
    public boolean hasTemplate(TemplateType type) {
        Objects.requireNonNull(type);
        return hasSQLTemplate(type);
    }

    @Override
    public CampaignStatisticsData getStatistics() {
        return statistics;
    }

    @Override
    public OutstandingTest getOutstandingTest(String id) throws NotFoundException {
        Objects.requireNonNull(id);
        if (!hasOutstandingTest(id)) {
            throw new NotFoundException();
        }
        return new SQLOutstandingTest(id, new CampaignDescription(this.name), this.statistics, this.dataSource);
    }

    @Override
    public boolean hasOutstandingTest(String id) {
        Objects.requireNonNull(id);
        return hasSQLOutstandingTest(id);
    }

    @Override
    public AddressbookDescription getAddressbook() throws NotFoundException {

        try {
            return new AddressbookDescription(getSQLAddressbook());
        } catch (IllegalArgumentException e) {
            // addressbook name invalid
            throw new NotFoundException();
        }

    }

    @Override
    public void setAddressbook(AddressbookDescription description) {
        Objects.requireNonNull(description);
        setSQLAddressbook(description.getName());
    }

    @Override
    public void recordTests(Collection<TestDescription> tests) {
        Objects.requireNonNull(tests);

        final String query = "INSERT INTO OutstandingTest(id, campaign, date) VALUES(?,?,?)";
        try (Connection connection = dataSource.getConnection()) {
            connection.createStatement().execute("BEGIN TRANSACTION;");
            PreparedStatement statement = connection.prepareStatement(query);
            for (TestDescription testDescription : tests) {
                statement.setString(1, testDescription.getTestID());
                statement.setString(2, name);
                statement.setString(3, String.valueOf(System.currentTimeMillis()));
                statement.executeUpdate();
            }
            connection.createStatement().execute("COMMIT;");
            statistics.increaseSentMails(tests.size());

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while inserting into OutstandingTest");
            throw new IllegalStateException(e);
        }
    }

    @Override
    public List<Recipient> deleteTests(List<Recipient> recipients) {
        Objects.requireNonNull(recipients);

        try (Connection connection = dataSource.getConnection()) {

            List<Recipient> deletedRecipients = new ArrayList<Recipient>();

            connection.createStatement().execute("BEGIN TRANSACTION;");

            final String query = "DELETE FROM OutstandingTest WHERE id = ?;";

            PreparedStatement statement = connection.prepareStatement(query);
            for (Recipient recipient : recipients) {
                statement.setString(1, recipient.getTestId().getTestID());
                if (statement.executeUpdate() > 0) {
                    deletedRecipients.add(recipient);
                }
            }

            connection.createStatement().execute("COMMIT;");
            return deletedRecipients;

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while removing OutstandingTest", e);
            throw new IllegalStateException(e);
        }
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getRecipientSetSize() {
        return getSQLCampaignIntegerAttribute("recipientSetSize");
    }

    @Override
    public void setRecipientSetSize(int recipientSetSize) {
        if (recipientSetSize <= 0) {
            throw new IllegalArgumentException("Recipient set size must be greater zero.");
        }
        setSQLCampaignIntegerAttribute("recipientSetSize", recipientSetSize);
    }

    @Override
    public CampaignStatus getStatus() {
        return CampaignStatus.fromInt(getSQLCampaignIntegerAttribute("status"));
    }

    @Override
    public void setStatus(CampaignStatus status) throws MutationException {
        Objects.requireNonNull(status);
        if (status == CampaignStatus.ACTIVE && !this.isSetUp()) {
            throw new MutationException();
        }
        setSQLCampaignIntegerAttribute("status", status.toInt());
    }

    @Override
    public int getMailLifetime() {
        return getSQLCampaignIntegerAttribute("mailLifetime");
    }

    @Override
    public void setMailLifetime(int mailLifetime) {
        if (mailLifetime <= 0) {
            throw new IllegalArgumentException("Mail lifetime must be greater zero.");
        }
        setSQLCampaignIntegerAttribute("mailLifetime", mailLifetime);
    }

    @Override
    public int getMailInterval() {
        return getSQLCampaignIntegerAttribute("mailInterval");
    }

    @Override
    public void setMailInterval(int mailInterval) {
        if (mailInterval <= 0) {
            throw new IllegalArgumentException("Mail interval must be greater zero.");
        }
        setSQLCampaignIntegerAttribute("mailInterval", mailInterval);
    }

    private void createCampaign() {
        final String query = "INSERT INTO Campaign(id) VALUES(?)";

        try (Connection connection = this.dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, this.name);
            statement.executeUpdate();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while inserting into Campaign");
            throw new IllegalStateException(e);
        }

    }

    private boolean hasCampaign() {
        final String query = "SELECT id FROM Campaign WHERE id=?";

        try (Connection connection = this.dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, this.name);
            ResultSet resultSet = statement.executeQuery();
            return resultSet.next();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while reading Campaign id");
            throw new IllegalStateException(e);
        }
    }

    private int getSQLCampaignIntegerAttribute(String attributeName) {
        if (!ATTRIBUTE_PATTERN.matcher(attributeName).matches()) {
            throw new IllegalArgumentException("Wrong attribute format");
        }

        final String query = "SELECT " + attributeName + " FROM Campaign WHERE id=?";

        try (Connection connection = this.dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, this.name);
            ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) {
                throw new IllegalStateException();
            }
            return resultSet.getInt(attributeName);

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while reading Campaign attribute");
            throw new IllegalStateException(e);
        }
    }

    private void setSQLCampaignIntegerAttribute(String attributeName, int value) {
        if (!ATTRIBUTE_PATTERN.matcher(attributeName).matches()) {
            throw new IllegalArgumentException("Wrong attribute format");
        }

        final String query = "UPDATE Campaign SET " + attributeName + "=? WHERE id=?";

        try (Connection connection = this.dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, value);
            statement.setString(2, this.name);
            statement.executeUpdate();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while updating Campaign id");
            throw new IllegalStateException(e);
        }
    }

    private String getSQLTemplateName(TemplateType type) {
        final String query = "SELECT template FROM CampaignTemplateRelation WHERE campaign=? AND type=?";

        try (Connection connection = this.dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, this.name);
            statement.setInt(2, type.toInt());
            ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) {
                return null;
            }
            return resultSet.getString("template");

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while reading CampaignTemplateRelation template");
            throw new IllegalStateException(e);
        }
    }

    private boolean hasSQLTemplate(TemplateType type) {
        final String query = "SELECT template FROM CampaignTemplateRelation WHERE campaign=? AND type=?";

        try (Connection connection = this.dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, this.name);
            statement.setInt(2, type.toInt());
            ResultSet resultSet = statement.executeQuery();
            return resultSet.next();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while reading CampaignTemplateRelation template");
            throw new IllegalStateException(e);
        }
    }

    private void addSQLTemplate(TemplateType type, TemplateDescription description) {
        final String query = "INSERT INTO CampaignTemplateRelation(campaign, type, template) VALUES(?,?,?)";

        try (Connection connection = this.dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, this.name);
            statement.setInt(2, type.toInt());
            statement.setString(3, description.toString());
            statement.executeUpdate();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while inserting into CampaignTemplateRelation");
            throw new IllegalStateException(e);
        }
    }

    private void removeSQLTemplate(TemplateType type) {
        final String query = "DELETE FROM CampaignTemplateRelation WHERE campaign=? AND type=?";

        try (Connection connection = this.dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, this.name);
            statement.setInt(2, type.toInt());
            statement.executeUpdate();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while deleting in CampaignTemplateRelation");
            throw new IllegalStateException(e);
        }
    }

    private String getSQLAddressbook() throws NotFoundException {
        final String query = "SELECT addressbook FROM Campaign WHERE id=?";

        try (Connection connection = this.dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, this.name);
            ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) {
                throw new NotFoundException();
            }
            return resultSet.getString("addressbook");

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while reading Campaign addressbook");
            throw new IllegalStateException(e);
        }
    }

    private void setSQLAddressbook(String addressbook) {
        final String query = "UPDATE Campaign SET addressbook=? WHERE id=?";

        try (Connection connection = this.dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, addressbook);
            statement.setString(2, this.name);
            statement.executeUpdate();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while updating Campaign");
            throw new IllegalStateException(e);
        }
    }

    private boolean hasSQLOutstandingTest(String testId) {
        final String query = "SELECT id FROM OutstandingTest WHERE id=? AND campaign=?";

        try (Connection connection = this.dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, testId);
            statement.setString(2, this.name);
            ResultSet resultSet = statement.executeQuery();
            return resultSet.next();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while reading OutstandingTest id");
            throw new IllegalStateException(e);
        }
    }


    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SQLCampaign that = (SQLCampaign) o;
        return Objects.equals(name, that.name);
    }


}
