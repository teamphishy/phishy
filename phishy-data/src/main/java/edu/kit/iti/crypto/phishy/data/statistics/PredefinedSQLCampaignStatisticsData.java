package edu.kit.iti.crypto.phishy.data.statistics;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PredefinedSQLCampaignStatisticsData implements CampaignStatisticsData {

    private static final Logger logger = Logger.getLogger(PredefinedSQLCampaignStatisticsData.class.getName());

    private final String campaign;
    private final int sentMails;
    private final int reportedMails;
    private final int phishedMails;

    public PredefinedSQLCampaignStatisticsData(String campaign, int sentMails, int reportedMails, int phishedMails) {
        this.campaign = campaign;
        this.sentMails = sentMails;
        this.reportedMails = reportedMails;
        this.phishedMails = phishedMails;
    }

    @Override
    public String getCampaignName() {
        return campaign;
    }

    @Override
    public int getSentMails() {
        return this.sentMails;
    }

    @Override
    public void increaseSentMails(int amount) {
        throw new UnsupportedOperationException("Predefined Statistics Data can't be increased");
    }

    @Override
    public int getReportedMails() {
        return this.reportedMails;
    }

    @Override
    public void increaseReportedMail(int amount) {
        throw new UnsupportedOperationException("Predefined Statistics Data can't be increased");
    }

    @Override
    public int getPhishedMails() {
        return this.phishedMails;
    }

    @Override
    public void increasePhishedMail(int amount) {
        throw new UnsupportedOperationException("Predefined Statistics Data can't be increased");
    }

    public static Collection<CampaignStatisticsData> getAll(DataSource dataSource) {
        try (Connection con = dataSource.getConnection()) {
            PreparedStatement stmt = con.prepareStatement("SELECT * FROM Statistics");

            ResultSet resultSet = stmt.executeQuery();

            Collection<CampaignStatisticsData> statCollection = new ArrayList<>();
            while (resultSet.next()) {

                statCollection.add(new PredefinedSQLCampaignStatisticsData(resultSet.getString("campaign"),
                        resultSet.getInt("sentMails"),
                        resultSet.getInt("reportedMails"),
                        resultSet.getInt("phishedMails")));
            }

            return statCollection;
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while getting all campaign statistics");
            throw new IllegalStateException(e);
        }
    }
}
