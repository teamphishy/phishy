package edu.kit.iti.crypto.phishy.data.addressbook;

import edu.kit.iti.crypto.phishy.data.campaign.TestDescription;

import java.util.Date;

/**
 * Recipient capsulates data corresponding to a Recipient
 * This can be used with an id to be filled out by an addressbook
 * or prefilled returned by an addressbook
 */
public class Recipient {

    private final String id;
    private String mail;
    private String surname;
    private String givenname;
    private String name;
    private Date latestContact;
    private TestDescription testId;


    public Recipient(String id, Date latestContact) {
        this.id = id;
        this.latestContact = latestContact;
    }

    public Recipient(String id) {
        this.id = id;
    }

    public Recipient(String id, String mail, String name) {
        this.id = id;
        this.mail = mail;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getLatestContact() {
        return latestContact;
    }

    public void setLatestContact(Date latestContact) {
        this.latestContact = latestContact;
    }

    public TestDescription getTestId() {
        return testId;
    }

    public void setTestId(TestDescription testId) {
        this.testId = testId;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getGivenName() {
        return givenname;
    }

    public void setGivenName(String givenname) {
        this.givenname = givenname;
    }

    public Recipient clone() {

        Recipient rcpt = new Recipient(this.getId());
        rcpt.setName(name);
        rcpt.setSurname(surname);
        rcpt.setGivenName(givenname);
        rcpt.setMail(mail);
        rcpt.setLatestContact(latestContact);
        rcpt.setTestId(testId);

        return rcpt;

    }

}
