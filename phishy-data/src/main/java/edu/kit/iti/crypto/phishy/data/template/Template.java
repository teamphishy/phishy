package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;

public interface Template {

    String getName();

    TemplateFile getFile(String path) throws NotFoundException;

    boolean hasFile(String path);

    TemplateDescription getDescription();

}
