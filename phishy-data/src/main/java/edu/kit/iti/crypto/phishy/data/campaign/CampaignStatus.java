package edu.kit.iti.crypto.phishy.data.campaign;

public enum CampaignStatus {
    INACTIVE(0),
    ACTIVE(1);

    private int id;

    CampaignStatus(int id) {
        this.id = id;
    }

    public int toInt() {
        return id;
    }

    public static CampaignStatus fromInt(int id) {
        switch (id) {
            case 0:
                return INACTIVE;
            case 1:
                return ACTIVE;
            default:
                throw new IllegalArgumentException("invalid value");
        }
    }

    @Override
    public String toString() {
        switch (this) {
            case INACTIVE:
                return "inactive";
            case ACTIVE:
                return "active";
            default:
                throw new IllegalStateException("invalid enum value");
        }
    }

    public static CampaignStatus fromString(String status) {
        switch (status.toLowerCase()) {
            case "inactive":
                return INACTIVE;
            case "active":
                return ACTIVE;
            default:
                throw new IllegalArgumentException("invalid value");
        }
    }
}
