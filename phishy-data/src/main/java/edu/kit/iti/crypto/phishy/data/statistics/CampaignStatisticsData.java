package edu.kit.iti.crypto.phishy.data.statistics;

/**
 * Represents a statistic item holding information for a specific campaign on send, reported and phished mails.
 */
public interface CampaignStatisticsData extends StatisticsData {

    /**
     * @return name of campaign this statistics item is holding information for.
     */
    String getCampaignName();

    /**
     * Increases the statistic for number of sent mails.
     *
     * @param amount to increase by.
     */
    void increaseSentMails(int amount);

    /**
     * Increases the statistic for number of reported mails.
     *
     * @param amount to increase by.
     */
    void increaseReportedMail(int amount);

    /**
     * Increases the statistic for number of phished mails.
     *
     * @param amount to increase by.
     */
    void increasePhishedMail(int amount);

}
