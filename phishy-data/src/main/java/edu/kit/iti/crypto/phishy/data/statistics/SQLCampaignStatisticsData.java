package edu.kit.iti.crypto.phishy.data.statistics;


import edu.kit.iti.crypto.phishy.data.campaign.CampaignDescription;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class SQLCampaignStatisticsData implements CampaignStatisticsData {

    private static final String VALID_COLUMN_NAME = "^[a-zA-Z][a-zA-Z0-9_]*$";
    private static final Pattern VALID_COLUMN_NAME_PATTERN = Pattern.compile(VALID_COLUMN_NAME);

    private final String campaign;
    private final DataSource dataSource;
    private final Logger logger;

    public SQLCampaignStatisticsData(CampaignDescription campaign, DataSource dataSource) {
        this(campaign.getName(), dataSource);
    }

    public SQLCampaignStatisticsData(String campaign, DataSource dataSource) {
        this.logger = Logger.getLogger(this.getClass().getName());

        if (!doesCampaignExist(campaign, dataSource)) {
            logger.log(Level.SEVERE, "Tried to create a statistics item for a non existing campaign " + campaign);
            throw new IllegalArgumentException("campaign does not exist");
        }
        createStatDatabaseEntryIfNotAlready(campaign, dataSource);

        this.campaign = campaign;
        this.dataSource = dataSource;
    }

    private boolean doesCampaignExist(String campaign, DataSource dataSource) {
        final String campaignQuery = "SELECT id FROM Campaign WHERE id = ?";
        try (Connection con = dataSource.getConnection()) {
            PreparedStatement stmt = con.prepareStatement(campaignQuery);
            stmt.setString(1, campaign);
            ResultSet resultSet = stmt.executeQuery();

            return resultSet.next();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while trying to check if campaign exists");
            throw new IllegalStateException(e);
        }
    }

    private void createStatDatabaseEntryIfNotAlready(String campaign, DataSource dataSource) {
        // check if statistics exists
        final String statQuery = "SELECT campaign FROM Statistics WHERE campaign = ?";
        try (Connection con = dataSource.getConnection()) {
            PreparedStatement stmt = con.prepareStatement(statQuery);
            stmt.setString(1, campaign);
            ResultSet resultSet = stmt.executeQuery();

            if (!resultSet.next()) {
                // create new statistics item
                final String statCreateQuery = "INSERT INTO Statistics (campaign) VALUES (?)";
                PreparedStatement createStmt = con.prepareStatement(statCreateQuery);
                createStmt.setString(1, campaign);
                createStmt.executeUpdate();
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while checking existence/creating new statistics entry");
            throw new IllegalStateException(e);
        }
    }

    @Override
    public String getCampaignName() {
        return campaign;
    }

    @Override
    public int getSentMails() {
        return sqlGetStat("sentMails");
    }

    @Override
    public void increaseSentMails(int amount) {
        if (amount < 0) {
            logger.log(Level.SEVERE, "increaseSentMail was called with a negative amount");
            throw new IllegalArgumentException("Amount can't be negative!");
        }
        sqlIncStat("sentMails", amount);
    }

    @Override
    public int getReportedMails() {
        return sqlGetStat("reportedMails");
    }

    @Override
    public void increaseReportedMail(int amount) {
        if (amount < 0) {
            logger.log(Level.SEVERE, "increaseReportedMail was called with a negative amount");
            throw new IllegalArgumentException("Amount can't be negative!");
        }
        sqlIncStat("reportedMails", amount);
    }

    @Override
    public int getPhishedMails() {
        return sqlGetStat("phishedMails");
    }

    @Override
    public void increasePhishedMail(int amount) {
        if (amount < 0) {
            logger.log(Level.SEVERE, "increasePhishedMail was called with a negative amount");
            throw new IllegalArgumentException("Amount can't be negative!");
        }
        sqlIncStat("phishedMails", amount);
    }

    public static void removeStatistic(CampaignDescription description, DataSource dataSource) {
        final String query = "DELETE FROM Statistics WHERE campaign=?";
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, description.getName());
            statement.executeUpdate();
        } catch (SQLException e) {
            Logger.getLogger(SQLCampaignStatisticsData.class.getName()).log(Level.SEVERE, "SQL error while deleting a statistic");
            throw new IllegalStateException(e);
        }
    }

    private int sqlGetStat(String stat) {
        if (!VALID_COLUMN_NAME_PATTERN.matcher(stat).matches()) {
            logger.log(Level.SEVERE, "get campaign statistics was called with invalid stat name");
            throw new IllegalArgumentException("Invalid stat name");
        }

        final String query = "SELECT " + stat + " FROM Statistics WHERE campaign=?";
        try (Connection con = this.dataSource.getConnection()) {
            PreparedStatement stmt = con.prepareStatement(query);

            stmt.setString(1, this.campaign);

            ResultSet resultSet = stmt.executeQuery();

            if (!resultSet.next()) {
                throw new IllegalStateException("Statistics item in database was deleted");
            }
            return resultSet.getInt(stat);

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while getting campaign statistics data");
            throw new IllegalStateException(e);
        }
    }

    private void sqlIncStat(String stat, int amount) {
        if (!VALID_COLUMN_NAME_PATTERN.matcher(stat).matches()) {
            logger.log(Level.SEVERE, "increase campaign statistics was called with invalid stat name");
            throw new IllegalArgumentException("Invalid stat name");
        }

        final String query = "UPDATE Statistics SET " + stat + " = " + stat + " + ? WHERE campaign = ?";
        try (Connection con = this.dataSource.getConnection()) {
            PreparedStatement stmt = con.prepareStatement(query);

            stmt.setInt(1, amount);
            stmt.setString(2, this.campaign);

            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while increasing campaign statistics data");
            throw new IllegalStateException(e);
        }
    }
}
