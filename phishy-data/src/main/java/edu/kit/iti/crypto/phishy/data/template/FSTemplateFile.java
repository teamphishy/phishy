package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.mime.MimeMap;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public class FSTemplateFile implements TemplateFile {

    private final Path path;
    private final Path basePath;

    public FSTemplateFile(Path basePath, Path filePath) {
        assert basePath != null;
        assert filePath != null;

        this.path = filePath;
        this.basePath = basePath;

        if (!Files.exists(filePath)) {
            throw new IllegalArgumentException("Template File does not exist");
        }

    }

    @Override
    public String getVirtualPath() {

        return basePath.relativize(path).toString().replace(File.separatorChar, '/');

    }

    @Override
    public String getMimeType() {

        String filename = path.getFileName().toString();
        int lastDot = filename.lastIndexOf('.');
        String ext = lastDot > 0 ? filename.substring(lastDot + 1) : "";

        return MimeMap.getInstance().getMime(ext);

    }

    @Override
    public long getEstimatedSize() {
        try {
            return Files.size(path);
        } catch (IOException e) {
            return 0;
        }
    }

    @Override
    public InputStream getStream() {
        try {
            return Files.newInputStream(path, StandardOpenOption.READ);
        } catch (IOException e) {
            return InputStream.nullInputStream();
        }
    }
}
