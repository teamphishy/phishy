package edu.kit.iti.crypto.phishy.data.addressbook;

import edu.kit.iti.crypto.phishy.data.ManagerFactory;

import javax.naming.Context;
import javax.naming.Name;
import javax.naming.spi.ObjectFactory;
import java.util.Hashtable;

/**
 * Create a new Addressbook instance for JNDI
 */
public abstract class AddressbookFactory implements ObjectFactory {

    @Override
    public Object getObjectInstance(Object obj, Name name, Context nameCtx, Hashtable<?, ?> environment)
            throws Exception {
        return getAddressbook(getCache(), obj, name, nameCtx, environment);
    }

    private AddressbookCache getCache() {

        return ManagerFactory.getAddressbookCache();

    }

    protected abstract Object getAddressbook(AddressbookCache cache, Object obj, Name name, Context nameCtx,
                                             Hashtable<?, ?> environment) throws Exception;

}
