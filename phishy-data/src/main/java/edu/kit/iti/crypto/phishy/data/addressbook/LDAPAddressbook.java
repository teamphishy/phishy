package edu.kit.iti.crypto.phishy.data.addressbook;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import javax.naming.ldap.*;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class LDAPAddressbook implements Addressbook {

    private final Logger logger;
    private final String name;
    protected final AddressbookCache cache;
    private final String server;
    protected final String filter;
    protected final String basePath;
    private final String userPattern;
    private final String bindUser;
    private final String bindSecret;
    private final int pageSize;

    public LDAPAddressbook(String name, AddressbookCache cache, String server, String filter, String basePath,
                           String userPattern, String bindUser, String bindSecret, int pageSize) {
        this.name = name;
        this.cache = cache;
        this.server = server;
        this.filter = filter;
        this.basePath = basePath;
        this.userPattern = userPattern;
        this.bindUser = bindUser;
        this.bindSecret = bindSecret;
        this.pageSize = pageSize;
        this.logger = Logger.getLogger(this.getClass().getName());
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Recipient> getRandomRecipients(int num, Date notTestedSince) {

        if (num < 0) {
            throw new IllegalArgumentException("number must be greater than zero");
        }

        List<Recipient> recipients = cache.getRandomRecipients(num, notTestedSince);

        return completeRecipients(recipients);
    }


    @Override
    public boolean checkCredentials(String name, String password) {

        if ((name == null) || (password == null)) {
            throw new IllegalArgumentException("Name and Password must not be null");
        }

        Hashtable<String, String> env = new Hashtable<>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, server);
        env.put(Context.SECURITY_PRINCIPAL, ldapUserFromName(name));
        env.put(Context.SECURITY_CREDENTIALS, password);
        env.put(Context.REFERRAL, "follow");

        try {
            new InitialDirContext(env);
            // Login successful
            return true;
        } catch (NamingException e) {
            return false;
        }

    }

    protected String ldapUserFromName(String name) {
        return userPattern.replaceAll("\\{USER\\}", name);
    }

    @Override
    public void recordTest(Collection<Recipient> recipients) {

        if (recipients == null) {
            return;
        }

        Collection<Recipient> updatedRecipients = recipients.stream()
                .map(recipient -> {
                    Recipient updatedRecipient = recipient.clone();
                    updatedRecipient.setLatestContact(new Date());
                    updatedRecipient.setTestId(null);
                    return updatedRecipient;
                })
                .collect(Collectors.toList());

        cache.storeRecipients(updatedRecipients);

    }

    protected LdapContext openLDAPLookup() throws NamingException {

        Hashtable<String, String> env = new Hashtable<>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, server);
        env.put(Context.SECURITY_PRINCIPAL, bindUser);
        env.put(Context.SECURITY_CREDENTIALS, bindSecret);
        env.put(Context.REFERRAL, "follow");

        return new InitialLdapContext(env, null);

    }

    @Override
    public void updateCache() {

        try {
            loadEntries();
        } catch (NamingException | IOException e) {
            e.printStackTrace();
            logger.log(Level.SEVERE, "Error loading entries from ldap: " + e.getMessage());
        }


    }

    protected Recipient recipientFromEntry(SearchResult entry) throws NamingException {

        Attributes attr = entry.getAttributes();
        String id = attr.get("mail").get().toString();
        String mail = attr.get("mail").get().toString();

        Recipient recipient = new Recipient(id, mail, "");

        return recipient;

    }

    protected void loadEntries() throws NamingException, IOException {

        LdapContext ctx = openLDAPLookup();
        ctx.setRequestControls(new Control[]{new PagedResultsControl(pageSize, Control.CRITICAL)});
        outer:
        while (true) {

            List<Recipient> recipients = new LinkedList<>();
            SearchControls ctls = new SearchControls();
            ctls.setReturningAttributes(new String[]{"mail", "id"});
            ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            NamingEnumeration<SearchResult> result = ctx.search(basePath, filter, ctls);
            while (result.hasMoreElements()) {

                SearchResult entry = result.nextElement();
                recipients.add(recipientFromEntry(entry));

            }
            cache.storeRecipients(recipients);
            for (Control ctrl : ctx.getResponseControls()) {
                if (ctrl instanceof PagedResultsResponseControl) {
                    PagedResultsResponseControl ct = (PagedResultsResponseControl) ctrl;
                    if (ct.getCookie() == null) {
                        break outer;
                    }
                    ctx.setRequestControls(
                            new Control[]{new PagedResultsControl(pageSize, ct.getCookie(), Control.CRITICAL)});

                }
            }


        }
    }

    private List<Recipient> completeRecipients(List<Recipient> recipients) {

        assert recipients != null;

        try {
            DirContext ctx = openLDAPLookup();
            List<Recipient> completedRecipients = new ArrayList<Recipient>();

            for (int i = 0; i < recipients.size(); i += pageSize) {
                completedRecipients.addAll(
                        completeRecipients(recipients.subList(
                                i, Math.min(i + pageSize, recipients.size())), ctx));
            }

            return completedRecipients;
        } catch (NamingException e) {
            logger.log(Level.SEVERE, "LDAP Error in Addressbook " + this.name + ": " + e.getMessage());
            return recipients;
        }

    }

    private List<Recipient> completeRecipients(List<Recipient> recipients, DirContext ctx) {

        assert recipients != null;
        assert ctx != null;

        try {
            SearchControls ctls = new SearchControls();
            ctls.setReturningAttributes(new String[]{"sn", "mail", "displayName", "givenName"});
            ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            StringBuilder searchQuery = new StringBuilder("(&(objectclass=*) (|");
            Map<String, Recipient> mappedRecipients = new HashMap<String, Recipient>();
            List<Recipient> completedRecipients = new ArrayList<Recipient>();

            for (Recipient recipient : recipients) {
                searchQuery.append("(mail=").append(recipient.getId()).append(")");
                mappedRecipients.put(recipient.getId(), recipient);
            }
            searchQuery.append("))");
            NamingEnumeration<SearchResult> result = ctx.search(basePath, searchQuery.toString(), ctls);


            while (result.hasMoreElements()) {

                SearchResult entry = result.nextElement();

                Attributes attr = entry.getAttributes();
                String surname = attr.get("sn") != null ? attr.get("sn").get().toString() : "";
                String mail = attr.get("mail").get().toString();
                String displayName = attr.get("displayName") != null ? attr.get("displayName").get().toString() : "";
                String givenName = attr.get("givenName") != null ? attr.get("givenName").get().toString() : "";

                Recipient ret = mappedRecipients.get(mail).clone();
                ret.setGivenName(givenName);
                ret.setSurname(surname);
                ret.setName(displayName);
                ret.setMail(mail);
                completedRecipients.add(ret);
            }
            return completedRecipients;

        } catch (NamingException e) {
            // e.message might contain sensitive information
            logger.log(Level.SEVERE, "LDAP Error in Addressbook " + this.name + " while looking up user info");
            return recipients;
        }

    }

    private Recipient completeRecipient(Recipient recipient, DirContext ctx) {

        assert recipient != null;
        assert ctx != null;

        try {
            SearchControls ctls = new SearchControls();
            ctls.setReturningAttributes(new String[]{"sn", "mail", "displayName", "givenName"});
            ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            NamingEnumeration<SearchResult> result =
                    ctx.search(basePath, "(&(objectclass=*)(mail=" + recipient.getId() + "))", ctls);

            if (result.hasMoreElements()) {

                SearchResult entry = result.nextElement();
                Attributes attr = entry.getAttributes();
                String surname = attr.get("sn") != null ? attr.get("sn").get().toString() : "";
                String mail = attr.get("mail").get().toString();
                String displayName = attr.get("displayName") != null ? attr.get("displayName").get().toString() : "";
                String givenName = attr.get("givenName") != null ? attr.get("givenName").get().toString() : "";


                Recipient ret = recipient.clone();
                ret.setGivenName(givenName);
                ret.setSurname(surname);
                ret.setName(displayName);
                ret.setMail(mail);

                return ret;
            } else {
                return recipient;
            }

        } catch (NamingException e) {
            // e.message might contain sensitive information
            logger.log(Level.SEVERE, "LDAP Error in Addressbook " + this.name + " while looking up user info");
            return recipient;
        }

    }

    public Recipient completeRecipient(Recipient recipient) {

        if (recipient == null) {
            throw new IllegalArgumentException("Recipient must not be null");
        }

        try {
            DirContext ctx = openLDAPLookup();

            return completeRecipient(recipient, ctx);


        } catch (NamingException e) {
            logger.log(Level.SEVERE, "LDAP Error in Addressbook " + this.name + ": " + e.getMessage());
            return recipient;
        }
    }
}
