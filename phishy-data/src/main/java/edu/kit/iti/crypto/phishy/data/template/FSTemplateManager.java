package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.eclipse.jgit.transport.resolver.RepositoryResolver;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class FSTemplateManager implements TemplateManager {

    private final Path rootPath;

    public FSTemplateManager(Path rootPath) {

        if (!Files.isDirectory(rootPath)) {
            throw new IllegalArgumentException("rootPath must be a valid directory");
        }
        this.rootPath = rootPath;
    }

    public FSTemplateManager(String rootPath) {
        this(Paths.get(rootPath));
    }

    @Override
    public TemplateGroup getGroup(String name) throws NotFoundException {

        if (name.isEmpty()) {
            throw new NotFoundException();
        }

        Path groupPath = rootPath.resolve(name);
        if (!Files.isDirectory(groupPath)) {
            throw new NotFoundException();
        }

        return new FSTemplateGroup(groupPath);
    }

    @Override
    public boolean hasGroup(String name) {

        if (name.isEmpty()) {
            return false;
        }


        Path groupPath = rootPath.resolve(name);
        return Files.exists(groupPath);
    }

    @Override
    public TemplateGroup createGroup(String name) throws CreationException {

        Path groupPath = rootPath.resolve(name);
        try {
            Files.createDirectory(groupPath);

        } catch (IOException e) {
            throw new CreationException();
        }

        return new FSTemplateGroup(groupPath);
    }

    @Override
    public void deleteGroup(String name) throws DeletionException {

        Path groupPath = rootPath.resolve(name);

        try {
            Files.walk(groupPath)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (IOException e) {
            throw new DeletionException();
        }

    }

    @Override
    public Template getTemplate(TemplateDescription description) throws NotFoundException {

        TemplateGroup group = getGroup(description.getGroup());
        return group.getTemplate(description);

    }

    @Override
    public boolean hasTemplate(TemplateDescription description) {

        TemplateGroup group = null;
        try {
            group = getGroup(description.getGroup());
        } catch (NotFoundException e) {
            return false;
        }
        return group.hasTemplate(description);
    }

    @Override
    public RepositoryResolver<HttpServletRequest> getGitResolver() {

        return new NullTemplateRepositoryResolver();

    }

    @Override
    public void invalidateCache() {

        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "FSTemplateManager has no cache");
        // ignore here because we do not cache

    }

    @Override
    public Collection<TemplateGroup> listGroups() {

        try {
            return Files.list(rootPath)
                    .filter(file -> Files.isDirectory(file))
                    .map(FSTemplateGroup::new)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            return Collections.emptyList();
        }

    }
}
