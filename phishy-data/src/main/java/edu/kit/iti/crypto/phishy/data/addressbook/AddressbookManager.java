package edu.kit.iti.crypto.phishy.data.addressbook;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import java.util.Collection;

/**
 * Manages Addressbooks
 */
public interface AddressbookManager {

    /**
     * List all available Addressbooks
     *
     * @return Collection of Addressbooks known to this Manager
     */
    Collection<Addressbook> listAddressbooks();

    /**
     * Check if an addressbook matching this description is available
     *
     * @param description Addressbook's description to look for
     * @return true if addressbook is available; false otherwise
     */
    boolean hasAddressbook(AddressbookDescription description);

    /**
     * Return the addressbook matching the given description
     *
     * @param description Addressbook's description
     * @return Matching Addressbook
     * @throws NotFoundException if Addressbook is not available
     */
    Addressbook getAddressbook(AddressbookDescription description) throws NotFoundException;

}
