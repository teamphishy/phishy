package edu.kit.iti.crypto.phishy.data.addressbook;

import javax.naming.Context;
import javax.naming.Name;
import javax.naming.RefAddr;
import javax.naming.Reference;
import java.util.Enumeration;
import java.util.Hashtable;

public class LDAPAddressbookFactory extends AddressbookFactory {
    @Override
    protected Object getAddressbook(AddressbookCache cache, Object obj, Name name, Context nameCtx,
                                    Hashtable<?, ?> environment)
            throws Exception {

        Reference reference = (Reference) obj;
        Enumeration<RefAddr> references = reference.getAll();

        String bindUser = "";
        String bindSecret = "";
        String server = "";
        String userPattern = "{USER}";
        String basePath = "";
        String filter = "";
        int pageSize = 100;

        while (references.hasMoreElements()) {

            RefAddr address = references.nextElement();
            String type = address.getType();
            String content = (String) address.getContent();


            switch (type) {
                case "binduser":
                    bindUser = content;
                    break;
                case "bindsecret":
                    bindSecret = content;
                    break;
                case "server":
                    server = content;
                    break;
                case "userpattern":
                    userPattern = content;
                    break;
                case "basepath":
                    basePath = content;
                    break;
                case "filter":
                    filter = content;
                    break;
                case "pagesize":
                    pageSize = Integer.parseInt(content);
                    break;
                default:
                    break; // NOP
            }


        }

        return new LDAPAddressbook(name.get(0), cache, server, filter, basePath, userPattern, bindUser, bindSecret, pageSize);

    }
}
