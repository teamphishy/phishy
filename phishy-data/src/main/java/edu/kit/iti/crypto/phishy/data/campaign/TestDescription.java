package edu.kit.iti.crypto.phishy.data.campaign;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class TestDescription {
    private final CampaignDescription campaignDescription;
    private final String testID;

    private static final char SEPARATOR = '_';
    private static final String ID_REGEX = "[a-zA-Z0-9=]+" + SEPARATOR + "[a-zA-Z0-9-]+";
    private static final String UUID_REGEX = "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$";

    public TestDescription(CampaignDescription campaign, String test) {
        this.campaignDescription = campaign;
        this.testID = test;
    }

    public static TestDescription fromIdentifier(String identifier) {
        if (!identifier.matches(ID_REGEX)) {
            throw new IllegalArgumentException("Identifier has to be Base64 + \"" + SEPARATOR + "\" + UUID");
        }

        if (identifier.indexOf(SEPARATOR) != identifier.lastIndexOf(SEPARATOR)) {
            throw new IllegalArgumentException("The separator char can't exist more than once");
        }

        String[] splitted = identifier.split("_");

        if (!splitted[1].matches(UUID_REGEX)) {
            throw new IllegalArgumentException("Identifier has to be Base64 + \"" + SEPARATOR + "\" + UUID");
        }

        byte[] decodedCampaignNameBytes = Base64.getDecoder().decode(splitted[0]);
        String decodedCampaignName = null;
        decodedCampaignName = new String(decodedCampaignNameBytes, StandardCharsets.UTF_8);

        CampaignDescription campaignDescription = new CampaignDescription(decodedCampaignName);
        String testID = splitted[1];

        return new TestDescription(campaignDescription, testID);
    }

    public Campaign resolveCampaign(CampaignManager cman) throws NotFoundException {
        return cman.getCampaign(campaignDescription);
    }

    public OutstandingTest resolveTest(Campaign campaign) throws NotFoundException {
        return campaign.getOutstandingTest(testID);
    }

    public String toIdentifier() {
        String encodedCampaignName = new String(Base64.getEncoder().encode(campaignDescription.getName().getBytes()), StandardCharsets.UTF_8);
        return encodedCampaignName + SEPARATOR + testID;
    }

    public CampaignDescription getCampaignDescription() {
        return campaignDescription;
    }

    public String getTestID() {
        return testID;
    }
}
