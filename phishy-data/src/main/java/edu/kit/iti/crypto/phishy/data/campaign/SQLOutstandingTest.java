package edu.kit.iti.crypto.phishy.data.campaign;

import edu.kit.iti.crypto.phishy.data.statistics.SQLCampaignStatisticsData;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import javax.sql.DataSource;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class SQLOutstandingTest implements OutstandingTest {

    private static final String ATTRIBUTE_FORMAT = "^[a-zA-Z][a-zA-Z0-9_]*";
    private static final Pattern ATTRIBUTE_PATTERN = Pattern.compile(ATTRIBUTE_FORMAT);

    private final DataSource dataSource;
    private final CampaignDescription description;
    private final SQLCampaignStatisticsData statistics;
    private final String testID;
    private final Logger logger;

    public SQLOutstandingTest(String testID, CampaignDescription description, SQLCampaignStatisticsData statistics, DataSource dataSource) {
        this.testID = testID;
        this.description = description;
        this.statistics = statistics;
        this.dataSource = dataSource;
        logger = Logger.getLogger(this.getClass().getName());

        if (!hasSQLOutstandingTest()) {
            createSQLOutstandingTest();
        }
    }

    @Override
    public void increaseLoginAttempt() {
        int logins = getLoginAttempt() + 1;
        setSQLIntegerAttribute("loginAttempts", logins);
    }

    @Override
    public String getId() {
        return testID;
    }

    @Override
    public Date getDate() throws NotFoundException {
        return getSQLDate();
    }

    @Override
    public void delete() {
        final String query = "DELETE FROM OutstandingTest WHERE id = ?";
        try (Connection connection = this.dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, this.testID);
            statement.executeUpdate();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while removing OutstandingTest");
            throw new IllegalStateException(e);
        }
    }

    @Override
    public int getLoginAttempt() {
        return getSQLIntegerAttribute("loginAttempts");
    }

    private boolean hasSQLOutstandingTest() {
        final String query = "SELECT id FROM OutstandingTest WHERE id=?";
        try (Connection connection = this.dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, this.testID);
            ResultSet resultSet = statement.executeQuery();
            return resultSet.next();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while reading OutstandingTest id");
            throw new IllegalStateException(e);
        }
    }

    private void createSQLOutstandingTest() {
        final String query = "INSERT INTO OutstandingTest(id, campaign, date) VALUES(?,?,?)";
        try (Connection connection = this.dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, this.testID);
            statement.setString(2, this.description.getName());
            statement.setString(3, String.valueOf(System.currentTimeMillis()));
            statement.executeUpdate();
            //initially increase statistics
            statistics.increaseSentMails(1);

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while inserting into OutstandingTest");
            throw new IllegalStateException(e);
        }
    }

    private int getSQLIntegerAttribute(String attributeName) {
        if (!ATTRIBUTE_PATTERN.matcher(attributeName).matches()) {
            throw new IllegalArgumentException("Wrong attribute format");
        }

        final String query = "SELECT " + attributeName + " FROM OutstandingTest WHERE id=?";
        try (Connection connection = this.dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, this.testID);
            ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) {
                throw new IllegalStateException();
            }
            return resultSet.getInt(attributeName);

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while reading OutstandingTest attribute");
            throw new IllegalStateException(e);
        }
    }

    private void setSQLIntegerAttribute(String attributeName, int value) {
        if (!ATTRIBUTE_PATTERN.matcher(attributeName).matches()) {
            throw new IllegalArgumentException("Wrong attribute format");
        }

        final String query = "UPDATE OutstandingTest SET " + attributeName + "=? WHERE id=?";
        try (Connection connection = this.dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, value);
            statement.setString(2, this.testID);
            statement.executeUpdate();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while update OutstandingTest id");
            throw new IllegalStateException(e);
        }
    }

    private Date getSQLDate() throws NotFoundException {
        final String query = "SELECT date FROM OutstandingTest WHERE id=?";
        try (Connection connection = this.dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, this.testID);
            ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) {
                throw new NotFoundException();
            }
            return resultSet.getDate("date");

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while reading OutstandingTest date");
            throw new IllegalStateException(e);
        }
    }
}
