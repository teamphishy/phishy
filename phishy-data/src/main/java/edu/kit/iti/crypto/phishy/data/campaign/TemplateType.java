package edu.kit.iti.crypto.phishy.data.campaign;

public enum TemplateType {
    INFO_MAIL(1),
    INFO_WEBSITE(2),
    PHISH_MAIL(3),
    PHISH_WEBSITE(4);

    private int id;

    TemplateType(int id) {
        this.id = id;
    }

    public int toInt() {
        return id;
    }

    public static TemplateType fromInt(int id) {
        switch (id) {
            case 0:
                return INFO_MAIL;
            case 1:
                return INFO_WEBSITE;
            case 2:
                return PHISH_MAIL;
            case 3:
                return PHISH_WEBSITE;
            default:
                throw new IllegalArgumentException("invalid value");
        }
    }

    @Override
    public String toString() {
        switch (this) {
            case INFO_MAIL:
                return "infoMail";
            case INFO_WEBSITE:
                return "infoWebsite";
            case PHISH_MAIL:
                return "phishMail";
            case PHISH_WEBSITE:
                return "phishWebsite";
            default:
                throw new IllegalStateException("Invalid enum state");
        }
    }

    public static TemplateType fromString(String type) {
        switch (type.toLowerCase()) {
            case "infomail":
                return INFO_MAIL;
            case "infowebsite":
                return INFO_WEBSITE;
            case "phishmail":
                return PHISH_MAIL;
            case "phishywebsite":
                return PHISH_WEBSITE;
            default:
                throw new IllegalArgumentException("invalid value");
        }
    }

}
