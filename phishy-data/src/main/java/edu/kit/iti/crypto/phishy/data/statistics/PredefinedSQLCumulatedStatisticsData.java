package edu.kit.iti.crypto.phishy.data.statistics;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PredefinedSQLCumulatedStatisticsData implements CumulatedStatisticsData {

    private static final Logger logger = Logger.getLogger(PredefinedSQLCumulatedStatisticsData.class.getName());

    private final int sentMails;
    private final int reportedMails;
    private final int phishedMails;

    public PredefinedSQLCumulatedStatisticsData(int sentMails, int reportedMails, int phishedMails) {
        this.sentMails = sentMails;
        this.reportedMails = reportedMails;
        this.phishedMails = phishedMails;
    }

    @Override
    public int getSentMails() {
        return this.sentMails;
    }

    @Override
    public int getReportedMails() {
        return this.reportedMails;
    }

    @Override
    public int getPhishedMails() {
        return this.phishedMails;
    }

    public static CumulatedStatisticsData get(DataSource dataSource) {
        try (Connection con = dataSource.getConnection()) {
            PreparedStatement stmt = con.prepareStatement("SELECT SUM(\"sentMails\") AS sumSentMails, " +
                    "SUM(\"reportedMails\") AS sumReportedMails, SUM(\"phishedMails\") AS sumPhishedMails FROM Statistics");
            ResultSet resultSet = stmt.executeQuery();
            if (!resultSet.next()) {
                throw new IllegalStateException("Database has no statistics items");
            }

            return new PredefinedSQLCumulatedStatisticsData(resultSet.getInt("sumSentMails"),
                    resultSet.getInt("sumReportedMails"),
                    resultSet.getInt("sumPhishedMails"));
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while getting cumulated statistics");
            throw new IllegalStateException(e);
        }
    }
}
