package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GitTemplate implements Template {

    private final Repository repo;
    private final String templateName;
    private final String groupName;
    private final Logger logger;

    public GitTemplate(Repository repo, String groupName, String templateName) {
        this.repo = repo;
        this.groupName = groupName;
        this.templateName = templateName;
        this.logger = Logger.getLogger(this.getClass().getName());
    }

    @Override
    public String getName() {
        return templateName;
    }

    @Override
    public TemplateFile getFile(String path) throws NotFoundException {

        if (!hasFile(path)) {
            throw new NotFoundException();
        }

        return new GitTemplateFile(repo, templateName, path);

    }

    @Override
    public boolean hasFile(String path) {

        String fullPath = templateName + "/" + path;

        ObjectId lastCommit;
        try {
            lastCommit = repo.resolve("refs/heads/release");
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error accessing git repo: " + e.getMessage());
            return false;
        }

        try (RevWalk revWalk = new RevWalk(repo)) {
            RevCommit commit = revWalk.parseCommit(lastCommit);
            RevTree tree = commit.getTree();

            try (TreeWalk treeWalk = new TreeWalk(repo)) {
                treeWalk.addTree(tree);
                treeWalk.setRecursive(true);
                treeWalk.setFilter(PathFilter.create(fullPath));

                while (treeWalk.next()) {

                    if (fullPath.equals(treeWalk.getPathString())) {
                        revWalk.dispose();
                        return true;
                    }

                }

                revWalk.dispose();
                return false;

            }

        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error accessing git repo: " + e.getMessage());
            return false;
        }

    }

    @Override
    public TemplateDescription getDescription() {
        return new TemplateDescription(groupName, templateName);
    }
}
