package edu.kit.iti.crypto.phishy.data.template;

import org.eclipse.jgit.errors.RepositoryNotFoundException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.ServiceMayNotContinueException;
import org.eclipse.jgit.transport.resolver.RepositoryResolver;
import org.eclipse.jgit.transport.resolver.ServiceNotAuthorizedException;
import org.eclipse.jgit.transport.resolver.ServiceNotEnabledException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class GitTemplateRepositoryResolver implements RepositoryResolver {

    private Path basePath;

    public GitTemplateRepositoryResolver(Path basePath) {
        this.basePath = basePath;
    }

    @Override
    public Repository open(Object req, String name)
            throws RepositoryNotFoundException, ServiceNotAuthorizedException, ServiceNotEnabledException,
            ServiceMayNotContinueException {

        Path groupPath = basePath.resolve(name);
        Path gitDir = groupPath; //.resolve(".git");

        if (!Files.isDirectory(gitDir)) {
            throw new RepositoryNotFoundException("Repository " + name + " not found");
        }

        try {
            Repository repo = new FileRepository(gitDir.toFile());
            return repo;
        } catch (IOException e) {
            throw new RepositoryNotFoundException("Error accessing repository " + name);
        }
    }
}
