package edu.kit.iti.crypto.phishy.data.campaign;

import edu.kit.iti.crypto.phishy.data.statistics.*;
import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class SQLCampaignManager implements CampaignManager {

    private static final String NAME_FORMAT = "^[a-zA-Z][a-zA-Z0-9_]*$";
    private static final Pattern NAME_PATTERN = Pattern.compile(NAME_FORMAT);

    private final DataSource dataSource;
    private final Logger logger;

    public SQLCampaignManager(DataSource dataSource) {
        this.dataSource = dataSource;
        logger = Logger.getLogger(this.getClass().getName());
    }

    @Override
    public Campaign getCampaign(CampaignDescription description) throws NotFoundException {
        if (!hasSQLCampaign(description.getName())) {
            throw new NotFoundException();
        }
        return new SQLCampaign(description, dataSource);
    }

    @Override
    public boolean hasCampaign(CampaignDescription description) {
        return hasSQLCampaign(description.getName());
    }

    @Override
    public Collection<Campaign> listCampaigns() {
        return getSQLCampaigns();
    }

    @Override
    public Campaign createCampaign(String name) throws CreationException {

        if (!NAME_PATTERN.matcher(name).matches()) {
            throw new IllegalArgumentException("Name must match: " + NAME_FORMAT);
        }
        if (hasCampaign(new CampaignDescription(name))) {
            throw new CreationException();
        }
        return new SQLCampaign(name, dataSource);
    }

    @Deprecated
    @Override
    public Campaign createCampaign(String name, int recipientSetSize, int mailLifetime, int mailInterval) throws CreationException {
        if (!NAME_PATTERN.matcher(name).matches()) {
            throw new IllegalArgumentException("Name must match: " + NAME_FORMAT);
        }
        if (hasCampaign(new CampaignDescription(name))) {
            throw new CreationException();
        }
        SQLCampaign campaign = new SQLCampaign(name, dataSource);
        campaign.setRecipientSetSize(recipientSetSize);
        campaign.setMailLifetime(mailLifetime);
        campaign.setMailInterval(mailInterval);
        return campaign;
    }

    @Override
    public void deleteCampaign(CampaignDescription description) throws DeletionException {
        if (!hasCampaign(description)) {
            throw new DeletionException();
        }
        SQLCampaignStatisticsData.removeStatistic(description, dataSource);
        removeSQLCampaign(description.getName());
    }

    @Override
    public CumulatedStatisticsData getCumulatedStatistics() {
        return PredefinedSQLCumulatedStatisticsData.get(dataSource);
    }

    @Override
    public Collection<CampaignStatisticsData> getCampaignStatisticsData() {
        return PredefinedSQLCampaignStatisticsData.getAll(dataSource);
    }

    private boolean hasSQLCampaign(String name) {
        final String query = "SELECT id FROM Campaign WHERE id=?";
        try (Connection connection = this.dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();
            return resultSet.next();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while reading Campaign id");
            throw new IllegalStateException(e);
        }
    }

    private ArrayList<Campaign> getSQLCampaigns() {
        final String query = "SELECT id FROM Campaign";
        try (Connection connection = this.dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);

            ArrayList<Campaign> campaigns = new ArrayList<>();
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String id = resultSet.getString("id");

                SQLCampaign campaign = new SQLCampaign(id, dataSource);
                campaigns.add(campaign);
            }
            return campaigns;

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while reading Campaign id, recipientSetSize, status, mailLifetime, addressbook");
            throw new IllegalStateException(e);
        }
    }

    private void removeSQLCampaign(String name) {
        final String query = "DELETE FROM Campaign WHERE id=?";
        try (Connection connection = this.dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, name);
            statement.executeUpdate();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL error while deleting a Campaign");
            throw new IllegalStateException(e);
        }
    }
}