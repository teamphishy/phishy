package edu.kit.iti.crypto.phishy.data.campaign;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import java.util.Objects;

public class CampaignDescription {

    private static final String CAMPAIGN_FORMAT = "^[a-zA-Z][a-zA-Z0-9_]*$";

    private final String name;

    public CampaignDescription(String name) {
        if (name == null || !name.matches(CAMPAIGN_FORMAT)) {
            throw new IllegalArgumentException("Name must match: " + CAMPAIGN_FORMAT);
        }
        this.name = name;
    }

    public Campaign resolve(CampaignManager cman) throws NotFoundException {
        return cman.getCampaign(this);
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CampaignDescription that = (CampaignDescription) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
