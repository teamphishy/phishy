package edu.kit.iti.crypto.phishy.data.addressbook;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class CSVAddressbook implements Addressbook {

    private final String name;
    private final AddressbookCache cache;
    private final Path csvPath;
    private final Logger logger;

    public CSVAddressbook(String name, AddressbookCache cache, Path csvFile) {
        this.name = name;
        this.cache = cache;
        this.csvPath = csvFile;
        this.logger = Logger.getLogger(this.getClass().getName());
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Recipient> getRandomRecipients(int num, Date notTestedSince) {

        if (num < 0) {
            throw new IllegalArgumentException("Number must be greater than zero");
        }

        return cache.getRandomRecipients(num, notTestedSince)
                .stream()
                .map(this::completeRecipient)
                .collect(Collectors
                        .toList());

    }

    @Override
    public boolean checkCredentials(String name, String password) {
        return true; // accept all credentials
    }

    @Override
    public void recordTest(Collection<Recipient> recipients) {

        if (recipients == null) {
            throw new IllegalArgumentException("recipients must not be null");
        }

        Collection<Recipient> updatedRecipients = recipients.stream()
                .map(recipient -> {
                    Recipient updatedRecipient = recipient.clone();
                    updatedRecipient.setLatestContact(new Date());
                    updatedRecipient.setTestId(null);
                    return updatedRecipient;
                })
                .collect(Collectors.toList());

        cache.storeRecipients(updatedRecipients);
    }

    @Override
    public void updateCache() {

        try {
            CSVReader reader = new CSVReader(Files.newBufferedReader(csvPath));

            // first line is header
            String[] header = reader.readNext();
            Map<String, Integer> headerToIndex = translateHeaderToIndex(header);
            Collection<Recipient> recipients = new LinkedList<>();

            for (String[] line : reader) {

                String id = line[headerToIndex.get("id")];
                String mail = line[headerToIndex.get("mail")];
                String givenName = line[headerToIndex.get("givenname")];
                String surName = line[headerToIndex.get("surname")];

                Recipient recipient = new Recipient(id, mail, givenName + " " + surName);

                recipients.add(recipient);

            }

            cache.storeRecipients(recipients);

        } catch (CsvValidationException e) {
            logger.log(Level.SEVERE, "Invalid CSV File " + csvPath + "; " + e.getMessage());
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error reading " + csvPath + ": " + e.getMessage());
        }
    }

    private Map<String, Integer> translateHeaderToIndex(String[] header) {

        assert header != null;

        Map<String, Integer> map = new HashMap<>();

        for (int i = 0; i < header.length; ++i) {
            map.put(header[i].toLowerCase(), i);
        }

        return map;

    }

    public Recipient completeRecipient(Recipient recipient) {

        if (recipient == null) {
            throw new IllegalArgumentException("recipient must not be null");
        }

        try {
            CSVReader reader = new CSVReader(Files.newBufferedReader(csvPath));

            String[] header = reader.readNext();
            Map<String, Integer> headerToIndex = translateHeaderToIndex(header);

            for (String[] line : reader) {

                String id = line[headerToIndex.get("id")];

                if (!recipient.getId().equals(id)) {
                    continue;
                }

                recipient.setMail(line[headerToIndex.get("mail")]);
                recipient.setGivenName(line[headerToIndex.get("givenname")]);
                recipient.setSurname(line[headerToIndex.get("surname")]);
                recipient.setName(line[headerToIndex.get("givenname")] + " " + line[headerToIndex.get("surname")]);

                return recipient;

            }

        } catch (CsvValidationException e) {
            logger.log(Level.SEVERE, "Invalid CSV File " + csvPath + "; " + e.getMessage());
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error reading " + csvPath + ": " + e.getMessage());
        }

        return recipient;
    }
}
