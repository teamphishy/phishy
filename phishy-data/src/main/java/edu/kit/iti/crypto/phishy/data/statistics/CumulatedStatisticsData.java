package edu.kit.iti.crypto.phishy.data.statistics;

/**
 * Represents a statistic item holding cumulative information on send, reported and phished mails.
 */
public interface CumulatedStatisticsData extends StatisticsData {

}
