package edu.kit.iti.crypto.phishy.data.addressbook;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;

/**
 * Capsulated description neccessary to identify an addressbook
 * This class is used to reference Addressbooks from different program parts.
 * An actual Addressbook instance is retrieved from an AddressbookManager
 */
public class AddressbookDescription {

    private static final String NAME_TEMPLATE = "[a-zA-Z][a-zA-Z0-9\\.\\-]*";
    private final String name;

    /**
     * Create a description from an addressbook's name
     *
     * @param name
     */
    public AddressbookDescription(String name) {

        if (name == null || !name.matches(NAME_TEMPLATE)) {
            throw new IllegalArgumentException("Addressbook Name invalid");
        }

        this.name = name;
    }

    public String getName() {
        return name;
    }

    /**
     * Find an actual Addressbook instance with this description in an AddressbookManager
     *
     * @param addressbookManager Manager used to look up this addressbook
     * @return Addressbook instance matching this description
     * @throws NotFoundException if no matching Addressbook was found
     */
    public Addressbook resolve(AddressbookManager addressbookManager) throws NotFoundException {
        assert addressbookManager != null;
        return addressbookManager.getAddressbook(this);
    }
}
