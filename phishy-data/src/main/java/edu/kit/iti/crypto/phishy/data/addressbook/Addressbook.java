package edu.kit.iti.crypto.phishy.data.addressbook;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Represents an Addressbook containing Recipients
 * <p>
 * Addressbooks might cache their Recipient's ids and information
 * that cannot be stored in the original source.
 * Addressbooks MUST NEVER store a testID
 */
public interface Addressbook {

    /**
     * @return Addressbook's name
     */
    String getName();

    /**
     * Get a random set of recipients not contacted since `notTestedSince`
     * This returns at max `num` recipients but might return less
     *
     * @param num            Max number of recipients to return
     * @param notTestedSince Returned Recipients must not be contacted since this date
     * @return list of recipients
     */
    List<Recipient> getRandomRecipients(int num, Date notTestedSince);

    /**
     * Check whether the provided credentials are valid for this addressbook.
     * If no actual check is performed, this function should return `true`
     *
     * @param name     Username
     * @param password User's password
     * @return true if authentication was successful or addressbook does not
     * perform one. false otherwise and on error
     */
    boolean checkCredentials(String name, String password);

    /**
     * Record the current date as lastContactedDate for the given set of recipients
     *
     * @param recipients Recipients to update lastContactedDate
     */
    void recordTest(Collection<Recipient> recipients);

    /**
     * Update this addressbook's cache
     * Might do nothing if addressbook does not use a cache
     */
    void updateCache();

    /**
     * Complete a given recipient with the data from this addressbook's source
     * and the cache (if any used)
     *
     * @param recipient Incomplete recipient (id must be set)
     * @return Completed Recipient if possible; input otherwise
     */
    Recipient completeRecipient(Recipient recipient);


}
