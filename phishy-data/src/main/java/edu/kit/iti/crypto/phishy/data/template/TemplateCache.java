package edu.kit.iti.crypto.phishy.data.template;

import java.util.LinkedHashMap;
import java.util.Map;

public class TemplateCache {

    private static final int NUM_ENTRIES = 10;
    Map<String, byte[]> cache = new LinkedHashMap<>(NUM_ENTRIES, .75f) {
        @Override
        protected boolean removeEldestEntry(Map.Entry<String, byte[]> eldest) {
            return this.size() >= NUM_ENTRIES;
        }
    };

    public synchronized byte[] fetchItem(TemplateDescription description, String path) {

        return cache.getOrDefault(description.toString() + "/" + path, null);

    }

    public synchronized void putItem(TemplateDescription description, String path, byte[] data) {

        cache.put(description.toString() + "/" + path, data);

    }

    public void invalidate() {
        cache.clear();
    }


}
