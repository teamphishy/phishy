package edu.kit.iti.crypto.phishy.data.template;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class CachingTemplateFile implements TemplateFile {

    private static final int MAX_CACHE_SIZE = 2048;

    private final TemplateDescription templateDescription;
    private final TemplateFile originalFile;
    private final TemplateCache cache;

    public CachingTemplateFile(TemplateCache cache, TemplateDescription templateDescription, TemplateFile originalFile) {
        this.originalFile = originalFile;
        this.cache = cache;
        this.templateDescription = templateDescription;
    }

    @Override
    public InputStream getStream() {

        byte[] cachedFile = cache.fetchItem(templateDescription, getVirtualPath());
        if (cachedFile == null) {
            // Load directly
            if (getEstimatedSize() < MAX_CACHE_SIZE) {
                // load and cache
                try {
                    byte[] data = originalFile.getStream().readAllBytes();
                    cache.putItem(templateDescription, getVirtualPath(), data);
                    return new ByteArrayInputStream(data);
                } catch (IOException e) {
                    return InputStream.nullInputStream();
                }

            } else {
                return originalFile.getStream();
            }

        } else {
            return new ByteArrayInputStream(cachedFile);
        }

    }

    @Override
    public String getVirtualPath() {
        return originalFile.getVirtualPath();
    }

    @Override
    public String getMimeType() {
        return originalFile.getMimeType();
    }

    @Override
    public long getEstimatedSize() {
        return originalFile.getEstimatedSize();
    }
}
