package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.eclipse.jgit.transport.resolver.RepositoryResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

public interface TemplateManager {

    TemplateGroup getGroup(String name) throws NotFoundException;

    boolean hasGroup(String name);

    TemplateGroup createGroup(String name) throws CreationException;

    void deleteGroup(String name) throws DeletionException;

    Collection<TemplateGroup> listGroups();

    Template getTemplate(TemplateDescription description) throws NotFoundException;

    boolean hasTemplate(TemplateDescription description);

    RepositoryResolver<HttpServletRequest> getGitResolver();

    void invalidateCache();


}
