package edu.kit.iti.crypto.phishy.data;

import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookCache;
import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookManager;
import edu.kit.iti.crypto.phishy.data.addressbook.JNDIAddressbookManager;
import edu.kit.iti.crypto.phishy.data.addressbook.SQLAddressbookCache;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignManager;
import edu.kit.iti.crypto.phishy.data.campaign.SQLCampaignManager;
import edu.kit.iti.crypto.phishy.data.template.CachingTemplateManager;
import edu.kit.iti.crypto.phishy.data.template.GitTemplateManager;
import edu.kit.iti.crypto.phishy.data.template.TemplateCache;
import edu.kit.iti.crypto.phishy.data.template.TemplateManager;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Provides Managers, that can be configured through JNDI
 */

public abstract class ManagerFactory {

    private static CampaignManager campaignManager;
    private static TemplateManager templateManager;
    private static AddressbookManager addressbookManager;
    private static AddressbookCache addressbookCache;

    /**
     * returns a global CampaignManager
     *
     * @return the global CampaignManager
     */
    public static synchronized CampaignManager getCampaignManager() {
        if (campaignManager == null) {
            try {
                InitialContext initContext = new InitialContext();
                campaignManager = (CampaignManager) initContext.lookup("java:comp/env/comp/campaignManager");

            } catch (NamingException e) {
                // not found -> create it
                try {
                    InitialContext initContext = new InitialContext();
                    DataSource database = (DataSource) initContext.lookup("java:comp/env/jdbc/phishy");
                    campaignManager = getCampaignManager(database);

                } catch (NamingException ex) {

                    Logger.getLogger(CampaignManager.class.getName())
                            .log(Level.SEVERE, "No DataSource found: " + ex.getMessage());
                    throw new IllegalStateException();

                }

            }

        }
        return campaignManager;
    }

    /**
     * returns a global TemplateManager
     *
     * @return the global TemplateManager
     */
    public static synchronized TemplateManager getTemplateManager() {
        if (templateManager == null) {
            try {
                InitialContext initContext = new InitialContext();
                templateManager = (TemplateManager) initContext.lookup("java:comp/env/comp/templateManager");
            } catch (NamingException e) {

                try {
                    InitialContext initContext = new InitialContext();
                    String templateDirectoryPath = (String) initContext.lookup("java:comp/env/phishy/templates");
                    Path templateDirectory = Paths.get(templateDirectoryPath);
                    templateManager = getTemplateManager(templateDirectory);
                } catch (NamingException ex) {
                    Logger.getLogger(CampaignManager.class.getName())
                            .log(Level.SEVERE, "No Templates info found: " + ex.getMessage());
                    throw new IllegalStateException();
                }
            }

        }
        return templateManager;
    }

    /**
     * returns a global AddressbookManager
     *
     * @return global AddressbookManager
     */
    public static synchronized AddressbookManager getAddressbookManager() {
        if (addressbookManager == null) {
            try {
                Context initContext = new InitialContext();
                addressbookManager = (AddressbookManager) initContext.lookup("java:comp/env/comp/addressbookManager");

            } catch (NamingException e) {
                addressbookManager = getJNDIAddressbookManager("java:comp/env/phishy/addressbooks");
            }

        }
        return addressbookManager;
    }

    /**
     * Gibt den globalen Empfängercach zurück; Falls noch kein Empfängercach existiert
     * oder in JNDI verfügbar ist, wird eine Instanz der Standardimplementierung
     * zurückgegeben.
     *
     * @return global AddressbookCache
     */
    public static synchronized AddressbookCache getAddressbookCache() {
        if (addressbookCache == null) {
            try {
                InitialContext initContext = new InitialContext();
                addressbookCache = (AddressbookCache) initContext.lookup("java:comp/env/comp/addressbookCache");

            } catch (NamingException e) {

                try {
                    InitialContext initContext = new InitialContext();
                    DataSource database = (DataSource) initContext.lookup("java:comp/env/jdbc/phishy");
                    addressbookCache = new SQLAddressbookCache(database);
                } catch (NamingException ex) {
                    Logger.getLogger(CampaignManager.class.getName())
                            .log(Level.SEVERE, "No DataSource found: " + ex.getMessage());
                    throw new IllegalStateException();
                }
            }

        }
        return addressbookCache;
    }


    /**
     * Gibt eine Instanz der Standardimplementierung eines Kampagnenmanager für die
     * gegebene Datenbank zurück.
     *
     * @param database the DataSource used for this CampaignManager
     * @return CampaignManager on database
     */
    private static synchronized CampaignManager getCampaignManager(DataSource database) {
        return new SQLCampaignManager(database);
    }


    /**
     * Gibt eine Instanz der Standardimplementierung eines Adressbuchmanagers für den
     * gegebenen JNDI-Pfad zurück.
     *
     * @param path JNDI Path where Addressbooks are listed
     * @return AddressbookManager on path
     */
    private static synchronized AddressbookManager getJNDIAddressbookManager(String path) {
        return new JNDIAddressbookManager(path);
    }


    /**
     * Gibt eine Instanz der Standardimplementierung eines Vorlagenmanagers für das
     * gegebene Verzeichnis zurück.
     *
     * @param path Filesystem path where templates are located
     * @return TemplateManager for path
     */
    private static TemplateManager getTemplateManager(Path path) {
        GitTemplateManager gitTemplateManager = new GitTemplateManager(path);
        TemplateCache templateCache = new TemplateCache();
        return new CachingTemplateManager(templateCache, gitTemplateManager);
    }

    public static void reset() {
        campaignManager = null;
        templateManager = null;
        addressbookManager = null;
        addressbookCache = null;
    }

}
