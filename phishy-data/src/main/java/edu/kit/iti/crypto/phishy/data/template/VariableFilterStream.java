package edu.kit.iti.crypto.phishy.data.template;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

/**
 * Replace variables in given stream
 */
public class VariableFilterStream extends FilterInputStream {

    private enum Source {
        STREAM, // Directly return next bytes from stream
        BUFFER // return from buffer
    }


    private static final int BUFSIZE = 255;

    private final InputStream originalStream;
    private final Collection<Variable> variables;
    private byte[] buffer = null;
    private int currentBufferLength = 0;
    private int currentBufferPointer = 0;

    private Source source = Source.STREAM;

    public VariableFilterStream(InputStream stream, Collection<Variable> variables) {
        super(stream);
        this.originalStream = stream;
        this.variables = variables;
    }

    @Override
    public boolean markSupported() {
        return false;
    }

    @Override
    public synchronized void mark(int readlimit) {
        throw new UnsupportedOperationException("Mark is not supported on this stream");
    }

    @Override
    public synchronized void reset() throws IOException {
        throw new IOException("Reset is not supported on this stream");
    }

    @Override
    public int read() throws IOException {

        if (source == Source.STREAM) {
            // read next bytes directly from stream

            int nextByte = originalStream.read();
            if (nextByte == -1) {
                return -1;
            } // end of stream

            if (nextByte == '$') { // start of variable
                // buffer next bytes
                bufferVariable();
                // replace variable in buffered bytes
                replaceVariable();

                // switch source to buffer if buffer is filled
                if (currentBufferLength > 0) {
                    source = Source.BUFFER;
                }
                // return next byte
                return read();
            } else {
                return nextByte;
            }

        } else if (source == Source.BUFFER) {
            // return from buffer

            int nextByte = buffer[currentBufferPointer];
            ++currentBufferPointer;

            // switch to stream source if buffer was read
            if (currentBufferPointer >= currentBufferLength) {
                source = Source.STREAM;
            }

            return nextByte;
        }

        return -1;

    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {

        if (b == null) {
            throw new NullPointerException();
        }
        if (off < 0 || len < 0 || len > b.length - off) {
            throw new IndexOutOfBoundsException();
        }

        if (len == 0) {
            return 0;
        }

        // read len bytes and put them in b array
        int count = 0;
        for (int j = 0; j < len; ++j) {
            int charByte = this.read();
            if (charByte == -1) {
                return count == 0 ? -1 : count;
            }
            b[off + j] = (byte) charByte;
            ++count;
        }

        return count;

    }

    @Override
    public long skip(long n) throws IOException {

        // skip n bytes
        int count = 0;
        for (int i = 0; i < n; ++i) {
            if (this.read() == -1) {
                return count;
            }
            ++count;
        }

        return count;

    }


    /**
     * Buffer until end of stream or end of variable: '}' or non-ascii byte
     *
     * @throws IOException on stream read exception
     */
    private void bufferVariable() throws IOException {

        // allocate a buffer; variable names may not be longer than BUFSIZE
        buffer = new byte[BUFSIZE];
        currentBufferLength = 1; // buffer is empty
        currentBufferPointer = 0; // point to beginning

        // fill buffer
        buffer[0] = '$';
        for (int i = 1; i < BUFSIZE; ++i) {
            int nextByte = originalStream.read();
            if (nextByte == -1) {
                return; // No more bytes
            }

            buffer[i] = (byte) nextByte;
            ++currentBufferLength;

            if (nextByte == '}') {
                // end of variable; stop filling buffer
                return;
            } else if (nextByte >= 128) {
                // out of ascii range; this is not a variable
                return;
            }

        }

    }

    /**
     * Replace the named variable in buffer if found in variables list
     */
    private void replaceVariable() {

        assert buffer != null;

        String bufferedString = new String(buffer, 0, currentBufferLength);
        if (bufferedString.length() < 4) {
            return;
        }
        String variableName = bufferedString.substring(2, bufferedString.length() - 1);

        for (Variable variable : variables) {
            if (variableName.equals(variable.getName())) {
                buffer = variable.getValue().getBytes(StandardCharsets.UTF_8); // set buffer to variable's value
                currentBufferLength = variable.getValue().getBytes(StandardCharsets.UTF_8).length; // set buffer length correctly
                currentBufferPointer = 0; // reset pointer to beginning of buffer
                return;
            }
        }

    }

}
