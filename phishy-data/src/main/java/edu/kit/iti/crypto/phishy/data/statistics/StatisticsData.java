package edu.kit.iti.crypto.phishy.data.statistics;

/**
 * Represents a statistic item holding information on send, reported and phished mails.
 */
public interface StatisticsData {

    /**
     * Gets the statistic for number of sent mails.
     *
     * @return number of send mails in this statistic item,
     */
    int getSentMails();

    /**
     * Gets the statistic for number of reported mails.
     *
     * @return number of reported mails in this statistic item,
     */
    int getReportedMails();

    /**
     * Gets the statistic for number of phished mails.
     *
     * @return number of phished mails in this statistic item,
     */
    int getPhishedMails();

}
