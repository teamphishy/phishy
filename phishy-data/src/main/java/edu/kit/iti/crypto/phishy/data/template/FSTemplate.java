package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

public class FSTemplate implements Template {

    private final TemplateGroup group;
    private final String name;
    private final Path basePath;

    public FSTemplate(Path groupPath, String name, TemplateGroup group) {
        assert groupPath != null;
        assert name != null;
        assert group != null;

        this.basePath = groupPath.resolve(name);

        if (!Files.isDirectory(basePath)) {
            throw new IllegalArgumentException("Template dir must exist");
        }

        this.name = name;
        this.group = group;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public TemplateFile getFile(String path) throws NotFoundException {

        if (path == null || path.isEmpty()) {
            throw new IllegalArgumentException("Invalid path");
        }

        Path filePath = this.basePath.resolve(path.replace('/', File.separatorChar));
        if (!Files.exists(filePath)) {
            throw new NotFoundException();
        }
        return new FSTemplateFile(this.basePath, filePath);
    }

    @Override
    public boolean hasFile(String path) {

        if (path == null || path.isEmpty()) {
            throw new IllegalArgumentException("Invalid path");
        }

        Path filePath = this.basePath.resolve(path);
        return Files.exists(filePath);
    }

    @Override
    public TemplateDescription getDescription() {
        return new TemplateDescription(group.getName(), this.getName());
    }
}
