package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

public class FSTemplateGroup implements TemplateGroup {

    private final Path groupPath;
    private final String name;

    public FSTemplateGroup(Path groupPath) {

        if (!Files.isDirectory(groupPath)) {
            throw new IllegalArgumentException("groupPath must be a valid directory");
        }

        this.groupPath = groupPath;
        this.name = groupPath.getFileName().toString();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Template getTemplate(TemplateDescription description) throws NotFoundException {

        assert description != null;

        if (!description.getGroup().equals(this.getName())) {
            throw new IllegalArgumentException("Requested not a template for this group");
        }

        if (description.getName().isEmpty()) {
            throw new NotFoundException();
        }

        Path templatePath = groupPath.resolve(description.getName());
        if (!Files.isDirectory(templatePath)) {
            throw new NotFoundException();
        }

        return new FSTemplate(groupPath, description.getName(), this);
    }

    @Override
    public boolean hasTemplate(TemplateDescription description) {

        assert description != null;

        if (!description.getGroup().equals(this.getName())) {
            throw new IllegalArgumentException("Requested not a template for this group");
        }

        if (description.getName().isEmpty()) {
            return false;
        }

        Path templatePath = groupPath.resolve(description.getName());
        return Files.exists(templatePath);
    }

    @Override
    public Collection<Template> listTemplates() {

        try {
            return Files.list(groupPath)
                    .filter(file -> Files.isDirectory(file))
                    .map(file -> new FSTemplate(groupPath, file.getFileName().toString(), this))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            return Collections.emptyList();
        }

    }
}
