package edu.kit.iti.crypto.phishy.data.campaign;

import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookDescription;
import edu.kit.iti.crypto.phishy.data.addressbook.Recipient;
import edu.kit.iti.crypto.phishy.data.statistics.CampaignStatisticsData;
import edu.kit.iti.crypto.phishy.data.template.TemplateDescription;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.MutationException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import java.util.Collection;
import java.util.List;

/**
 * encapsulates all features a campaign has to offer
 */
public abstract class Campaign {

    /**
     * determines whether a Campaign is ready to be started
     *
     * @return true when all four template types are set
     */
    public boolean isSetUp() {
        try {
            return hasTemplate(TemplateType.INFO_MAIL) && hasTemplate(TemplateType.INFO_WEBSITE) &&
                    hasTemplate(TemplateType.PHISH_MAIL) && hasTemplate(TemplateType.PHISH_WEBSITE) &&
                    getAddressbook() != null;
        } catch (NotFoundException e) {
            return false;
        }
    }

    /**
     * links a given TemplateType and TemplateDescription to the Campaign.
     * Overwrites an existing link
     *
     * @param type        the type of template
     * @param description the TemplateDescription of the template
     */
    public abstract void linkTemplate(TemplateType type, TemplateDescription description);

    /**
     * unlinks a given TemplateType and TemplateDescription from the Campaign
     *
     * @param type the type of template
     * @throws DeletionException when TemplateType does not exist
     */
    public abstract void unlinkTemplate(TemplateType type) throws DeletionException;

    /**
     * returns the TemplateDescription for a given TemplateType
     *
     * @param type the TemplateType
     * @return the TemplateDescription
     */
    public abstract TemplateDescription getTemplate(TemplateType type) throws NotFoundException;

    /**
     * determines whether a given TemplateType is set
     *
     * @param type the TemplateType
     * @return true when the TemplateType is set
     */
    public abstract boolean hasTemplate(TemplateType type);

    /**
     * returns all statistics for the Campaign
     *
     * @return the StatisticsData object
     */
    public abstract CampaignStatisticsData getStatistics();

    /**
     * finds the OutstandingTest for a given id
     *
     * @param id the id of the OutstandingTest
     * @return the matching OutstandingTest
     * @throws NotFoundException when no OutstandingTest for the given id was found
     */
    public abstract OutstandingTest getOutstandingTest(String id) throws NotFoundException;

    /**
     * determines whether an OutstandingTest with the given id exists for this Campaign
     *
     * @param id the id of the OutstandingTest
     * @return true if there is an OutstandingTest for the given id
     */
    public abstract boolean hasOutstandingTest(String id);

    /**
     * returns the associated AddressbookDescription
     *
     * @return the AddressbookDescription
     * @throws NotFoundException when no Addressbook is found
     */
    public abstract AddressbookDescription getAddressbook() throws NotFoundException;

    /**
     * @param description the description of the new Addressbook
     */
    public abstract void setAddressbook(AddressbookDescription description);

    /**
     * marks the OutstandingTest for a given set of recipients as sent
     *
     * @param tests the recipients that should be marked as sent
     */
    public abstract void recordTests(Collection<TestDescription> tests);

    /**
     * deletes the OutstandingTest for a given set of recipients
     * filters recipients that didn't had an OutstandingTest
     *
     * @param recipients the tests in recipients that should be marked as phished or reported
     * @return the recipients with the tests that were deleted
     */
    public abstract List<Recipient> deleteTests(List<Recipient> recipients);

    /**
     * @return the name of the Campaign
     */
    public abstract String getName();

    /**
     * @return the amount of recipients in a set
     */
    public abstract int getRecipientSetSize();

    /**
     * @param recipientSetSize the new amount of recipients in a set
     */
    public abstract void setRecipientSetSize(int recipientSetSize);

    /**
     * @return the current status of the Campaign
     */
    public abstract CampaignStatus getStatus();

    /**
     * @param status the new status of the Campaign
     */
    public abstract void setStatus(CampaignStatus status) throws MutationException;

    /**
     * @return the amount of time in which the phishing mail is valid
     */
    public abstract int getMailLifetime();

    /**
     * @param mailLifetime the new amount of time in which the phishing mail is valid
     */
    public abstract void setMailLifetime(int mailLifetime);

    /**
     * @return the amount of time in after which the recipient can receive a new phishing mail
     */
    public abstract int getMailInterval();

    /**
     * @param mailInterval the new amount of time in after which the recipient can receive a new phishing mail
     */
    public abstract void setMailInterval(int mailInterval);
}
