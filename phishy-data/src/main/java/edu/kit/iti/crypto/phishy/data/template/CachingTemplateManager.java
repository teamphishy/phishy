package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.exception.CreationException;
import edu.kit.iti.crypto.phishy.exception.DeletionException;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.eclipse.jgit.transport.resolver.RepositoryResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CachingTemplateManager implements TemplateManager {

    private final TemplateCache cache;
    private final TemplateManager manager;

    public CachingTemplateManager(TemplateCache cache, TemplateManager manager) {
        this.cache = cache;
        this.manager = manager;
    }

    public TemplateGroup getGroup(String name) throws NotFoundException {

        TemplateGroup group = manager.getGroup(name);
        return new CachingTemplateGroup(cache, group);

    }

    public boolean hasGroup(String name) {
        return manager.hasGroup(name);
    }

    public TemplateGroup createGroup(String name) throws CreationException {
        TemplateGroup group = manager.createGroup(name);
        return new CachingTemplateGroup(cache, group);
    }

    public void deleteGroup(String name) throws DeletionException {
        manager.deleteGroup(name);
    }

    public Template getTemplate(TemplateDescription description) throws NotFoundException {

        Template template = manager.getTemplate(description);
        return new CachingTemplate(cache, template);

    }

    public boolean hasTemplate(TemplateDescription description) {
        return manager.hasTemplate(description);
    }

    public RepositoryResolver<HttpServletRequest> getGitResolver() {
        return manager.getGitResolver();
    }

    public void invalidateCache() {

        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Invalidating cache of CachingTemplateManager");
        cache.invalidate();
    }

    @Override
    public Collection<TemplateGroup> listGroups() {
        return manager.listGroups();
    }
}
