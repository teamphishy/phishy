package edu.kit.iti.crypto.phishy.data.campaign;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import java.util.Date;

/**
 * Every sent phishing mail is managed by an OutstandingTest
 */
public interface OutstandingTest {

    /**
     * increases the amount of login attempts by one and writes the change into the database
     */
    void increaseLoginAttempt() throws NotFoundException;

    /**
     * @return the unique id of the test
     */
    String getId();

    /**
     * @return the date on which the test is canceled
     */
    Date getDate() throws NotFoundException;

    /**
     * @return the number of login attempts by the recipient
     */
    int getLoginAttempt() throws NotFoundException;

    /**
     * Remove this OutstandingTest and it's associated data
     */
    void delete();
}
