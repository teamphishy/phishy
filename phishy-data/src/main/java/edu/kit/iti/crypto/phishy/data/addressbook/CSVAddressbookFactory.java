package edu.kit.iti.crypto.phishy.data.addressbook;

import javax.naming.Context;
import javax.naming.Name;
import javax.naming.RefAddr;
import javax.naming.Reference;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.Hashtable;

public class CSVAddressbookFactory extends AddressbookFactory {

    @Override
    protected Object getAddressbook(AddressbookCache cache, Object obj, Name name, Context nameCtx,
                                    Hashtable<?, ?> environment)
            throws Exception {

        Reference reference = (Reference) obj;
        Enumeration<RefAddr> references = reference.getAll();

        String path = "";

        while (references.hasMoreElements()) {

            RefAddr address = references.nextElement();
            String type = address.getType();
            String content = (String) address.getContent();


            if ("path".equals(type)) {
                path = content;
            }


        }

        assert !path.isEmpty();

        return new CSVAddressbook(name.get(0), cache, Paths.get(path));

    }

}
