package edu.kit.iti.crypto.phishy.data.addressbook;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Date;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SQLAddressbookCache implements AddressbookCache {

    private DataSource dataSource;
    private Logger logger;

    public SQLAddressbookCache(DataSource dataSource) {
        this.dataSource = dataSource;
        this.logger = Logger.getLogger(this.getClass().getName());
    }

    @Override
    public void storeRecipients(Collection<Recipient> recipients) {

        Collection<Recipient> existingRecipients = getExistingRecipients(recipients);

        try (Connection con = dataSource.getConnection()) {
            con.createStatement().execute("BEGIN TRANSACTION;");
            PreparedStatement stmtUpdate = con.prepareStatement("UPDATE Recipients SET lastTest = ? WHERE id = ?");
            PreparedStatement stmtInsert = con.prepareStatement("INSERT INTO Recipients (id, lastTest) VALUES (?, ?)");

            for (Recipient recipient : recipients) {
                Date latestContact = recipient.getLatestContact();

                if (existingRecipients.contains(recipient)) {
                    if (latestContact == null) {
                        stmtUpdate.setNull(1, Types.DATE);
                    } else {
                        stmtUpdate.setDate(1, new java.sql.Date(latestContact.getTime()));
                    }
                    stmtUpdate.setString(2, recipient.getId());
                    stmtUpdate.executeUpdate();

                } else {
                    stmtInsert.setString(1, recipient.getId());
                    if (latestContact == null) {
                        stmtInsert.setNull(2, Types.DATE);
                    } else {
                        stmtInsert.setDate(2, new java.sql.Date(latestContact.getTime()));
                    }
                    stmtInsert.executeUpdate();
                }
            }
            con.createStatement().execute("COMMIT;");
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Cannot access Database: " + e.getMessage());
            throw new IllegalStateException(e);
        }

    }

    private Collection<Recipient> getExistingRecipients(Collection<Recipient> recipients) {

        assert recipients != null;

        Collection<Recipient> existingRecipients = new ArrayList<>();
        try (Connection con = dataSource.getConnection()) {
            PreparedStatement stmt = con.prepareStatement("SELECT id FROM Recipients WHERE id = ?");
            for (Recipient recipient : recipients) {
                stmt.setString(1, recipient.getId());
                ResultSet resultSet = stmt.executeQuery();
                if (resultSet.next()) {
                    // there is at least one matching row
                    existingRecipients.add(recipient);
                }
            }

            return existingRecipients;

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Cannot access Database: " + e.getMessage());
            throw new IllegalStateException(e);
        }

    }

    @Override
    public List<Recipient> getRandomRecipients(int num, Date notTestedSince) {

        List<Recipient> recipients = new LinkedList<>();
        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement stmt = conn.prepareStatement("SELECT id FROM Recipients WHERE lastTest <= ? OR lastTest IS NULL ORDER BY RANDOM() LIMIT " + num);
            stmt.setDate(1, new java.sql.Date(notTestedSince.getTime()));
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                recipients.add(new Recipient(result.getString("id")));
            }

            return recipients;

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Cannot access Database: " + e.getMessage());
            throw new IllegalStateException(e);
        }

    }

    @Override
    public Recipient completeRecipient(Recipient recipient) {

        try (Connection con = dataSource.getConnection()) {
            PreparedStatement stmt = con.prepareStatement("SELECT id, lastTest FROM Recipients WHERE id = ?");
            stmt.setString(1, recipient.getId());

            ResultSet resultSet = stmt.executeQuery();
            Recipient resultRecipient = recipient.clone();

            java.sql.Date lastContact = resultSet.getDate("lastTest");
            resultRecipient.setLatestContact(lastContact);

            return resultRecipient;

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Cannot access Database: " + e.getMessage());
            throw new IllegalStateException(e);
        }
    }
}
