package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import java.util.Collection;

public class CachingTemplateGroup implements TemplateGroup {

    private final TemplateCache cache;
    private final TemplateGroup group;

    public CachingTemplateGroup(TemplateCache cache, TemplateGroup group) {
        this.cache = cache;
        this.group = group;
    }

    @Override
    public String getName() {
        return group.getName();
    }

    @Override
    public Template getTemplate(TemplateDescription description) throws NotFoundException {

        Template template = group.getTemplate(description);
        return new CachingTemplate(cache, template);

    }

    @Override
    public boolean hasTemplate(TemplateDescription description) {
        return group.hasTemplate(description);
    }

    @Override
    public Collection<Template> listTemplates() {
        return group.listTemplates();
    }
}
