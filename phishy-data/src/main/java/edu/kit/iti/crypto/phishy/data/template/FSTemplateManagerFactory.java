package edu.kit.iti.crypto.phishy.data.template;

import javax.naming.Context;
import javax.naming.Name;
import javax.naming.RefAddr;
import javax.naming.Reference;
import javax.naming.spi.ObjectFactory;
import java.util.Enumeration;
import java.util.Hashtable;

public class FSTemplateManagerFactory implements ObjectFactory {
    @Override
    public Object getObjectInstance(Object obj, Name name, Context nameCtx, Hashtable<?, ?> environment)
            throws Exception {

        Reference reference = (Reference) obj;
        Enumeration<RefAddr> references = reference.getAll();

        String basepath = "";

        while (references.hasMoreElements()) {

            RefAddr address = references.nextElement();
            String type = address.getType();
            String content = (String) address.getContent();


            switch (type) {
                case "basepath":
                    basepath = content;
                    break;
                default:
                    break; // NOP
            }


        }

        if (basepath.isEmpty()) {
            throw new IllegalStateException("basepath must not be empty");
        }

        return new FSTemplateManager(basepath);

    }
}
