package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.mime.MimeMap;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GitTemplateFile implements TemplateFile {

    private final Repository repo;
    private final String templatePath;
    private final String virtualPath;
    private final Logger logger;

    public GitTemplateFile(Repository repo, String templatePath, String virtualPath) {
        this.repo = repo;
        this.templatePath = templatePath;
        this.virtualPath = virtualPath;
        this.logger = Logger.getLogger(this.getClass().getName());
    }

    @Override
    public String getVirtualPath() {
        return virtualPath;
    }

    @Override
    public String getMimeType() {

        int lastDot = virtualPath.lastIndexOf('.');
        if (lastDot == -1) {
            return "application/octet-stream";
        }
        String ext = virtualPath.substring(lastDot + 1);

        return MimeMap.getInstance().getMime(ext);
    }

    @Override
    public long getEstimatedSize() {
        try {
            ObjectLoader loader = getLoader();
            return loader.getSize();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error accessing git repo: " + e.getMessage());
            return 0;
        }
    }

    private String filePath() {
        return templatePath + "/" + virtualPath;
    }

    private ObjectLoader getLoader() throws IOException {

        String fullPath = filePath();

        ObjectId lastCommit = repo.resolve("refs/heads/release");

        try (RevWalk revWalk = new RevWalk(repo)) {
            RevCommit commit = revWalk.parseCommit(lastCommit);
            RevTree tree = commit.getTree();

            try (TreeWalk treeWalk = new TreeWalk(repo)) {
                treeWalk.addTree(tree);
                treeWalk.setRecursive(true);
                treeWalk.setFilter(PathFilter.create(fullPath));

                while (treeWalk.next()) {

                    if (fullPath.equals(treeWalk.getPathString())) {


                        ObjectId fileId = treeWalk.getObjectId(0);
                        ObjectLoader loader = repo.open(fileId);

                        revWalk.dispose();
                        return loader;

                    }

                }

                revWalk.dispose();
                return null;

            }

        }

    }

    @Override
    public InputStream getStream() {

        try {
            ObjectLoader loader = Objects.requireNonNull(getLoader());
            return loader.openStream();
        } catch (IOException | NullPointerException e) {
            logger.log(Level.SEVERE, "Error accessing git repo: " + e.getMessage());
            return InputStream.nullInputStream();
        }

    }
}
