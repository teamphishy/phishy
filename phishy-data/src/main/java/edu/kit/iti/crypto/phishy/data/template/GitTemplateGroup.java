package edu.kit.iti.crypto.phishy.data.template;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GitTemplateGroup implements TemplateGroup {

    private final Path groupPath;
    private final Logger logger;
    private final Repository repo;

    public GitTemplateGroup(Path groupPath) {

        if (!Files.isDirectory(groupPath)) {
            throw new IllegalArgumentException("Not a valid group path");
        }

        this.groupPath = groupPath;
        this.logger = Logger.getLogger(this.getClass().getName());
        try {
            this.repo = new FileRepository(groupPath.toFile());
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Not a valid Git repo: " + groupPath);
            throw new IllegalArgumentException("Not a valid Git repo: " + groupPath);
        }
    }

    @Override
    public String getName() {

        return groupPath.getFileName().toString();
    }

    @Override
    public Template getTemplate(TemplateDescription description) throws NotFoundException {

        if (!hasTemplate(description)) {
            throw new NotFoundException();
        }

        return new GitTemplate(repo, getName(), description.getName());

    }

    @Override
    public boolean hasTemplate(TemplateDescription description) {

        if (!getName().equals(description.getGroup())) {
            throw new IllegalArgumentException("Group names do not match");
        }

        if (description.getName().isEmpty()) {
            return false;
        }

        ObjectId lastCommit;
        try {
            lastCommit = repo.resolve("refs/heads/release");
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error accessing git repo: " + e.getMessage());
            return false;
        }

        try (RevWalk revWalk = new RevWalk(repo)) {
            RevCommit commit = revWalk.parseCommit(lastCommit);
            RevTree tree = commit.getTree();

            try (TreeWalk treeWalk = new TreeWalk(repo)) {
                treeWalk.addTree(tree);
                treeWalk.setFilter(PathFilter.create(description.getName()));
                if (!treeWalk.next()) {
                    return false;
                }

                revWalk.dispose();
                return true;

            }

        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error accessing git repo: " + e.getMessage());
            return false;
        }

    }

    @Override
    public Collection<Template> listTemplates() {

        Collection<Template> templates = new ArrayList<>();

        ObjectId lastCommit;
        try {
            lastCommit = repo.resolve("refs/heads/release");
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error accessing git repo: " + e.getMessage());
            return List.of();
        }

        try (RevWalk revWalk = new RevWalk(repo)) {
            RevCommit commit = revWalk.parseCommit(lastCommit);
            RevTree tree = commit.getTree();

            try (TreeWalk treeWalk = new TreeWalk(repo)) {
                treeWalk.addTree(tree);

                while (treeWalk.next()) {

                    String templateName = treeWalk.getNameString();
                    templates.add(new GitTemplate(repo, getName(), templateName));

                }

                revWalk.dispose();

            }

        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error accessing git repo: " + e.getMessage());
            return List.of();
        }

        return templates;
    }
}
