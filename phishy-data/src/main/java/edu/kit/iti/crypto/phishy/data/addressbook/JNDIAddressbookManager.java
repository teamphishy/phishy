package edu.kit.iti.crypto.phishy.data.addressbook;

import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import javax.naming.Binding;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Manages addressbook registered in JNDI
 */
public class JNDIAddressbookManager implements AddressbookManager {

    private final String root;
    private Context rootContext;
    private final Logger logger;

    /**
     * New AddressbookManager that looks for Addressbooks in given JNDI path
     *
     * @param root JNDI path containing addrsssbooks
     */
    public JNDIAddressbookManager(String root) {

        this.root = root;
        this.logger = Logger.getLogger(this.getClass().getName());
    }

    @Override
    public Collection<Addressbook> listAddressbooks() {

        Collection<Addressbook> list = new ArrayList<>();

        try {

            InitialContext ctx = new InitialContext();
            Context rootContext = (Context) ctx.lookup(root);

            Enumeration<Binding> e = rootContext.listBindings("");
            while (e.hasMoreElements()) {

                Binding binding = e.nextElement();
                Object obj = binding.getObject();

                if (obj instanceof Addressbook) {
                    list.add((Addressbook) obj);
                }

            }

        } catch (NamingException e) {
            logger.log(Level.SEVERE, "JNDI error:" + e.getMessage());
        }


        return list;

    }

    @Override
    public boolean hasAddressbook(AddressbookDescription description) {

        assert description != null;

        try {

            InitialContext ctx = new InitialContext();
            Context rootContext = (Context) ctx.lookup(root);

            Object obj = rootContext.lookup(description.getName());
            return obj instanceof Addressbook;


        } catch (NamingException e) {
            logger.log(Level.SEVERE, "JNDI error:" + e.getMessage());
            return false;
        }

    }

    @Override
    public Addressbook getAddressbook(AddressbookDescription description) throws NotFoundException {

        assert description != null;


        try {

            InitialContext ctx = new InitialContext();
            Context rootContext = (Context) ctx.lookup(root);

            Object obj = rootContext.lookup(description.getName());
            if (obj instanceof Addressbook) {

                return (Addressbook) obj;

            } else {
                throw new NotFoundException();
            }


        } catch (NamingException e) {
            logger.log(Level.SEVERE, "JNDI error:" + e.getMessage());
            throw new NotFoundException();
        }

    }
}
