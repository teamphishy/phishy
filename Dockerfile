FROM tomcat
COPY res/dependencies/sqlite*.jar $CATALINA_HOME/lib/sqlite.jar
COPY res/dependencies/javax.mail*.jar $CATALINA_HOME/lib/javax.mail.jar
COPY res/dependencies/javax.activation*.jar $CATALINA_HOME/lib/javax.activation.jar
RUN mkdir -p $CATALINA_HOME/phishy/templates
COPY res/db.sqlite $CATALINA_HOME/phishy/db.sqlite
COPY phishy-bundle/target/phishy-bundle*.war $CATALINA_HOME/deploy/phishy.war
COPY res/tomcat/tomcat-users.xml $CATALINA_HOME/conf/tomcat-users.xml
COPY res/tomcat/localhost.xml $CATALINA_HOME/conf/Catalina/localhost/ROOT.xml
EXPOSE 8080
CMD ["catalina.sh", "run"]