#!/bin/bash

STATUS=0
cd ./1-campaign 
./run-campaign.sh; STATUS=$(($STATUS + $?));
cd ..

cd ./2-template
./run-template.sh; STATUS=$(($STATUS + $?));
cd ..

cd ./4-mails
./run-mails.sh; STATUS=$(($STATUS + $?));
cd ..

cd ./5-web
./run-web.sh; STATUS=$(($STATUS + $?));
cd ..

cd ./6-retrieve
./run-retrieve.sh; STATUS=$(($STATUS + $?));
cd ..

cd ./7-statistics
./run-statistics.sh; STATUS=$(($STATUS + $?));
cd ..

exit $STATUS
