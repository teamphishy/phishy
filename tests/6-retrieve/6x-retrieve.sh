#!/bin/bash
PHISHY_SERVER="$1"
PHISHY_CLI="$2"
MAIL_SMTP_HOST="$3"
MAIL_SMTP_PORT="$4"
MAIL_IMAP_HOST="$3"
MAIL_IMAP_PORT="$5"

CAMPAIGN_NAME="test"
PHISHY_ID=`cat jwt.txt`

echo "* Set up Campaign"
$PHISHY_CLI -h $PHISHY_SERVER campaign link $CAMPAIGN_NAME phishMail "test-infomail" 
$PHISHY_CLI -h $PHISHY_SERVER campaign link $CAMPAIGN_NAME infoMail "test-infomail"
$PHISHY_CLI -h $PHISHY_SERVER campaign link $CAMPAIGN_NAME phishWebsite "test-infomail"
$PHISHY_CLI -h $PHISHY_SERVER campaign link $CAMPAIGN_NAME infoWebsite "test-infomail"
$PHISHY_CLI -h $PHISHY_SERVER campaign resume $CAMPAIGN_NAME

if [[ "$?" != "0" ]]; then
    echo "ERROR: could not set up campaign"
    exit -1;
fi


echo "* Set up Mails"

echo "HELO test.phishy.ip
MAIL FROM:<test@phishy.ip>
RCPT TO:<report@phishy.ip>
DATA
From: Foo <foo@bar.ip>
To: Victim <victim@baz.ip>
Subject: Some Bait Mail
X-Phishy-ID: $PHISHY_ID

TESTMAIL
.
QUIT" | nc -w 10 $MAIL_SMTP_HOST $MAIL_SMTP_PORT

echo "* Test T60/T61"

$PHISHY_CLI -h $PHISHY_SERVER evaluate -c test --yes

sleep 10

IMAP_RESULT=`echo "a1 login victim@baz.ip password
a2 select inbox" | nc -w 10 $MAIL_IMAP_HOST $MAIL_IMAP_PORT`

echo "$IMAP_RESULT"

if [[ $IMAP_RESULT == *"[UNSEEN 1]"* ]]; then
    echo "1 unseen"
    exit 0
else
    echo "no mail sent"
    exit -1
fi



