#!/bin/bash
PHISHY_SERVER="http://localhost:8888"
PHISHY_CLI="java -jar ../../phishy-cli/target/phishy-cli.jar"
MAIL_SMTP_HOST="localhost"
MAIL_SMTP_PORT="3025"
MAIL_IMAP_PORT="3143"
SAMPLE_UUID="00000000-0000-0000-0000-000000000000"

echo "** 6 - Retrieve Tests"

# 0. Create sqlite db
cp ../../res/db.sqlite ./db.sqlite
sqlite3 ./db.sqlite "insert into Campaign (id, addressbook) values ('test', 'sampleAddressbook');"
sqlite3 ./db.sqlite "insert into OutstandingTest (id, campaign) values ('$SAMPLE_UUID', 'test');"

sleep 4

# 1. Start Docker service

docker-compose -f ./docker.yml up --build > docker.log &
sleep 2
while [[ `docker ps` != *"phishy-test-retrieve"* ]]; do
    sleep 2
done
sleep 4

# 2. Run tests

STATUS=0
./6x-retrieve.sh "$PHISHY_SERVER" "$PHISHY_CLI" "$MAIL_SMTP_HOST" "$MAIL_SMTP_PORT" "$MAIL_IMAP_PORT"; STATUS=$(($STATUS + $?));

# 3. Shutdown Docker service
docker-compose -f ./docker.yml down
rm ./db.sqlite


if [[ "$STATUS" != "0" ]]; then
    echo "TESTS FAILED"
    exit -1
else
    echo "TESTS OK"
    exit 0
fi


