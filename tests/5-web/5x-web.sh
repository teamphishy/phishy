#!/bin/bash

#!/bin/bash


source ../util.shinc

PHISHY_SERVER="$1"
PHISHY_CLI="$2"
SAMPLE_UUID="$3"

CAMPAIGN_NAME="test"
COOKIE_FILE="cookies.txt"

TUSER_NAME="M"
TUSER_PW="M"

echo "* Setup Tests"

$PHISHY_CLI -h $PHISHY_SERVER campaign update $CAMPAIGN_NAME -a "sampleAddressbook" 
$PHISHY_CLI -h $PHISHY_SERVER campaign link $CAMPAIGN_NAME phishMail "test-info" 
$PHISHY_CLI -h $PHISHY_SERVER campaign link $CAMPAIGN_NAME infoMail "test-info"
$PHISHY_CLI -h $PHISHY_SERVER campaign link $CAMPAIGN_NAME phishWebsite "test-login"
$PHISHY_CLI -h $PHISHY_SERVER campaign link $CAMPAIGN_NAME infoWebsite "test-info"
$PHISHY_CLI -h $PHISHY_SERVER campaign resume $CAMPAIGN_NAME

if [[ "$?" != "0" ]]; then
    echo "ERROR: could not set up campaign"
    exit -1;
fi

echo "* Test T50"

LOGIN_RESPONSE=`curl -s -c "$COOKIE_FILE" "$PHISHY_SERVER/login?dGVzdA==_$SAMPLE_UUID"`

if [[ "$LOGIN_RESPONSE" != "LOGIN" ]]; then
    echo "FAIL: could not get login site"
    exit -1;
fi

echo "* Test T51 & T52"

## INFO: Do not use -X POST to ensure that redirects will use GET
INFO_RESPONSE=`curl -L -s -b "$COOKIE_FILE" "$PHISHY_SERVER/login" -H "Content-Type: application/x-www-form-urlencoded" -d "name=$TUSER_NAME&password=$TUSER_PW"`


if [[ "$INFO_RESPONSE" != "INFO" ]]; then
    echo "FAIL: Login failed or info site not accessible"
    exit -1
fi

rm $COOKIE_FILE
exit 0;
