#!/bin/bash
PHISHY_SERVER="http://localhost:8888"
PHISHY_CLI="java -jar ../../phishy-cli/target/phishy-cli.jar"
SAMPLE_UUID="00000000-0000-0000-0000-000000000000"


echo "** 5 - Web Tests"

# 0. Create sqlite db
cp ../../res/db.sqlite ./db.sqlite
sqlite3 ./db.sqlite "insert into Campaign (id) values ('test');"
sqlite3 ./db.sqlite "insert into OutstandingTest (id, campaign) values ('$SAMPLE_UUID', 'test');"

# 1. Start Docker service

docker-compose -f ./docker.yml up --build > docker.log &
sleep 2
while [[ `docker ps` != *"phishy-test-web"* ]]; do
    sleep 2
done
sleep 4


# 2. Run tests

STATUS=0
./5x-web.sh "$PHISHY_SERVER" "$PHISHY_CLI" "$SAMPLE_UUID"; STATUS=$(($STATUS + $?));

# 3. Shutdown Docker service
docker-compose -f ./docker.yml down
rm ./db.sqlite


if [[ "$STATUS" != "0" ]]; then
    echo "TESTS FAILED"
    exit -1
else
    echo "TESTS OK"
    exit 0
fi


