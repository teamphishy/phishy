#!/bin/bash

# Testfall T22
# Voraussetzungen: T20
# Angepasst auf aktuelles Datenmodell
# Der Administrator entfernt die Volragengruppe `vorlagen`
source ../util.shinc

PHISHY_SERVER="$1"
PHISHY_CLI="$2"

E_NAME="vorlagen"


echo "* Test T22"


if [[ `$PHISHY_CLI -h $PHISHY_SERVER template list` != *"$E_NAME"* ]]; then
    echo "ERROR: template group $E_NAME does not exist"
    exit -1;
fi


$PHISHY_CLI -h $PHISHY_SERVER template group remove $E_NAME --yes

if [[ `$PHISHY_CLI -h $PHISHY_SERVER template list` == *"$E_NAME"* ]]; then
    echo "FAIL: template group $E_NAME iwas not removed"
    exit -1;
fi

exit 0;
