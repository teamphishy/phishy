#!/bin/bash

# Testfall T20
# Angepasst auf aktuelles Datenmodell
# Der Administrator erstellt eine neue Vorlagengruppe
# mit dem namen `vorlagen`

source ../util.shinc

PHISHY_SERVER="$1"
PHISHY_CLI="$2"

E_NAME="vorlagen"

echo "* Test T20"

if [[ `$PHISHY_CLI -h $PHISHY_SERVER template list` == *"$E_NAME"* ]]; then
    echo "ERROR: template group $E_NAME existst already"
    exit -1;
fi

$PHISHY_CLI -h $PHISHY_SERVER template group create $E_NAME

if [[ `$PHISHY_CLI -h $PHISHY_SERVER template list` != *"$E_NAME"* ]]; then
    echo "ERROR: template group $E_NAME was not created"
    exit -1;
fi

exit 0
