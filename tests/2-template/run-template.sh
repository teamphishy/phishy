#!/bin/bash
PHISHY_SERVER="http://localhost:8888"
PHISHY_CLI="java -jar ../../phishy-cli/target/phishy-cli.jar"
PHISHY_GIT="http://test:Test@localhost:8888"


echo "** 2 - Template Tests"

# 1. Start Docker service

docker-compose -f ./docker.yml up --build > docker.log &
sleep 2
while [[ `docker ps` != *"phishy-test-template"* ]]; do
    sleep 2
done
sleep 4


# 2. Run tests

STATUS=0
./20-template.sh "$PHISHY_SERVER" "$PHISHY_CLI"; STATUS=$(($STATUS + $?));
./21-template.sh "$PHISHY_SERVER" "$PHISHY_CLI" "$PHISHY_GIT"; STATUS=$(($STATUS + $?));
./22-template.sh "$PHISHY_SERVER" "$PHISHY_CLI"; STATUS=$(($STATUS + $?));

# 3. Shutdown Docker service
docker-compose -f ./docker.yml down


if [[ "$STATUS" != "0" ]]; then
    echo "TESTS FAILED"
    exit -1
else
    echo "TESTS OK"
    exit 0
fi

