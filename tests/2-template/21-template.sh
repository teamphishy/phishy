#!/bin/bash

# Testfall T21
# Voraussetzungen: T20
# Angepasst auf aktuelles Datenmodell
# Der Administrator bucht das Depot `vorlagen` aus,
# erstellt eine neue Vorlage `mail` in dem Depot
# und lädt dieses anschliessend wieder auf den Server
source ../util.shinc

PHISHY_SERVER="$1"
PHISHY_CLI="$2"
PHISHY_GIT="$3"

E_NAME="vorlagen"
E_TEMPLATE="mail"

echo "* Test T21"

R_GIT=`$PHISHY_CLI -h $PHISHY_SERVER template group git $E_NAME`

if [[ "" == "$R_GIT" ]];  then
    echo "ERROR: no git repository returned"
    exit -1;
fi

TEMP_DIR=`mktemp -d`

if [[ "$?" != "0" ]]; then
    echo "ERROR: could not create temp dir"
    exit -1;
fi
if [[ "" == "$TEMP_DIR" ]]; then
    echo "ERROR: temp dir name was empty"
    exit -1;
fi

echo "Git credentials: test:Test"
git -C "$TEMP_DIR" clone "$PHISHY_GIT/$R_GIT" .
mkdir "$TEMP_DIR/$E_TEMPLATE"
echo "To: example@example.org" > "$TEMP_DIR/$E_TEMPLATE/mail.eml"
git -C "$TEMP_DIR" add .
git -C "$TEMP_DIR" commit -m "Template Test"
echo "test\nTest" | git -C "$TEMP_DIR" push



RESULT=`$PHISHY_CLI -h $PHISHY_SERVER template group list $E_NAME`
if [[ "$RESULT" != *"$E_TEMPLATE"* ]]; then
    echo "FAIL: Template $E_TEMPLATE was not created"
    exit -1;
fi

exit 0;

