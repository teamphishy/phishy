#!/bin/bash

# Testfall T40
# Voraussetzungen:
# Eine konfigurierte Kampagne `test` mit hishing-Mail-Vorlage `test-phishmail01`.
# Eine weitere Phishing-Mail-Vorlage `test-phishmail02`
#
# Testfall:
# Generieren der Phishing-Mails für die Kampagne test. 
# Die generierten E- Mails entsprechen der Vorlage `test-phishmail01`.

source ../util.shinc

PHISHY_SERVER="$1"
PHISHY_CLI="$2"
MAILHOG_SERVER="$3"

CAMP_NAME="test"
AB_NAME="sampleAddressbook"
TEMPL_NAME="test-phishmail01"
RECPT_SIZE=1
MAIL_LIFETIME=14
MAIL_INTERVAL=90

echo "* Test T40"

$PHISHY_CLI campaign create $CAMP_NAME -h $PHISHY_SERVER -a $AB_NAME --phishMail $TEMPL_NAME --infoMail $TEMPL_NAME --phishWebsite $TEMPL_NAME --infoWebsite $TEMPL_NAME --recipientSetSize $RECPT_SIZE --mailLifetime $MAIL_LIFETIME --mailInterval $MAIL_INTERVAL

if [[ "$?" != "0" ]]; then
    echo "Error creating test campaign"
    exit -1
fi

$PHISHY_CLI campaign resume $CAMP_NAME -h $PHISHY_SERVER

if [[ "$?" != "0" ]]; then
    echo "Error activating test campaign"
    exit -1
fi

$PHISHY_CLI addressbook update -h $PHISHY_SERVER
sleep 2

$PHISHY_CLI send --yes -h $PHISHY_SERVER

if [[ "$?" != "0" ]]; then
    echo "Error sending mails"
    exit -1
fi

sleep 4

CORRECT_TMPL=`curl -s $MAILHOG_SERVER/api/v2/messages | grep -o "X-Template: test-phishmail01"`
INCORRECT_TMPL=`curl -s $MAILHOG_SERVER/api/v2/messages | grep -o "X-Template: test-phishmail02"`


if [[ "$CORRECT_TMPL" == "" ]]; then
    echo "FAIL: No Mail with template test-phishmail01 was sent"
    exit -1;
fi

if [[ "$INCORRECT_TMPL" != "" ]]; then
    echo "FAIL: A mail with wrong template test-phishmail02 was sent"
    exit -1;
fi


exit 0
