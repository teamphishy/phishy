#!/bin/bash
PHISHY_SERVER="http://localhost:8888"
PHISHY_CLI="java -jar ../../phishy-cli/target/phishy-cli.jar"
MAILHOG_SERVER="http://localhost:8025"


echo "** 4 - Mail Tests"

# 1. Start Docker service

docker-compose -f ./docker.yml up --build > docker.log &
sleep 2
while [[ `docker ps` != *"phishy-test-mails"* ]]; do
    sleep 2
done
sleep 4


# 2. Run tests

STATUS=0
./40-mails.sh "$PHISHY_SERVER" "$PHISHY_CLI" "$MAILHOG_SERVER"; STATUS=$(($STATUS + $?));

# 3. Shutdown Docker service


docker-compose -f ./docker.yml down


if [[ "$STATUS" != "0" ]]; then
    echo "TESTS FAILED"
    exit -1
else
    echo "TESTS OK"
    exit 0
fi


