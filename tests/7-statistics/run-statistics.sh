#!/bin/bash
PHISHY_SERVER="http://localhost:8888"
PHISHY_CLI="java -jar ../../phishy-cli/target/phishy-cli.jar"

echo "** 7 - Statistics Tests"

# 0. Create sqlite db
cp ../../res/db.sqlite ./db.sqlite
sqlite3 ./db.sqlite "insert into Campaign (id) values ('foo');"
sqlite3 ./db.sqlite "insert into Campaign (id) values ('bar');"
sqlite3 ./db.sqlite "insert into Statistics (campaign, sentMails, phishedMails, reportedMails) values ('foo', 150, 100, 20);"
sqlite3 ./db.sqlite "insert into Statistics (campaign, sentMails, phishedMails, reportedMails) values ('bar', 333, 136, 45);"

sleep 4

# 1. Start Docker service

docker-compose -f ./docker.yml up --build > docker.log &
sleep 2
while [[ `docker ps` != *"phishy-test-statistics"* ]]; do
    sleep 2
done
sleep 4

# 2. Run tests

STATUS=0
./72-statistics.sh "$PHISHY_SERVER" "$PHISHY_CLI"; STATUS=$(($STATUS + $?));

# 3. Shutdown Docker service
docker-compose -f ./docker.yml down
rm ./db.sqlite


if [[ "$STATUS" != "0" ]]; then
    echo "TESTS FAILED"
    exit -1
else
    echo "TESTS OK"
    exit 0
fi


