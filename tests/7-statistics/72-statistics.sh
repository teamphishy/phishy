#!/bin/bash
PHISHY_SERVER="$1"
PHISHY_CLI="$2"

echo "* Test T72"

METRICS_RETURN=`curl -s $PHISHY_SERVER/metrics`

STATUS=0
if [[ "$METRICS_RETURN" != *"phishy{campaign=\"foo\", state=\"send\"} 150"* ]]; then
  echo "FAIL: foo sent mails missing"
  STATUS=$(($STATUS - 1));
fi

if [[ "$METRICS_RETURN" != *"phishy{campaign=\"bar\", state=\"send\"} 333"* ]]; then
  echo "FAIL: bar sent mails missing"
  STATUS=$(($STATUS - 1));
fi

if [[ "$METRICS_RETURN" != *"phishy{campaign=\"foo\", state=\"phished\"} 100"* ]]; then
  echo "FAIL: foo phished mails missing"
  STATUS=$(($STATUS - 1));
fi

if [[ "$METRICS_RETURN" != *"phishy{campaign=\"bar\", state=\"phished\"} 136"* ]]; then
  echo "FAIL: bar phished mails missing"
  STATUS=$(($STATUS - 1));
fi

if [[ "$METRICS_RETURN" != *"phishy{campaign=\"foo\", state=\"reported\"} 20"* ]]; then
  echo "FAIL: foo reported mails missing"
  STATUS=$(($STATUS - 1));
fi

if [[ "$METRICS_RETURN" != *"phishy{campaign=\"bar\", state=\"reported\"} 45"* ]]; then
  echo "FAIL: bar reported mails missing"
  STATUS=$(($STATUS - 1));
fi

exit $STATUS