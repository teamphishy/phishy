#!/bin/bash

# Testfall T13
# Voraussetzung: T10
# Der Administrator entfernt die Kampagne test


source ../util.shinc

PHISHY_SERVER="$1"
PHISHY_CLI="$2"

echo "* Test T13"

E_NAME="test"

RESULT=`$PHISHY_CLI -h $PHISHY_SERVER campaign list`

if [[ "$RESULT" != *"$E_NAME"* ]]; then
    echo "ERROR: Campaign $E_NAME does not exist"
    exit -1;
fi

$PHISHY_CLI -h $PHISHY_SERVER campaign remove $E_NAME --yes

RESULT=`$PHISHY_CLI -h $PHISHY_SERVER campaign list`

if [[ "$RESULT" == *"$E_NAME"* ]]; then
    echo "ERROR: Campaign $E_NAME was not removed"
    exit -1;
fi

exit 0
