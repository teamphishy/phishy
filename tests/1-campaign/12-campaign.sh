#!/bin/bash

# Testfall T12
# Voraussetzung: T10
# Angepasst auf aktuelles Datenmodell
# Der Administrator pausiert die Kampagne test und startet sie danach wieder.

source ../util.shinc

PHISHY_SERVER="$1"
PHISHY_CLI="$2"

echo "* Test T12"

E_NAME="test"


RESULT=`$PHISHY_CLI -h $PHISHY_SERVER campaign print $E_NAME -h $PHISHY_SERVER`
R_STATUS=`getItem "Status" "$RESULT"`

assert "inactive" "$R_STATUS";
if [[ "$?" != "0" ]]; then
    echo "FAIL: campaign was not inactive before"
    exit -1
fi


# Activate
$PHISHY_CLI -h $PHISHY_SERVER campaign resume test

RESULT=`$PHISHY_CLI -h $PHISHY_SERVER campaign print $E_NAME -h $PHISHY_SERVER`
R_STATUS=`getItem "Status" "$RESULT"`

assert "active" "$R_STATUS";
if [[ "$?" != "0" ]]; then
    echo "FAIL: campaign was not activated"
    exit -1
fi

# Deactivate
$PHISHY_CLI -h $PHISHY_SERVER campaign pause test

RESULT=`$PHISHY_CLI -h $PHISHY_SERVER campaign print $E_NAME -h $PHISHY_SERVER`
R_STATUS=`getItem "Status" "$RESULT"`

assert "inactive" "$R_STATUS";
if [[ "$?" != "0" ]]; then
    echo "FAIL: campaign was not deactivated"
    exit -1
fi

# Activate
$PHISHY_CLI -h $PHISHY_SERVER campaign resume test

RESULT=`$PHISHY_CLI -h $PHISHY_SERVER campaign print $E_NAME -h $PHISHY_SERVER`
R_STATUS=`getItem "Status" "$RESULT"`

assert "active" "$R_STATUS";
if [[ "$?" != "0" ]]; then
    echo "FAIL: campaign was not activated"
    exit -1
fi

exit 0
