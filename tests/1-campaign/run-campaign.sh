#!/bin/bash
PHISHY_SERVER="http://localhost:8888"
PHISHY_CLI="java -jar ../../phishy-cli/target/phishy-cli.jar"


echo "** 1 - Campaign Tests"

# 1. Start Docker service

docker-compose -f ./docker.yml up --build > docker.log &
sleep 2
while [[ `docker ps` != *"phishy-test-campaign"* ]]; do
    sleep 2
done
sleep 4


# 2. Run tests

STATUS=0
./10-campaign.sh "$PHISHY_SERVER" "$PHISHY_CLI"; STATUS=$(($STATUS + $?));
./11-campaign.sh "$PHISHY_SERVER" "$PHISHY_CLI"; STATUS=$(($STATUS + $?));
./12-campaign.sh "$PHISHY_SERVER" "$PHISHY_CLI"; STATUS=$(($STATUS + $?));
./13-campaign.sh "$PHISHY_SERVER" "$PHISHY_CLI"; STATUS=$(($STATUS + $?));

# 3. Shutdown Docker service

docker-compose -f ./docker.yml down


if [[ "$STATUS" != "0" ]]; then
    echo "TESTS FAILED"
    exit -1
else
    echo "TESTS OK"
    exit 0
fi
