#!/bin/bash

# Testfall T10
# Angepasst auf aktuelles Datenmodell
# Der Administrator erstellt eine Kampagne mit Namen test, der Info-Webseite sample-infoSite01,
# der Phishing-Webseite sample-phishSite01, der Phishing-Mail sample-phishMail01, 
# der Info-Mail sample-infoMail01. 
# Er nutzt das Adressbuch sampleAddressbook. Er stellt die Empfängersatzgröße auf 20. 
# Die Verfallszeit des Phishing-Links stellt er auf 2 Wochen. Das Mail Intervall stellt er auf 30 Tage.

source ../util.shinc

PHISHY_SERVER="$1"
PHISHY_CLI="$2"

E_NAME="test"
E_AB="sampleAddressbook"
E_PMAIL="sample-phishMail01"
E_IMAIL="sample-infoMail01"
E_PWEB="sample-phishSite01"
E_IWEB="sample-infoSite01"
E_RSETS="20"
E_MLIFET="14"
E_MINTERV="30"

echo "* Test T10"


$PHISHY_CLI campaign create $E_NAME -h $PHISHY_SERVER -a $E_AB --phishMail $E_PMAIL  --infoMail $E_IMAIL --phishWebsite $E_PWEB --infoWebsite $E_IWEB --recipientSetSize $E_RSETS --mailLifetime $E_MLIFET --mailInterval $E_MINTERV

if [[ "$?" != "0" ]]; then
    echo "ERROR while testing"
    exit -1
fi

RESULT=`$PHISHY_CLI campaign print $E_NAME -h $PHISHY_SERVER`

R_AB=`getItem "Addressbook" "$RESULT"`
R_PMAIL=`getItem "phishMail" "$RESULT"`
R_IMAIL=`getItem "infoMail" "$RESULT"`
R_PWEB=`getItem "phishWebsite" "$RESULT"`
R_IWEB=`getItem "infoWebsite" "$RESULT"`
R_RSETS=`getItem "Recipient Set Size" "$RESULT"`
R_MLIFET=`getItem "Mail Lifetime" "$RESULT"`
R_MINTERV=`getItem "Mail Interval" "$RESULT"`




STATUS=0


assert "$E_AB" "$R_AB"; STATUS=$(($STATUS + $?));
assert "$E_PMAIL" "$R_PMAIL"; STATUS=$(($STATUS + $?));
assert "$E_IMAIL" "$R_IMAIL"; STATUS=$(($STATUS + $?));
assert "$E_PWEB" "$R_PWEB"; STATUS=$(($STATUS + $?));
assert "$E_IWEB" "$R_IWEB"; STATUS=$(($STATUS + $?));
assert "$E_RSETS" "$R_RSETS"; STATUS=$(($STATUS + $?));
assert "$E_MLIFET" "$R_MLIFET"; STATUS=$(($STATUS + $?));
assert "$E_MINTERV" "$R_MINTERV"; STATUS=$(($STATUS + $?));


exit $STATUS

