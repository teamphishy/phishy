#!/bin/bash

# Testfall T11
# Voraussetzung: T10
# Angepasst auf aktuelles Datenmodell
# Der Administrator ändert in der Kampagne test die Info-Webseite sample-infoSite01 auf sample-infoSite02, 
# die Phishing-Webseite sample-phishSite01 auf sample-phishSite02, die Phishing-Mail sample-phishMail01 auf 
# sample-phishMail02, die Info-Mail sample-infoMail01 auf sample-nfoMail02. Er ändert das Adressbuch zu 
# sampleAddressbook02 und die Größe der Empfängersätze auf 30. Die Verfallszeit des Phishing-Links ändert 
# er zu 3 Wochen. Das MailIntervall ändert er auf 60 Tage.

source ../util.shinc

PHISHY_SERVER="$1"
PHISHY_CLI="$2"


E_NAME="test"
E_AB="sampleAddressbook02"
E_PMAIL="sample-phishMail02"
E_IMAIL="sample-infoMail02"
E_PWEB="sample-phishSite02"
E_IWEB="sample-infoSite02"
E_RSETS="30"
E_MLIFET="27"
E_MINTERV="60"

echo "* Test T11"


$PHISHY_CLI campaign update $E_NAME -h $PHISHY_SERVER -a $E_AB --recipientSetSize $E_RSETS --mailLifetime $E_MLIFET --mailInterval $E_MINTERV

if [[ "$?" != "0" ]]; then
    echo "ERROR while testing"
    exit -1
fi

$PHISHY_CLI -h $PHISHY_SERVER campaign link $E_NAME phishMail $E_PMAIL -v
$PHISHY_CLI -h $PHISHY_SERVER campaign link $E_NAME infoMail $E_IMAIL
$PHISHY_CLI -h $PHISHY_SERVER campaign link $E_NAME phishWebsite $E_PWEB
$PHISHY_CLI -h $PHISHY_SERVER campaign link $E_NAME infoWebsite $E_IWEB

if [[ "$?" != "0" ]]; then
    echo "ERROR while testing"
    exit -1
fi

RESULT=`$PHISHY_CLI campaign print $E_NAME -h $PHISHY_SERVER`

R_AB=`getItem "Addressbook" "$RESULT"`
R_PMAIL=`getItem "phishMail" "$RESULT"`
R_IMAIL=`getItem "infoMail" "$RESULT"`
R_PWEB=`getItem "phishWebsite" "$RESULT"`
R_IWEB=`getItem "infoWebsite" "$RESULT"`
R_RSETS=`getItem "Recipient Set Size" "$RESULT"`
R_MLIFET=`getItem "Mail Lifetime" "$RESULT"`
R_MINTERV=`getItem "Mail Interval" "$RESULT"`




STATUS=0


assert "$E_AB" "$R_AB"; STATUS=$(($STATUS + $?));
assert "$E_PMAIL" "$R_PMAIL"; STATUS=$(($STATUS + $?));
assert "$E_IMAIL" "$R_IMAIL"; STATUS=$(($STATUS + $?));
assert "$E_PWEB" "$R_PWEB"; STATUS=$(($STATUS + $?));
assert "$E_IWEB" "$R_IWEB"; STATUS=$(($STATUS + $?));
assert "$E_RSETS" "$R_RSETS"; STATUS=$(($STATUS + $?));
assert "$E_MLIFET" "$R_MLIFET"; STATUS=$(($STATUS + $?));
assert "$E_MINTERV" "$R_MINTERV"; STATUS=$(($STATUS + $?));


exit $STATUS

