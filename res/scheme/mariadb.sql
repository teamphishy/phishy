create user phishy identified by 'test';
create database phishy;
grant insert, select, update, delete on phishy.* to phishy;

use phishy;

create table Campaign (
    id varchar(32) not null,
    status boolean not null default 0,
    recipientSetSize smallint not null default 30,
    mailLifetime smallint not null default 14,
    mailInterval int not null default 30,
    addressbook varchar(32) default null,
    primary key (id)
);

create table CampaignTemplateRelation (
    campaign varchar(32) not null,
    type tinyint(2) not null,
    template varchar(32) not null,
    primary key (campaign, type),
    foreign key (campaign) references Campaign(id)
);

create table Statistics (
    campaign varchar(32) not null,
    sentMails int not null default 0,
    reportedMails int not null default 0,
    phishedMails int not null default 0,
    primary key (campaign),
    foreign key (campaign) references Campaign (id)
);

create table OutstandingTest (
    id varchar(36) not null,
    campaign varchar(32) not null,
    `date` Date not null,
    loginAttempts tinyint(2) not null default 0,
    primary key (id),
    foreign key (campaign) references Campaign(id)
);

create table Recipients (
    id varchar(100) not null,
    lastTest Date default null,
    primary key (id)
);


create trigger statistics_create
    after insert on Campaign
    for each row
        insert ignore into Statistics (campaign) values (NEW.id)
;

create trigger statistics_delete
    before delete on Campaign
    for each row
        delete from Statistics where campaign = OLD.id
;

create trigger templates_delete
    before delete on Campaign
    for each row
        delete from CampaignTemplateRelation where campaign = OLD.id
;

create trigger outstandingtests_delete
    before delete on Campaign
    for each row delete
        from OutstandingTest where campaign = OLD.id
;

