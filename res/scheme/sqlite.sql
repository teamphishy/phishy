create table Campaign (id text not null, recipientSetSize int default 100 not null, status int default 0 not null, mailLifetime int default 14 not null, mailInterval int default 30 not null, addressbook text default null, primary key(id));
create table CampaignTemplateRelation(campaign text not null, type int not null, template text not null, primary key(campaign, type), foreign key(campaign) references campaign(id));
create table Statistics (campaign text not null, sentMails int default 0 not null, reportedMails int default 0 not null, phishedMails int default 0 not null, primary key(campaign), foreign key(campaign) references campaign(id));
create table OutstandingTest (id text not null, campaign text not null, "date" date, loginAttempts int default 0 not null, primary key(id));
create table Recipients (id int not null, lastTest date default null, primary key(id));
