package edu.kit.iti.crypto.phishy.server.websiteloader;


import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.RSAKeyProvider;
import edu.kit.iti.crypto.phishy.data.ManagerFactory;
import edu.kit.iti.crypto.phishy.data.addressbook.Addressbook;
import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookDescription;
import edu.kit.iti.crypto.phishy.data.addressbook.AddressbookManager;
import edu.kit.iti.crypto.phishy.data.campaign.Campaign;
import edu.kit.iti.crypto.phishy.data.campaign.OutstandingTest;
import edu.kit.iti.crypto.phishy.data.campaign.TemplateType;
import edu.kit.iti.crypto.phishy.data.campaign.TestDescription;
import edu.kit.iti.crypto.phishy.data.template.Variable;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import edu.kit.iti.crypto.phishy.server.JWTKeyProvider;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class LoginWebsiteLoader extends WebsiteLoader {

    private static final String LOGIN_END_PATH = ".*/login/?$";
    private static final Pattern LOGIN_END_PATH_PATTERN = Pattern.compile(LOGIN_END_PATH);

    private final RSAKeyProvider rsaKeyProvider = new JWTKeyProvider();
    private final Logger logger;


    public LoginWebsiteLoader() {

        this.logger = Logger.getLogger(this.getClass().getName());

    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws NotFoundException, IOException {

        String pathInfo = request.getPathInfo();

        // 1. Try to load testDescription from Session
        TestDescription testDescription = getTestDescriptionFromSession(request);

        // 2. if not found, get from query string
        if (testDescription == null) {
            testDescription = getTestDescriptionFromQuery(request);
            // store in session for next access
            storeTestDescriptionInSession(request, testDescription);
        }

        if (testDescription == null) {
            denyAccess(response);
            return;
        }

        // 3. load campaign
        Campaign campaign;
        try {
            campaign = testDescription.resolveCampaign(ManagerFactory.getCampaignManager());
        } catch (NotFoundException e) {
            logger.log(Level.FINE, "User tried to access campaign that doesn't exist");
            denyAccess(response);
            return;
        }

        if (!campaign.hasOutstandingTest(testDescription.getTestID())) {
            denyAccess(response);
            return;
        }

        String filePath = LOGIN_END_PATH_PATTERN.matcher(pathInfo).matches()
                ? "index.html"
                : pathInfo.split(".*/login/?", 2)[1];

        List<Variable> variables = prepareVariables(request, campaign);

        loadTemplates(filePath, campaign, TemplateType.PHISH_WEBSITE, variables, response);
    }

    private List<Variable> prepareVariables(HttpServletRequest request, Campaign campaign) {

        List<Variable> variables = new ArrayList<>();
        variables.add(new Variable("loginFailed", wasLoginFailed(request) ? "true" : "false"));

        return variables;

    }

    private boolean wasLoginFailed(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session == null) {
            return false;
        }

        Object loginFailed = session.getAttribute("wasLoginFailed");
        if (loginFailed == null) {
            return false;
        }
        return (Boolean) loginFailed;
    }

    private void setLoginFailed(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session == null) {
            return;
        }

        session.setAttribute("wasLoginFailed", true);
    }

    private TestDescription getTestDescriptionFromSession(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return null;
        }
        return (TestDescription) session.getAttribute("testId");
    }

    private TestDescription getTestDescriptionFromQuery(HttpServletRequest request) {
        try {
            return TestDescription.fromIdentifier(request.getQueryString());
        } catch (IllegalArgumentException e) {
            logger.log(Level.WARNING, "Tried to use illegal token: " + request.getQueryString());
            return null;
        }
    }

    private void storeTestDescriptionInSession(HttpServletRequest request, TestDescription testDescription) {
        HttpSession session = request.getSession(true);
        if (session == null) {
            throw new IllegalStateException("Could not create Session");
        }
        session.setAttribute("testId", testDescription);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws NotFoundException, IOException {

        // 1. get test description from session
        TestDescription testDescription = getTestDescriptionFromSession(request);

        if (testDescription == null) {
            throw new IllegalStateException("TestDescription was not found in session");
        }

        Campaign campaign = testDescription.resolveCampaign(ManagerFactory.getCampaignManager());
        OutstandingTest outstandingTest = testDescription.resolveTest(campaign);

        if (outstandingTest == null) {
            throw new IllegalStateException("No matching outstandingTest found");
        }
        Addressbook addressbook = getAddressbookFromCampaign(campaign);
        String userName = request.getParameter("name");
        String password = request.getParameter("password");

        if ((userName == null) || (password == null)) {
            throw new IllegalStateException("no username and password supplied");
        } else if ((userName.isEmpty()) || (password.isEmpty())) {
            // handle as if credentials were wrong
            setLoginFailed(request);
            response.sendRedirect("./");
            return;
        }

        if (addressbook.checkCredentials(userName, password)) {

            setTestToPhished(campaign, outstandingTest);
            sendToInfoSite(response, campaign);

        } else {
            outstandingTest.increaseLoginAttempt();
            if (outstandingTest.getLoginAttempt() >= 3) {

                setTestToPhished(campaign, outstandingTest);
                sendToInfoSite(response, campaign);
            } else {
                setLoginFailed(request);
                response.sendRedirect("./");
            }
        }
    }

    private void setTestToPhished(Campaign campaign, OutstandingTest outstandingTest) {

        campaign.getStatistics().increasePhishedMail(1);
        outstandingTest.delete();

    }

    private void sendToInfoSite(HttpServletResponse response, Campaign campaign) throws IOException {
        String jwtToken = setUpJwtToken(campaign);
        response.sendRedirect("/info/?" + jwtToken);
    }

    private Addressbook getAddressbookFromCampaign(Campaign campaign) throws NotFoundException {
        AddressbookDescription addressbookDescription = campaign.getAddressbook();
        AddressbookManager addressbookManager = ManagerFactory.getAddressbookManager();
        return addressbookDescription.resolve(addressbookManager);
    }

    private String setUpJwtToken(Campaign campaign) {
        Calendar expiry = Calendar.getInstance();
        expiry.add(Calendar.MINUTE, 2);
        Algorithm algorithm = Algorithm.RSA256(rsaKeyProvider);
        return JWT.create()
                .withExpiresAt(expiry.getTime())
                .withClaim("campaign", campaign.getName())
                .sign(algorithm);
    }

}
