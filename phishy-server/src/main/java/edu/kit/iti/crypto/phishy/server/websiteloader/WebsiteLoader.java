package edu.kit.iti.crypto.phishy.server.websiteloader;

import edu.kit.iti.crypto.phishy.data.ManagerFactory;
import edu.kit.iti.crypto.phishy.data.campaign.Campaign;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignDescription;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignManager;
import edu.kit.iti.crypto.phishy.data.campaign.TemplateType;
import edu.kit.iti.crypto.phishy.data.template.*;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public abstract class WebsiteLoader {

    void doGet(HttpServletRequest request, HttpServletResponse response) throws NotFoundException, IOException {

    }

    void doPost(HttpServletRequest request, HttpServletResponse response) throws NotFoundException, IOException {

    }

    protected String getTokenFromQuery(String query, String name) {

        if (query == null) {
            throw new IllegalStateException("no token supplied");
        }

        String token = "";
        String[] queries = query.split("&");
        for (String queryElement : queries) {
            if (queryElement.startsWith(name + "=")) {
                token = queryElement.split("=", 2)[1];
                System.out.println("Token is: " + token);
                break;
            }
        }
        if (token.equals("")) {
            throw new IllegalStateException();
        } else {
            return token;
        }
    }

    protected Campaign getCampaignFromString(String name) throws NotFoundException {
        CampaignDescription campaignDescription = new CampaignDescription(name);
        CampaignManager campaignManager = ManagerFactory.getCampaignManager();
        return campaignDescription.resolve(campaignManager);
    }

    protected void loadTemplates(String filePath, Campaign campaign, TemplateType templateType, List<Variable> variables, HttpServletResponse response) throws NotFoundException, IOException {
        Template template = getTemplate(campaign, templateType);
        assert template != null;
        if (template.hasFile(filePath)) {
            if (template.hasFile(filePath + ".header")) {
                TemplateFile headerFile = template.getFile(filePath + ".header");
                Map<String, String> headerContent = createHeader(headerFile, variables);


                headerContent.forEach(response::addHeader);
            }

            TemplateFile templateFile = template.getFile(filePath);
            String mimeType = templateFile.getMimeType();
            InputStream inputStream;
            if (mimeType.startsWith("text/") || mimeType.startsWith("application/")) {
                inputStream = new VariableFilterStream(templateFile.getStream(), variables);
            } else {
                inputStream = templateFile.getStream();
            }

            response.setContentType(mimeType);
            OutputStream outputStream = response.getOutputStream();

            inputStream.transferTo(outputStream);

            outputStream.flush();
            outputStream.close();
        }
    }

    protected Template getTemplate(Campaign campaign, TemplateType templateType) throws NotFoundException {
        if (campaign.hasTemplate(templateType)) {
            TemplateDescription templateDescription = campaign.getTemplate(templateType);
            TemplateManager templateManager = ManagerFactory.getTemplateManager();
            return templateDescription.resolve(templateManager);
        } else {
            return null;
        }
    }

    protected Map<String, String> createHeader(TemplateFile headerContent, List<Variable> variables) {
        Map<String, String> headerMap = new HashMap<>();
        Pattern validHeaderPattern = Pattern.compile("[a-zA-Z][a-zA-Z0-9\\-]*");

        new BufferedReader(new InputStreamReader(new VariableFilterStream(headerContent.getStream(), variables), StandardCharsets.UTF_8))
                .lines()
                .map(header -> header.split(": ", 2))
                .filter(header -> validHeaderPattern.matcher(header[0]).matches())
                .forEach(header -> headerMap.put(header[0], header[1]));
        return headerMap;
    }


    protected void denyAccess(HttpServletResponse response) throws IOException {
        response.setStatus(403);
        response.setContentType("text/html");
        Writer writer = response.getWriter();
        writer.write("<!DOCTYPE html>");
        writer.write("<html>");
        writer.write("<body>Access denied</body>");
        writer.write("</html>");
    }

}
