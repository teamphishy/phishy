package edu.kit.iti.crypto.phishy.server;

import com.auth0.jwt.interfaces.RSAKeyProvider;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.IOException;
import java.io.Writer;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JWTKeyProvider implements RSAKeyProvider {

    Logger logger;
    private RSAPublicKey publicKey = null;
    private RSAPrivateKey privateKey = null;

    public JWTKeyProvider() {
        logger = Logger.getLogger(this.getClass().getName());
    }

    @Override
    public RSAPublicKey getPublicKeyById(String s) {

        if (publicKey == null) {
            synchronized (this) {
                if (publicKey != null) {
                    return publicKey;
                }
                try {
                    // With a little help from my friends:
                    // https://gist.github.com/destan/b708d11bd4f403506d6d5bb5fe6a82c5

                    Path pubKeyPath = getPublicKeyPath();
                    if (!Files.exists(pubKeyPath)) {
                        this.generateKeyPair();
                    }

                    String publicKeyContent = new String(Files.readAllBytes(pubKeyPath));

                    publicKeyContent = publicKeyContent.replaceAll("\\n", "").replaceAll("\\r", "")
                            .replace("-----BEGIN PUBLIC KEY-----", "").replace("-----END PUBLIC KEY-----", "");

                    KeyFactory kf = KeyFactory.getInstance("RSA");

                    X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(
                            Base64.getDecoder().decode(publicKeyContent));

                    publicKey = (RSAPublicKey) kf.generatePublic(keySpecX509);

                } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException | NullPointerException e) {
                    logger.log(Level.SEVERE, "No Public Key for JWT found");
                    return null;
                }
            }
        }
        return publicKey;
    }

    @Override
    public RSAPrivateKey getPrivateKey() {
        if (privateKey == null) {
            synchronized (this) {
                if (privateKey != null) {
                    return privateKey;
                }
                try {
                    // With a little help from my friends:
                    // https://gist.github.com/destan/b708d11bd4f403506d6d5bb5fe6a82c5
                    Path privKeyPath = getPrivateKeyPath();
                    if (!Files.exists(privKeyPath)) {
                        this.generateKeyPair();
                    }

                    String privateKeyContent = new String(Files.readAllBytes(privKeyPath));
                    privateKeyContent = privateKeyContent
                            .replaceAll("\\n", "")
                            .replaceAll("\\r", "")
                            .replace("-----BEGIN PRIVATE KEY-----", "")
                            .replace("-----END PRIVATE KEY-----", "");

                    KeyFactory kf = KeyFactory.getInstance("RSA");
                    PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(
                            Base64.getDecoder().decode(privateKeyContent));

                    privateKey = (RSAPrivateKey) kf.generatePrivate(keySpecPKCS8);
                } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException | NullPointerException e) {
                    logger.log(Level.SEVERE, "No Private key for JWT found");
                    return null;
                }
            }
        }
        return privateKey;
    }

    @Override
    public String getPrivateKeyId() {
        return null;
    }

    private Path getPublicKeyPath() {

        try {
            InitialContext ctx = new InitialContext();
            return Paths.get((String) ctx.lookup("java:comp/env/key/jwtPublic"));

        } catch (NamingException e) {
            logger.log(Level.INFO, "Public Key file not declared");
            try {
                return Paths.get(this.getClass().getResource("public.key.pub").toURI());
            } catch (URISyntaxException uriSyntaxException) {
                throw new IllegalStateException();
            }
        }

    }

    private Path getPrivateKeyPath() {

        try {
            InitialContext ctx = new InitialContext();
            return Paths.get((String) ctx.lookup("java:comp/env/key/jwtPrivate"));

        } catch (NamingException e) {
            logger.log(Level.INFO, "Private Key file not declared");
            try {
                return Paths.get(this.getClass().getResource("private.key").toURI());
            } catch (URISyntaxException uriSyntaxException) {
                throw new IllegalStateException();
            }
        }

    }

    private void generateKeyPair() {

        try {
            KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
            kpg.initialize(4096);
            KeyPair kp = kpg.generateKeyPair();

            Key pub = kp.getPublic();
            Key priv = kp.getPrivate();

            Base64.Encoder encoder = Base64.getEncoder();

            Writer pubOut = Files.newBufferedWriter(getPublicKeyPath());
            pubOut.write("-----BEGIN PUBLIC KEY-----\n");
            pubOut.write(encoder.encodeToString(pub.getEncoded()));
            pubOut.write("\n-----END PUBLIC KEY-----\n");
            pubOut.close();

            Writer privOut = Files.newBufferedWriter(getPrivateKeyPath());
            privOut.write("-----BEGIN PRIVATE KEY-----\n");
            privOut.write(encoder.encodeToString(priv.getEncoded()));
            privOut.write("\n-----END PRIVATE KEY-----\n");
            privOut.close();


        } catch (NoSuchAlgorithmException | IOException e) {
            logger.log(Level.SEVERE, "Error generating JWT Keys");
        }


    }

}
