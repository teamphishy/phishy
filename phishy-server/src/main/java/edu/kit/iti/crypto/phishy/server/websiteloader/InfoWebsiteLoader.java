package edu.kit.iti.crypto.phishy.server.websiteloader;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.RSAKeyProvider;
import edu.kit.iti.crypto.phishy.data.campaign.Campaign;
import edu.kit.iti.crypto.phishy.data.campaign.TemplateType;
import edu.kit.iti.crypto.phishy.data.template.Variable;
import edu.kit.iti.crypto.phishy.exception.NotFoundException;
import edu.kit.iti.crypto.phishy.server.JWTKeyProvider;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class InfoWebsiteLoader extends WebsiteLoader {

    private static final String INFO_END_PATH = ".*/info/?$";
    private static final Pattern INFO_END_PATH_PATTERN = Pattern.compile(INFO_END_PATH);

    private final RSAKeyProvider rsaKeyProvider = new JWTKeyProvider();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws NotFoundException, IOException {

        String pathInfo = request.getPathInfo();
        // 1. try to get Campaign from Session
        Campaign campaign = getCampaignFromSession(request);

        // 2. if not found, get campaign from JWT
        if (campaign == null) {
            DecodedJWT jwt = getJwtFromQuery(request);
            campaign = getCampaignFromJwt(jwt);
            storeCampaignInSession(request, campaign);
            storeJwtInSession(request, jwt.getToken());
        }

        if (campaign == null) {
            throw new IllegalStateException("No campaign provided");
        }


        String filePath = INFO_END_PATH_PATTERN.matcher(pathInfo).matches()
                ? "index.html"
                : pathInfo.split(".*/info/?")[1];

        loadTemplates(filePath, campaign, TemplateType.INFO_WEBSITE, prepareVariables(request), response);
    }

    private List<Variable> prepareVariables(HttpServletRequest request) {

        List<Variable> variables = new ArrayList<>();

        variables.add(new Variable("jwt", getJwtFromSession(request)));

        return variables;

    }

    private void storeCampaignInSession(HttpServletRequest request, Campaign campaign) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            throw new IllegalStateException("Could not create session");
        }

        session.setAttribute("campaign", campaign);
    }

    private Campaign getCampaignFromSession(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return null;
        }

        return (Campaign) session.getAttribute("campaign");

    }

    private String getJwtFromSession(HttpServletRequest request) {

        HttpSession session = request.getSession(false);
        if (session == null) {
            return null;
        }

        return (String) session.getAttribute("jwt");

    }

    private void storeJwtInSession(HttpServletRequest request, String jwt) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            throw new IllegalStateException("Could not create session");
        }

        session.setAttribute("jwt", jwt);
    }

    private DecodedJWT getJwtFromQuery(HttpServletRequest request) {

        String token = request.getQueryString();

        if (token == null) {
            throw new IllegalStateException("No Token supplied");
        }

        try {
            Algorithm algorithm = Algorithm.RSA256(rsaKeyProvider);
            JWTVerifier verifier = JWT.require(algorithm).build();
            return verifier.verify(token);

        } catch (JWTVerificationException e) {
            throw new IllegalStateException("Token invalid");
        }

    }

    private Campaign getCampaignFromJwt(DecodedJWT jwt) throws NotFoundException {


        String campaignName = jwt.getClaim("campaign").asString();
        return getCampaignFromString(campaignName);

    }


}
