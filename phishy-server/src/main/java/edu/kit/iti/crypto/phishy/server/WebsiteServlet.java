package edu.kit.iti.crypto.phishy.server;

import edu.kit.iti.crypto.phishy.server.websiteloader.InfoWebsiteLoader;
import edu.kit.iti.crypto.phishy.server.websiteloader.LoginWebsiteLoader;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 * The WebsiteServlet manages the interaction between User and Phishing-website
 */
public class WebsiteServlet extends HttpServlet {

    private static final String INFO_PATH = ".*/info(/.*)?$";
    private static final String LOGIN_PATH = ".*/login(/.*)?$";
    private static final String LOGIN_END_PATH = ".*/login/?$";
    private static final Pattern INFO_PATH_PATTERN = Pattern.compile(INFO_PATH);
    private static final Pattern LOGIN_PATH_PATTERN = Pattern.compile(LOGIN_PATH);
    private static final Pattern LOGIN_END_PATH_PATTERN = Pattern.compile(LOGIN_END_PATH);

    private static final long serialVersionUID = 1L;
    private final Logger logger;
    private final LoginWebsiteLoader loginWebsiteLoader;
    private final InfoWebsiteLoader infoWebsiteLoader;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public WebsiteServlet() {
        super();
        logger = Logger.getLogger(WebsiteServlet.class.getName());
        loginWebsiteLoader = new LoginWebsiteLoader();
        infoWebsiteLoader = new InfoWebsiteLoader();
    }

    /**
     * Processes the request to load the correct website Templates and writes them in the response
     *
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            String pathInfo = request.getPathInfo();
            if (LOGIN_PATH_PATTERN.matcher(pathInfo).matches()) {

                loginWebsiteLoader.doGet(request, response);

            } else if (INFO_PATH_PATTERN.matcher(pathInfo).matches()) {

                infoWebsiteLoader.doGet(request, response);
            }

        } catch (Exception e) {

            e.printStackTrace();
            logger.log(Level.SEVERE, "Error accessing website: " + e.getMessage());
            try {
                supplyErrorWebsite(response);
            } catch (IOException i) {
                i.printStackTrace();
                logger.log(Level.SEVERE, "Error sending error message: " + i.getMessage());
            }
        }
    }

    private void supplyErrorWebsite(HttpServletResponse response) throws IOException {

        response.setStatus(500);
        response.setContentType("text/html");
        Writer writer = response.getWriter();
        writer.write("<!DOCTYPE html>");
        writer.write("<html>");
        writer.write("<body>An Error Occurred</body>");
        writer.write("</html>");

    }


    /**
     * Processes the user input and redirects the User accordingly
     *
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

        try {
            String pathInfo = request.getPathInfo();
            if (LOGIN_END_PATH_PATTERN.matcher(pathInfo).matches()) {

                loginWebsiteLoader.doPost(request, response);
            }
        } catch (Exception e) {

            e.printStackTrace();
            logger.log(Level.SEVERE, e.getMessage());
            try {
                supplyErrorWebsite(response);
            } catch (IOException i) {
                i.printStackTrace();
                logger.log(Level.SEVERE, i.getMessage());
                throw new IllegalStateException(i.getMessage());
            }
            throw new IllegalStateException();
        }
    }


}
