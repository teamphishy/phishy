package edu.kit.iti.crypto.phishy.monitoring;

import edu.kit.iti.crypto.phishy.data.ManagerFactory;
import edu.kit.iti.crypto.phishy.data.campaign.CampaignManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Class expands HttpServlet from javax.servlet.http.
 * Provides a HTTP Endpoint with statistics formatted in the Prometheus Exposition Format.
 */
public class PrometheusServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private final CampaignManager campaignManager;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PrometheusServlet() {
        super();
        campaignManager = ManagerFactory.getCampaignManager();
    }

    /**
     * If not already done the CampaignManager will be retrieved from the ManagerFactory.
     * Retrieves statistics from CampaignManager and prints them on the writer in the Prometheus Exposition
     * Format.
     *
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        long currentTime = System.currentTimeMillis();
        PrintWriter writer = response.getWriter();

        campaignManager.getCampaignStatisticsData().forEach((stat) -> {
            String campaignName = stat.getCampaignName();
            writer.println("# Phishy campaign " + campaignName + ":");
            writer.println("phishy{campaign=\"" + campaignName + "\", state=\"send\"} " + stat.getSentMails() + " " + currentTime);
            writer.println("phishy{campaign=\"" + campaignName + "\", state=\"phished\"} " + stat.getPhishedMails() + " " + currentTime);
            writer.println("phishy{campaign=\"" + campaignName + "\", state=\"reported\"} " + stat.getReportedMails() + " " + currentTime);
            writer.println("");
        });
    }
}
